#### Book 4, The Dragonblood Warrior – Chapter 11, A Meeting

The Fragrant Pavilion Road was filled with people, but Yale, George, and Reynolds clearly and distinctly could tell who a certain female was, not too far away from them. Since Linley and Alice had been together for a long time now, Yale, George, and Reynolds had all been formally introduced to Alice. Naturally, they recognized her.

“It’s Alice.” George said in a low voice.

Right at this moment, Alice was walking hand-in-hand with another young man, a hint of a smile on her face. If Linley was here, he would definitely have been able to recognize that this young man was Kalan.

“Bastard.” A murderous look was on Yale’s face.

Reynolds was furious as well. “These past two months, Linley has been going to her home time and time again, waiting bitterly for her. He’s been recording all of his activities down in a memory crystal as well, like an idiot. And he even told us that in the future, he was going to marry this Alice. F*ck this!”

“In what way is our Third Bro not worthy of her?” George was starting to get upset as well.

Yale let out a sneer. “It’s not convenient for us to interfere. We’ll go to the Jade Water Paradise, and we’ll talk to Third Bro about it when he’s back. The most important thing for us to do now is to help Third Bro mentally prepare for this. If he doesn’t prepare? I’m afraid that he won’t be able to take this blow.”

George and Reynolds all nodded as well.

……

Within their private room at the Jade Water Paradise, Yale, George, and Reynolds all sat, frowns on their faces. They didn’t ask for any courtesans to accompany them, and the only thing in their cups was juice. They were afraid that they might get drunk, and would not behave appropriately when dealing with Linley.

“I know Third Bro all too well.” George said worriedly. “He normally doesn’t say much, and he’s very hard working as well. There are so many girls at our school who are pursuing him. He’s never accepted a single one of them. But a guy like him, once he falls for someone, he will fall much harder than you, Boss, or you, Fourth Bro.”

Yale and Reynolds both nodded.

To Yale and Reynolds, losing a girl just meant getting a new one. It was no big deal at all. But in this past year, every day, when they were joking with Linley, they could tell from Linley’s reactions that he had really developed genuine feelings for Alice.

“This is pissing me off.” Yale drank all the juice in his cup at one go.

Reynolds snorted. “Boss Yale, don’t be too pissed. It’s just a girl. Third Bro will be in a lot of pain this time, but after he’s over it, everything will be fine.”

Yale nodded as well.

Yale, Reynolds, and George were all members of large clans, and thus they were influenced accordingly since youth. For Reynolds and George, it wasn’t too bad, as their clans had strict rules. But Yale had been buried in women since he was a kid.

Time passed on, one second at a time, one minute at a time. Yale and the others all sat there quietly.

One in the morning. With a creak, the door swung open. Linley walked in, reeking of wine. “Hey. All of you guys are still here?”

Yale laughed loudly. “We were waiting for you.”

“Third Bro, you weren’t waiting for that Alice this entire time, were you?” George said in an intentionally casual manner.

Linley nodded silently, and then sat down. “You guys aren’t drinking alcohol tonight?” Bending down, Linley retrieve a flagon of strong liquor from a chest, and immediately poured himself a cup.

“Third Bro, we need to talk to you about something.” Yale said with a grin.

“Talk.” Linley was in a very foul mood.

Yale said softly, “Tonight, when we were on the streets, we saw a girl. She looked a lot like your Alice. Honestly. We were a bit far away, so we couldn’t clearly tell. But that girl was holding hands with another guy.”

“Lies.” Linley said in a steely tone that brooked no argument.

Yale couldn’t help but start.

Reynolds clapped Linley on the shoulder with a laugh. “Third Bro. We’re all men. As men, how can we let women ride on our heads? Alice hasn’t shown up several times now. If I were you, I would’ve thrown her off a long time ago. Even if she knelt in front of me, I wouldn’t pay her any mind.”

“Fourth Bro, you’re just a punk ass kid. What would you know?” Linley said with a laugh, and then he drank a large cup of liquor. “Come, enough chitchat. I’m in a foul mood. Drink with me.”

Reynolds, Yale, and George all exchanged glances. They couldn’t do anything besides sit down and drink with Linley.

Early next morning, Linley, Yale, George, and Reynolds were all sleeping, stretched across the table. Linley was the first one to wake up.

Seeing his three dear friends, a bitter smile was on Linley’s face. In his heart, he murmured to himself, “Boss Yale, Second Bro, Fourth Bro…all of you accompanied me in drinking and said so many words of encouragement to me. I understand what you guys are thinking. For Alice to miss our appointment these past two, three times, I too had a bad feeling, but…I don’t believe it. I’m not willing to.”

Linley walked over to the window, looking down.

It was five or six in the early morning. The city of Fenlai seemed to have just woken up as well. Only a small number of people were walking about, preparing to work. The vast majority of people were still sleeping.

“Linley.” Doehring Cowart flew out from within the Coiling Dragon ring.

Doehring Cowart was forever dressed in those pristine, long white robes. His white beard was forever long.

“Grandpa Doehring.” Upon seeing Doehring Cowart appear, Linley suddenly felt as though he himself was a lonely boat that had finally reached the harbor.

Glancing at the sleeping dorm mates, Doehring Cowart laughed. “Linley, you have three really good friends. As far as the affairs of the heart between men and women? I can only say this. In the 1300 years when I was alive, from what I’ve seen, perhaps only one time out of ten would I see a person’s be successful in his first love.”

“Grandpa Doehring, I get it.” Linley barely nodded. “But…I trust her.”

Doehring Cowart nodded as well. He no longer spoke.

….

In the middle of November, Linley put on his backpack, making sure to secure the two memory crystals within, and then headed towards Fenlai City again, once more arriving at the two story house.

“Uncle Hudd, has Alice come back yet?” Linley said courteously to the guard named Hudd.

Hudd shook his head. “No. It’s been over a month since Miss Alice has come back. She hasn’t returned a single time.”

“Not a single time?” Linley frowned, furrows appearing in his forehead. “Then Uncle Hudd, I’ll head out now.” Linley courteously bid farewell.

Walking alone on the Dry Road, Linley walked over to the bar, but did not enter. Bebe mentally said to him, “Boss, don’t be so worried. For Alice to not appear, maybe she just has some important things going on? For example, maybe she went to do training. That’s always a possibility. Don’t stand here thinking idle thoughts.”

“Right. Maybe she’s busy dealing with something and can’t get free.” Linley’s eyes suddenly became alive again.

Seeing this, Bebe couldn’t help but wrinkle his little nose. “Boss, you are so love-struck that you’ve gone dumb. Just a few words of encouragement and you’re incredibly excited.”

“You little punk. No alcohol for you today, as punishment.” Linley didn’t know whether to laugh or cry.

But Linley also had to admit that after joking around with Bebe, his mood improve a little.

……

November 29th. This was a blizzard day, and snow covered everything in white. Linley, Reynolds, Yale, and George were all seated within a carriage. The driver was someone belonging to Yale’s merchant clan, and behind them there were several knights escorting Linley’s sculptures.

“Third Bro. In the next few days, the end-of-the-year exams will be coming. I wonder if that fellow who was once proclaimed the number one genius of our institute has become a magus of the sixth rank yet.” Yale chuckled.

George and Reynolds were all extremely proud.

Because in the previous week? Linley had reached the realm of the sixth rank.

In truth, Linley had reached the fourth rank when he was 13, the 5th rank when he was 14, and by now, he was almost 17. After two and a half years, Linley finally made the transition from being a magus of the fifth rank to the sixth rank.

Two and a half years!

What about that Dixie, who was previously regarded as the ultimate genius of the Institute?

Dixie became a magus of the fifth rank when he was twelve, but now he’s also around seventeen. It’s been five years. Honestly speaking, Dixie’s progression was also extremely fast. However, in comparison with Linley, who was assisted by the Straight Chisel School’s technique of stonecarving, he was much slower.

If, at the end-of-the-year exams, Linley had reached the sixth rank while Dixie had not, then Linley would be known as the indisputable number one genius of the Ernst Institute.

“Third Bro, try and smile. Becoming a magus of the sixth rank is something you should be happy about.” Reynolds said encouragingly.

Linley quirked his lips.

“You call that a smile?” Reynolds intentionally tried to tease Linley.

Linley finally let out a smile. “Alright, Fourth Bro, let me be quiet for a while.” Linley had already decided that this time, no matter what, he was going to meet Alice. If he couldn’t see her in Fenlai City, he would go directly to the Wellen Institute to look for her.”

No matter what, he had to have a face-to-face with Alice and sort things out.

Opening the carriage window, Linley let a cold gust of air inside. He couldn’t help but squint. Outside, everything was blanketed in white, and the sky itself was filled with feather-like plumes of snow. While enjoying the winter scenery, the time passed quickly, and they arrived at Fenlai City.

After delivering the three sculptures to the Proulx Gallery, the four of them had a meal, then temporarily parted ways.

By now, Linley’s income was very high. Almost each month, he was able to collect around 20,000 gold pieces. Thus Linley didn’t really care much about money anymore. Carrying his backpack with two memory crystals, Linley headed directly to Alice’s home.

“Boss, if I recall correctly, this is the fourth time that you’ve headed to Fenlai City with these memory crystals, right?” Bebe said disapprovingly. “How about you give them to Delia instead? I rather like Delia.”

From October until now, this indeed was the fourth time that Linley had carried these memory crystal balls to Fenlai City.

“That’s enough, Bebe.” Linley said with a frown.

Walking on the snow-covered street, crunching noises could be heard with each step Linley took. In short order, he arrived at that familiar, two-story house.

After seeing and briefly speaking with Hudd, Linley could only turn and depart.

“Once again, not back.” Linley was frowning severely. “Wellen Institute!” Linley immediately decided to head off to the Wellen Institute.

Fenlai City. The Fragrant Pavilion Road.

Alice was walking on the streets, holding hands with Kalan. Kalan gently said, “Alice, are you not planning to make things clear to Linley?”

“Maybe later.” Alice shook her head.

Kalan nodded and no longer spoke.

His eyes on Alice, who was holding hands with him, Kalan couldn’t help but smile. He had grown up with Alice and was childhood sweethearts with her. In his heart, he had always liked Alice, but he didn’t expect that Alice would get together with Linley so quickly.

When he first discovered that Alice and Linley had started dating, Kalan was exploding with rage.

Ever since he was a kid, Kalan had always regarded Alice as his. Even if Linley had previously helped him, when it came to love, Kalan wasn’t going to back off. Thus…he used a few small tricks to achieve what he wanted.

“Love at first sight? The hero rescuing the damsel in distress?” Kalan was filled with contempt. “When faced with reality, all of that is as flimsy as a piece of white paper.”

Holding Alice’s hand, Kalan was totally content.

“Alice, when do you think you’ll make things clear to Linley?” Kalan asked again. Kalan really didn’t want Alice and Linley to stay entangled much longer.

Alice shook her head. “I don’t know either. But I believe that if I don’t meet with big brother Linley for a long period of time, in time, the feelings will fade. By then, if I say goodbye to him, he won’t have as strong a reaction.”

“You’re right. After all, Linley saved us once.” Kalan nodded.

As they walked, they reached the intersection between the Dry Road and the Fragrant Pavilion Road. Kalan noticed that Alice suddenly came to a halt. He couldn’t help but look curiously at Alice, but Alice, looking stunned, was looking at a place on the Dry Road. Her face was ashen. Kalan also turned his head….

A young man, dressed in a moon-white robe, was standing there, not moving in the slightest. He was staring at them, stunned. His face was devoid of all color, as white as snow.

“Linley!” Kalan immediately frowned.
