#### Book 4, The Dragonblood Warrior – Chapter 3, Hogg

The next morning, while seated at the dining table in their dining hall, Linley was astonished to see his father looking radiant, with energy levels seemingly like Linley had never seen.

Putting down his knife and fork, Hogg smiled as he looked at Linley. “Linley, this time you should stay at home a bit longer. It’s been quite some time since I have seen you. The two of us, father and son, need to spend some quality time together.”

His father was asking him to stay at home longer?

Linley was a bit astonished. After all, in all these years, his father had never said these type of words to him. Originally, Linley was planning to go back to Fenlai City to stroll about and maybe visit Alice. But upon hearing these, he put all thoughts of visiting her aside.

“Okay, father.” Linley happily nodded.

Hogg nodded with pleasure, but in Hogg’s eyes, there seemed to be an hint of something indecipherable.

….

This time, Linley stayed for ten full days in Wushan township. Even when the start date for the next semester at the Ernst Institute arrived, he still didn’t go back, and Hogg didn’t rush him either.

Upon the mountain peaks of Mt. Wushan, rain clouds drifted hither and to. Linley was seated in a meditative pose, refining mageforce.

Earth elemental essence and wind elemental essence swirled around Linley, entering his body from every direction and being absorbed into his muscles, his skeleton, and his veins, improving his body’s strength. After part was absorbed, the rest was transformed into mageforce and stored in his central dantian.

Like an ocean being fed by a hundred rivers, all of the flows of elemental essence in his body would eventually end up here.

Linley just sat there for half a day. By the time Linley opened his eyes, it was already sunset.

“Time to go back to school.” Linley rose to his feet and took a deep breath. “Ever since I gave those magicite cores to my father, my father has changed for the better. He’s been much closer to me as well.”

These ten days Linley had spent here had been the closest ten days he had ever spent with his father.

“What caused father to change so much? The magicite cores? I don’t think father would have changed just because of money. Perhaps…it was the scars on my body?” Linley pondered, but in the end, he still couldn’t fully understand why his father’s attitude towards him had changed so much.

‘Asking if one was cold, worried that one might be hot’; this idiom expressing concern perfectly captured how considerate and caring Hogg was towards Linley.

After entering the Baruch clan manor, Linley immediately saw his father, book in hand. “Father, it’s getting dark. Why don’t you finish the book tomorrow?”

“Oh, Linley’s back.” Laughing, Hogg closed the book. “Your words have merit. I’ll finish it tomorrow.”

“Linley, after spending all this time training, you should be thirsty.” Hogg poured a glass of hot water from the tea carafe he kept by his side. “Here, have something for your throat. The temperature of this water is just right, not too cold, not too hot.”

“Thanks, father.” Linley’s heart felt warm.

This was how Hogg had treated Linley during these past ten days; incomparably well. While in the past, Hogg was always strict and solemn. Rarely would he show his affectionate side.

While drinking the water, Linley said, “Father, I’ve been at home for some time now. I’m planning to go back to school tomorrow.”

“Tomorrow?” Hogg paused for a moment, seemingly stunned, but then nodded. “Alright. Come back earlier for your end-of-the-year holiday this year.”

“Sure.” Linley assented.

Hogg said in a soft voice, “Linley, your father doesn’t have much ability. In the future, our clan will depend on you. By giving me these magicite cores, your little brother’s tuition expenses are guaranteed as well. I am already extremely satisfied. But in my mind, I still constantly think about our family’s humiliation. I hope that you will never forget that our ancestral heirloom is still in the hands of others.”

Linley could sense his father’s faith being placed in him. Taking a deep breath, he nodded slightly.

“Right now, I don’t have any other desires. I only hope that before my death, I will be able to see with my own eyes the ‘Slaughterer’ warblade.” Hogg’s voice became even more quiet.

Linley could feel that something was amiss. He immediately said, “Father, don’t be so gloomy. You are only forty years old this year. You have lots of time left. I have confidence that within ten years, I can definitely bring our warblade ‘Slaughterer’ back, and once more place it within the ancestral hall in our manor.”

“Ten years. Good, good.” Hogg gently nodded.

….

The second day, after lunch, Linley departed from Wushan township. That night, in the main hall at the Baruch manor, two people sat together. Hogg, and Hillman. The door to the hall was closed, and on the main table in that hall, the sack of magicite cores was on display.

Hillman had been totally stupefied by this sack of magicite cores. Finally, Hogg spoke. “Hillman, I plan to sell off these magicite cores. I want to entrust that gold into your safekeeping.”

Hillman immediately recollected himself. He hurriedly said, “Lord Hogg, no. How can you hand such a vast sum of money to me? Why don’t you take care of it?”

“Hillman, don’t call me Lord Hogg. You can just address me as big brother Hogg again.” Hogg laughed in a very kind way.

Suddenly, Hogg stood up, facing the east. “Me, take care of it? Haha…Hillman, perhaps there is nobody besides you who knows more about the affairs of the Baruch clan…and about me.”

Hillman started. He didn’t know why Hogg had suddenly said this.

“That affair has been buried in the deepest reaches of my heart for eleven years now. For eleven years, I’ve felt as though my heart has been chewed on by ants. I’ve been suppressing it all this time. Suppressing it, one day after another, one year after another…and in the blink of an eye, eleven years went by.”

Hogg’s entire body began to tremble.

Hillman’s face changed. He suddenly stood up, saying in astonishment, “Lord Hogg, are you going to…?!”

“Right. I am going to investigate what happened that year. I must get vengeance for Lina [Lin’na].” Hogg’s face was fierce and violent, filled with baleful aura.

“Lord Hogg.” Hillman hurriedly said. “Didn’t we investigate it back in the day? The opponent has tremendous power. Just the small part of it that we encountered was already terrifying. If you keep investigating, it’ll mean the death of you.”

Hogg let out a low growl. “Death? You think I fear death? Hillman, you have no idea how much pain I’ve been in these past eleven years, the sort of mental torment I’ve been under. I’ve had enough. The value of the magicite cores should be worth around 80,000 gold coins or so. This will totally be enough to pay for Wharton’s tuition. With this sum of money, I have no worries or cares at all now.”

“All these years, I’ve been suppressing myself, why? Because of my two sons. Now that Linley has grown up, and Wharton has reached the O’Brien Empire, I have nothing to worry about anymore.”

Hogg tightly clenched Hillman’s shoulders with his hands, staring into Hillman’s eyes. “Hillman, although you have always addressed me as Lord Hogg, after all these years, the two of us have developed genuine brotherly affection towards each other. For the sake of that brotherly love, I hope you can help me.”

“Hogg, you…” Hillman was frantic.

Hillman knew very well that once Hogg really went to investigate that affair, he would very likely lose his life.

“My mind is set. Hillman, you must understand, this life I have been living is worse than death.” Hogg’s eyes were turning red. Seeing Hogg like this, Hillman felt helpless. He could understand how Hogg felt.

Why was it that over these years, Hogg had become so solemn, so cold?

Others might not know, but Hillman knew very well. Before the birth mother of Linley and Wharton, Lina, had died, Hogg was a very easy-going, open-minded person. But after Lina’s death, Hogg’s character and disposition had changed.

Although Hogg had told others that Lina had died in childbirth, Hillman and Housekeeper Hiri knew the truth.

“Hillman, don’t try to persuade me. I just want to ask you – will you help me, or won’t you?” Hogg fixed his gaze upon Hillman.

Staring at Hogg, in the end, Hillman let out a long sigh. “Fine. I’ll help.” A hint of a smile blossomed on Hogg’s face. The smile of relief and liberation.
