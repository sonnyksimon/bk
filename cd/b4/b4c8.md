#### Book 4, The Dragonblood Warrior – Chapter 8, Experts Everywhere (part 2)

Linley was slightly startled. He hesitated. In his mind, many thoughts flashed by. But in the end, he still nodded. “Yes. Her name is Alice.”

Delia’s eyes immediately turned red. “Congratulations.”

Delia hurriedly turned away, unable to prevent her tears from coursing down her face. She quickly ran out of the reading room.

But Linley himself did not see Delia’s tears.

“Sigh.” After directly telling Delia the truth, Linley felt restless and annoyed. But at the same time, he also felt relaxed.

After this event, Linley had no desire to keep reading. After noting down the name of this book, he returned it to the shelf.

On his way back to his dormitory, Linley couldn’t help but feel rather grumpy.

“Boss, I get it. You also like that Delia girl, right?” Bebe said, engaging in a bit of schadenfreude. “You know, I think Delia is a great gal. She’s better than Alice, y’know.”

“Shut your mouth.” Linley yelled at him mentally.

“Hrmph, hrmph, I was right on the money, wasn’t I.” Bebe said delightedly.

Linley let out a deep sigh. After a while, a hint of a smile appeared on his face. “Forget it. Since I’ve made things clear to Delia, this won’t be on my mind anymore. Mm, right. I’m meeting with Alice again tomorrow. I have to prepare a present.”

As he began thinking about Alice, Linley felt much more happier and relaxed.

……

December 29th. Evening. Linley split apart from Yale and his other bros, and headed off by himself to Alice’s house for his rendezvous. This time, Linley was going to be able to spend some extra time with Alice.

The first day of the first month of each year was known as the ‘Yulan Festival’. This was the biggest holiday in the entire Yulan continent. On this day, every year the Radiant Church would organize a huge religious mass.

As Fenlai City was known as the ‘Holy Capital’, with the headquarters of the Radiant Church located in West Fenlai City, naturally the religious mass in Fenlai City would be the largest one in the entire Yulan continent. When the time came, the Holy Emperor himself would officiate over the proceedings. This was always an incredible mass, and many, many people attended each year.

January 1st.

West Fenlai City, the headquarters of the Radiant Church. The Radiant Temple. This was a huge building that rose up nearly a hundred meters. Anyone at any place within Fenlai City could see it in the skyline.

In front of the Radiant Temple was an enormous city plaza, over a thousand meters in length. The plaza was paved with smooth, equally sized white stones. At this moment, the plaza was filled with a sea of people, and Linley and Alice were amongst them.

Many mounted knights of the Radiant Church were there as well, keeping order amongst the crowd. But in general, all of the people there were very orderly and obedient.

“Big brother Linley, at eight o’clock, a group of high-level officials of the Radiant Church will appear, including the Holy Emperor himself.” Alice said to Linley in a soft voice.

Linley nodded, glancing at the knights of the Radiant Church maintaining order. “Alice, look at all of these guardian knights here. There’s got to be at least a few thousand of them, and from the looks of it, none of them are weak.”

“Of course. This is the Yulan Festival. The ones who are guarding the event are the elite knights of the Radiant Church. Every single one of them present is at least a warrior of the fifth rank.” Alice, having grown up in Fenlai City, clearly knew much more about it than Linley.

Linley’s heart skipped a beat.

All knights of the fifth rank or higher? Such a powerful troop of knights, all consisted of knights of the fifth rank, would possess inconceivable power. As a mere magus of the fifth rank, he was nothing in front of their might.

Alice pointed at some magnificently dressed people in front. “Look, many of the highest ranking nobles have come today. In a bit, the royal clans of the six nations of the Holy Union will come as well.”

Time passed very quickly. In the blink of an eye, it was eight.

Suddenly, that hundred-meter high Radiant Temple began to radiate light, bathing the plaza in white. The enormous statue of an angel, located in the middle of the plaza, also began to dimly glow. At the same time, the entire plaza was suddenly filled with a beautiful song that seemed to have come from the realm of the gods.

At this point in time, from a building to the side of the Radiant Temple, a group of people walked out. In front of them were several rows of men clad in gleaming white armor and helmets with red plumes. These were the guardian knights of the Radiant Temple itself. Each and every one of them was a majestic, knightly sight to behold. This group of nearly a hundred knights all marching in perfect unison made for an awesome, high-pressure sight which quickly silenced the entire crowd.

“I didn’t realize the Radiant Church had this much power. Those hundred or so knights must all be warriors of the seventh rank at least.” Doehring Cowart appeared next to Linley, carefully inspecting the people present. “And there are even Saint-level combatants here today? Forget it, best I hide inside the ring.”

And then, Doehring Cowart promptly disappeared again.

“Saint-level combatants?” Linley couldn’t help but also carefully inspect that group of people.

Behind those hundred guardian knights of the Radiant Temple, there were ten or so people dressed in long, flowing white robes. And behind them, surrounded by several Cardinals wearing crimson, was a bald-headed old man dressed in silver robes.

“The Holy Emperor!”

Clearly, the bald-headed old man dressed in silver was the center and heart of this group of people. Linley couldn’t help but focus all of his attention on this man. The Holy Emperor was a tall man, perhaps almost two meters tall. In his left hand, the Holy Emperor was wielding a scepter that was nearly as tall as he himself was.

Behind the Holy Emperor and the Cardinals, there were four old men all dressed in black, as well as over a hundred warriors dressed in violet. This group of people walked in an orderly fashion to the center of the plaza. None of the hundred thousand people gathered in the plaza dared to make a sound.

“Grandpa Doehring, you said there are Saint-level combatants present. Which of these are Saint-level combatants?” Linley asked mentally.

“I could tell at a single glance. That Holy Emperor as well as one of those four old men in black are both Saint-level combatants. They are quite self-confident, it seems; they didn’t attempt to mask their power in the slightest. I didn’t expect that after five thousand years, that little Radiant Church which was hiding within the Pouant Empire would develop to such a level.” Doehring Cowart sighed nonstop.

“Not mask their power?”

Startled, Linley looked at the group of people again. Honestly speaking, when looking at the Holy Emperor, the Cardinals, and the four old men in black, Linley only felt they were imposing and majestic, but didn’t sense any powerful aura emanating from them at all.

But Doehring Cowart had just said…that those two Saint-level combatants weren’t masking their power at all?

“Linley, you have a long way to go. In the Yulan continent, a magus of the fifth rank isn’t much. Only upon reaching the seventh rank are you qualified to be considered ‘powerful’. But a combatant of the seventh rank, in front of one of the mightiest forces in this continent, is only a small fry as well.”

“On this continent, the Radiant Church, the Cult of Shadows, the Four Great Empires, and various other secretive organizations, all combined, have far more experts than you can imagine. Right now, you have very little power. You haven’t had any contact with people of this level. In the future, you’ll understand.” Doehring Cowart chuckled as he spoke. “Your biggest advantage is your youth. The strength of those powerful people was cultivated over many years of constant, bitter training. In the future, you will also become powerful.”

Linley nodded slightly.

Because at the Ernst Institute, he was praised as a genius, in his heart, Linley really did think rather highly of himself. But these words by Doehring Cowart startled him and woke him up. In comparison to the Yulan continent as a whole, Linley really didn’t count for much.

By the time the Holy Emperor’s group arrived, everyone on the plaza began discoursing amongst themselves quietly.

“Big brother Linley, look. The six royal clans have all arrived. That one in front is the royal clan of our Fenlai City, while that big, golden-haired man is his Royal Majesty, who also happens to be a powerful warrior of the ninth rank.” Alice whispered quietly into Linley’s ears.
