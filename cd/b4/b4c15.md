#### Book 4, The Dragonblood Warrior – Chapter 15, Returning to the Foggy Valley

Beneath him was a roiling, watery white mist. Standing at the precipice of the cliff, there was simply no way to see the bottom.

Straight chisel in hand, Linley was peering down into the Foggy Valley. Linley had spent three thousand gold coins to purchase straight chisel, and in terms of sharpness, it even exceeded that black dagger Linley previously used. After all, to Linley, the straight chisel was more suited to his hand than daggers.

Linley had already been inside the Mountain Range of Magical Beasts for over a month and a half. He felt that right now, he was at the peak of his strength, in the best possible condition.

A dense layer of earth-elemental essence began to swirl around Linley as Linley softly chanted the words a magical spell. Finally, it formed into a seemingly simple set of armor, but if one took a closer look at it, one would find that the material it was formed from looked very much like jadestone, with the only difference being that this jadestone armor emanated with earth elemental essence.

Earth-style spell of the seventh rank – Earthguard armor (Jadestone level).

A magus of the seventh rank was far more powerful than a magus of the sixth rank. The power of the defensive spells alone multiplied tenfold.

“Now, if I run into those Dragonhawks again, just using my jadestone armor, I’ll be able to easily handle their blows.” Linley felt very confident. Next, Linley began to mutter the words to a wind-element spell. Air began to swirl about Linley’s body, until finally, Linley lifted up into the air and began to drift into the Foggy Valley.

Linley was actually quite confident when it came to investigating the Foggy Valley.

“I now have both jadestone armor and also the Soaring Technique. In addition, my physical fitness level is that of a warrior of the fourth rank. When aided still further by a Supersonic spell of the seventh rank….survival should not be a problem.” Linley slowly made his way through the Foggy Valley, not at all rushing.

This was because….

Of the Blueheart Grass!

Blueheart Grass was extremely important to Linley. Aside from procuring Blueheart Grass, Linley was also extremely curious as to why so many magical beasts were all living here, especially given they all belonged to different categories of beasts.

“Boss, be careful. Don’t forget how you were almost hunted down last time.” Bebe mentally reminded him.

“Don’t worry.”

The further down Linley flew, the greater the gap between the two cliffs grew. Clearly, this valley was astonishingly large. Within the misty fog, Linley flew very carefully while inspecting his surroundings. Bebe was also watching with extreme caution. Both of them were looking to find more Blueheart Grass.

The first target Linley aimed at was naturally the place he had seen that Blueheart Grass last time, where he did not have the chance to gather it.

Hugging the cliff, Linley proceeded forward with caution.

“Boss, I see Blueheart Grass. It’s right there!” Bebe’s eyes were very sharp. Linley took a look as well, and instantly his eyes lit up.

The grass blades were green, but a faint current of blue emanated and flowed throughout them.

“There aren’t any Green Tattooed Pythons, are there?” Linley didn’t dare to be too rash. Although he no longer feared the Green Tattooed Python, once he began to fight with it, most likely many other magical beasts would be drawn here as well. He definitely didn’t have sufficient confidence in dealing with an army of magical beasts.

As the Green Tattooed Python was green in color, it was very easy to miss it in the surrounding green vines, so Linley had to be absolutely careful.

After closely inspecting his surroundings and verifying that there was no Green Tattooed Python nearby, Linley carefully flew closer.

Gathering the Blueheart Grass, Linley once more felt its icy cold in his hands. A hint of a smile appeared on Linley’s face. This chilling sensation was proof that this grass was indeed Blueheart Grass. Linley carefully stored it inside his backpack, and then continued to make his way forward in search of more.

“Growl…”

“Shriiiiek…”

All sorts of howls from magical beasts emanated from below. Their wild, mixed roars caused Linley’s heart to quiver. Those howling sounds came from below. Just judging from the roars alone, there must be an enormous number of magical beasts below!

Peering through the thinning white fog, Linley could now vaguely make out the rich grassland below.

“Boss, be careful. I don’t want to be attacked and chased and flee in all directions.” Bebe reminded.

“I know.” Linley was at maximum alert, and his eyes constantly scanned his surroundings, especially the green vines near the cliff walls. Linley was very much concerned that a Green Tattooed Python might be hiding amidst the vines. Being discovered by a single magical beast was the same as being discovered by all of them.

“Dragonhawk.” Linley discovered that far away, a large, flying magical beast was lazily soaring through the air. Hurriedly, Linley flew away from it.

Fortunately, the valley was filled with white fog, causing distant objects to have only a faint silhouette. The Dragonhawk was huge and easy to notice, but Linley was comparatively much smaller. Naturally, he had something of an advantage in this regard.

“Shriek, shriek!” Suddenly, a series of strange howls could be heard, and even worse, the howls were heading in Linley’s direction.

“Not good.” Linley’s facial expression changed.

Linley, who had been in close contact with Dragonhawks before, knew that this was the call of a Dragonhawk. Looking in the direction of the origin of the noise, he saw the hazy outlines of roughly two or three dozen giant Dragonhawks flying in this direction.

The Dragonhawks were simply too huge in size. Twenty or thirty of them flying in a row made for a formation that blotted out the sun and covered the skies.

With so many Dragonhawks present, there was virtually nowhere Linley could hide.

Right now, Linley had three choices. The first was to do battle with these Dragonhawks. The second was to fly up and flee for his life. The third….was to fly down, deep into the belly of the mountain.

“Whoosh!”

Without hesitating at all, Linley immediately threw himself downwards, blazing his way through the white mist. In the space of a breath, Linley had transformed himself into an arrow, shooting himself into the middle of the grassy plains. And then, not moving in the slightest, he threw himself face-down, hiding in the grass.

Linley carefully began to crawl to the edge of the plains. At the edge of the grass, he peered out, carefully assessing the valley.

This was an enormous valley, filled with rivers as well as huge grassy fields, appearing like a pristine utopia. But, this pristine utopia was filled with countless gigantic crawling creatures.

Two stories tall, and thirty meters long, with rocky, stone-like carapaces, each scale the size of half a person.

The relevant information immediately sprang to Linley’s mind. “Landwyrm. Magical beast of the sixth rank. Fire element.”

“If there was only one Landwyrm, it wouldn’t be much of a threat, but…” Linley scanned the area. “There’s over a hundred Landwyrms here. If a hundred Landwyrms all attacked, there’d be no way to block them.”

“But they aren’t fast enough. To me, they shouldn’t pose much of a threat.” Linley looked towards the other magical beasts.

Within the valley, Landwyrms only made up a small part of the total magical beast population. There were also a large number of…Velocidragons. Velocidragons were not pack animals, and so most of them were spaced out in various places in the valley. At the same time, the skies were filled with Dragonhawks. If one looked carefully, within the various grassy plains, gigantic boas could also be seen slithering about.

And these were just what Linley could see at a glance.

“Just from that short glance, I can at least be sure that this valley runs from east to west. In the north, I can just barely make out the cliff walls.” Linley turned his head and looked back. From the west, he could also see the cliff walls. It was only the cliff walls to the east that he could not see clearly.

Especially that east-west running river, which was continuously flowing to the east.

“Bebe, you be careful too.” Linley executed the supporting wind-style Supersonic spell, and then carefully made his way through the grass. There were many grassy areas within this valley, possibly because all the magical beasts here were carnivores which did not eat grass.

While carefully making his way forward, Linley suddenly noticed something.

“What an extremely high density of natural elemental essence. The density of the natural elemental essence here is at least six or seven times higher than in the outside world.” Upon entering the valley, Linley was extremely keyed up, and actually didn’t notice this fact until now.

“I wonder what has caused this place to have such a high elemental density?”

Linley carefully crawled eastwards through the valley. Landwyrms, Velocidragons, Green Tattooed Pythons, and Dragonhawks were all exceedingly large creatures. Thus, the little speck which Linley was in comparison to them wasn’t very visible at all.

“This valley is really long!”

After crawling nearly 20 kilometers eastwards, Linley still hadn’t come to the end of the Valley. At the same time, Linley discovered some new magical beast packs.

Magical beast of the sixth rank, Winged Pegasus. Magical beast of the seventh rank, Thunderwing Pegasus.

All sorts of pegasi were flying about in the air, while others slowly walked about in the valley, eating the grass.

“Boss, there’s all sorts of underbrush here. How should we get across?” Bebe was worried.

Linley was starting to frown as well. The underbrush in front of him was all over the place, and it rose up half the length of his leg.

“The distance on the ground is too long. There’s no way to crawl there. I’ll have to go by air.” Linley carefully backed up about a few hundred meters, as far away from the pegasi flocks as possible, and then exercised the Soaring Technique.

“Whoosh!”

Directly soaring into the air, Linley immediately scurried into the dense white fog. Within the dense white fog, only occasionally would a pegasus draw near. After all, pegasi were fairly small and didn’t take up too much space, thus when they did draw near, Linley could dodge them.

Carefully flying eastwards, Linley kept close to the southern walls while carefully inspecting the cliffs for Blueheart Grass. But as Linley continued going forward, he began to frown again.

“Aside from that first patch of Blueheart Grass, I haven’t found any more at all.” Linley was starting to grow impatient.

But Linley continued flying eastwards. After flying roughly ten kilometers, Linley noticed that he was no longer seeing any pegasi in the upper reaches, and so he once again descended to the valley floor.

“Linley, there’s all sorts of magical beasts here. Many of the creatures here normally never travel in packs, such as the Velocidragon or the Black Bear, or the agile Dragoncat. Doehring Cowart wafted out of the ring, appearing by Linley’s side as they went forward together.

Linley carefully snuck forwards, while Doehring Cowart leisurely walked with him.

“Ah!”

As though struck by lightning, Linley suddenly halted and stood there stupidly. Roughly fifty meters ahead of Linley, in a knoll of grass with a diameter of roughly seven or eight meters, there was one patch of green-colored grass after another.

The fact that the grass was green was not of surprise. What mattered was…these grassy patches all emanated a blue aura.

“Blueheart Grass. All of it is Blueheart Grass!”

At this moment, Linley’s very heartbeat stopped. Heavens. A single patch of Blueheart Grass was worth tens of thousands of gold coins, and it would be considered a priceless item that would rarely even be seen on the market. But fifty meters in front of him, within that seven or eight meter wide patch of grass, there were at least a hundred patches of Blueheart Grass.

“So much! I could grab them seven or eight at a time!” Linley sucked in a deep breath.

Doehring Cowart’s eyes lit up. “Linley, for the purposes of drinking live dragon’s blood, most likely four or five Blueheart Grass would be enough. To have so much Blueheart Grass in one place is inconceivable. However…the area around the Blueheart Grass is empty, with no place to hide. How will you get there?”

Perhaps Blueheart Grass was inimical to normal grass.

In a 30 meter area around Blueheart Grass, there wasn’t a single blade of normal grass.

“There aren’t too many magical beasts around here, and the ones that are here are not pack beasts. They’re scattered all over the place.” Linley carefully observed that large cluster of Blueheart Grass and also the surrounding area. “There’s only seven magical beasts located near the Blueheart Grass. As long as I move fast enough, I shouldn’t have any problems escaping with my life.”

Linley forced himself to calm down, letting himself reach the maximum state of readiness.

“Boss, are you stupid? Have you forgotten about me, Bebe?” Bebe suddenly mentally said to Linley.

Linley started. Turning to look at Bebe, he saw Bebe delightedly winked at him. “Boss, my speed is much faster than yours, and my body is much smaller as well. How about I go do the gathering? There won’t be any problems at all. All you have to do is open your backpack and wait to get the grass.”

“Whoosh!”

Transforming into a black blur, in the blink of an eye, Bebe scurried into the middle of the grassy patch, and then using his sharp little claws, Bebe began agilely and voraciously digging up all the Blueheart Grass. As his little claws danced, quite soon that grassy patch became totally empty, while next to Bebe, there was now a pile of Blueheart Grass that was almost as tall as Bebe himself was.
