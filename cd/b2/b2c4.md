#### Book 2, Growing Up, Chapter 4 – The Ernst Institute

As time passed, the little Shadowmouse, which had not known much love from others, began to fear Linley less and less. By the eighth day, when Linley put down the rabbit, he moved away only two steps, and that little Shadowmouse still immediately ran over to eat, and even squeaked twice at Linley.

The tenth day!

“Right, today I’ll give the little Shadowmouse some cooked meat.” Linley covered a wild chicken with a cloth sack, and then happily went to the back of the ancient courtyard in the manor.

Doehring Cowart was walking by Linley’s side as well, but aside from Linley, no one else could see him. Doehring Cowart was smiling so widely that his white whiskers were leaning horizontal. “Linley, over these past nine days, the little Shadowmouse has lost all fear of you. Today, you are even giving him cooked meat. He’s going to be extremely excited and will become even closer to you.”

Hearing his words, Linley couldn’t help but grin as well.

Just as Linley walked into the courtyard….

“Squeak, squeak!” The little Shadowmouse immediately ran up to Linley, and began hopping up and down while squeaking at him.

“I haven’t even taken the food out, and he’s already run up to me. He really isn’t afraid of me at all.” Linley felt joy in his heart.

Next to him, Doehring Cowart smiled merrily at the little Shadowmouse, which didn’t notice his presence at all. Doehring Cowart said with a smile, “Looks like he’s already feeling quite close to you.”

“Squeeaaaak!” The little Shadowmouse looked at Linley with its innocent black eyes and began to squeak with impatience, as though telling Linley to hurry up and give him the food already.

“Don’t be impatient.” Linley took the roasted chicken out of the clothsack.

Upon smelling the roasted chicken, the little Shadowmouse’s eyes shone, and then it looked at Linley pitifully. Seeing this, Linley couldn’t help but laugh until his stomach hurt. In the past, when Linley gave good food to little Wharton, little Wharton would say, “Big bro, I want!” while staring at him in a pitiful manner.

Now this little Shadowmouse was doing the same!

“Hehe, all yours!” Linley gave the cooked chicken to the Shadowmouse.

The little Shadowmouse squeaked with joy, immediately seizing the roast chicken. After taking a single bite, the little Shadowmouse began to eat faster and faster. In a very short time, the roast chicken, which was about the same size as the Shadowmouse itself, had been completely devoured.

“I really don’t get how his stomach can contain so much. How can he swallow that much food?” Linley laughed while sighing.

It seemed as though this time, the little Shadowmouse had enjoyed his meal very much. He was so happy that he immediately began to hop up and down while squeaking at Linley, while even hugging Linley’s leg with his own front arms. Linley couldn’t help but feel pleased; this was the first time that the little Shadowmouse had acted so intimately towards him, even after eating.

“Linley, try and use your hand to smooth his fur. Usually, most magical beasts like their family members to groom them and stroke their fur.” Doehring Cowart advised.

Linley tentatively stretched his hand out and placed it on the little Shadowmouse’s head. The little Shadowmouse didn’t dodge in the slightest. Instead, it contentedly half-closed its eyes. Linley immediately felt more confident, and began to stroke his fur, causing the Shadowmouse to feel so comfortable that it began to snore.

“This little guy is so adorable.” Linley was really beginning to like this little Shadowmouse more and more.

“Grandpa Doehring, magical beasts are so strange. That Velocidragon is so huge and has such tough scales, making it a magical beast of the seventh rank. But this little Shadowmouse, when he grows up, will also become a magical beast of the seventh rank. Both of them have the same rank, but why is there such a big difference between them?”

While petting the little Shadowmouse, Linley couldn’t help but feel amazed.

“You can’t judge them just based on their appearances. Perhaps an ordinary old geezer that you meet on the street is able to ride a flying dragon and level a mountain with the wave of a hand.” Doehring Cowart laughed merrily.

Linley understood this logic.

But unconsciously, he still used appearances to judge.

For example, that Velocidragon. Seeing how huge body was and seeing how its scales gleamed with a frozen golden light, anyone could tell how powerful it was.

“I really wonder when this little Shadowmouse will initiate a ‘bond of equals’ with me.” Linley mumbled. There was nothing he could do. The ‘bond of equals’ could only be initiated by magical beasts, so he could only passively wait.

Doehring Cowart laughed. “Things are progressing very well. Remember. You must have patience.”

“Right. I got it.” Linley laughed as well.

…..

In the blink of an eye, time passed. Linley had fed the little Shadowmouse for twenty days now, and the little Shadowmouse was behaving extremely familiarly with Linley. But for some reason, even though the two of them had become extremely close, the little Shadowmouse still had not initiated the ‘bond of equals’.

Darkness covered the land, and the entire Wushan township was very quiet.

Within the Baruch clan’s living room, candlelight flickered from within as Linley and his family, along with Housekeeper Hiri, were enjoying supper together on the long dining table.

“Linley, I hear that you’ve often been bringing roasted hares to the back courtyards?” Halfway through the meal, Hogg put down his utensils and turned to Linley.

Linley was startled.

“Looks like it is time for me to confess.” Linley said to himself, then looked at Hogg and nodded. “Father, recently I discovered a cute animal living in our back courtyard, an extremely cute animal. So I often bring him some food.”

“A cute animal?” Little Wharton’s eyes shone.

“Oh.”

Hogg nodded. “People rarely visit the back courtyard, so its normal for there to be animals there. Right. In a week or so, the Fenlai City is going to begin another round of magical aptitude testing and magus recruitment. Do you want to participate?”

“Oh, the magus testing and recruiting event?” Linley suddenly remembered this event.

The ray of light which only Linley could see shot out from within the Coiling Dragon Ring, turning into the white-bearded Doehring Cowart. Doehring Cowart laughed at Linley, “Linley, the magus testing and recruiting event is optional for you. Under my guidance, will you achieve less than at a magus academy?”

Linley agreed with this line of thought.

Doehring Cowart was a Saint-level Grand Magus. Would any magus academies require a Saint-level Grand Magus to teach there?

“What, you don’t want to go?” Hogg’s face, previously smiling, immediately grew cold as he frowned.

Hogg remembered clearly that ever since the battle between the dual-element magus of the eighth rank and the small party, Linley had very much wanted to become a magus. Why was he hesitating now? In Hogg’s heart, he too hoped that his son could become a magus.

“Father, I…”

“No, Linley, accept your father’s offer.” Doehring Cowart frowned and hurriedly said.

Linley’s words died unspoken on his lips. At the same time, he suspiciously asked, “Grandpa Doehring, I have you to teach me, right? With you teaching me, why would I need to go to a magus academy? Isn’t that a waste of family resources?”

“No.” Doehring Cowart said seriously. “I haven’t interacted with the Yulan continent for over five thousand years. Five thousand years, Linley! You must understand that many magi in the world have been continuously researching and developing new spells during this time period. Who knows how many new spells have been developed in the interim.”

Linley suddenly understood.

“And Linley, you must know that Wushan township is not the stage on which you will perform. You must step onto a far wider stage.” Doehring Cowart said seriously.

“A far wider stage…”

Linley couldn’t help but be moved.

He couldn’t help but remember that huge Velocidragon, and the destructive power unleashed by the ‘Dance of the Fire Serpents’, as well as the Saint-level Grand Magus ‘Rudi’, who effortless controlled those countless boulders to cause an absolute calamity.

“The future…”

Linley’s heart began to beat faster. If he could one day step atop the head of a dragon and control cataclysmic power, if he too could feel the power of standing at the very pinnacle of mankind, that must be an amazing feeling. When he thought of this, Linley felt his blood begin to boil.

“Linley, what are you thinking about?” Hogg was beginning to grow unhappy. When he was talking to Linley, Linley was daydreaming.

“Oh, nothing!” Linley immediately looked at Hogg and quickly nodded while saying solemnly, “Father, in my heart, I really want to become a magus. In a week, please arrange for me to go to Fenlai City to take part in the magus testing and recruiting event.”

Upon hearing these words, Hogg finally smiled.

“Magus, ooo, ooo, like that fire-breathing magus?” While listening, little Wharton clapped his little hands together.

“Wharton, that was just a circus trick! Don’t mix up circus tricks and real sorcery.” Hogg said seriously.

“Oh.” Little Wharton pouted and stopped talking.

Linley chuckled, then turned to look at Hogg. “Father, there must be many magus academies. Which ones are good? Right, are there any combined magus academy/warrior academy schools?”

Hogg laughed as well. “Actually, all four of the major empires and both the major alliances have their own elite academies. You should know that one of the four major empires, the O’Brien Empire, is the empire with the strongest military power.”

Linley nodded. Everyone knew that.

“The most elite school in the O’Brien Empire is the O’Brien Academy, which is reputed to be the number one warrior academy in the entire Yulan continent. But as far as magus academies go…” Hogg chuckled. “The number one magus academy in the entire Yulan continent belongs to our Holy Union. Its name comes from a legendary Holy Emperor of the Holy Church, ‘Holy Emperor Ernst’. The ‘Ernst Institute’.”
