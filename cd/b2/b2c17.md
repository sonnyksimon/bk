#### Book 2, Growing Up, Chapter 17 – A Learning Period (part 1)

Spring left and autumn came. In the blink of an eye, Linley had spent half a year at the Ernst Institute.

During those days in school, Linley was like a thirsty man of the desert, frantically drinking up the basic fundamentals to magic. With regards to wind-style magic, Linley’s knowledge and strength continued to rise as well, and Doehring Cowart would give him pointers every so often as well.

Today, the sunlight was bright and beautiful.

The four bros of dorm 1987 had just finished lunch. They were wearing a set of sky-blue robes, the school uniform of the Institute. Because of constant physical training, Linley appeared all the more mesmerizing, with his elegant form covered by the sky-blue robes. This was why quite a few of the young girls in the wind magic class liked to chat with Linley.

At this moment, the four bros were walking while chatting idly.

“Right, Linley, today the rest of us are going to attend the new students fellowship. Are you going?” George chortled.

George loved to participate in student unions and fellowships, and he was excellent in ferreting out news and making new friends. Although he had only been in school for half a year, amongst the first grade students of the Ernst Institute, George had become a mover and a shaker.

“Nope.” Linley’s answer was succinct and direct.

“Haha, I knew Linley definitely wouldn’t go.” Reynolds laughed loudly.

Putting his arm around Linley’s shoulders, Yale sighed, “Linley, my man, there’s no need to be this diligent when it comes to studying. Based on your talent, if you just expend a bit of effort, in thirty years you can easily become a magus of the sixth rank. Why do you have to work so crazy hard? You should learn to relax and enjoy life. There’s a lot of cute girls who will be at the fellowship, you know.”

“Right. Really cute girls.” Reynolds opened his eyes wide and nodded.

Linley could only sigh helplessly.

Under the guidance of the Yale, that innocent youngster, Reynolds, had begun to go astray.

“Yale, you pervert, stop tugging at me. Alright, time for me to go train. Tomorrow is the end of the month, I’ll hang out with you guys then.” Linley laughed. The last two days of each month, Linley let himself take two days break.

Knowing Linley’s temperament, Yale, Reynolds, and George all nodded.

Linley immediately walked off, quietly but quickly heading towards the mountains behind the school. There were thousands of students at the Ernst Institute, and there were also many magi who were researching new spells here. There were also many servers. In short, the Ernst Institute was a well-populated place.

On the road to the mountains, many students wearing blue gowns could be seen as well.

“Growl…” A low roar sounded.

Linley turned aside to look, and his eyes brightened. “A magical beast!”

A flowing mane, slick cyan fur, and four thick, forceful limbs. A pair of eyes filled with wildness, viciousness, and a cold fierceness. Those coldly flashing golden claws made onlookers’ hearts tremble.

The magical beast, ‘Windwolf’.

A terrifying magical beast that moved as fast as the wind itself.

The most terrifying thing one could encounter in a forest of magical beasts was a pack of Windwolves. If you encountered them, based on their speed, there was no way you could escape.

A handsome, black-haired man was seated atop the Windwolf. The young man was staring delightedly around him, seeming to be very proud of having such a fine magical beast.

“This should be a magical beast of the fifth or sixth rank,” Linley decided.

At the Ernst Institute, there were indeed quite a few people who had magical beasts. Aside from the magi who had been invited to come to the Institute, some fifth and sixth grade students were able to buy soulbinding formation scrolls and had managed to tame some magical beasts to serve as their mounts.

“It’s just a magical beast. Why be so cocky about it?” Linley looked somewhat contemptuously at the self-pleased youngster.

After departing from the school, Linley entered the mountain in the rear.

The mountain behind the Ernst Institute was an extremely wide ranging one. Long, long ago, magical beasts used to live in this mountain, but as time went on, all of the magical beasts were exterminated by the magi of the Institute. By now, there were only a few normal beasts still living here.

Upon entering the mountain, Linley’s speed increased dramatically.

He naturally began to use the wind-style ‘Supersonic’ spell, turning his entire body as light as a leaf. Like a spirit, he wound his way through the mountains. After running for several kilometers, Linley reached his target destination, an empty area next to some flowing water.

“Squeak squeak.” Bebe chirped at Linley.

Linley chuckled and said, “You want to go out and play again? Fine, but don’t run off too far.” Linley had a lot of faith in Bebe. A year had passed since he had met the little guy, but although Bebe still hadn’t grown larger, and was still just twenty centimeters long, his speed had dramatically improved.

“Magi? Perhaps a warrior of the eighth rank would be able to catch the little Shadowmouse, but only a Saint-level magus would be able to do the same.” Linley knew very well how strong the bodies of most magi were.

The little Shadowmouse, Bebe, scurried into the mountain forests.

“Grandpa Doehring, please come out and instruct me.” Linley immediately said mentally.

A mist flew out, transforming into Doehring Cowart. Doehring Cowart blinked and glanced at Linley. “Linley, what’s going on? In the past, haven’t you always ignored this old fellow and entered the meditative trance first? Why are you calling me out now? I was having a wonderful nap just now, hmph. You ruined my beautiful dream.”

Linley quirked his lips.

Although Grandpa Doehring was a Saint-level Grand Magus, after getting to know him, Linley realized that although he looked kindly and amiable on the outside, on the inside, he was a playful scamp.

“Grandpa Doehring, I feel like I have reached the level of a magus of the second rank. I want you to take a look and see for yourself.” Linley finally said.

“A magus of the second rank?”

Intrigued, Doehring Cowart ran some calculations. “Hmm, right, about a year has passed since you started learning with me. Right, first, perform the introductory spell of ‘Shattered Rocks’. Do your absolute best, understood?”

‘Shattered Rocks’ could be considered a spell which scaled.

There was a ‘Shattered Rocks’ spell of the first rank, but there was also a Saint-level spell for the ‘Shattered Rocks’; only, the name for it was called ‘Heavenly Meteor’s Descent’. Naturally, when the strength of an earth-style magus increased, his power in using the ‘Shattered Rocks’ would also increase.

“Yes, Grandpa Doehring.”

Linley immediately began to quietly mouth the words to a spell. The words had long since been memorized by Linley to the point where he could recite them without thinking. As the words to the spell continued, Linley could feel his entire spirit enter a special mode.

The earth-style mageforce in his chest began to roil about, and natural elemental essence began to gather there as well.

Suddenly, the nearby earth began to crack and shatter.

Five skull-sized pieces of rock flew up and began to circle around Linley’s head. These five rocks were all covered with earthen specks of light, and as Linley’s eyes began to shine, he let out a deep shout. The five rocks rapidly shot off to a far distance, carrying a gust of wind with them.

“SMASH!”

The five stones covered in earthen light smashed into a thick tree trunk. The tree swayed, but its trunk did not shatter. In the end, the five stones still came tumbling down to the ground.

“Yeah, not bad.” Doehring Cowart’s eyes lit up. “To be able to control five stones at once with such impressive speed shows that you do, in fact, have the power of a magus of the second rank.” Doehring Cowart was very much satisfied with Linley’s performance.

Linley couldn’t help but reveal a hint of a smile on his face as well.

He had just taken another step towards his goals.

Linley would never be able to forget the words his father had said to him when he left. “If you cannot bring it back, even when I die, I won’t forgive you!” These words had pierced Linley’s heart like a sharp knife, and he was constantly reminded of them.

Right now, Doehring Cowart was chortling happily. “But Linley, you must understand that a magus of the second rank counts for little. Based on our ranking systems, magi of the first and second rank are all considered ‘entry-level magi’. Magi of the third and fourth ranks are considered ‘mid-level magi’, and fifth and sixth ranks are ‘high-level magi’. A magus of the seventh rank is called a ‘senior magus’, a magus of the eighth rank is a ‘master magus’, and a magus of the ninth rank is a ‘arch magus’. These ranks of seven through nine are the highest. The road you have to travel is a long way.”

“I know.” Linley nodded.

“Good. Train hard.” Doehring Cowart once more entered the Coiling Dragon Ring.

Linley collected himself, suppressing his excitement at becoming a magus of the second rank. He once more tranquilled sat and entered the meditative trance. The strong became strong one step at a time and through achieving many accomplishments.

Approximately three kilometers away from Linley.

Linley’s wind-style magus instructor, the sixth-ranked magus Trey, frowned. “Hmm, the earth magic spell, ‘Shattered Rocks’? Based on its power, it should be of the second rank. An entry-level magus has come to the mountain to train? Who is it?”

Just then, Trey had utilized the ‘Windscout’ spell, and had sensed the earth-style magic which Linley had just cast.

Based on the magic vibrations, Trey was able to determine what spell it was.

Trey curiously walked in that direction. Based on his prowess as a magus of the sixth rank, his execution of the ‘Supersonic’ spell was far stronger than that of Linley’s. Like a passing fog or cloud, Trey easily and tranquilly flowed through the mountain.

In the blink of an eye, Trey had reached a spot two hundred meters next to Linley.

Standing next to a large tree, Trey saw Linley from afar.

“It’s him?”

Naturally, Trey recognized his own student. “This kid called Linley never talks in class. Even when experimenting in new spells, others will try them out, but he will just stand and watch from afar, never showing his strength. It seems…this kid called Linley is already a magus of the second rank. I remember him being one of our new students. Didn’t expect him to be so talented.”

Linley already knew how to cast spells, so of course when the instructors told the other students to give it a try, he would just stand and watch.

Never participating in any group activities, Linley’s secretiveness was acknowledged by everyone who knew him.

“Hehe, looks like I have a genius amongst my students. Mm. Looks like this year, I should receive a reward when the first grade student competition commences.” A brilliant smile was on Trey’s face. As for Linley, right now, being in a meditative trance, he couldn’t sense anything more than a hundred meters away from him.
