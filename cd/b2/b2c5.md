#### Book 2, Growing Up, Chapter 5 – ‘Bebe’ the Shadowmouse (part 1)

“The Ernst Institute is the number one magus academy in the world. All of the graduates of the Ernst Institute are at least magi of the sixth rank, and there’s even many who are of the seventh rank! If our Baruch clan was able to produce a magus of the seventh rank, we at least would stand a chance of recovering our ancestral heirloom.”

While speaking, Hogg looked at Linley eagerly.

Linley could feel the hope which Hogg was placing on him.

“Our ancestral heirloom. For our ancestral heirloom to be lost to us is a humiliation that must be washed away.” Linley could also feel his heart grow heavy.

As a scion of the ancient Dragonblood Warrior clan, he felt proud of his ancient and mighty lineage. But the mighty Dragonblood Warrior clan had lost its own ancestral heirloom. What a humiliation! Hogg and countless elders who had passed away had all felt ashamed whenever they thought about it.

Unfortunately, the type of family which could collect the warblade ‘Slaughterer’ was not an ordinary one, and the current Baruch clan was far too weak.

“Ernst? The legendary Holy Emperor of the Radiant Church?” The nearby Doehring Cowart started.

“What is it, Grandpa Doehring?” Linley asked questioningly. “I bet all of the hundreds of millions of citizens in the six kingdoms and fifteen dukedoms of the Holy Union know about the legendary Holy Emperor Ernst of the Radiant Church.” Linley, also, knew much about the affairs and history of the legendary Holy Emperor Ernst.

He had dramatically raised the profile of the Radiant Church, and single-handedly created the Holy Union.

“I didn’t imagine that kid, Ernst, ended up having such accomplishments. And he even became a legendary Holy Emperor of the Radiant Church!” Doehring Cowart sighed.

“Grandpa Doehring, you knew Holy Emperor Ernst?” Linley was somewhat surprised.

But then, Linley thought things through.

That’s right. In the past, when the Pouant Empire was still unified, the Radiant Church, the Cult of Shadows, and even the Pavilion of Divinities all had many churches within the empire. But all of those churches were under the control of the Pouant Empire.

“Naturally. Ernst was a genius who entered the Saint-level when he was merely fifty or so years old. But in my age, he could only be considered a promising latecomer.” Doehring Cowart said calmly.

When Doehring Cowart was still alive, Ernst had still been developing himself. When Ernst had finally entered the Saint-level, Doehring Cowart had already been standing at the very pinnacle of the Yulan continent for a long time. Even amongst Saint-level combatants, Doehring Cowart would have been considered one of the greatest.

Doehring Cowart had an extremely high status within the Pouant Empire, which Ernst didn’t come close to matching, at the time.

If Ernst had run into him, he would have had to courteously bow and pay his respects.

“I didn’t expect that after I died, Ernst would become so incredible.” Doehring Cowart laughed faintly.

Linley couldn’t help but feel a deep sense of veneration for Doehring Cowart from his heart. A Saint-level Grand Magus of the Pouant Empire, and one of the most powerful persons in the Yulan continent. And now, Doehring Cowart was carefully instructing himself in magic. How fortunate Linley was!

As dinner progressed, the conversation amongst the Baruch clan manors was quite cheerful.

“Linley, in a week’s time, I’ll arrange for Uncle Hillman to take you to Fenlai City and attend the magus testing and recruiting event.” Hogg smiled towards Linley.

“Yes, father.”

Linley nodded.

“Young master Linley, I’m sure that you will be able to enter the finest of magus academies.” Housekeeper Hiri chortled.

“The finest. Oh. The finest!” Little Wharton’s hands were covered in grease from eating, but still beamed as he waved his greasy hands.

Hogg smiled faintly as he said, “Becoming a magus is no easy thing. Perhaps only one in ten thousand has the talent. The requirements for entering the Ernst Institute are even higher. Only someone with an extremely high aptitude for magic will be admitted. If Linley can become a magus, I will be very satisfied, regardless of what academy he is accepted to.”

“I won’t let you down, father.” Linley’s words were filled with confidence.

Because Linley, after all, was already a magus of the first rank.

…..

As time flowed onwards, in the blink of an eye seven days had passed.

Linley was lying on the grass near the back courtyard, while the little Shadowmouse was hopping up and down around Linley. It was squeaking nonstop, but Linley paid him no mind.

The little Shadowmouse rolled its eyes, then stood up on its hind feet and placed its front feet on top of Linley’s body.

“Squeeeeak.” The Shadowmouse called out with displeasure.

Linley rubbed the little Shadowmouse’s head. “Alright, stop making a fuss. Tomorrow, I’m going to leave home and go to the capital. After the magus recruitment event is over, I’m going to be going to a magus academy. I’m afraid we won’t have many chances to meet after that.”

There was no way he could bring a little Shadowmouse into a magus academy.

Not a single student in a magus academy was an ordinary one, and there were many powerful magi there as well. If they found a little Shadowmouse there, they would probably immediately subdue and tame him. Even magi of the seventh and eighth ranks were present in magus academies. Catching a little Shadowmouse wouldn’t be too hard.

After all, he hadn’t bonded with the little Shadowmouse yet, so anybody could subdue and tame him.

“Sniff, sniff…” Hearing Linley speak, the little Shadowmouse also began to sniff in a low tone.

“You don’t even know what I’m saying,” Linley shook his head helplessly.

“I don’t know how much time I will have to spend in a magus academy, or how many years I will be there for. Will we ever meet again?” Linley stroked the little Shadowmouse’s fur, somewhat unwilling to part from it. After playing with the little Shadowmouse for the past month, he had really come to care for the cute little Shadowmouse.

The little Shadowmouse enjoyed the petting so much that its eyes grew half-lidded as it squeaked quietly in contentment.

…..

The next day, after lunch. The Baruch family’s front courtyard.

Hogg stood there, straight as a ramrod. Staring directly at Linley, he said, “Linley, Wushan township is located fairly close to the capital, just ninety or so kilometers away. You should be able to make it to the capital before nightfall. Remember, when you reach the capital, don’t cause any trouble. There are too many rich and powerful people in the capital.”

“Yes, father.” Linley bowed as he said.

“Hillman, I entrust Linley to you.” Hogg looked at the nearby Hillman.

Hillman smiled as he said, “Lord Hogg, please set your mind at ease.”

“Alright, you can go now.” Hogg laughed.

“Farewell, father.” Linley said respectfully, and then smiled at little Wharton. “Wharton, your big brother is gonna leave now.”

Little Wharton immediately squinted towards Linley. In a sad voice, he said, “Big brother, bye bye!”

Linley glanced at the back courtyard, thinking to himself, “I’m afraid no one is going to come bring meat to the little Shadowmouse in the next few days.” Hillman, who was next to him, said to Linley, “Linley, let’s go!”

“Yes, Uncle Hillman.”

Linley didn’t think about it anymore, and immediately followed Uncle Hillman as they departed from the manor.

“Squeak.” On the rooftops above the living room of the Baruch clan manor, the little Shadowmouse watched Linley and Hillman depart. The little Shadowmouse’s mind was filled with questions. In his eyes, this was the time when Linley should be going off to kill a wild hare. Why had he taken up a bag and headed off with someone else?

The little Shadowmouse really liked Linley.

Over the past month, the friendless little Shadowmouse had really come to view Linley as family.

“Squeak!”

The little Shadowmouse’s body flickered and in the blink of an eye, disappeared from atop the eaves of the Baruch clan’s manor. In two or three movements, it moved, reappearing on top of a nearby peasant’s house, still watching Linley and Hillman. As it followed behind Linley, the little Shadowmouse soon had left Wushan township.
