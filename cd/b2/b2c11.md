#### Book 2, Growing Up, Chapter 11 – The Secret Dragonblood Training Tome (part 2)

“The Secret Dragonblood Training Method?” Linley couldn’t help but look strangely at his father.

Hogg smiled. “Not only is it the Secret Dragonblood Training Method. This tome also discusses many things related to our Baruch clan. The Secret Dragonblood Training Tome is included within, as well as the method to create and control the Dragonblood Needles, as well as the history of some of the elders of our clan.”

Linley carefully flipped through it.

Indeed, the tome was divided into four sections. The first part was regarding the ‘Secret Dragonblood Training Tome’, while the rest were regarding other matters pertaining to the clan.

“Linley, even if this tome falls into the hands of outsiders, it would be useless to them, as there is simply no way an outsider can train in accordance with the Secret Dragonblood Training Method. As for our family history, so what if someone learns about it? What’s more, we have multiple copies of this tome as well. This one is also just a copy. After so many years have passed, the original has long since turned to dust.” Hogg laughed as he spoke.

Linley immediately laughed as well.

“Makes sense. Even if someone acquires it, it would be useless.” Linley immediately began to more curiously flip through the pages of the tome and read through each section.

Secret Dragonblood Training Tome, Chapter 1.

“If one wants to utilize the Secret Dragonblood Training Method, one must be able to call forth the blood of the Dragonblood Warriors flowing through their veins. There are two ways of calling forth the Dragonblood. The first method requires the density of the Dragonblood having reached a certain level. But if the density is insufficient, there is still a second method…”

Reading this, Linley was stunned.

Aside from a high density of Dragonblood, there was another method? Why hadn’t anyone in the family succeeded in all these years, then?

“The second method is to take a deep drink of the blood of a living dragon, or of the blood of a dragon that just died a few minutes ago. The longer a dragon has been dead, the lower the chance of awakening the Dragonblood! A deep drink of dragon’s blood can activate the inherent Dragonblood flowing in each member of our clan’s veins. For the best results, drink the blood of a Saint-level dragon. If one only drinks the blood of a dragon of the ninth rank, the chances of activating one’s Dragonblood is rather low.”

Reading through this, Linley was stunned.

“Our clan elders really were formidable. They actually came up with the idea of drinking the blood of a living dragon in order to utilize the Secret Dragonblood Training Method.” Linley didn’t know whether to laugh or to cry.

“Drink the blood of a living dragon, and a Saint-level one at that? Linley, your ancestors really were extremely formidable.” Doehring Cowart had appeared by Linley’s side and was reading the Tome as well. Seeing the introductory paragraphs, he couldn’t help but feel shocked as well.

Hogg, of course, couldn’t discover Doehring Cowart’s existence at all. Hogg laughed bitterly at Linley. “Linley, did you see that? Based on our ancestor’s method, the Dragonblood is lurking hidden within all of our veins. To call it forth, there are just two methods. But the second method requires one to drink the blood of a living dragon. How can that be an easy task? What’s more…Linley, flip to the back and take a look.”

Linley flipped the page.

“However, this second method of drinking live dragon’s blood is extremely risky. Dragon’s blood is extremely forceful. When it is rubbed on one’s body, it has the effect of improving the quality of one’s body, rapidly increasing one’s strength. However, it will also cause pain comparable to one’s skin being peeled off. And this is just a topical application. If one actually drinks dragon’s blood, then one’s body will feel as though it is being scorched, to the point where one can actually be burned to death, with veins exploding, causing immediate death.”

Upon seeing this part, Linley was utterly speechless.

“Father, who wrote this Secret Dragonblood Training Tome? Since it is so dangerous, why did he even include it?” Linley didn’t know what to say.

Hogg said with a solemn face, “Linley, this Secret Dragonblood Training Tome was written by our founder and first ancestor, the very first Dragonblood Warrior to appear in the Yulan continent, Baruch! He naturally must have had his own reasons for writing this down. Nonetheless, in our family history, there have been two descendants who drank the blood of a Saint-level dragon, and in the end, both of their veins erupted and they died.”

“There’s been people who have actually drank the blood of a Saint-level dragon?” Linley was somewhat shocked.

But actually, it was quite normal.

In the past, when the first, second, and third generation of Baruch clan members were all Dragonblood Warriors, the clan was in its glorious ascendancy. At that period in time, it wasn’t impossible to procure the blood of a Saint-level dragon.

“The events of the past happened too long ago. The real secrets of that era, this book has not revealed. All I know is that because of this, the dragon race sent representatives to engage in discussions with our Baruch clan’s clanlord. After this, our descendants no longer attempted this method. Later on, when our family line weakened, even when we wanted to drink dragon’s blood, we no longer were able to.” Hogg shook his head and sighed.

Linley nodded.

The arrogance of the dragon race was something discussed in many books.

Capturing a live Saint-level dragon to engage in bloodletting? How great a humiliation would this be for the dragon race? It was quite lucky for the Baruch clan that the dragon race didn’t exterminate them in a fiery rage. However, from this, one can imagine how powerful the Baruch clan was at that time.

“This can’t be right, father. If no one has ever successfully become a Dragonblood Warrior as a result of drinking dragon’s blood, then why did our ancestor write that it is possible to use dragon’s blood to refine our own? And even say that the blood of a dragon of the ninth rank would also have some effect?” Linley was really puzzled.

Hogg was startled.

“Linley, don’t ask too much. Honestly, I only know a little bit about our family history as well. As far as what happened four thousand years ago, there’s no way we can clearly know what happened.” Hogg laughed towards Linley.

Linley nodded.

But in his heart, Linley was still suspicious. If no one in history had ever successfully become a Dragonblood Warrior by drinking dragon’s blood, then why would this method be written down in the Secret Dragonblood Training Tome?

“Linley, it’s getting late. You should go back and get some rest.” Hogg laughed.

Linley nodded.

Night.

Linley had returned to his own bedroom and was reading the tome, but his heart was still full of questions.

“Grandpa Doehring, what do you think. If no one has ever succeeded using this method, how could it have been discovered?” Linley simply couldn’t understand the logic.

Doehring Cowart was so old that he had become as crafty as a fox. Stroking his white beard, he said in a self-satisfied manner, “Linley, the answer is simple. Based on what I know, the dragon race is extremely proud, and also extremely large and powerful! I wager that drinking the blood of a live dragon is probably an effective method, but your clan came under tremendous pressure from the dragon clan, and therefore altered the contents of this book.”

Linley immediately understood.

This was very possible.

Under pressure from the dragon race, the Dragonblood Warriors of the Baruch clan were undoubtedly forced to stop catching live dragons for bloodletting.

“But of course, that’s just my conjecture.” Doehring Cowart said placidly. “And Linley, based on what I know, drinking the blood of a live dragon is not necessarily a road to death. As long as you combine it with some Blueheart Grass, the negative effects of dragon’s blood will be negated. But I bet there’s very few people nowadays who know this secret.”

Linley was stunned.

And then, he was wildly overjoyed. “Grandpa Doehring, are you saying that fresh dragon’s blood, when mixed with Blueheart Grass, is safe to drink?”

Doehring Cowart confidently nodded. “Of course. In the past, in the Pouant Empire, when a princess acquired a serious disease, in the end, the only method of curing her was a medicine that included a mixture of fresh dragon’s blood and Blueheart Grass. As a matter of fact, I was the one who personally caught a Saint-level dragon.”

“I remember the master physician who provided the prescription saying that everything in this world has its equal and opposite. For every single ingredient, there was another that would match with it. In that era, the only person who knew how to mix fresh dragon’s blood with Blueheart Grass was that old physician. Since six thousand years have gone by, no doubt no one knows it any longer.” Doehring Cowart said calmly.

Linley nodded.

“Fresh dragon’s blood and Blueheart Grass…” Linley’s eyes shone with excitement. “In the future, when I am powerful enough and become a magus of the ninth rank or even higher, I will use fresh dragon’s blood and Blueheart Grass to let little Wharton become a Dragonblood Warrior.”

Linley even hoped that…

If he had the chance, he himself would use this recipe.

If he could become both a Saint-level magus and a Dragonblood Warrior….but of course, that was just a dream. To even be able to catch a Saint-level dragon was a distant, untouchable dream.

“The road ahead is still long. Time to sleep, time to sleep. I need to train tomorrow.”
