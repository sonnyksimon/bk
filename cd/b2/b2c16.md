#### Book 2, Growing Up, Chapter 16 – Wind-Style Magic

For the academic calendar of the Ernst Institute, every month, the first twenty eight days had classes. Only the last two days were free.

Earth magic classes were taught from 8:00 AM to 10:00 AM in the morning, fire magic was taught from 10:30 AM to 12:30 PM in the afternoon, water magic from 2:00 PM to 4:00 PM in the afternoon, wind magic from 4:30 PM to 6:30 PM in the afternoon, lightning magic from 7:00 PM to 9:00 PM at night, and light magic from 9:30 PM to 11:30 PM at night.

But since the majority of students were single element, they only had to take two hours of classes a day. Linley was dual-element, which meant each day he had just four hours of classes. But because these classes were on a voluntary basis, if you didn’t want to go, no one would force you.

The school of earth magic was divided into six classes, with each class having its own building. New students and first rank magi attended the first grade class, magi of the second rank attended the second grade class, magi of the third rank attended the third grade class…and so on, up until the sixth rank who attended the sixth grade class.

Magi of the sixth rank could choose to graduate at any point in time. But naturally, if they elected not to, they could continue to study.

February 10. Within the classroom of first grade classes.

The classroom for first grade earth magic was extremely large, and was capable of seating hundreds of students. Twenty students had already arrived, and Linley selected a seat located relatively in front, sitting down. At 8:00 AM, there were around fifty students present.

“I expect only part of the students present are new. I wonder how long the others have spent here.” Linley wondered to himself.

After all, for a new student to reach the second rank, usually they would need to train for several years.

“Greetings, everyone.” An amiable, kindly looking brown-haired middle aged man stood in front of the class. “My name is Wendi [Wen’di], and I will be your instructor in earth magic. Today, we have approximately twenty new students. So, same as always, first we are going to have our new students introduce themselves, so that we can all get to know each other.”

Immediately, one new student after another began to introduce themselves.

“My name is Gerhans [Ge’er’han]. I come from the great grasslands to the far east.”

Upon hearing Gerhan’s self-introduction, Linley was shocked. “The students here really do come from all over the Yulan continent. There’s even someone from the great grasslands in the far eastern part of the Yulan continent.”

In the great map of the Yulan continent…

The Holy Union and the Dark Alliance were located west of the Yulan continent’s Mountain Range of Magical Beasts. East of the range were the Four Great Empires, but even further east of the empires was a vast grassland, which contained three kingdoms of its own. The distance between the great grasslands and the Ernst Institute was unbelievably great. A one way trip alone would take at least three years!

“My name is Linley. I’m from the Holy Union.” Linley as well walked to the front of the classroom and gave a basic introduction of himself.

After the self-introductions were complete, the earth-style magus Wendi began to brag about earth-style magic’s power. Only in the second hour of the class did he actually begin to instruct in earth-style magic.

Linley and the group of students just listened quietly. Next to Linley, Doehring Cowart appeared as well.

“This fellow has a very solid foundation. Although he isn’t very strong, in terms of teaching magi of the first rank, not even magi of the eighth or ninth rank would necessarily be a better teacher.” Doehring Cowart nodded as he sighed in praise.

Linley knew a great deal about earth-style magic by now, so listening to the lecture was very easy for him.

“But Grandpa Doehring, although his foundation is solid, he isn’t able to distill the profound into simple words like you. He seems to make it more complicated.” Linley said.

Doehring Cowart laughed self-confidently. Stroking his white beard, he said, “Naturally. A Saint-level Grand Magus’ understanding of magic is far greater than that of a magus of the eighth or ninth rank. The Saint-level is a totally new realm of existence. Naturally, my teachings regarding magic are more profound and point more directly to the underlying nature of magic.”

After listening to this class, Linley made a decision.

“From today forward, I will only attend the earth magic class once a month.” Linley didn’t want to waste his time.

Linley had it all planned out. Every day, he would spend some time outside training in magic. As for the place he would do the training…Linley had already chosen a place, a mountain located right behind the Ernst Institute. Being located near a mountain range, naturally there were many mountains near the Institute.

Four in the afternoon.

Linley was intently listening to the teachings being given in the wind-style magic class.

“Greetings, everyone,” a handsome, yellow-haired youngster said with a smile. “I am a sixth grade student, Trey [Te’lei]. From today onwards, I will be responsible for teaching you wind-style magic. I live in dorm 0298, so if you have any questions after class ends, you can come find me there.”

Sixth grade students, being magi of the sixth rank who could apply for graduation at any time, were fully qualified to teach students of the first or second grades.

“Before this, let’s all first introduce ourselves.” Trey smiled.

This was a basic rule to start off every class for the first time. All of the students gave self-introductions.

“Hey, Linley, have you noticed? There’s lots of cute girls amongst the wind-style students. Check it. That little blonde girl just smiled at you.” Doehring Cowart, next to Linley, pointed as he spoke. “Based on what that little blonde girl just said, her name seems to be Delia [Di’li’ya]. Delia. Such a cute name. Based on my 1300 years of experience, when this little girl grows up, she’ll be a beauty for sure. Linley, smile at her and build a good foundation. That way, in the future, you’ll be able to advance the relationship.”

Right now, Linley was totally ignoring Doehring Cowart.

Linley was focused on the wind-style magus instructor ‘Trey’, and closely listening to Trey’s teachings.

“Wind-style magi are the fastest, most nimble magi in the world. In addition, we are the only magi who can fly before reaching the Saint-level!” Trey’s words and mannerisms all conveyed the love which he felt towards wind magic. “Do you wish to use your own power to fly above the skies? To soar in the air and gaze down upon countless mountains? How wonderful the feeling is, and how many people desire it!”

The eyes of many of the children who were seated below, listening, began to shine.

Fly?

Who wouldn’t want to?

“A Saint-level magus can fly, yes, but the Ernst Institute can perhaps produce just one, at most, in a century! But we magi of the wind-style can, upon attaining the fifth rank, immediately execute the ‘Floating Technique’.” Trey said confidently, “And wind-style magi are extremely fast. When they execute the ‘Supersonic’ technique, they can dramatically increase their speed.”

“But of course, those are just common techniques. The legendary forbidden technique, ‘Annihilating Tempest’, is the most powerful destructive technique of them all. There’s also the legendary forbidden technique, ‘Dimensional Edge’, which is the most powerful one-on-one attacking technique.” Trey’s voice was filled with reverence.

Many of the youths stared wide-eyed.

“Hmph, how can the Annihilating Tempest be considered the most powerful destructive technique? What about my earth-style’s ‘Heaven Collapses, Earth Shatters’ and ‘Heavenly Meteor’s Descent’?” Doehring Cowart, upon hearing these words, was somewhat unhappy.

“Grandpa Doehring, what is this ‘Dimensional Edge’ technique?” Linley asked.

Given that Grandpa Doehring had not mentioned the ‘Dimensional Edge’ spell, Linley believed that perhaps it really was the most powerful one-on-one attacking technique of them all.

“The Dimensional Edge spell? It can slice through the dimensional walls which separates matter itself. Of course it is powerful. But although it is ridiculously strong in one-on-one combat, it’s still only a one shot spell. How can it compare to our earth-style’s ‘World Protector’, which can battle nonstop with the enemy?” Doehring Cowart was quibbling and equivocating.

But Linley could tell.

This Dimensional Edge spell clearly possessed a terrifying power. And most likely, it wasn’t as simple as Grandpa Doehring made it out to be. A one-shot technique? Even a one-shot technique could be enough, if the opponent couldn’t dodge.

“If I can become both a Dragonblood Warrior as well as utilize wind-style magic, then…” Linley’s heart was moved.

And then, he just continued to listen to the class. Linley became more and more intrigued by wind-style magic. Of the four elements of earth, fire, water and wind, each contained profound mysteries which were as deep as the sea. The ocean of magical knowledge was an endless one. And now, Linley had begun to wade into its depths.
