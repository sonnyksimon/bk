#### Book 2, Growing Up, Chapter 14 – The Bros of Dorm 1987 (part 1)

“Whew, I’m exhausted. Linley, how come you are in such good shape?” Reynolds was panting for breath, but Linley didn’t feel anything.

“What, you are tired already?” Linley started to laugh. How short a distance had they just run?

He didn’t even feel too tired after running from Wushan township to the Ernst Institute.

“Hey, just put it down there. Right. Put the box down there. Put it down carefully. If you break it, there’s no way you can afford to compensate for it!” From within dorm 1987, the clear voice of another youth could be heard. Linley and Reynolds glanced at each other, then entered curiously. Immediately upon entering, they saw several muscular men busily moving things about.

A gaudily-dressed youth was standing in the center of the room, directing their moves.

Immediately upon seeing Linley and Reynolds, the young man’s eyes brightened, and he excitedly ran over. “Haha, you guys are my dormmates, right? I’ve waited so long for you guys. Up til now, it’s just been me here. Lemme introduce myself. My name is Yale [Ye’lu], and I suppose I just barely qualify as a member of the Holy Union.”

“What do you mean, you just barely qualify as a member of the Holy Union?” Reynolds mumbled, and then said, “My name is Reynolds. I’m from the O’Brien Empire.”

“My name is Linley. I’m from the Holy Union’s Kingdom of Fenlai.” Linley smiled as well.

As long term dormmates, in the future, they would be together for a long period of time.

“Oh, Reynolds, Linley, I am so happy to see you fellows. Hey, where did my exercise equipment go?” Yale turned his head and stared at his servants.

“Exercise equipment?” Reynolds blinked at Yale. “Yale, what do you have those for? Are you going to be a warrior?”

Yale wrinkled his nose as he chortled. “Although I am a dignified magus, I still need to work out and have a good physique. Otherwise, how will I be able to seduce beautiful women? There’s many beautiful women amongst the ranks of the magi. And the female magi of the Ernst Institute are not only pretty; they are also very classy. Plus, there’s a lot of face to be gained by being able to brag to others that I have an Ernst Institute student as my girlfriend.”

“Uh…” Reynolds was speechless.

Linley didn’t know what to say either. Seeing the exercise equipment, Linley wanted to go work out, but he didn’t expect that these were the tools which Yale planned to use to do bodybuilding to seduce pretty girls.

“I’m eight years old. How about you, Yale?” Reynolds clearly was very open-minded.

Yale was extremely tall. The nine-year old Linley was already 1.5 meters tall, but Yale was half a head taller than even Linley.

“Me? I’m ten. Haha, but I’m not getting any younger. My elder brother lost his virginity at age twelve. I’ve got to do some advance preparations as well.” Yale’s eyes shone.

“What does ‘losing virginity’ mean?” Reynolds looked questioningly at Yale.

“Yeah, what’s ‘losing virginity’?” Linley also looked curiously at Yale.

Staring at his two dormmates, Yale became momentarily speechless as well. Besides Linley, the ghostly form of Doehring Cowart was holding his belly as he laughed uproariously. This made Linley ask him curiously, “Grandpa Doehring, why are you laughing?”

“Young master, we’ve arranged everything.” An extremely muscular man said respectfully.

“Mm. You can leave now. Go back and tell my father that in the future, if there isn’t something urgent, not to bother me. Oh, right. Remember…every year, he can’t forget to transfer money into my magicite card. He should know very well that a magus needs a lot of money for his magistaff and socketable gems.” Yale said loudly and casually.

“Yes, young master.” The man said respectfully.

Yale nodded, satisfied, then dismissed the men with a wave of his hand, as though he were a general.

“Magicrystal card?” Reynolds stared at him in amazement. “The magicrystal card is only offered by the ‘Golden Bank of the Four Empires’, which all four of the great empires established together. I heard that the processing fees for requesting a card totals a hundred gold coins.”

“Right on.” Yale was quite knowledgeable about this. “The minimum starting balance for a magicrystal card is at least a thousand gold coins. But I’m afraid that a thousand coins wouldn’t be enough to even sustain a month’s worth of expenditures for me.”

Linley, upon hearing these words…

“Rich guy.” Linley sighed to himself.

His own father gave him only a hundred gold coins each year for living costs. In fact, in Linley’s eyes, a hundred gold coins was more than enough. After all, most commoners would only make twenty or thirty gold coins in wages after a year of hard labor.

“You really are a rich guy. My dad only gives me two hundred gold coins a year.” Reynolds mumbled. “And he even said that he wants me to spend my time focused on studying magic.”

“Just a hundred for me,” Linley laughed. “But for a simple life, it’s enough.”

“Bah, bros, my money is your money. If you run out, just come find me! In the future, we’ll probably be living together for decades. We’ll be bros for decades. Why quibble about ‘yours’ and ‘mine’?” Yale was extremely expansive, but just as he finished speaking…

Linley and Reynolds both started.

“Decades?” Linley stared at Yale in shock.

Yale said casually and naturally, “Linley, you can only graduate from the Ernst Institute if you reach the rank of a magus of the sixth rank. For a magus, the higher you progress, the harder it becomes. For most people, it takes a couple decades to become a magus of the sixth rank.”

Linley frowned.

Decades? He was going to be a fiscal burden to his father for decades?

“Grandpa Doehring, why didn’t you tell me this?”

Doehring Cowart’s voice rang out in his mind. “Linley, relax. For most people, decades will be needed to reach the sixth rank, yes. Under my tutelage, I can let you become a magus of the sixth rank in just ten years.”

Ten years.

In ten years, Linley would only be nineteen years old. Only now did Linley relax.

“Is everyone here already?” A clear voice rang out, as a child walked into the room. Approximately the same height as Reynolds, this child looked a bit more mature. “Hello, everyone. My name is George [Qiao’zhi]. I’m ten, and I’m from the Yulan Empire.”

Yale, Reynolds, and Linley all gave basic introductions about themselves to the newcomer.

“The Yulan Empire?” Linley was startled.

The Yulan Empire. The most ancient of the empires of the Yulan continent. When the Yulan calendar was first started, ten thousand years ago, the Yulan Empire controlled the entire Yulan continent. And then, as time passed, the Yulan continent began to fall into war, causing the Yulan Empire to fragment as well.

By this era, the Yulan Empire had become just one of the Four Great Empires.

But despite this, the Yulan Empire was still the most economically powerful of the empires, and it was also filled with magi. The magus academy of the Yulan Empire was second only to the Ernst Institute.

“George, the magus academies of the Yulan Empire aren’t that bad. Why did you rush all the way here?” Yale said in amazement.

George smiled. “Although the magus academies of the Yulan Empire are very good, they are still a bit weaker than the Ernst Institute. If you’re going to go to school, you should go to the best. Although the journey was a bit long, it could be considered a form of training as well.”

“George, you are ten? But you look the same as me.” Reynolds said to the side.

George immediately began to laugh.

The eight-year old Reynolds and the ten-year old George were of the same height. Both were the shortest in the group. Linley was half a head taller than them, while Yale was the tallest of them all.

“Enough of that topic. I just found out from the admissions office that every one of the hundred new students have at least high levels in both elemental affinity and spiritual essence. I even discovered guys who have ‘exceptional’ levels in both elemental affinity and spiritual essence. What monsters.” George seemed to have good inside information.

Yale pursed his lips. “That’s very normal. Which student in the Ernst Institute is weak? Myself, my elemental affinity and spiritual essence are both high level, putting me towards the bottom of the pack of our one hundred. If it wasn’t for the fact that my old man has a special relationship with the Radiant Church, I probably wouldn’t even be able to make it in.”

Linley couldn’t help but stare at Yale in shock.

This Yale fellow’s dad surely was something quite amazing, to have a special relationship with the Radiant Church.

“The person in our dorm with the highest natural talent is Linley. But have you guys heard of the unmatchable talent who is studying at the Ernst Institute?” Yale glanced at the other three.

Linley and Reynolds both shook their heads.

But George smiled as he nodded. “I’ve heard of him. The number one genius of the Ernst Institute, ‘Dixie’ [Di’ke’xi], a talent that appears once in a century. He is a dual-element magus, and has exceptional levels of elemental affinity and spiritual essence. But his spiritual essence is especially amazing; 62 times that of others his age. Usually, reaching 30 times is considered ‘exceptional’ level, so his precise level should be ‘super exceptional’, but since the highest level is ‘exceptional’, that’s what he is classified as.”

Linley understood.

Dual-element. Exceptional elemental affinity and spiritual essence.

“I’m just ten-something times that of other people my age, but that genius has 68 times the spiritual essence of people his age.” Linley sighed in amazement.

The Ernst Institute really did have as many talents as there were clouds in the sky. It could also be said to have congregated all of the magical geniuses of the Yulan continent. Here, Linley could only be considered above average. However…behind Linley, there was a five-thousand year old Saint-level Grand Magus!
