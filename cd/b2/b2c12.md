#### Book 2, Growing Up, Chapter 12 – Instructions

Time flowed on, and in the blink of an eye, months had passed. Many new trees had begun to sprout on Wushan township, filling the area with a feeling of spring.

Beneath a pine tree.

Linley was seated cross-legged in a meditative trance, generating mageforce.

After having entered the meditative trance, Linley could clearly sense large amounts of earthen specks of light and green specks of light. These countless specks of lights continuously swirled into his body, and through his limbs and his bones, were purified and stored within the central dantian in his chest.

Within his central dantian, there was a smoky earthen mist intermingled with a smoky green mist.

The earthen mist was his earth element mageforce, while the green mist was his wind element mageforce.

“Whew.” Slowly releasing a breath, Linley exited his meditative trance.

Doehring Cowart, wearing a moon-white robe, was seated cross-legged next to him, a smile on his face as he enjoyed the surrounding scenery. Seeing Linley awaken, he laughed. “Linley, tomorrow you are heading to the Ernst Institute, yet you are still hard at work today?”

Linley’s lips curved up in a smile. “Grandpa Doehring, I believe you were the one who said that strong combatants must work hard every single day, and not relax for even an instant. Only long term training will produce astonishing power.”

“Little punk, so now you are going to give me instructions?” Doehring Cowart laughed while ‘grumbling’.

“Hehe,” Linley chortled.

“Woosh!” A black shadow from far away came flashing towards them, appearing on Linley’s shoulders in the blink of an eye. It was the Shadowmouse, ‘Bebe’. Young Bebe leaned towards Linley, making a chewing motion with his mouth, while pointing at a nearby dead hare.

Just from the look on Bebe’s face, Linley knew what was up.

“You want me to cook it?” Linley laughed as he spoke.

Bebe nodded repeatedly.

“Linley.” The nearby Doehring Cowart mentally spoke to him. “This little Shadowmouse is really quite strange. It’s been months, but judging from his size, it’s almost as though he hasn’t grown at all. For an infant Shadowmouse, the early childhood growth rate should be quite noticeable.”

“I have no idea either.” Linley shook his head.

Although Shadowmouse ‘Bebe’ did not increase in size, his speed was improving quite remarkably.

“It really is bizarre.” Doehring Cowart looked at Bebe. Right now, Bebe didn’t have any idea that a spirit was mentally weighing him.

“It’s getting late. I’ll need to start warrior training soon.” Linley stood up and grabbed the dead hare as he began heading down the mountain. Doehring Cowart flew by his side, unhappily saying, “Linley, in the future, you will be a magus. Why are you still engaging in warrior training?”

Linley laughed, “Grandpa Doehring, I’ve discovered that warrior training can increase my endurance, and with increased endurance, my spiritual essence can increase as well.”

“I know that, of course.” Doehring Cowart said, dissatisfied. “But how could those basic training methods compare to the meditative trance in turns of how fast one’s spiritual essence increases?”

Linley shut his mouth and no longer spoke.

While it was true that fighter training allowed one to improve one’s spiritual essence, that wasn’t the real reason.

The real reason that Linley continued his fighter training was this. “In the future, if I have the chance to drink fresh dragon’s blood, I will be able to practice according to the Secret Dragonblood Training Tome. I have to keep up my physical training. The body is like a vessel, while battle-qi is like wine. The body is extremely important. The earlier I begin building my fundamentals, the faster my improvement will be when I study the Secret Dragonblood Training Tome in the future.”

Actually, based on Linley’s affinity for elemental essences, each day, he didn’t have to spend too much time or effort to gather and generate mageforce.

Most of his time was spent in the meditative trance, training his spiritual essence.

But spending significant amounts of time training spiritual essence was exhausting. Warrior training served as a form of rest and alternative exercise.

……

The next morning, all of the commoners of Wushan township gathered on the main road in town, all for the purpose of sending off Linley. It was definitely an incredibly glorious thing for Wushan township to be able to produce a magus who would attend the Ernst Institute.

Each year, the Ernst Institute only accepted a hundred students from across the entire Yulan continent.

At the moment, Linley was still within the Baruch clan manor, while Hillman and the others were all outside. The only people within the manor were Hogg, Linley, little Wharton, and Housekeeper Hiri.

“Linley, today you are going to go to the Ernst Institute and formally become an Ernst Institute student. When you graduate from the Ernst Institute, you will be a powerful magus! Before you depart, as your father, I want to say to you…” On this last day, Hogg had a belly full of things he wanted to speak to Linley.

But after pausing for a long time, Hogg only said a few simple sentences. “Linley, remember the ardent desire that the elders of the Baruch clan have held for centuries, and remember the humiliation of the Baruch clan!”

Hogg’s face was turning slightly green.

“When you graduate, you will be at least a magus of the sixth rank. If you work hard and train hard, it won’t be too hard to become a magus of the seventh rank. And in addition, you are a dual-element magus! A dual-element magus of the seventh rank would definitely be a major force in the Kingdom of Fenlai. In the future, you will definitely be capable of retrieving our clan’s ancestral heirloom. If you do not, even if I die, I will not forgive you!” Hogg fixed a deathly stare on Linley.

“Even if I die, I will not forgive you!”

These words made Linley’s heart tremble.

These were the instructions his father gave to him upon their parting.

“Father, don’t worry. So long as I live, I will ensure that the ancestral heirloom of our Baruch clan is restored to us. I so swear!” Linley promised, meeting his father’s steely gaze, his own eyes filled with resolve as well.

Hogg’s eyes began to shine, and he patted Linley on the shoulder with a mighty clap.

“I believe in you, son!”

….

On the road headed east of Wushan township, Linley turned his head saw the hundreds of familiar faces which had come to send him off, with his father, Hogg, and his younger brother Wharton standing in the lead.

“Big brother, bye bye!” Little Wharton waved mightily.

Seeing his father and his younger brother, Linley also waved, his eyes turning red.

“Father. Wharton.” Linley’s heart was filled with longing.

Ever since he was born, Linley had never left home for an extended period of time, but this time, he would be gone for extremely long. At this moment, the little Shadowmouse, ‘Bebe’, was obediently perched on Linley’s shoulders, not making a sound, as if he sensed Linley’s thoughts. The nearby Doehring Cowart, in spirit form, also looked encouragingly at Linley.

“Linley, let’s go.” Hillman said. Hillman was escorting Linley to the Institute, acting as his bodyguard in the event they met with any bandits.

Linley unwillingly took one last glance at his family, and then finally forced himself to turn away and begin traveling in the direction of the Ernst Institute.

“Farewell, my family. Farewell, my home.”

Yulan calendar, year 9991. The nine year old Linley, accompanied by the young Shadowmouse, ‘Bebe’, and the Baruch clan’s guard captain, Hillman, departed from Wushan township.
