#### Book 2, Growing Up, Chapter 8 – The Magical Aptitude Test (part 1)

Under the guidance of the church official, all of the people in the square were walked into the main hall of the cathedral.

Within the cathedral.

The great hall of the cathedral had a floor paved with marble, and hanging above was a massive crystal chandelier. It could easily fit the hundreds of people who entered yet still feel spacious.

In the very front of the great hall, there were a line of chairs, seated upon which were representatives and recruiters of each of the great magus academies. Directly in the middle of the great hall was the testing location.

The black robed church official smiled and said in a clear voice, “The testing location is right in the center. All testees, please come one at a time. No one else can enter the circle in the center. All testees, please get in line. Family and friends, please step to one side.”

“Linley, here is the examination fee. Here is your proof of identification. Go quickly. Oh, and right, let the little Shadowmouse stay with me. It will be difficult to have the little Shadowmouse with you as you take the test.” Hillman said.

“Bebe, stick with Uncle Hillman for now. I’m going to take the test.” Linley mentally instructed the little Shadowmouse, who somewhat unwillingly shuffled around a bit under Linley’s clothes. But after multiple requests from Linley, the little Shadowmouse directly scurried into Hillman’s clothes.

Linley then accepted the ten gold coins and headed towards the line. The youths there ranged in age from six or seven years old to seventeen years old. These children organized themselves into two long lines, while the cathedral pursers collected the fees from each of them.

The central circle was ten or so meters wide, and there were three adults within it. Two of them were responsible for administering the test, while one was responsible for recording the results. The testing equipment consisted of a crystal sphere and and a complicated, six-sided magical formation.

“First.”

The bald old man pointed at the crystal ball and said, “Place your hand atop the crystal ball. We will test your elemental essence affinities.”

The first testee was a twelve or thirteen year old young man. That young man nervously placed his right hand atop the crystal ball. Immediately, the entire crystal ball began to emanate a hazy, light red glow, with the occasional hint of green mixed in.

The bald elder glanced at the scrap of paper in his hands, and emotionlessly said, “Age, twelve. Elemental essence affinities – Fire, average affinity. Wind, low affinity.

“Now, step into the magical formation. Time to test your spiritual essence. Remember, stand there. Don’t kneel or fall down. Let’s see how long you can take it.” The bald elder remained as cold as ever. The young man nodded, then stepped into the six-cornered magical formation. A holy white aura immediately emanated from the bald elder, which shot into the middle of the magical formation.

Light-style elemental magic – Overawe!

“Looks like the testing procedures in this era is the same as it was in the past.” Doehring Cowart flew out of the ring and appeared next to Linley.

“Grandpa Doehring.” Seeing Doehring Cowart, Linley felt himself calm down.

“In the magical aptitude test, the elemental essence affinity test is secondary. The spiritual essence test is the main one. After half a year of meditation, your spiritual essence should be sixteen or seventeen times that of most people your age.” Doehring Cowart chuckled at Linley. “For you, this test will be extremely easy.”

In a short period of time, the youth in the middle of the magical formation could no longer hold on.

“Spiritual essence, two times stronger than the average person of the same age. Not qualified to become a magus.” The bald elder coldly announced as the magical formation deactivated, and the youngster quietly departed.

A burst of noise from nearby.

“Silence.” The bald elder coldly said, and immediately a large group of nobles no longer dared to speak. “Next.”

Doehring Cowart watched with interest from the side.

One after another youngster was tested. Of the first ten, none met the requirements. Right now, there was a young lady in the magical formation, who had been able to hold out for longer than any of the ten before her.

“Hrm?” The bald elder’s eyes shone, and he immediately increased the power of the magical formation.

After a long period of time, the young lady finally dropped down to one knee.

The bald elder nodded in a satisfied manner. A hint of a smile on his face, he said, “Spiritual essence, eight times stronger than most people your age. The minimum qualifications for becoming a magus have been met. You also possess average elemental essence affinity. You can become a magus!” The judgment of the bald elder had just determined the fate of this young woman.

“Oh, how wonderful!” The first person to shout with joy was not the young woman. Rather, it was the young woman’s father, a bald, middle aged, gentlemanly looking person.

“Quiet!” The bald elder snapped in a cold, unhappy voice.

Immediately, the ushers came and escorted the girl and her father to where the line of magus academies recruiters sat.

Many envious eyes were cast towards the young woman.

As time went on, the people in the main hall grew more and more numerous. The magical testing event would go on for seven days, so most people didn’t see the urge to come right away at the beginning. When Linley’s turn came, the line of test-takers had already stretched out the main doors of the cathedral.

“Next.” The bald elder said again.

Linley calmly walked into the center, with Doehring Cowart remaining by his side. In Doehring Cowart’s eyes, only a Saint-level combatant could, just barely, detect his presence. These ordinary magi definitely couldn’t detect him.

Linley placed his right hand on the crystal ball.

Instantly!

The crystal ball suddenly burst forth with light, as though it were the sun! Earthen rays of light intersected with green rays of light, and there were even some thin lines of red spaced in between. That eye-piercing brightness forced even the people nearby to squint their eyes.

Seeing the sun-like brightness emanating forth from the crystal globe, everyone in the great hall was stunned.

The bald elder quivered as he stared at the piece of paper in his hands. It was written clearly on top that Linley was eight years old.

“Age, eight. Elemental essence affinities – Earth and Wind, affinity level of exceptional for both! Fire affinity, average.” That bald elder felt his heart thumping wildly. Most magi had average elemental essence affinity. Even high elemental essence affinities were quite rare, and as for exceptional affinity…exceptional affinity was ridiculously rare!

By way of explaining, an ordinary magus might take ten hours to produce a certain amount of mageforce, but Linley would only require a single hour to get the same result.

“Ooooooo.”

The entire hall was shocked. Not only was the kid’s elemental essence affinity of the exceptional level, it was for two different elements! This was simply too terrifying.

“Exceptional affinity for the wind element?” The nearby Doehring Cowart was shocked.

“Whoah, I, I have affinity for the wind-style as well?” Linley was stunned. He couldn’t help but turn to look at Doehring Cowart.

Doehring Cowart squeezed out a smile. “Linley, I did tell you early on that I could only test for the earth elemental essence affinity. Right. When you absorbed natural elemental essence, did you never sense any wind essence?”

“Wind elemental essence?” Linley was stunned. “The first time you taught me to process elemental essence, you told me to not be distracted, so although I did notice some green-colored specks of light around me, I didn’t pay any attention to them. But later on, when I began to absorb earth elemental essence, I would be surrounded by earth essence and the green specks would no longer appear.

Doehring Cowart now understood.

When training mageforce, especially dual-element mageforce, if one only focused on training one element such as earth, all the nearby earth elemental essence would be drawn near while all other essences, including wind, were pushed aside.

“Afterwards, whenever I trained, I only sensed earth elemental essence nearby. I didn’t think about those green specks of light.” Linley was feeling extremely happy as well.

Because he knew how powerful a dual-element magus was; far more powerful than a single-element magus.

After the elemental essence affinity test, the spiritual essence test!
