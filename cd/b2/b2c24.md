#### Book 2, Growing Up, Chapter 24 – The Straight Chisel School

Many days later, at the Ernst Institute.

It was morning. Linley had eaten breakfast, and was now headed to the back mountains, preparing to begin training.

While walking on the road out of the Institute, the little Shadowmouse was on Linley’s shoulders, scanning about in all directions. There were quite a few people at the Ernst Institute who had magical beast companions, and thus no one cared at all that Linley had a little Shadowmouse as a companion. But just at that moment…

“That guy is Linley, the number one magus amongst us first graders.” A clear voice rang out from not too far up ahead.

Linley couldn’t help but stare at the direction of the voice, and saw two cute girls chatting to each other while staring at him. When Linley glanced at them, the two girls began to titter in a quiet voice.

“I’ve become famous.” Linley mocked himself.

Over the past few days, he would often run into people discussing him. Since he had defeated Rand, the victor of the first grade tournament, everyone had tacitly agreed that he was the number one expert amongst first graders.

“Oh, in front is?” Linley suddenly saw a slender, small frame up ahead.

Short golden hair, with a body as slender as that of Reynolds. A cold aura emanated from him as he calmly walked along the road.

“Dixie?” Linley’s pupils contracted.

Dixie was nine years old as well, and in fact was actually a month younger than Linley. But this nine year old child had already become a magus of the third rank. Although it became harder and harder to progress in the higher ranks, a nine-year old magus of the third rank was still very astonishing.

“It’s Dixie. I heard that yesterday at the annual magus assessment test, Dixie showed that he had already reached the requirements for the fourth rank.” A number of seventeen and eighteen year old girls said from the side.

Most of the students in the third grade were more than sixteen years old, with only the genius Dixie as a clear exception!

“A magus of the fourth rank!”

Linley felt his heart violently shudder. They were both nine years old, and Dixie was even a month younger than him. But he had already become a magus of the fourth rank, while Linley was only of the second rank.

Demeanor as cold as ice, Dixie walked past Linley.

The absolute genius, Dixie. No one his age could come close to matching him.

A white line shone out of the Coiling Dragon Ring, and Doehring Cowart appeared besides Linley, smiling. “Linley, there actually isn’t a huge difference between you two. When Dixie enrolled, his spiritual essence was 68 times that of his peers. This means that even before training, his spiritual essence had reached the level of a magus of the third rank. That’s why in his first year, all he had to do was accumulate sufficient mageforce for him to become a magus of the third rank. By now, he’s been at the Ernst Institute for almost two more years, so it is very normal for him to become a magus of the fourth rank.”

Linley understood this in his heart.

This person simply had too much natural talent. He was born with tremendous spiritual essence, and he had exceptional elemental affinity as well. Clearly, he must have accumulated mageforce very quickly as well.

“Although his training speed right now is fast, I expect him to need another three or four years to advance from the fourth rank to the fifth rank. And to go from the fifth rank to the sixth rank, he will need four or five years.”

“Right now, you are a magus of the second rank, while he is of the fourth rank. But I am confident that in ten years, you will catch up to him.” Doehring Cowart said confidently.

But Linley didn’t believe it.

“Grandpa Doehring, the more natural talent one has, the faster one will progress. He has much more talent than I do, and holds two more ranks than I do. How could I possibly catch up to him in ten short years?” Linley was no fool. His studies at the Ernst Institute had made him aware of how difficult it was for a magus to advance a rank.

In the past, Doehring Cowart had told Linley that he would become a magus of the sixth rank in ten years, but Linley had always had reservations about that claim. After all, to date, his rate of improvement was clearly insufficient.

As he said these words, Linley had already left the gates of the Ernst Institute and entered the back mountains. As he passed through the mountain forests, Doehring Cowart suddenly said, “Linley, go to a place next to the mountainside.”

“Next to a mountainside?” Linley was confused.

“Don’t ask too many questions. When you arrive, I’ll explain.” Doehring Cowart laughed.

Most of the back mountain was covered with wild grass and many different large trees. But after a while, Linley found a place that satisfied Doehring Cowart’s requirements. The place was a mountain peak that rose hundreds of meters into the air. At the base of the peak, Linley stood.

“Grandpa Doehring, what do you want me to do here?” Linley said questioningly.

Laughing, Doehring Cowart said, “Linley, do you disbelieve my claim that I can let you reach his level in ten short years? Haha…Linley, as a mighty Saint-level Grand Magus, I, in fact, am in possession of a method to improve one’s spiritual essence.”

“A method to improve one’s spiritual essence? Isn’t the meditative trance enough for that?” Linley stared at Doehring Cowart questioningly.

Doehring Cowart smiled calmly. “Linley, I will admit that the meditative trance has very good results. But after meditating, one will feel extremely tired.”

“Of course I would feel tired. The meditative trance involves me using my spiritual essence non-stop. After totally exhausting my spiritual essence, I would then allow it to recover. It’d be strange if it wasn’t exhausting.” Linley frowned.

Doehring Cowart proudly said, “But my method is different. It doesn’t cost spiritual essence at all. In fact, it is a form of entertainment.”

“Entertainment?” Linley was dazed.

“Right. This form of entertainment is – stonesculpting!” A prideful look appeared on Doehring Cowart’s face.

“Stonesculpting?” Linley said, astonished. “Like the sculptures in the Proulx Gallery?”

Doehring Cowart smiled and said, “Right. When others sculpt stone, they will exert a lot of energy and exhaust themselves. But my stonesculpting method is different. Although it is also tiring when you first begin to train in it, towards the end, it will have extremely good results.”

“Are you serious?” Linley couldn’t quite believe it.

Doehring Cowart stared at him. “Linley, you don’t believe me? As a venerable Saint-level Grand Magus of the Pouant Empire, in the past, there were several sculptures I made which nobles offered a million gold coins to purchase. But how could I, a Saint-level Grand Magus, be willing to give the sculptures which I was the most proud of to others?”

“You were that good? How come I’ve never heard of your name amongst the other grandmaster sculptors, then, Grandpa Doehring?” Linley said suspiciously.

Doehring Cowart said awkwardly, “Well, I hid all of my works in an underground vault which no one knew about. After five thousand years, I’m no longer even sure where it is located.” Five thousand years is enough for a sea to turn into farmland. The entire Pouant Empire had been eliminated. Who knew where the vault was now?

“Oh ho, so no one’s ever heard of you?” Linley began to chortle.

“You don’t believe me?” Doehring Cowart stared at him. “Back in the day, when Proulx was just a young kid, he came to me and earnestly begged me to allow him to view my sculptures. After analyzing my sculptures, that kid Proulx had a mental breakthrough which in the end allowed him to become a grandmaster sculptor. As a matter of fact, he can even be considered a student of mine.”

Linley was stunned.

“Proulx?” Linley was truly terrified now.

Proulx, the man who had been acclaimed throughout the ages at the finest sculptor in history, could be considered a student of Doehring Cowart.

“Of course, if one can describe Proulx’s works as being in pursuit of perfection, my works are in pursuit of a different extreme. I named my sculpting method the ‘Straight Chisel School’. The Straight Chisel School is totally different from all other sculpting methods. It pursues a totally different extreme. This method, in the beginning, is very exhausting, but as one masters it, you will realize its true fruits.” A look of absolute confidence was on Doehring Cowart’s face.

Glancing at Linley, a smile appeared on Doehring Cowart’s face. “ But of course, in the past, I was the only member of the Straight Chisel School. From today forward, you will be a second member.”

In his heart, Linley had total confidence in Grandpa Doehring, so of course he had decided to study sculpting with him.

And what’s more…

If Grandpa Doehring’s words were true, and he could grow stronger while also becoming a master sculptor, just based on his sculpting skills alone, he would be able to support his little brother’s tuition.

“Written, recorded history goes back only a few tens of thousands of years at most. In the long ages before then, before the writing system had even been invented, stonesculpting had already existed.” Doehring Cowart said with a sigh. “Hundreds of thousands of years, or even millions of years ago, our ancestors would record their memories and their visions in sculptures. This is the most ancient method of recording culture and history.”

Linley nodded as well.

There was no form of culture at all which was older than stonesculpting.

“Throughout the ages, sculpting has always been very hard to do. And creating a sculpture with a unique aura is even harder. The harder something is to do, the more valuable a success would be.” Doehring Cowart sighed emotionally.

Linley agreed in his heart.

If you wanted to paint a single stroke, you could easily do so. But if you wanted to carve out a paint-stroke, it would be extremely difficult, because stone is too unyielding.

“A stone’s appearance, quality, grains, and coloration impact not only its appearance, but its entire potential and true form. We use chisels to remove the excess parts and allow its natural beauty to be revealed. This is stonesculpting.”

“The stonesculpting way is really a way of controlling space and appearance. When stonesculpting, one must carve from the outside to the inside, one step at a time, slowly drawing out a ‘form’ from within. And then, slowly, one would remove the excess parts, allowing the form to become more and more clear. This will allow the sculptor to naturally feel as though his work of art is ‘evolving’ beautifully.

….

Once he started, Doehring Cowart couldn’t stop talking about carving.

But Linley could clearly tell how much Doehring Cowart revered this art.

“Most stonesculpting methods use many tools, such as the butterfly chisel, a straight chisel, a skew chisel, a triangular chisel, a jade bowl knife, hammers, saws, and more. The reason there are so many tools is because stone is very firm and hard. Thus, they will use a butterfly chisel to draw the form, the straight chisel for the initial cuts, the triangular chisel….”

Listening to him speak, Linley began to understand more about the basics of stonesculpting.

Doehring Cowart suddenly laughed. “But my stonesculpting method is totally different from that of others. This is because my stonesculpting method uses only a single tool – the straight chisel! This is why I have named my sculpting method, the ‘Straight Chisel School’!”

“How is that possible? You carve just using a straight chisel?” Linley immediately argued. “You just said yourself that more tools are needed. For example, the scales of a fish. How would you use a straight chisel to carve that? Isn’t that totally impossible?”

“Wrong. Although others cannot, we earth-style magi can!”

Doehring Cowart said confidently, “Earth-style magi can totally sense the entirety of a rock’s form. With sufficient wrist strength, we can sculpt stone using just a straight chisel. But of course, the ‘Straight Chisel School’ is not a simple one to enter. Today, your mission is to go purchase a sufficiently sharp straight chisel. From today onwards, every day, I will spend three hours guiding you in learning how to sculpt stone.”
