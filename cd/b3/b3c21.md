#### Book 3, Mountain Range of Magical Beasts, Chapter 21 – The Foggy Gulch (part 1)

If one desired to train using the ‘Secret Dragonblood Manual’, one must rouse up the Dragonblood in one’s veins. But there were only two possible ways by which one could agitate the Dragonblood in one’s veins. The first was to reach a certain minimum level of Dragonblood density in one’s veins. The second was to drink fresh blood from a live dragon.

But drinking blood from a living dragon was very dangerous.

Dragon’s blood, even when applied topically, would cause terrible pain, to say nothing of drinking it. However, everything in the world had its equal opposite. Blueheart Grass, when paired with dragon’s blood, made for an extremely potent mixture. But Blueheart Grass was extremely rare. Linley had previously asked about the price.

A single patch of Blueheart Grass was worth tens of thousands of gold coins. What’s more, it was a rare item that often couldn’t be bought even if one had the money. Doehring Cowart had once said: “Live dragon’s blood is incredibly powerful. Usually, a single patch of Blueheart Grass is insufficient. If you are going to drink a large amount of live dragon’s blood, you will need even more Blueheart Grass.”

A single patch of Blueheart Grass was already that expensive. How could Linley afford it? Perhaps his entire fortune of 70,000, acquired over this month, would only be enough to buy a single patch.

“Blueheart Grass, Blueheart Grass! Heaven is being so kind to me.” Linley felt unspeakable joy.

Linley energetically leapt down directly, falling several dozen meters before landing against the cliff on the other side. And then, he immediately began to mumble the words to a spell. In a short time, Linley’s entire body was surrounded by flowing air elemental essences, and flows of air began to surround him as well.

Wind-style spell of the fifth rank – Floating Technique.

At his current level, Linley was only able to allow his body to float, rather than actually fly. Floating meant allowing himself to float up or down vertically. Taking a step forward, Linley stood in mid-air before slowly beginning to float down, gradually descending into the deep, foggy canyon. Bebe enviously stood on Linley’s shoulder as they descended. Although Bebe was rather powerful, he wasn’t capable of flight. He wasn’t a flying-type magical beast, thus he would only be able to fly upon becoming a Saint-level magical beast.

This canyon was filled with the white fog, which roiled about. The deeper Linley went, the greater the distance between the canyon walls became. Quite soon, Linley landed near the Blueheart Grass.

“Blueheart Grass is deep green in color, but emanates a faint blue light. Blueheart Grass is cool to the touch. When the blades of grass are torn apart, they will leak out a dark green fluid which is very cool when drunk.” Linley remembered quite well this explanation in the Ernst Institute library about Blueheart Grass.

Staring at the Blueheart Grass growing out from the cliff, rustling gently in the wind, Linley took a deep breath, then carefully uprooted the Blueheart Grass.

“It really is cold.” When he touched the Blueheart Grass, he felt as though he had touched a piece of ice. He immediately placed the Blueheart Grass into his backpack, and then looked all around. “I wonder if there is any more Blueheart Grass here!”

A place which was capable of giving birth to one patch of Blueheart Grass was capable of giving birth to a second.

Using the Floating Technique, Linley continued to drift downwards into the roiling white fog. At the same time, Linley kept a close eye out, despite the fog making everything blurry. He could make out countless vines twisting about the cliffs.

“That’s huge!”

The farther down into the canyon he went, the more Linley realized how enormous this place truly was. At the top of the canyon, the distance between the two walls was perhaps only a few hundred meters, but by now, Linley was certain that the distance was absolutely at least several thousand meters. He continued to float close to the wall. Using his vision, his flotation speed, and his angle against the wall, he was able to guesstimate this distance.

“Roar…”

“Grrr….”

All sorts of low-pitched growls emanated from below, occasionally sounding out. They came from all over the place. Just judging from the sound alone, there had to be over a hundred magical beasts below. Linley couldn’t help but feel his heart quail. “Magical beasts. There are many magical beasts below!” Just from hearing the sound, Linley could tell.

Linley fixed himself against the cliff walls while gripping onto the vines with his hands as he descended more slowly and more carefully.

“Boss, I can sense great danger below.” Bebe suddenly said to Linley through their mental link.

Linley also felt as though his heart was tightening. The further down he went, the clearer the growls of the magical beasts became. Those low growls were powerful. Clearly, they were coming from magical beasts of large size. Generally speaking, large magical beasts were not weak. Powerful magical beasts weren’t necessarily large, but large magical beasts were generally powerful.

“Blueheart Grass!”

Linley suddenly saw that directly below him, far away, was another patch of Blueheart Grass. Surrounding the Blueheart Grass was many green vines and shrubs. As Linley was not a fearful person to begin with, upon seeing the Blueheart Grass, Linley began float down while keeping his hands gripped to the rattan vines.

But at this point in time, Linley totally failed to notice…

Coiled up amidst the green vines surrounding the Blueheart Grass was a giant green python snake, at least twenty meters long and thick enough that it would take two men to put their arms around it. That giant python was very green and also coiled up like a rattan vine. Given that it was also covered slightly by the fog, Linley didn’t notice that it was there at all.

As he descended, Linley drew nearer and nearer to the Blueheart Grass.

“Boss, careful! That’s a monstrous python!” Bebe suddenly, urgently said to Linley through their link.

“Python?” Linley was startled.

Virtually all python-type magical beasts were exceedingly powerful. Even the weakest Trihorn Python was a magical beast of the sixth rank. Linley immediately surveyed his surroundings carefully. By now, Linley was roughly around a hundred meters away from the giant python. After carefully searching for it, he quickly located the giant python.

“Whoah.” Linley sucked in a deep breath.

That thirty-meter long python, as thick as a water barrel, made Linley’s heart quail. “Green Tattooed Python. A magical beast of the seventh rank – the Green Tattooed Python.” The information he knew about this type of Python immediately sprang to mind.

By now, Linley also realized why it was that this canyon had so much white fog.

“The Mist Technique is just a water-style technique of the first rank. A single Green Tattooed Python, a magical beast of the seventh rank, can generate enormous, almost unlimited amounts of white mist in its surroundings. With this canyon having so much mist of such density, there’s definitely more than one Green Tattooed Python here.”

Linley immediately came to this realization.

The canyon had a depth and width of around ten kilometers long. For such a huge canyon to be totally covered in white mist, one could only imagine how many Pythons were here. That Green Tattooed Python which lay hidden amidst the vines suddenly moved. Its enormous head turned to stare at Linley, and its two cold eyes stared death at him.

“Grrrr….”

A terrifying sound rumbled out from the Green Tattooed Python’s maw, and at the same time, it shot forward at high speed.

“Rawr!” “Hiss!” “Grrr!” The entire canyon began to fill up with the calls of various beasts. At the same time, loud, sonorous movement sounds could be heard. Glancing below, Linley saw that over ten enormous creatures were moving towards him. And, Linley could tell that these ten made up just a tiny fraction of the creatures in this gorge.

“Flee!”

Faced with the attack by the Green Tattooed Python, Linley immediately began floating up at maximum speed. Controlling the force of the wind, he was able to make the flotation pressure exceed his body weight, causing him to rocket upwards at an astonishing speed. While flying upwards, Linley could already see a monstrously large Green Tattooed Python crawl up after him along the cliff walls. Its cold, serpentine eyes stared at Linley, promising death while the serpent itself hissed nonstop.

“Screech! Screech!”

A high-pierced bird cry split the air, and from below, dozens of giant birds suddenly charged forward in pursuit of Linley.

“Dragonhawks! Those are Dragonhawks!” Linley’s face immediately turned paper white.
