#### Book 3, The Mountain Range of Magical Beasts – Chapter 22, The Foggy Gulch (part 2)

Over ten Dragonhawks, each larger than a Griffon, were flying in fast pursuit of Linley. Through the Coiling Dragon ring, Linley immediately expended his mageforce to make himself rise even faster, while at the same time beginning to mumble the words to the Earthguard spell.

“Whoosh!”

Only the roaring wind could be heard. Linley had long since left the Green Tattooed Python behind, but the Dragonhawks flew at an amazing speed, and were drawing closer and closer to Linley. Even after Linley flew out of the canyon, those ten Dragonhawks continued in hot pursuit of Linley, following him outside.

Running at his maximum speed, Linley made his way through the forest as quickly as possible, but no matter how fast his legs were, how could he compare with the speed of the winged Dragonhawks?

“Screeeech!” The Dragonhawks issued piercing cries.

The wingspan of the Dragonhawks, at maximum extension, was over twenty meters long. These ten-plus Dragonhawks blotted out the sky as they all flew directly at Linley. Linley felt as though the entire world was growing dark. As the Dragonhawks descended upon Linley, they all opened their beaks and belched forth plumes of flame at him, immediately turning the surrounding trees into blazing pyres.

Fortunately, the Earthguard armor which Linley summoned continued to protect him, covering his entire body.

“Crackle, crackle.” The fires roared and blazed against the Earthguard armor. Earth-colored elemental essence swirled all about Linley.

Amongst the dragon-type creatures, Dragonhawks and Landwyrms were the weakest of their kin, but even they, the weakest of dragon-type creatures, were magical beasts of the sixth rank. What’s more, Landwyrms and Dragonhawks were pack-type beasts. Faced with an aerial assault from over ten magical beasts of the sixth rank, even a warrior of the seventh rank would flee.

The Dragonhawks charged forward, descending upon Linley….

“Smash!” A Dragonhawk’s sharp talons smote Linley’s Earthguard armor a mighty blow. The Earthguard armor shuddered visibly, and specks of golden light began to gently flicker on top of it.

“I can’t take those hits head on!”

That clawed attack terrified Linley. At the highest speed he could muster, he scurried deeper into the forest, charging into the densest, hardest-to-traverse area. Jumping, leaping, crawling…Linley went all out in his attempt to flee. But those Dragonhawk’s continued to strike viciously at Linley’s head with their vicious claws.

“Hissss!”

Bebe let out a fierce screech of his own, and then he rose on his hind legs, suddenly transforming in size from twenty centimeters to half a meter tall. But compared to the Dragonhawks, with their 20-meter long wingspans, Bebe was still just a small speck.

“Swish!” Bebe leapt off of Linley’s shoulders, transforming into a black blur as he shot directly towards the closest attacking Dragonhawk.

The terrifying sound of bones splintering could suddenly be heard, along with the agonized cries of the Dragonhawk. That Dragonhawk directly fell from the sky, but before it did, Bebe used it as a launchpad to leap at the next closest Dragonhawk. With two vicious bites, he directly bit this one to death as well.

Dragonhawks were just beasts of the sixth rank, while Bebe was able to force a magical beast of the seventh rank, a Velocidragon, to flee in defeat.

What’s more…

There was a huge gap in difficulty to advance as well as in power from the sixth rank to the seventh rank. Bebe wasn’t capable of flight, but once he got into physical contact with a Dragonhawk, it was as good as dead. In a few short moments, three of the ten-plus Dragonhawks were dead.

The other Dragonhawks all flew higher in terror. Seeing them fly higher, there was nothing that Bebe could do either, as he himself could not fly.

Those Dragonhawks hovered around Linley for a while, before finally letting out a few mournful cries as they began flying back towards the canyon.

“What a terrifying gorge.” Only now did Linley finally let out a sigh.

While collecting the magicite cores of the three dead Dragonhawks, Linley pondered the question of the Foggy Gorge.

“Grandpa Doehring.” Linley suddenly called out, and Doehring Cowart flew out of the Coiling Dragon ring. Still appearing to wear a pristine, moon-white robe, Doehring Cowart smiled as he spoke to Linley. “Linley, is there something you need?”

Linley had not yet calmed down.

“Grandpa Doehring, just now, I entered a foggy gorge. I didn’t expect the place to be brimming with magical beasts. There was a Green Tattooed Python there, and huge crawling creatures. I didn’t get a good look at them, but in terms of size, they definitely were not any smaller than a Velocidragon. There were Dragonhawks there as well…and I could tell that this was in just a small portion of the gorge. I have no idea how large the entirety of the Foggy Gorge was.”

Thinking back, Linley felt a surge of fear again. He had actually stumbled into such a gathering spot for magical beasts in that gorge.

“Oh?”

Doehring Cowart seemed rather surprised. “This Foggy Gorge had so many magical beasts? Interesting. Generally speaking, only magical beasts of the same type will gather together, but the magical beasts you just mentioned were all of different types. They actually all gathered together in this Foggy Gorge? Interesting. How interesting. If I were still alive, I would most likely go inside and take a look myself.”

Linley shook his head helplessly and laughed, “That gorge even contained Blueheart Grass. There was one patch that I didn’t have time to gather. I was only able to gather one.”

“Blueheart Grass?” Doehring Cowart’s eyes lit up. “Any place where Blueheart Grass can grow definitely is no ordinary place. There definitely must be some sort of precious treasure within that Foggy Gorge, or perhaps some extremely powerful magical beast, such as a magical beast of the ninth rank, or even a Saint-level magical beast. However…”

Doehring Cowart began to frown. “Generally speaking, powerful magical beasts are very territorial. If there was a powerful magical beast there, they probably wouldn’t permit creatures like Dragonhawks and Green Tattooed Pythons to live there as well.”

“But Dragonhawks, Green Tattooed Pythons, and those huge crawling beasts you mentioned are all able to live there together? Bizarre. How bizarre.” Doehring Cowart couldn’t understand either. This Foggy Gorge seemed to be full of contradictions.

Linley laughed. “Grandpa Doehring, don’t overthink it. When I become a magus of the seventh rank, I’ll be able to use the ‘Soaring Technique’. At that time, we’ll come for another investigation.

Upon becoming a magus of the seventh rank, his Earthguard would have reached the level of generating jadestone armor. The additional speed granted by the Supersonic spell would also dramatically improve. By then, Linley would have full confidence in his ability to deal with the Dragonhawks. And with the ability to use the Soaring Technique to fly, Linley would be able to easily enter and leave the gorge.

“Magus of the seventh rank? You are only a magus of the fifth rank right now. You have a long way to go.” Doehring Cowart said, pouring cold water over Linley’s enthusiasm.

In his heart, Linley knew this as well.

Perhaps becoming a magus of the sixth rank wouldn’t be too hard, but there was a huge gap between the sixth rank and the seventh rank.

“All roads are traversed one step at a time.” Linley smiled. “It’s been about two months since I entered the Mountain Range of Magical Beasts. It is about time for me to go back. It’ll take several days to get back anyhow. I’ll use that time to do some more training.”

With Bebe on his shoulders, Linley embarked on his return trip back home.
