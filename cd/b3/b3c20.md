#### Book 3, Mountain Range of Magical Beasts – Chapter 20, The Black Dagger (part 2)

The dark-robed man, just five or six meters away from Linley, had considered the dark Shadowmouse beneath notice, but upon seeing Bebe’s amazing speed, his cold face showed an expression of astonishment. “What is this speed?!” The dark-robed man hurriedly waved his dagger to block.

Clearly, this dark-robed man was somewhat stronger than the original assassin. At least when facing Bebe, he had the presence of mind and speed to wield his dagger.

“Swish!” Bebe swung his sharp claws fiercely.

“Clang!”

As Bebe’s claws slammed into the assassin’s dagger, the black dagger exploded into fragments, while Bebe’s claws, undamaged, immediately slashed violently against the dark-robed man’s head, directly shattering it. The man died on the spot.

“The gap between the sixth rank and the seventh rank really is enormous.” Seeing this, Linley couldn’t help but sigh.

Bebe was a terrifying Shadowmouse who could even force the mighty Velocidragon, a magical beast of the seventh rank, to flee. Based on the power of Bebe’s sharp claws and sharp fangs, killing a warrior of the sixth rank was as easy as eating rice.

“Rip!” Linley ran over to the corpse and tore the dark-garbed man’s clothes apart, immediately grabbing the hidden backpack. Without doing anything else, he immediately turned and fled northwards. Gusts of winds arose around his legs, and he began moving with such grace and agility that he left almost no trail in his wake.

After a while, a second group of dark-garbed men finally arrived. Seeing the injury on #2’s head, all of them frowned.

“A magical beast?” The images of many different magical beasts suddenly began to swim about in the mind of a dark-garbed man. “A Blue Shadowmouse of the sixth rank? Or a Violet Shadowmouse of the seventh rank? Or a Gold Stoneater Rat of the seventh rank?” This fierce but tiny claw mark must have been left by a rodent-type magical beast.

In the Mountain Range of Magical Beasts, some people believed that the most terrifying possibility was encountering a magical beast of the eighth or ninth ranks. Others believed that it was encountering a terrifying swarm of pack-type magical beasts. But in the heart of the dark-garbed man, the most terrifying possibility was encountering a Stoneater Rat swarm or a Shadowmouse swarm.

The Stoneater Rat had formidable defense, sharp teeth, and sharp claws.

The Shadowmouse had high speed, sharp teeth, and sharp claws.

If a swarm of thousands or tens of thousands of Shadowmice or Stoneater Rats attacked, even an army might be totally devoured, much less the ten of them.

“We’re going back now!” Without hesitating in the slightest, the dark-garbed leader issued his order.

……

Amidst towering mountains and ridges, Linley continued to run, winding his way atop of a mountain peak. After having run over a hundred kilometers at once, Linley believed that his pursuers would no longer be able to catch him.

“Boss, hurry up and open the backpack and see what’s inside!” Bebe immediately urged.

Linley’s heart was filled with anticipation as well. The more powerful an opponent was, the more magicite cores he should have in his backpack. That original assassin had left behind 15,000 gold coins worth of magicite cores and magicite gems. How much would this second assassin, who had been addressed as #2, have on him?

He opened the backpack.

“Two more sets of clean clothes.” Linley glanced at the clothes in the backpack, then withdrew two bulging pouches from within the backpack. This “#2” had been in the Mountain Range of Magical Beasts for a month longer and was a bit stronger than the original assassin, so logically speaking…

Seeing how many magicite cores these pouches contained, Linley couldn’t help but suck in a cold breath.

“So many? And most of them are magicite cores of the fifth rank. There’s plenty of magicite cores of the sixth rank as well.” After having seen so many magicite cores, Linley was now capable of recognizing the general rank of a magicite core at a glance. Linley immediately began to do a careful accounting of the cores.

“9 magicite cores of the sixth rank. 56 magicite cores of the fifth rank. 12 magicite cores of the fourth rank. Seven magicite gems. The total value, all together, would be roughly….20,000 gold coins. Adding this to the 50,000 gold coins worth that I already have, means that I should now hold at least 70,000 gold coins worth of magicite on me.” After tabulating his total wealth, Linley couldn’t help but take a deep breath.

70,000 gold coins!

If he placed this prodigious sum in front of his father, his father would most likely be stupefied.

Over the course of the 51 days he had spent in the Mountain Range of Magical Beasts, that assassin’s organization alone had ‘donated’ 35,000 gold coins to him. The other attempted killers he had run into had ‘donated’ a further 30,000 gold coins, while he himself had killed enough magical beasts to earn 5000 gold coins worth of cores as well.

Doehring Cowart appeared from within the Coiling Dragon ring, laughing as he watched the look on Linley’s face.

“I finally understood why so many people in the Mountain Range of Magical Beasts like to try and kill other humans. After spending a full month working so hard, I only earned a few thousand gold coins, but when I killed someone else, I gained the fruits of their two months of labor.” Linley placed the two pouches into his own backpack, then tossed the extra backpack into the grass.

“Of these 70,000 gold coins worth of magicite, only 5000 came from me killing magical beasts. The rest all came from assassins and killers.” Linley shook his head and sighed.

Doehring Cowart stroked his white beard while chuckling. “Looks like your youth actually helped you. If you looked just a bit more mature and experienced, there probably wouldn’t have been so many killers trying their luck against you.”

“Hehe.” Linley couldn’t help but laugh.

“Grandpa Doehring, just now, based on the words being exchanged by the people in that squad, it seems like they were on a training mission here in the Mountain Range of Magical Beasts?” Linley was rather curious.

Doehring Cowart smiled faintly. “Linley, every single one of the major powers of the Yulan continent has to have its own base of martial power in order to maintain its strength. But martial power has to be trained and cultivated. Many of the larger powers will often send groups of its subordinates out to the Mountain Range of Magical Beasts to train.”

Linley nodded.

“Linley, this continent has many powerful organizations which you don’t have a clue about. To be honest…even I don’t know about them. In the past five thousand years, all of the powers which existed in the era of the Pouant Empire have most likely collapsed.” Doehring Cowart said self-deprecatingly.

Linley didn’t ask too much. At this moment, Linley felt enormous pressure. The Yulan continent was far more complicated than he had imagined.

After organizing his possessions, Linley put on a shirt before continuing on his way. Making his way agilely through the mountain forests, sometimes skipping over fallen rocks, sometimes crawling over fallen trees, Linley pressed onwards. But after Linley crossed a particularly large mountain….

He saw that this mountain was hundreds of kilometers long. There were many trees here. Standing at the peak of the mountain, Linley could tell that there was a distance of hundreds of kilometers from here to the next peak, if he wanted to directly fly across the gap.

“What a bizarre canyon.”

Linley noticed that the canyon walls of these two mountain’s cliffs drew closer and closer to each other at the edges. Linley immediately began to jog down from the mountain peak. The farther down he jogged, the closer the canyon walls appeared to be. After jogging for five or six kilometers, the gap between the two mountains was only a meter across. One could cross it with a single step.

“It’s like this on this side. What is it like on the other side? The same?”

With one foot on each cliff, Linley peered across. Off in the distance, he seemed to see the two cliffs draw even closer, then become one.

“Bizarre. Bizarre.”

Having been in these mountains for some time, Linley had seen many things, but he had never encountered such a weird canyon. Looking down through the canyon gap, Linley only saw a white fog, so blurry that he couldn’t see anything at all.

“Immeasurably deep.” Linley felt extremely curious, but was also rather wary what lay within the belly of this mountain gulch.

Making his way along the edges of the canyon, Linley continued peering down, as though hoping he could see what was hidden by white fog. Aside from how close the canyon walls were, there was another oddity to this ravine.

It seemed that the farther down the ravine was, the farther apart the canyon walls drew again.

For example, towards the top of the ravine, the distance between the canyon walls was perhaps a hundred meters or so, but from what Linley could tell, towards the bottom, the distance was perhaps a few thousand meters, or even tens of kilometers.

“Hrm? That’s…”

Linley looked as though he had been struck by lightning. He carefully stared at a small patch of grass that was hidden beneath the fog beneath him. The small patch of grass growing alongside the cliffside was dark green, but the patch of grass emanated a faint blue aura.

“Blueheart Grass. It’s Blueheart Grass!” Linley had seen a picture of Blueheart Grass at the Ernst Institute’s library, and he remembered it clearly. His eyes shone. That ultra-rare, precious grass growing from the cliff was able to counteract the harmful effects which live dragon’s blood would have on the body. Blueheart Grass!
