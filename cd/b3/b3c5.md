#### Book 3, Mountain Range of Magical Beasts – Chapter 5, The Price (part 2)

Count Juneau still refused to make a bid. He planned to make his bid on June 30th. As time flowed past, the valuation of the three sculptures continued to rise, but because even an expert crafter’s work was valued at around a thousand gold, the price rose rather slowly.

500 gold coins. 510 gold coins. 515 gold coins.

The bids continued to rise slowly. By June 29th, they had only risen to 625 gold coins.

June 30th.

Count Juneau actually did not appear this morning, which was quite a rare occasion. He waited until nightfall, because the Proulx Gallery did not close until midnight. Linley’s three sculptures would also be removed from the gallery at midnight.

“The price yesterday was 625 gold coins. I’ll make my bid at the end.” Count Juneau smiled as he walked towards the three sculptures.

“900 gold coins? What idiot made this bid?” Upon seeing the highest bid, Count Juneau’s heart exploded with fury.

The price yesterday was just 625 gold coins, but in a day, the price had risen so dramatically. Although Count Juneau was furious, there was nothing he could do. He decided to wait patiently, and after a long period of time, he finally looked up to see the clock up above.

“It’s already 11 PM. In an hour, the place will close.” Count Juneau revealed a hint of a smile.

In Fenlai City, Count Juneau could be considered a middle-class noble. When he was young, Count Juneau was actually quite poor. Later, it was due to his shrewd investment in and collecting of sculptures that helped him slowly gain wealth. His current net worth was in the hundreds of thousands of gold coins. He could be considered a rather well off noble.

“Count Juneau, you are here as well?” A whiskered middle-aged man in a swallow shirt smiled as he walked over.

Upon seeing this person, Count Juneau’s countenance changed, but he still was able to smile calmly. “Count Demme [De’mu]! It’s almost eleven. Why are you here?” But in his heart, Count Juneau felt that things had just taken a turn for the worse.

Count Juneau and Count Demme were both considered rather famous collectors of sculpture within the noble circles of Fenlai City.

“Me? For these three sculptures, of course.” Count Demme stroked his whiskers, then said contentedly, “Count Juneau, take a look. The lines and aura of these three sculptures are so very mesmerizing. The expert who was able to produce such a unique aura must surely also be a unique person.”

Count Juneau’s heart trembled.

Indeed…

This Count Demme had also seen the value of these three sculptures. For him to arrive at eleven o’clock most likely meant he had the same idea as Count Juneau.

“Miss, come over here, please.” Count Demme said quite courteously to a nearby female attendant, who walked towards them with a smile. Count Demme pointed at Linley’s three sculptures. “I’m willing to pay a thousand gold coins for each one of these sculptures.”

The attendant said courteously, “Just a moment.”

She took out a record book and made some notations before placing the bidding slips next to the sculptures.

“A thousand gold coins?” The facial muscles on Count Juneau’s face twitched.

Count Demme said to him with a smile, “Count Juneau, these three sculptures really are exceptional. Right, what brings you out here so late at night, rather than resting at home? Are you here for these three sculptures as well?”

Count Juneau let out a light hum.

“I didn’t expect that Count Demme would be so interested in these three sculptures. Honestly, I hadn’t paid them much attention yet. Let me take a good look first.” Count Juneau smiled, then turned and began intensely studying the three sculptures, totally ignoring Count Demme.

Seeing the scene before him, Count Demme sneered mentally. “Old fellow, do you really think you can hide your thoughts from me?”

Like the murmurs of a river, the music continued to play in the main hall of the Proulx Gallery Count Juneau and Count Demme both quietly viewed various sculptures. The gallery remained as quiet as ever.

“Dong. Dong.” The clocks on the walls began to chime.

It was now midnight.

“Miss, please come here.” Count Juneau said to the attendant, who immediately ran over.

“These three sculptures, I am willing to buy for 1010 gold pieces.” Count Juneau made his bid at the last moment.

The attendant saw that the current bid on the sculptures was 1000 gold pieces. She couldn’t help but glance sideways at Count Juneau. It was quite fortunate that Count Juneau had added ten pieces, and not just one.

“Please wait a moment.” The attendant took out her record book.

“Count Juneau, you actually just overbid by ten gold pieces? I’ll offer 1100 gold pieces!” Count Demme’s voice rang out. Count Juneau frowned as he turned to stare at Count Demme, who was casually striding over with a jocular air, an arrogant look in his eyes.

As it turned out, Count Demme had been paying attention to Count Juneau this entire time, and as soon as Count Juneau made his bid, he came over.

“I bid 1200.” Count Juneau said in a low voice, his fury clearly visible. Seeing the oncoming struggle between the two nobles, the attendant closed her record book and stood off to the side, happily watching the battle. The attendants of the Proulx Gallery loved to see customers enter bidding wars.

Count Demme glanced at Count Juneau with ‘astonishment’. “Count Juneau, even the sculptures in the hall of the experts is worth only around a thousand gold coins. How could a frugal man such as you be willing to pay 1200 gold?”

Frugal?

Miserly was the word! Count Juneau was notorious for his miserliness.

“Count Juneau, if even you are willing to bid 1200, then I can’t be stingy either. 1300 gold pieces!”

Count Juneau’s gaze was ice cold. “The only reason why I am willing to offer a high price for these three sculptures is because I am fond of them. Their real value is only around a thousand gold or so. 1500 gold pieces! If you, Count Demme, are willing to make a higher bid, then you can take them.” Count Juneau made his final offer.

In all honesty, Count Demme was not as insightful as Count Juneau. He didn’t discover the unique, strange aura to these statues.

In Count Demme’s eyes, these statues didn’t hold any secrets. They were just three good pieces of art, worth a thousand gold or so. If he raised the price any further, there wouldn’t be much point.

“Haha.” Count Demme laughed. “It’s so rare for Count Juneau to be so refreshingly magnanimous in his bidding. In honor of this occasion, I certainly can’t rob a man of his beloved possessions. These three sculptures are all yours, Count Juneau.”

Only now did the attendant step forward again and begin recording the bid into her book.

“Milord Counts, it is already midnight. The gallery is about to close. Count Juneau, tomorrow I will arrange for people to deliver the sculptures to you.” The attendant smiled. Only now did Count Juneau also smile.

Count Juneau flicked a glance at Count Demme, feeling scornful. “Kid. How many years have I spent analyzing stonesculpting? You don’t have any insight, and you still want to bid against me?”
