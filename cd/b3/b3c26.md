#### Book 3, Mountain Range of Magical Beasts – Chapter 26, Violet in the Night Wind (part 2)

Night time. The four bros of dorm 1987 were walking along a dark, silent street of the Ernst Institute, casually talking about what had happened over these past two months.

“As vicious as that?” Reynolds, amazed, tugged aside Linley’s shirt. Seeing all the crisscrossing scars across Linley’s chest, he couldn’t help but hold his breath. The nearby George also went silent. Only Yale was able to laugh, “Haha, you guys have no experience. When I was a kid, I saw way worse than this.”

“Boss Yale, are you serious?” Reynolds said in astonishment.

Yale smiled cockily. “Of course I’m serious. And I’ve seen more than a few as well. For example, killing prisoners by torture. Or real people fighting against magical beasts with their bare hands. When they fought barehanded against the beasts, they were surrounded by a ring of rich spectators. The sight was really bloody.”

Hearing Yale’s words, Linley was able to picture the scene in his mind.

“It’s good to be on campus,” George sighed.

Linley also nodded in agreement. By this time of the night, many couples could be seen walking together on the road, some holding hands, others seated together on the backs of a magical beast. Campus life was very leisurely.

“Right. Boss Yale, aren’t you going to go spend tonight with your girlfriend? Why aren’t you getting ready to leave?” Reynolds suddenly said.

Yale said with dissatisfaction, “Girlfriend? My bro has just come back from the Mountain Range of Magical Beasts after encountering so many near-death situations. And I’m going to go spend time with my girlfriend? Reynolds, you have to remember these words: Bro’s are like your arms and legs, while girls are like your clothes. They’re just good for playing with.”

A look of contempt immediately appeared on Reynolds’ face.

“Linley!” A surprised voice suddenly rang out from far away.

Linley and the others all turned their heads and watched as a tall, slender, beautiful young woman with golden hair ran towards them happily. Upon reaching Linley, she exclaimed in surprise, “Linley, you’re back from the Mountain Range of Magical Beasts? This is wonderful. You disappeared for two full months this time. I was so worried. Are you injured?”

“Delia, I’m fine,” Linley laughed as he responded.

Delia was also someone whom Linley had met just as he had enrolled in school. They were on very close terms with each other. When he was together with Delia, Linley felt as though he could totally relax, and be without any mental pressure. It was just like when he was with his three dear bros.

“Delia, Uncle’s carriage is outside waiting for us. Let’s not waste any time.” A cold voice rang out.

Turning his head, Linley saw a youth dressed in long robes standing some distance away. It was Delia’s elder brother, Dixie, one of the two geniuses of the Ernst Institute. Dixie’s robe was extremely clean and neat, without a single blemish or stain. His eyes also seemed very clear and tranquil.

“Oh.” Letting out a disappointed sound, Delia looked at Linley. “Linley, father asked me and my brother to go back. Our carriage is outside waiting for us. I have to go back now.”

“Alright, Delia. We can chat when you come back.” Linley smiled as he replied.

“Right. Bye.” Delia clearly felt rather disappointed at not having more time to chat with Linley. Dixie walked over to them as well. He only glanced at Delia, and Delia immediately began walking towards him. But then, Dixie turned to look at Linley. “Linley, I heard you successfully returned from your training exercise in the Mountain Range of Magical Beasts. Congratulations.”

Linley was stunned.

This Dixie was actually speaking to him?

Dixie’s coldness and aloofness was legendary at the Ernst Institute. Most people would feel themselves to be under enormous pressure next to Dixie, especially when his cold, clear eyes fell upon them. That sort of psychological pressure was enough to cause some to break under the strain.

“Oh. Thanks.” Linley replied.

Dixie barely nodded, and then escorted his sister Delia to the school gates.

….

Austoni carefully looked at Linley, sighing in amazement, “Linley, I must say, you really are a genius, a super genius! A fifteen year old youngster who is a genius amongst the geniuses at the number one magus academy in the entire Yulan continent, and also someone who has reached an incredibly high level in the art of stonesculpting.”

“For you to be able to accomplish all this is a miracle.”

“Putting aside the fact that you are a genius magus, even in the world of artists, in this day and age, most sculptors who qualify to be invited by us to open up a private booth in the Hall of Experts are at least forty years old. You are the youngest one. Even in our entire history, there are only two unparalleled geniuses who are a match for you. But the difference is…not only are you a genius sculptor, you are also a genius magus. Wow…what a genius.”

Austoni’s words of praise caused Linley to be embarrassed and not know what to say.

“Austoni, stop wasting time. Hurry up and finish. We four bros are going to go out and have some fun.” Yale urged.

Only now did Austoni seem to come to himself. He hurriedly pulled over a stack of documents and withdrew a silver magicrystal card. Smiling, he presented it to Linley. “Linley, this silver magicrystal card was specially designed by the Golden Bank of the Four Empires. It represents that you are one of our expert sculptors. In the future, any and all proceeds from sales of your art will be directly transferred by us into the balance for this card.”

“Right now, this silver magicrystal card doesn’t have an owner imprinted. Use your fingerprint to seal it to you. In the future, you can use it.” Austoni respectfully handed the magicrystal card to Linley, then said in an eager voice, “Linley, might I ask if you brought any sculptures for us this time?”

Linley nodded his head slightly. “I have. Three in total.”

Austoni’s smile immediately became even more radiant.

….

Night time. Within the Jade Water Paradise. Linley, George, and two courtesans were there by themselves, drinking while talking and laughing. By now, Reynolds and Yale had long since retired to their rooms with their courtesans.

“Jeeze, those two, Boss Yale and fourth bro…” Linley drank a cup of wine as he spoke to George, who was in the middle of laughing and chatting with his girl. “Second bro, my head is getting a bit dizzy. I’m going to go out to cool off a bit.”

“Sure.” George replied, then continued to chat with his companion.

Heading downstairs, Linley directly left the Jade Water Paradise. Upon departing the lively premises, Linley suddenly felt a cold, refreshing night wind blow past him, helping to clear his mind. Compared to the Jade Water Paradise, the outside was much calmer and more tranquil. Linley began to take a casual walk around the streets of Fenlai City.

The cool night breeze was very refreshing.

There were some noble estates lining the streets, but compared to the Greenleaf Road, the estates on this street, Dry Street, were clearly on a lower level. And on the balcony of one two-story estate in particular, Alice was standing, enjoying the night breeze.

Staring up at the bright moon in the empty sky, Alice couldn’t help but think about Linley, who had saved her life.

At that moment, when she had fallen into despair, he had descended from the heavens and vanquished that Bloodthirsty Warpig and saved her life. That action had shaken her deeply. It could be said that that event had left a deep impression on her soul.

“Big brother Linley is a bit taciturn, but when he gets into discussing magic, he’s rather handsome.” A faint smile appeared on Alice’s face as she reminisced.

Suddenly, Alice saw a figure walking on the streets below. His frame seemed very familiar. Taking a closer look, she immediately recognized him, and a smile lit her face up. She hurriedly waved while shouting, “Big brother Linley, big brother Linley!”

Linley, who was walking on the street while enjoying the cool night, looked up suspiciously as he heard someone calling his name.

A distant balcony, a shadowy form dressed in violet, the bright moon illuminating from behind. The violet clothes fluttered in the night breeze, and under the glow of the moon, seemed to radiate. Long hair fluttering alongside the violet clothes. Suddenly, Linley seemed to smell Alice’s fragrance.

That fragrance, was so mesmerizing…

“Alice…” Linley couldn’t help but walk towards that balcony.
