#### Book 5, The Godsword, Bloodviolet – Chapter 7, Second in History

A dual-element magus of the seventh rank, compared to the Yulan continent as a whole, could only be considered someone who had just stepped into the field of the powerful figures.

But if you added the words ‘seventeen year old’ in front of the words ‘dual-element magus of the seventh rank’, the effect was totally different. The Radiant Church probably wouldn’t care too much about a dual-element magus of the seventh rank; after all, there were plenty of powerful figures in the Yulan continent.

However…

A seventeen year old dual-element magus of the seventh rank? Leaving the Radiant Church aside for now, perhaps each and every major power on the Yulan continent would be jealous to possess this.

“Genius. Genius!” Vice Chancellor Deland, a magus of the eighth rank, was extremely excited.

All of the watching magus instructors were in shock as well. All of them understood exactly what a seventeen year old dual-element magus of the seventh rank represented. This was a miracle! At the very least, it was the Ernst Institute’s miracle!

“Heh heh.” Yale, George, and Reynolds all started to snicker.

They had all been anticipating the expressions on the faces of these magi. And it was as priceless as they had hoped.

In terms of power, Vice Chancellor Deland couldn’t even rank amongst the top three, here at the Ernst Institute, but he had significant amounts of experience. He quickly was able to tamp down his excitement, and was the first to walk to Linley’s side. “Linley, do you know what being a seventeen year old dual-element magus of the seventh rank represents?”

“Heh, does he have to ask?” At this time, Doehring Cowart flew out of the ring, delightedly stroking his long, white beard. “How could the pupil of I, Doehring Cowart, not be outstanding?”

All of the teachers currently present were quite far from the Saint-level. Naturally, none of them were able to detect the presence of Doehring Cowart’s spirit.

“Seventeen years…” Deland sighed with praise. “In the entire history of the Ernst Institute, based on age, amongst all of the students to attain the seventh rank, you, Linley, are the youngest. The previous record holder, a genius who attained the seventh rank at age 19, went on to become a Saint-level Grand Magus.”

A silver-haired elder next to him spoke out. “Let’s not discuss the Ernst Institute for now. If we look at the Yulan continent as a whole, and look at the records of the continent as a whole, you are the second-youngest genius in all of recorded history to reach the seventh rank.”

The Yulan continent as a whole had been around for countless years, and also covered a huge amount of territory. There was no way for the Ernst Institute to match it in terms of records.

“The second in history?” Linley was rather surprised as well.

How many countless geniuses had the Yulan continent produced, over these years? For himself to be able to be the second youngest in history was a terrifying accomplishment.

“The youngest magus in the entire history of the Yulan continent to reach the seventh rank was a Saint-level Grand Magus who lived over 8000 years ago. He became a magus of the seventh rank when he was 16 years old. The previous second youngest, who has just become the third youngest, became a magus of the seventh rank when he turned 18. In the end, he topped out at the ninth rank. This was because afterwards, he suffered a huge setback, and his personality changed. We can put it like this…aside from you, of those top ten young geniuses who reached the seventh rank earliest, six of them became Saint-level Grand Magi, while the other four became arch magi of the ninth rank.”

Generally speaking, a magus of the seventh rank was given the title of ‘Senior Magus’.

A magus of the eighth rank would be respectfully titled ‘Master Magus’.

A magus of the ninth rank would be honored with the title of ‘Arch Magus’.

And a Saint-level magus could be venerated as a ‘Grand Magus’.

“Put another way…based on your talent, becoming a magus of the ninth rank is going to be virtually no problem at all. All you need is time. But if you continue to strive hard, you have the great potential to become a Saint-level Grand Magus. After all, you are the second youngest magus of the seventh rank in all of history.” That silver-haired elder looked at Linley solemnly.

Linley had some degree of eagerness towards eventually becoming a Saint-level magus, but that eagerness wasn’t too excessive.

This was because Linley knew very well that it was even harder for a magus to advance in power than it was for a warrior.

True, warriors and magi both needed spiritual energy. But they had different requirements as to how much spiritual energy was needed.

Magi didn’t train their bodies, focusing exclusively on spiritual energy. The vast majority of their time was spent building up their spiritual energy, because spiritual energy impacted their ability to gather mageforce, as well as to direct and control elemental essences. A mighty magus also needed a terrifying amount of spiritual energy.

But warriors were different.

To a warrior, the most important thing was still their body. Spiritual energy and battle-qi were both secondary. Only once they had a powerful body would they be able to contain lots of battle-qi. Spiritual energy was only used to more finely control the usage of that battle-qi.

If you compared a magus of the seventh rank and a warrior of the seventh rank, the different in spiritual energy could be as much as ten times more for the magus.

“Even if in the future, I reach the level of Saint-level Grand Magus, I surely would have taken a tremendous amount of time. By contrast, based on my inherent talent as a Dragonblood Warrior, I will reach the Saint-level at a much faster pace.” Linley knew very well his clan’s history. Dragonblood Warriors usually only needed a few scant decades to reach the Saint-level of power.

What’s more…

A Dragonblood Warrior who had reached the Saint-level in power was extremely formidable. Even amongst Saint-level combatants, a Dragonblood Warrior would be considered an ultimate-tier combatant.

“Linley, you are the most successful student in the entire history of our Institute. For these next few days, we ask that you please remain here at the Institute. We will invite some the absolute best painters and sculptors to come and paint paintings and carve sculptures of you, which we will keep in the Institute as mementos.” Vice Chancellor Deland immediately said.

As the second youngest magus to reach the seventh rank in the entire history of the Yulan continent, Linley naturally was the pride of the entire Ernst Institute.

“A painting?” Linley was stunned.

He realized that in front of these painters and sculptors, he would have to stand still for a very long period of time. As he realized this, Linley couldn’t help but think to himself, becoming the second youngest magus to reach the seventh rank in the entire history of the Yulan continent was perhaps not as wonderful as it sounded.

…….

The number one genius in the history of the Ernst Institute, and the number two genius in the history of the Yulan continent. A seventeen year old dual-element magus of the seventh rank. This astonishing news quickly spread across the entire Ernst Institute.

“A seventeen year old dual-element magus of the seventh rank? How is that possible?”

“There’s no way this news is fake. So many of the Institute’s teachers were present at that time, and Vice Chancellor Deland has even invited painters to come and paint pictures of Linley, with the intention of forever enshrining his image within our Institute.”

“My heavens, a seventeen year old dual-element magus of the seventh rank. Based on this speed, he should reach the eighth rank in ten years, and the ninth rank in twenty. He’ll be ninth-ranked Arch Magus in his forties. Most likely, within a century, he will become a Saint-level Grand Magus.”

“I just flipped through some of the books in the library. Aside from Linley, of the top ten geniuses in history, six became Saint-level Grand Magi, while the other four all became Arch Magi of the ninth rank. Linley is way too incredible.”

…..

The entire Ernst Institute was shaken upside down by this news. If a student was perhaps just slightly better than his peers, perhaps he would be viewed with jealously. But once a student’s achievements reached a level as high as this, becoming the second youngest magus to reach the seventh rank in the entire history of the Yulan continent, they would only be filled with respect and veneration.

In their eyes, Linley’s future prospects were limitless. There was no way for them to compare with him.

In the past, there were still some people who claimed that Dixie was the number one genius of the Institute. Now, no one said such a thing.

Without question, the number one genius of the Ernst Institute was Linley. And it wasn’t just now; Linley was the number one genius of the Ernst Institute in all of its five-thousand-year-long history. Dixie was currently just a magus of the sixth rank. Who knew how long it would take before he could reach the seventh rank?

“Linley, a magus of the seventh rank?” Having just completed his meditative training, Dixie fell silent upon hearing this news from his sister Delia.

After having ‘surpassed’ Linley when he became a magus of the sixth rank, Dixie had felt some sense of satisfaction. But this new bit of news seemed to push him into a deep abyss. Linley’s speed of improvement was simply too astonishing. Even when he chased after Linley with all his might, it seemed like he was still being thrown farther and farther behind by Linley.

“Big brother.” Delia said in a soft voice. She was a bit concerned about her big brother.

Delia knew all too well that ever since he was young, her big brother had been an extremely proud person. He was very cold to others, and also extremely strict with himself. Her big brother never submitted to anyone, but ever since Linley had rocketed up from the fourth rank to the fifth rank, her big brother had felt threatened.

Her big brother had worked extremely hard, and in the previous year had managed to cross the threshold of the sixth rank.

But Linley actually…

“Don’t worry. I’m fine.” Dixie slowly shook his head. “Delia, I suddenly feel as though there’s not that much point in remaining here at the Institute. I also plan to apply for graduation. In the upcoming days, I’ll return to the Empire and return to the clan.”

Delia was startled.

……..

Within a private area inside the Huadeli Hotel, there were four bedrooms and two living rooms. It was quite large. Linley and his three bros were currently living here.

Ever since the news that Linley had become a magus of the seventh rank had spread out, dorm 1987 hadn’t had a single peaceful day. Huge amounts of people came to pay their respects to Linley, forcing Linley to hide here, within the Huadeli Hotel. Due to the deep background and connections possessed by the Huadeli Hotel, few people dared to trespass here.

“Third Bro, when you are quiet, you are very low-key, but when you finally make your move, by the heavens do you cause a ruckus!” Yale sighed.

Linley chuckled.

Actually, this was a decision which he had arrived at after serious discussions with Doehring Cowart. After all, currently, the Baruch clan was still weak. If they wanted to strengthen it rapidly, the best way to do so was to quickly spread the word that he already possessed the might of a magus of the seventh rank.”

A seventeen year old dual-element magus of the seventh rank! This would cause every organization in the continent to send people inviting him to join them. Naturally, they would offer exceptional conditions as well.

And thus, Linley would do better and better in the future.

“Third Bro, I’m no longer going to hide this information from you. The Dawson Conglomerate, one of the three great trading unions in the Yulan continent, belongs to my clan. Are you interested in joining the Dawson Conglomerate?” Yale looked at Linley. In all honesty, Yale was very much hoping that Linley would become a member of the Dawson Conglomerate.

The number two genius in the entire history of the Yulan continent. If a genius like this entered the Dawson Conglomerate, his future status would unquestionably be very high. Naturally, this would also be hugely beneficial to Yale’s status within his clan.

“The Dawson Conglomerate?!” Reynolds let out a startled yelp. “Wow, Boss Yale, I always knew you were a member of the Dawson clan, but there are way too many clans with the name ‘Dawson’. But the Dawson clan you belong to is actually the Dawson clan behind the Dawson Conglomerate? The Dawson Conglomerate! My goodness, you are rich!”

George also looked at Yale.

“Boss Yale, this…” Linley hesitated.

“Don’t worry. You are my bro, first and foremost. I won’t force you.” Yale laughed. “I can’t guarantee other things, but what I can guarantee is that if you do decide to join the Dawson Conglomerate, then money will not be an issue. At the very least, we can provide you with a hundred million gold coins.”

“A hundred million gold coins?!” Linley, George, and Reynolds were all flabbergasted.

A hundred million gold coins. What a terrifyingly large sum that was.

Perhaps all of the combined assets of the richest clan in Fenlai City wouldn’t add up to a hundred million gold coins.

“Linley, the clan of this bro of yours is really too wealthy. A hundred million gold coins, damn…” Even Doehring Cowart was stunned.

Even a master sculptor’s most famous, legacy-making sculpture would only be worth a million gold coins at most. This was already a terrifying sum of money, and how many master sculptors were there?

“Third Bro, I can honestly tell you that aside from the other two trading unions, in the entire Yulan continent, not even the Four Great Empires or the two major alliances would be able to produce such a vast amount of money at once. As for those kingdoms…hmph.” Yale was very certain of his words.

The Four Great Empires and the two major alliances both had their own Saint-level combatants. But the Four Great Empires and the two major alliances had to pay the upkeep for their huge armies as well as provide for the entire country. Although they were wealthy, asking them to produce a hundred million gold coins all at once would be very difficult for them. At the very least, it would require lengthy, complicated internal deliberations.

For someone who wasn’t (yet) a Saint-level combatant? They wouldn’t be willing to do it.

Only the three major trading unions, with their terrifying amount of wealth, would. Although they possessed a staggering amount of money, in terms of military power, although they were strong, they were much weaker than the Four Great Empires and the two major alliances. Thus, they all urgently needed experts to join their ranks.

“Knock!” “Knock!” “Knock!”

Suddenly, the sound of their door being knocked on could be heard.

Yale frowned and walked over to the door. Opening it, he said, “I thought I gave instructions for us not to be disturbed?”

The manager of the Huadeli Hotel said awkwardly, “Young master Yale, a Cardinal of the Radiant Church, along with three clerics and a troop of Knights of the Radiant Temple, have arrived outside the hotel.”

Yale started.

One of the Cardinals, whose position and authority in the entire Radiant Church was second only to the Holy Emperor himself? The rank of each and every Cardinal was much higher than that of one of the kings of a kingdom. If a Cardinal had personally come, leading a troop of people, there was no way that he, a young master of the Dawson Conglomerate, could possibly block the way.

“Looks like Third Bro has quite a powerful appeal!”
