#### Book 5, The Godsword, Bloodviolet – Chapter 4, Grandmaster Sculptor?

Shortly after the Ernst Institute began the new school semester, Hillman arrived at the Ernst Institute in search of Linley.

In front of the Ernst Institute’s main gate, Hillman was frowning while pacing. Clearly, he had a belly full of bad thoughts. The Ernst Institute was under very strict management, and as an outsider without any particular status or power, he didn’t have the qualifications needed to enter.

After a while, Yale and Reynolds, both dressed in sky-blue robes, stepped out and walked towards him.

“You are Linley’s Uncle Hillman, right? I met you before.” Yale spoke out warmly.

Hillman had previously seen Linley’s three bros before. Upon seeing Yale and Reynolds, he immediately went over and asked them, “Hey…I know that you are classmates with Linley, and I wanted to ask, why didn’t Linley come back to celebrate the New Year? Every year in the past, he would come back.”

“Uh…” Yale and Reynolds exchanged glances.

That Linley had his heart broken wasn’t a happy event. It wouldn’t be good for them to reveal it to Linley’s elders.

Reynolds reaction speed was the fastest. Smiling, he said, “Uncle Hillman, Linley’s totally focused on his training, and long before the end-of-the-year examinations, had already reached the rank of magus of the sixth rank. And then, he once more entered the Mountain Range of Magical Beasts for training. Man, he’s so hard-working…he didn’t even bother coming back for the yearly examinations. That Dixie fellow was assessed as a magus of the sixth rank this year. Some people are now saying that Dixie has surpassed Linley.”

“Third Bro has no care for these superficial things. Right, Uncle Hillman, Linley headed off to the Mountain Range of Magical Beasts last December. He should be back very soon. Is there something important? If there is, you can tell us. We’ll definitely let him know when he’s back.” Yale said very courteously.

Hillman was silent for a while, then shook his head, a smile on his face that didn’t seem like a smile. “No…nothing important. It was just that Linley had always come back every year, and so this year, when he did not, the family grew worried and wanted to check up on him. Since we now know that Linley has entered the Mountain Range of Magical Beasts, we’re satisfied.”

“Uncle Hillman, don’t worry, when Third Bro comes back, I’ll definitely tell him to go home early so that you won’t be worried.” Yale immediately said.

Hillman shook his head. “No need, no need to rush him back. Let him focus on his training. When he has some free time, he can come back then. Nothing big is going on back home anyways. Thanks, the two of you. I’ll head off now.”

Watching Hillman depart, Yale and Reynolds smiled, then turned to leave as well.

Suddenly…

“Young master Yale, young master Reynolds!” From far away, an exceedingly friendly voice called out.

Yale and Reynolds turned to stare outside of the Institute. From far away, they could see a parked carriage guarded by four armored knights. Frowning, Yale said questioningly, “Who is calling out to me? Oh. It’s Austoni.” Yale saw Austoni poke his face out of the carriage.

Austoni was the first out of the carriage. He smiled humbly at Yale, and then respectfully stood off to the side. At this time, the screen door to the carriage was once more pushed open, and a very distinguished-looking bald gentleman with a cane slowly made his way out.

Yale and Reynolds exchanged glances.

“Who is this old geezer? Seems distinguished.” Reynolds said beneath his breath.

Yale shook his head. Also beneath his breath, he said, “I don’t recognize this old geezer either. But based on Austoni’s actions, he should be an important individual. Austoni is a high level manager at the Proulx Gallery who has fairly high status himself.”

Accompanied by Austoni, that distinguished looking old man walked over to them, smiling.

“Little Yale, hello.” The bald man smiled as he spoke to Yale. “I ran into your father not long ago. Your father was full of praises for you. Haha, for Mr. Dawson to have a son such as yourself at the Ernst Institute is a very proud thing.”

Yale looked questioningly at the bald man.

“He says he knows my father? And seems to be close to him?”

Austoni said from the side, “Young master Yale, this is the managing director of our Proulx Gallery. You can call him Director Maia [Mai’ya].”

“No need, just call me Uncle Maia. I’ve been friends with your father for decades.” The bald old man said with a smile.

Yale felt secretly shocked.

The Proulx Gallery was the holy land for the arts. Every single large city in the Yulan continent had a branch of the Proulx Gallery. Even here at Fenlai City alone, the total value of all the sculptures stored at the local Proulx Gallery would come to an astounding figure.

And that wasn’t the half of it.

The most important thing was status. To be the managing director of the holy land for the arts meant that the circle this Director Maia travelled in composed of the highest tier of people in the entire Yulan continent, and he might even be on friendly terms with Saint-level combatants. How could anyone look down upon someone like this?

What’s more, the Proulx Gallery had a extremely formidable armed force, as otherwise, how could they protect their valuable treasures?

“Uncle Maia.” Yale said humbly.

The bald Director Maia turned to look at Reynolds. “And this is?”

“This is a good bro of mine – Reynolds.” Yale immediately replied. Quite elegantly, Reynolds also said, “Very pleased to meet you, Director Maia.”

Director Maia nodded slightly. From Reynolds movements, he could tell that Reynolds had received excellent tutelage from when he was young.

“Uncle Maia, why have you come here, if I might ask?” Yale asked.

Although he was asking, in his heart, Yale already suspected the answer. “80% chance he’s here because of that sculpture of Third Bro – Awakening From the Dream.” The last time the Ernst Institute had a holiday break, due to the fact that it had been quite some time since Linley had sent any sculptures to the Proulx Gallery, Austoni had come over to see what the situation was.

But upon arriving at Linley’s dormitory, by chance, Austoni had caught a glimpse of that sculpture, which they had placed in the dorm.

Upon seeing it, Austoni had been totally stunned.

As a high level manager of the Proulx Gallery, Austoni’s eyes were exceedingly sharp. From that glimpse, he was absolutely certain that this sculpture of Linley’s was qualified to be described as standing at the pinnacle of the entire art of stonesculpting. It definitely was qualified to stand on the same pedestal as the Ten Great Sculptures.

The most important thing was, this sculpture of Linley’s was enormous, on par with five separate sculptures of most people.

Just like in the art of painting, the value of a sculpture was related in part to its size. Such an enormous sculpture would’ve required an incredibly large amount of effort. This sculpture which contained five lifelike images of people had already contained within it a unique soul and was on a totally different level.

Seeing that sculpture was the same as seeing five real-life beautiful women.

In the entire Yulan continent, there were very few master-level sculptors. But this sculpture by Linley had already surpassed the level of ‘masters’; it was qualified to be ranked amongst the works of the most venerated grandmaster sculptors in history, such as Proulx, Hope Jensen [Hu’pe Jin’sen], and Hoover [Huo’fu].

Those who were granted the title of master were able to produce sculptures of exceedingly high quality, with their own distinct aura and the ability to stir the soul of the viewers.

But their works, when compared to the works of Proulx, Hope Jensen, and the other sculptors who had received the title of ‘Grandmaster’, was still slightly inferior. Although the gap was very small, it still determined a difference in status.

Stonesculpting had a history of hundreds of thousands of years, and during that period of time, the vast majority of sculptures had been destroyed by the passage of time. Only a few special statues made of special materials could survive and be passed down to the present generation. Thus, of the so-called Ten Grandmasters, nine of them lived within the past hundred thousand years.

Ever since the Yulan Empire unified the Yulan continent, there had been only two sculptors that could be put on the same level as those ancient grandmasters: Proulx and Hope Jensen.

Hoover was a Grandmaster from over a hundred thousand years ago, and his famous sculpture, the Bloody-eyed Maned Lion, had survived all those years due to the unique properties of the material it was made from, thus ensuring Hoover’s fame would live on.

In the past ten thousand years, there had only been two Grandmaster sculptors. Now, of course, Proulx was actually the most formidable sculptor in all of history, and three of the Ten Masterpieces belonged to him. Not all of the Ten Grandmasters had produced sculptures which numbered amongst the Ten Masterpieces.

Of course, this was just the judgment of the later generations. In terms of actual sculpting ability, all of the Ten Grandmasters were about the same.

A new Grandmaster had been born…and he was a 17-year old youth!

What an amazing event this was! And this was the reason why the managing director of the Proulx Gallery himself had hurried over here, all the way from the Proulx Gallery located in the Dark Alliance.

“No rush. Let’s go to a private room in a hotel and have a nice, quiet chat.” Director Maia wasn’t in too big of a rush.

A Grandmaster sculptor?

What a joke!

Although Austoni’s eyes were keen, whether or not a sculpture was capable of being passed down the ages required extremely formidable judgment. The work of a master sculptor and that of a Grandmaster lay in its unique aura and soul.

Whether or not a work of art was qualified to be considered a Grandmaster-level piece of art was an extremely deep field of study.

…..

Within a deluxe room at the hotel.

In front of the four of them, there was a kettle of light tea. Laughing, Director Maia said, “This kid, Austoni, upon seeing Linley’s sculpture, insisted that it was on par with the Ten Masterpieces. Haha, isn’t that the same as saying that we now have a seventeen year old Grandmaster?”

‘Grandmaster’ was a title representing a certain status, representing that someone was at the peak of this art form.

But in casual conversation, most people would address someone as ‘master’, for example, ‘Master Proulx’.

“Grandmaster sculptor?” Yale was somewhat amazed. “I don’t know if Linley’s sculpture qualifies or not. After all, my experience is limited. But I am absolutely sure that this sculpture of Linley’s is, at the very least, comparable with the sculptures you have on display in your hall of the masters.”

“Oh?” Director Maia laughed. “Well-spoken. After all this chitchat, I suppose it’s best I take a look. I don’t know where this sculpture is. May I take a look?”

“Of course.” Yale smiled.

“Little Yale, even if this sculpture isn’t at the level of the Ten Masterpieces, I’ll wager it isn’t too far off. You have to protect it and make sure it isn’t stolen.” Director Maia reminded.

Yale confidently said, “Uncle Maia, please set your mind at ease. Right now, I’ve secreted the sculpture into the secret underground room within the Huadeli Hotel, and I have experts of the Dawson Conglomerate protecting it. What’s more, there are very few people who even know of the existence of this statue to begin with.”

“You’ve moved it to the hotel?” Austoni was somewhat surprised. The last time he saw it, it was in their dormitory.

Yale pursed his lips. “I trust my bros, but I don’t trust you.”

Austoni could only let out a few awkward chuckles.

“Uncle Maia, let’s go. I’ll lead you there.” Yale said warmly.

The Huadeli Hotel was actually a property under the banner of the Dawson Conglomerate. This was the reason why the upper-level management of the Huadeli Hotel knew Yale’s status.”

With a large stand-alone room inside the Huadeli Hotel, there were several seats as well as three experts who had been standing guard every day.

“Young master Yale.” The three warriors of the seventh rank bowed respectfully.

Yale nodded and smiled slightly. “Uncle Maia, please view to your heart’s content.” As he spoke, Yale gave a sharp tug to the heavy covering over the sculpture, revealing the enormous work of art. Those five beautiful women were incomparably immaculate and fine. One an image of tender love, another an adorable innocence, a third all bashful and shy, the fourth passionate and stirring, and the last…heartless.

All of them seemed to be as real as an actual person.

Seeing these five human shapes within the sculpture, Director Maia’s mouth hung open, and he stared at it, stunned, for a long time.

After a long time…

“Incredible. Incredible.” Only now did Director Maia awaken from his stupor. “This sculpture is at the master level, at the very least. A sculpture which links together five different human figures, all totally lifelike? How much effort did this cost? In terms of carving time alone, at least a year must have been spent on it.”

Director Maia knew very well how much effort sculpting took.

It took so much effort that sometimes, in the middle of carving a sculpture, a master sculptor might suddenly vomit blood and pass out from the exertion. In history, there were people who died in the middle of their sculpting. Sculptures such as this were formed from blood and effort.

“For a seventeen year old to be able to produce this sculpture is simply…simply…” Director Maia was at a loss for words. He excitedly walked closer to the sculpture for a closer examination. “Whether or not this sculpture is on par with the Ten Masterpieces requires further inspection from multiple angles.”

As he spoke, Director Maia glued himself next to the sculpture, beginning to carefully inspect every single carved line.
