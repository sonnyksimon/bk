#### Book 6, The Road to Revenge – Chapter 29, Kill However Many Come!

Within the palace.

The Glacial Snow Lion which Clayde had tamed had ten or so servants dedicated solely to his maintenance. After having tended him for so long, the Glacial Snow Lion’s attendants could already guess what the Glacial Snow Lion was saying when it roared.

“Where’s the Snow Lion?” A white-robed male palace attendant said in a high-pitched voice.

“Milord, the Snow Lion is currently asleep.” One of the Glacial Snow Lion’s attendants said respectfully.

“Mm.” The palace attendant nodded arrogantly.

“Roar! Roar!” Suddenly, a series of ferocious roars could be heard. The roars sounded frantic and worried.

Hearing the sound, the face of the attendant responsible for tending the Glacial Snow Lion instantly changed. The white-robed palace attendant was even more worried. He asked, “What’s going on? What’s wrong with the Snow Lion?”

Roaring furiously, the Snow Lion quickly charged forwards to them.

“His Majesty, his Majesty is in danger!” The servant charged with tending the Snow Lion was frantic. “Quick! Ten years ago, this happened once as well. His Majesty must be in grave danger! Quick, quick, go protect his Majesty! Milord, where is his Majesty right now?”

The expression on the face of the white-robed palace attendant changed as well. “His Majesty, his Majesty left the palace. Right. He went to Lord Linley’s manor.”

“Quick, quick, go protect his Majesty!” The attendant bellowed.

At the same time, the attendant directly leapt onto the Snow Lion’s back. After having spent every day feeding the Snow Lion, the creature held very little animosity towards him and was willing to let him ride atop itself. Just at this moment, five shadows suddenly flew over as well. These were five of the top experts of the palace.

“Snow Lion, is his Majesty in danger?” A golden-haired middle-aged man barked out to the Glacial Snow Lion.

The Snow Lion continued to bellow while nodding at the same time.

“Quick, to Lord Linley’s manor. His Majesty is there.” A jade-haired expert quickly said.

“Fourth Bro, you go find Lord Kaiser [Kai’sa].” The golden-haired middle-aged man shouted.

Lord Kaiser was the leader of these experts, and one of the most powerful combatants of the Kingdom of Fenlai. There were only a total of two combatants of the ninth level who had pledged loyalty to the Kingdom of Fenlai, with one being King Clayde himself, and the other being this Lord Kaiser.

Because of Lord Kaiser’s high status, there was no need for him to live long-term in the palace.

“Yes, Second Bro! You go protect his Majesty. I’ll find Lord Kaiser.” The jade-haired man immediately sped off.

“Snow Lion, let’s go.”

The four of them immediately sped off with the Snow Lion in the direction of Linley’s manor.

Within Linley’s manor. Right now, within Linley’s ‘recuperation’ courtyard, aside from two corpses, only Linley and Clayde were present.

“No…how do you know that my mother is dead? Didn’t you say you gave my mother to another person, a person even you dared not offend? I don’t believe that a person like that would abduct my mother just for the purpose of killing her.” Linley refused to believe it.

His father was already dead. Linley didn’t want for his mother to be dead as well.

Deep in his heart, Linley thirsted for his family to be alive!

“Haha…” Clayde began to laugh while looking at Linley with pity in his eyes. “Linley, I can tell you clearly, right now, that person didn’t instruct me to abduct your mother for him. I did it on my own initiative, abducting your mother, then gifting her to him. Because I knew…he really needed women like her.”

“And I also know very well that in the past, this lord had acquired quite a few women like your mother. And all of them, without exception. Perished.” A hint of mad laughter was in Clayde’s eyes.

Linley seemed to have been hit by a bolt of lightning. His body swayed.

“Without exception?” Linley stared at Clayde.

Clayde looked at Linley with pity in his eyes. “Linley, you should’ve had an extremely resplendent future. But you insisted on choosing this path. Since you’ve already chosen this path, your future has now been determined as well.”

“Haha…..hahahahahahahaha!” Linley suddenly began to laugh loudly, all of the muscles on his face twitching.

Linley stared at Clayde with eyes like death. “Clayde. It was you. You were the one who harmed my mother, and in the end caused my father to die. If it wasn’t for you, I probably would be enjoying a wonderful life with my parents right now. It was you. It was all you. It was you who ruined–”

Linley’s hand stretched out, grabbing a straight chisel by his side.

“What are you planning to do?” Clayde stared at Linley with his tiger-like eyes.

“What am I going to do?” Linley stared at the straight chisel in his hands. “In the past, I always engaged in stone sculpting. But today…I want to try flesh sculpting.” Linley’s eyes had already begun to turn a dark, gold color, just like those eyes of the Armored Razorback Wyrm. Heartless. Cold!

Within the Coiling Dragon ring, Doehring Cowart continued to maintain his silence.

Having watched Linley grow up, Doehring Cowart understood Linley very well.

Linley deeply valued his family and his bros. For the sake of his family and his bros, Linley wouldn’t fear death. Right now, the man responsible for the deaths of his mother and father were right in front of him. It was impossible for Linley to remain calm at a time like this.

“Flesh sculpting?” Clayde was startled. Linley’s gaze was fierce, and he carefully inspected Clayde’s entire body. “Don’t worry. You have such a strong, powerful body. I am confident that I will be able to slice you a thousand times before I let you die, as a woman.” Linley’s voice was freezing cold, and the murderous aura rolled from him in waves.

“You!” Clayde’s face turned icy cold as well, and he viciously snarled, “Linley, I will definitely kill you and let you reunite with your two unfortunate parents.”

“Reunite?”

Thinking of his parents, Linley’s urge to kill grew only stronger.

“Have a taste of my straight chisel technique.” Linley’s face appeared to be covered by a layer of frost. With a wave of his hand, he sent the straight chisel directly towards Clayde’s waist. But once the straight chisel got within ten centimeters or so of Clayde, it was suddenly impeded by a strange force.

A translucent sigil suddenly appeared in mid-air, easily blocking Linley’s chisel. “What is this?” Linley was totally shocked.

“I told you. I will definitely kill you.” Clayde stood up, looking at Linley arrogantly. His powerful body made him look like an enraged lion.

“Impossible.”

Linley’s body erupted with Dragonblood battle-qi, and the straight chisel in his hands chopped viciously towards Clayde’s body.

“Swish! Swish!” Seven chops in a row, all aimed at a different part of Clayde’s body. But no matter where he chopped, his chisel would be blocked by that translucent pattern at around ten centimeters away from Clayde’s body.

“You don’t have the ability to kill me.” Clayde said arrogantly.

“Raaaargh!” On Linley’s shoulders, Bebe’s mouth suddenly widened and expanded as he viciously bit down at Clayde. Facing Bebe’s bite attack, Clayde didn’t seem afraid in the slightest. Perhaps he was simply too confident in the power of this defense, as he didn’t even try to dodge.

When Bebe’s fangs crunched down against that translucent defense, the translucent barrier suddenly glowed with the seven colors of the rainbow for a moment, and then the colors vanished.

“Hrm?”

The expression on Clayde’s face changed. “What a powerful attack.” Clayde didn’t dare to let Bebe bite him again, and he quickly charged towards the outside.

“Boss, attack him, attack him! That defensive barrier on his body isn’t innate to him. It must be some sort of magical spell from a scroll or something. There’s got to be a limit to how much it can take! Your attacks will whittle away its energy, and once the energy is gone, he will definitely die!” Bebe frantically urged Linley.

Linley immediately understood this logic.

“You want to escape?!”

Linley’s skin suddenly began to be covered by black scales, and those sharp spikes began to jut out from his elbows and kneecaps. A long, iron-whip-like tail sprouted from behind him, and on Linley’s back, a row of spikes erupted from his spine.

Dragonform. Total Dragonform!

Even in his normal state, Linley was already a warrior of the seventh rank. After Dragonform, he was an early-stage warrior of the ninth rank.

“Swish!” Linley kicked off from the ground, and as he did, the marble beneath his feet cracked. Transforming into a blur, Linley charged directly at Clayde. Right now, Clayde was only able to rely on that comparatively pitifully small amount of muscle power to run, and thus couldn’t move at high speed.

Linley’s powerful, scale-covered right arm swept its claws ferociously at Clayde.

“Whap!” A terrifyingly powerful force smashed against Clayde’s defensive barrier. Although this barrier was able to protect Clayde, it would still be impacted by the momentum of the force. It was as though Clayde was inside an incredibly sturdy carriage. When others attacked the carriage, although Clayde wouldn’t be harmed, the carriage would be sent flying in a certain direction. Naturally, Clayde would be sent flying as well.

This was exactly that sort of situation.

Clayde’s body was sent flying forward, then smashed directly into wooden screen. The wooden screen totally disintegrated from the power of this blow, but Clayde wasn’t harmed at all. He rolled to his feet.

“Dragonblood Warrior. You actually can transform into a Dragonblood Warrior.” Seeing Linley having truly Dragonformed, Clayde was totally stunned.

Before, Linley’s strength wasn’t that impressive. But after having taken on the Dragonform, he actually possessed the power of a warrior of the ninth rank. The fame of the Supreme Warriors really wasn’t hollow.

“I can’t let this continue. Otherwise, this Fateguard is going to collapse.” The thing which Clayde counted on the most was this Fateguard. In the past, the Holy Emperor himself had bequeathed it to Clayde. This Fateguard came from one of the finest defensive magical scrolls in existence, and was powerful enough to allow Clayde to withstand a single blow from a Saint-level combatant!

Capable of blocking a full-power attack from a Saint-level combatant. As for a ninth-rank combatant, it could take dozens of blows before shattering.

“Clayde, I refuse to believe that the energy of your magical armor is endless and infinite.” The totally Dragonformed Linley walked towards Clayde, step by step.

Seeing Linley with spikes jutting from his back, his entire body covered in scales, and in particular with that long, whip-like tail, Clayde felt he had encountered a human-shaped magical beast. In the past, he wouldn’t have been the slightest bit afraid, but right now, he had less than a tenth of his usual power!

“Whoosh!” Clayde suddenly scurried forward, flying towards a window.

“Swish!”

Linley’s draconic tail swept over viciously. Despite moving later, it arrived first, landing directly on Clayde’s body. Clayde’s body was sent flying, smashing viciously at a corner of the window. Breaking through the window, Clayde’s body was sent rolling into courtyard. With a leap, Linley flew out as well, the ground beneath his feet splintering from his jump.

“You still want to escape?”

Linley’s Dragonformed claws and legs all ferociously attacked Clayde, while at the same time, Bebe continuously bit and scratched at Clayde, trying to whittle away the energy in his defensive barrier as quickly as possible.

Relying on his significant combat experience, as well as his natural strength as a warrior of the seventh rank, as well as the defensive power of the Fateguard, Clayde did his best to dodge Linley’s blows and delay as long as he could.

“Protect his Majesty! Protect his Majesty!”

“Roaaar!”

From outside, the sounds of many people shouting could be heard, as well as the roar of a magical beast.

“Linley, today, you are doomed to die.” Clayde was exultant. By now, he could sense that his Fateguard had only expended half of its energy. It had more than enough to continue to block Linley’s attacks. Linley’s gaze grew even colder.

“If one comes, I’ll kill one. If two come, I’ll kill a pair. I will kill however many come!” Linley’s killing intent had boiled to a crescendo.

“Whap!” Linley’s draconic tail smashed viciously down on Clayde, sending him flying into the courtyard’s wall, which immediately began to crack. At the same time, the sharp claws of a black blur fiercely swiped down at Clayde’s body, smashing Clayde hard against the ground yet again.

“Crash!”

The closed gate to the courtyard suddenly split open, sending its shattered shards flying everywhere. A five meter long, three meter tall lion with a body of pure white fur charged inside. From its mouth, it spat out hundreds of javelin-sized jade-blue spikes, while behind it, a group of palace experts charged in as well!
