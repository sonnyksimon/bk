#### Book 6, The Road to Revenge – Chapter 11, The Man Behind the Curtain

His entire body was covered in black scales, while sharp black spikes jutted out from his elbows and his knees. His entire back was lined with a row of sharp spikes coming from his spine. And his eyes had turned a dark golden color. Just seeing that cold, dark, golden set of eyes staring at him was enough to terrify Duke Patterson.

“Who are…who are you?” Duke Patterson was so terrified that his face was ashen white. His mouth flapped for a long while before he managed to say these words.

What was this monstrosity in front of him?

“Who am I?” Linley’s cold gaze was fixed on Patterson.

“Squeak, squeak.” The sounds of bones chattering emanated from throughout Patterson’s body, as Linley continued to apply force through his iron-whip-like tail in constricting Patterson. No matter how hard Patterson struggled, he couldn’t budge at all.

Pain began to spread from his arms to the rest of his body.

“You are from another plane?” Patterson’s eyes were filled with terror. From what he could tell, based on Linley’s current appearance, only a different species from another plane could do what Linley had just done. “Linley, I beg you, spare me, spare my life. I definitely will keep your secret, definitely.”

Transfixed by Linley’s dark golden gaze, Patterson had totally lost his equanimity.

“Spare your life?” A hint of a cold smile appeared on Linley’s face. “That’s not impossible. I want to ask you something. Around twelve or thirteen years ago, did you send some people out to kidnap a woman.”

Patterson was startled.

He immediately frantically tried to recollect the affairs of twelve or thirteen years past, but twelve or thirteen years was an extremely long period of time. Most importantly… “Linley, no, Lord Linley, I…I can’t remember.” Patterson said frantically.

“That was a long time ago, and I often would have women I took a fancy to captured and brought to my mansion. I don’t know exactly which one you are talking about.”

That murderous intent in Linley’s heart began to grow.

This Patterson actually often abducted women?

From Linley’s face, Patterson had no idea as to the transformation that was currently occurring in Linley’s heart. Having completely undergone the Dragonform, Linley appeared totally cold and emotionless, terrifying sinister.

“A woman who had just given birth not long beforehand, who had just finished a pilgrimage to the Radiant Temple, and then returned to her hotel.” Linley still stared icily at Patterson. His voice didn’t rise at all.

Hearing Linley say these things, Patterson’s entire body went stiff. And then he stared at Linley in astonishment.

“You remember now?” Linley said coldly.

Of course Patterson remembered now. Throughout all these years, he had only abducted women who had just given birth on two occasions. His memories of these affairs was quite keen. Especially that one time, thirteen years ago. That time, the person whom had instructed him to act had severely warned him to maintain secrecy.

“I really can’t remember.” Patterson said, terrified. “Lord Linley, I beg you, spare me. I really don’t know. You must be mistaken.”

Linley’s dark golden eyes flashed.

“You want to die?” Linley’s voice grew even colder.

“Ahhhh!” Patterson’s screamed in terror as Linley’s tail increased the pressure around him. This greater pressure was causing all of the bones in Patterson’s body to moan in protest.

“Clatter. Clatter.” The sound of bones nearly cracking was enough to make one’s heart shudder.

But Linley still only stared coldly at Patterson.

“Crunch!”

“Ahhhhh!”

The crisp sound of a bone snapping, mixed with the tortured screams of Patterson. His left arm bone had actually been snapped clean by this terrifying pressure.

“Not bad.” Linley’s lips quivered slightly. As though he were smiling.

But Patterson didn’t view it as a smile. Under the Dragonform, the slight curve of Linley’s lips only filled Patterson’s with even more fear.

“You know what matters and what doesn’t. The vast majority of your battle-qi has been used to protect your vital organs. Only a small amount of battle-qi was used to protect your arm. It’s true. A broken arm isn’t a life-threatening condition. But if your organs were to rupture, then you really will lose your life.” Linley’s voice was very calm.

Patterson felt his throat go dry.

He had never imagined that Linley would have such a terrifying side.

“Now, do you remember yet?” Linley asked again.

Patterson really wanted to answer him, but when he thought about the punishment which would await him if he spoke, he couldn’t help but shudder. His face growing still more pitiful, he cried out miserably, “Lord Linley, I beg of you, don’t torture me. I really don’t know. Even if you kill me, I still don’t know.”

Patterson firmly believed that, with this affair having been over thirteen years ago and Linley being so young, there was no way Linley could be certain about what had happened.

Most likely, Linley had received some sketchy details and was not absolutely certain. As long as he clenched his teeth and refused to speak, perhaps Linley would believe him in the end.

“Lord Linley, if I knew, I would’ve told you long ago, and avoided all this suffering. Lord Linley, I beg of you, please investigate this matter clearly.” Tears began to pour out of Patterson’s eyes, and his face was a picture of sincerity. If it weren’t for the fact that Linley had read that letter from his father, he might really have hesitated.

Staring at Patterson, Linley’s lips began to curve upwards even more.

Patterson’s heart felt a sudden chill.

“Good. Wonderful.” Linley’s tail was still wrapped around Patterson. Suddenly, the draconic tail sent Patterson smashing directly, viciously into the stone floor. Fortunately, though, Linley smashed Patterson feet-first, rather than head-first.

Linley gave full reign to the power of his draconic tail!

Patterson’s two legs smashed against the stone floor.

“Crush!”

The sound of bones splintering instantly, mixed with Patterson’s terrifying, high-pitched howls of agony.

On Patterson’s left knee, the shattered white bone was visible to the eye, piercing both through his leg and his pants. His right leg, even worse off, simply lay limply on the ground, while blood stained his pants around the ankles in particular.

“Ahhh! Ahhhh! Ahhhh!” Patterson was screaming nonstop.

This level of pain was killing him. Fortunately, though, his organs had been protected by his battle-qi, and so his life was not yet in danger.

“Demon. Demon.” Patterson was cursing nonstop in his heart. He knew what a tremendous force Linley was using. Based on his strength as a warrior of the seventh rank, he was only just barely able to protect his internal organs with his battle-qi, and couldn’t protect the rest of his body.

Patterson didn’t want to die.

Crippled legs?

Not a problem. With enough money, he definitely could invite an Arch Magus of the ninth-rank of the Radiant Church to use the ‘Song of Life’ on him. As long as he didn’t already die, any wound, no matter how serious, could be healed!

“Do you remember yet? That woman you abducted?” Linley’s voice was still very calm, not rising in the slightest.

But the terror in Patterson’s heart was growing.

“I remember. I remember.” Beads of sweat were flowing down Patterson’s face. Not from pain. From fear.

Patterson knew very well that in this sealed underground room in which he and Linley were currently in, nobody outside could hear anything, no matter how loud the screams. Perhaps someone directly outside, leaning against the stone door, could just barely hear something.

But who would be outside of this secret little room, pressing their ears against the stone door?

No matter how loud he screamed, no one would know.

“If you said so earlier, wouldn’t you have suffered less?” Linley’s dark golden eyes stared peacefully at Patterson. “Speak, then. Explain what happened to me.”

Patterson hurriedly nodded. “Lord Linley, that year, that woman was extremely beautiful. I was bewitched, and hatched an evil plot to abduct that woman and bring her back to my place. I wanted that woman to sleep with me, but she was too headstrong. She committed suicide by ramming her head against the stone wall.”

Stuttering as he spoke, Patterson looked at Linley.

In Patterson’s opinion, there were very few people who knew what had really happened to that woman. Linley shouldn’t have had any clue.

“You continue to lie!!!”

Linley finally grew angry. Those dark golden eyes seemed to slowly turn red. Using his draconic tail, Linley brought Patterson directly before him. Linley all but pressed his face directly against Patterson’s, coldly staring into his eyes.

Pressed against Linley, seeing Linley’s black scales and the black horn on his forehead, Patterson grew even more terrified.

“I’m not lying! I’m not lying!” Patterson hurriedly said.

Linley’s hands, already transformed into claws by the Dragonform, suddenly delivered a mighty slap to Patterson’s face.

“THWACK!” Five pieces of flesh were ripped from Patterson’s face, and blood began to flow out in a steady stream. Fortunately, Linley wasn’t trying to kill him. Otherwise, he would’ve crushed Patterson’s brain to a pulp with this blow.

“Sob…sob…sob…” Patterson was in so much pain that his voice changed.

Linley stared coldly at Patterson. “Patterson, listen closely. I already know very much about what had happened, which is why it’s best for you not to lie to me. Otherwise, the torment you will suffer definitely will not be limited to just this. Let me tell you this. The woman that you abducted was my mother!”

“Mother?” Patterson was stunned, even forgetting his pain for the moment.

“I am very clear about what happened that day with my mother, and I have been investigating this entire time. Thus, it’s best if you tell me everything about what happened to my mother. Otherwise…you will definitely die.” Linley’s voice grew even more freezing.

Actually, no matter what Patterson said, he was still definitely going to die.

Because Linley’s father had been pursued and heavily injured by Patterson’s men, and had died as a result. Patterson didn’t yet know that the person he had sent people out to hunt and kill was Linley’s father. If he had known…perhaps Patterson would be reacting in a totally different way.

“Tell me. Who did you give my mother to?” Linley stared at Patterson.

“You knew?” Patterson’s face turned pale.

Linley actually knew that he had given the woman away to someone else?

“Tell me his name, but you’d best not lie to me. If I discover that you have lied to me, I will make your life worse than death.” Linley’s voice was very calm again, not rising in the slightest.

Patterson hesitated for a moment.

“There’s no use for me to tell you. You can’t kill him.” Patterson said in a low voice.

“Can’t kill him?” Linley stared coldly at Patterson. “Patterson, listen to me. All you have to do is tell me who that person is. As for whether or not I can kill him, that’s none of your concern. Do you think you know what my real level of ability is?”

Hearing these words, Patterson secretly agreed.

The ‘Linley’ in front of him was too terrifying. The power he had previously displayed had already made others believe he was an absolute genius. But apparently, Linley’s real power was far greater than that of a warrior of the seventh rank. In front of Linley, he didn’t have the slightest ability to resist.

Patterson began to furiously calculate in his mind.

Linley didn’t rush him, only fixing Patterson with his dark golden gaze.

After pondering a long time, Patterson gritted his teeth and looked at Linley. “Linley, I’ll tell you who he is, but you have to guarantee that you definitely won’t let anyone know that I was the one who told you! And, you have to promise you won’t kill me.”

Linley’s face was still as cold as ever. “Fine. I guarantee that I will not tell anyone that you were the one to tell me. And, I guarantee I will not kill you.”

Only now did Patterson secretly let out his breath.

“About twelve years ago, on one occasion, we members of the royal clan of Fenlai went to pay a visit to the Radiant Temple. Within the Radiant Temple, we saw your mother. Afterwards, I sent people to abduct your mother.” Patterson immediately said, “But that wasn’t actually my own intent. I was obeying the orders of another.”

“Who?” Linley asked.

Patterson glanced at Linley. He slowly said, “The orders came from my elder brother. The current ruler of the Kingdom of Fenlai. King Clayde.”

“Clayde?” Linley was startled.

The pride of the Kingdom of Fenlai, the ‘Golden Lion’, Clayde? The warrior of the ninth rank, Clayde?

“Yes. It was Clayde.” Patteron said with certainty. “But I know that Clayde valued your mother highly. He even told me that no matter what, I couldn’t let this information out, as if I did, I would definitely die.”

Linley looked at Patterson.

“He should be telling the truth.” Doehring Cowart’s voice rang out in Linley’s mind. “I can sense the vibrations of his soul.”

Linley made up his mind.

Patterson looked beseechingly at Linley. “Linley, can you spare my life? I guarantee that I definitely won’t say a single word about what happened today to anyone.” Patterson’s eyes were filled with hope.

“Fine. I’ll keep my promise.” Linley’s draconic tail loosened.

Patterson’s body dropped to the floor. A look of wild joy appeared on Patterson’s face, and he looked at Linley with eyes filled with gratitude.

Right at this moment, a black blur flashed by.

“Crunch.”

The little Shadowmouse, Bebe, bit Patterson’s neck. Patterson stared with terror at Bebe. He had just escaped from death’s door, but now, he could already seem to feel the call of the Netherworld. Patterson could tell that the little Shadowmouse was the one which was always on Linley’s shoulders.

Disbelievingly, Patterson stared at Linley.

“I said I wouldn’t kill you. But I never said my magical beast wouldn’t kill you.” Linley looked coldly at Patterson, whose throat was spurting out blood. “Let me tell you something else as well. Several months ago, there was a man who snuck into your Duke’s mansion. Afterwards, you sent people after him to kill him. And that man…was my father!”
