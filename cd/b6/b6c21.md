#### Book 6, The Road to Revenge – Chapter 21, The Trial

Nimitz, Kalan’s two uncles, Kalan himself, Rowling, and Alice didn’t dare to seat themselves with their host absent. They simply waited quietly in the main hall.

“Kalan, when Lord Linley returns, you must remember to be a bit more humble.” Nimitz glared coldly at Kalan.

Kalan nodded. “Second Granduncle, I know.”

In actuality, Kalan’s heart was still filled with enmity towards Linley. After knowing the reason why he had been released from jail, he felt even more rage towards Linley!

“I would rather stay in that jail than have Alice go beg him!” Kalan’s heart was filled with fury.

In the past, when Linley and Alice had been together, Kalan began to hate Linley. After he took Alice back, he felt a bit smug. In his eyes, although Linley was quite formidable, when compared to his Debs clan, Linley was not even close to being on the same level. But after just a few months, Linley’s status had totally changed, becoming the brightest star within the Kingdom of Fenlai at one leap. Even his Majesty the King of Fenlai, and Cardinals of the Radiant Church, treated Linley with warmth. Even his own father acted so humbly towards Linley. All this filled Kalan’s heart with even more hatred.

They were both young men. Why was he so inferior?

Especially this time!

He had languished in prison. Although he ended up escaping, it had required Alice, the woman he loved dearest, to go beg Linley to free him.

This caused Kalan to feel humiliated. He very much wanted to not accept Linley’s kindness and continue to stay in that jail. How he wished he could angrily curse at Linley, or even kill Linley!

But for the sake of the clan, he, Kalan, had come humbly to Linley’s manor, and couldn’t even act the slightest bit disrespectfully.

Footsteps could be heard.

Kalan immediately cast aside his angry musings. Forcing a smile onto his face, he made himself appear courteous and modest.

“Forgive me for keeping everyone waiting.” Linley’s clear voice rang out.

Nimitz and the others all turned to look. Clearly, Linley had just washed. His hair was wet, and he was casually wearing a loose robe.

“You can all sit.” Linley comfortably sat down, gesturing casually with one hand.

Nimitz and the others all quickly expressed their thanks, then sat down. Nimitz was the first to smile and say, “Lord Linley, the purpose of our visit this time was to thank you. If it wasn’t for you, Kalan most likely wouldn’t have been able to get out this quickly. Kalan, hurry up and thank Lord Linley!”

Kalan was forced to rise to his feet again. Suppressing the anger in his heart, he forced himself to act humbly. “Thank you, Lord Linley.”

Linley smiled at Kalan. “Kalan. No need to thank me.”

“Mr. Nimitz. Very shortly, I’ll have to attend to some important affairs. I don’t know if you had any other purposes behind this visit? If you do, I hope you can speak of them now.” Linley smiled towards Nimitz.

In truth, Linley simply didn’t want to waste any time with these people. His time was meant to be reserved for training.

Nimitz was startled, but then he quickly adjusted. In a low voice, he said, “Lord Linley, our Debs clan has been framed and falsely accused of engaging in the smuggling of water jade. At this point, it’s very possible that our Debs clan will be entirely eradicated. Thus, our clan would like to beg you, Lord Linley, for your assistance. Once our clan overcomes this critical threat, we definitely will not forget your great kindness to us.”

As he spoke, Nimitz pulled out a black box from his side.

“Lord Linley, this is a very small gift from us to you as our thanks for your rescuing of Kalan. If our clan manages to survive this tribulation safely, we will once again show our gratitude towards you.” Nimitz sincerely held out that black box for Linley to look at.

“Swish.”

The little Shadowmouse, Bebe, suddenly scurried in front of Nimitz, and actually directly grabbed the box, then jumped onto Linley’s legs, planning on opening it up.

“Bebe!” Linley let out a low shout.

Bebe raised his head, staring at Linley unhappily. He didn’t open the box, only let out a few ‘hmph’ sounds, then fell silent.

“Mr. Nimitz, Bebe is rather naughty and mischievous. I’ll accept this gift, then, and offer my thanks to you.” Laughing, Linley put the black box off to one side, not even glancing at it.

Nimitz could sense that Linley was getting impatient.

Immediately, Nimitz glanced meaningfully at his companions, then was the first to stand up and bow. “Lord Linley, we won’t disturb you any further. This case involving our Debs clan will be tried a month from now. I hope that at that time, you can assist us, Lord.”

Linley casually nodded.

Nimitz and the others immediately left. That entire time, neither Alice nor Rowling had said a single word. Nimitz was the primary speaker.

Watching the group leave, Linley laughed coldly. “Nimitz, you old scoundrel. Did you think that by bringing Alice, I’d give you more face?” Linley flipped open the cover to the black box. Within it was a magicrystal card and a letter.

“A letter?”

As he toyed with the letter in his hands, a burst of flame suddenly erupted from his palms, incinerating it and turning it to ash. Linley couldn’t be bothered reading the letter.

Time passed quickly. September arrived.

This entire past month, Linley had focused on his training. His strength, agility, and other aspects of his body had all improved. The Dragonblood battle-qi in his dantian had become more pure as well.

Linley had the feeling that he had reached the late-stage of the sixth rank.

As far as his growth in spiritual energy, although Linley’s advancement rate was extremely rapid, even a genius would normally need around twenty years of training to advance from the seventh rank to the eighth rank. Despite his rapid improvement, a few months of growth wasn’t very noticeable.

The path of the magus was indeed a long, difficult one.

Within the Hot Springs Garden, the shadow of a chisel could be seen, and a human-shaped sculpture was become more and more clearly defined. Bits of rubble flew about in every direction, falling onto the grass. Suddenly, Linley came to a halt, withdrawing his chisel.

“Whew. Finally done.” Looking at the sculpture in front of him, Linley nodded with satisfaction.

This sculpture, which Linley had named the ‘King of Killers’, had truly cost Linley a great deal of effort. Each time, Linley had forced himself to totally enter the right state, so as to more perfectly carve out the statue of Cesar making his move.

The statue in front of him was as tall as a person.

Those two cold, calm eyes in particular gave people the sensation of being watched by a god. The aura emanating from this sculpture was the aura of a God of Death. Under the gaze of this sculpture, viewers would unconsciously feel a terrible, cold dread.

“Although this sculpture isn’t comparable to ‘Awakening From the Dream’, it is the most perfect statue that I can make while in a normal state.” Linley was extremely satisfied with this sculpture. He had spent an entire month on it, carefully, attentively sculpting. At last, it was completed.

Putting down his straight chisel, Linley soaked for a while in the hot springs, then put on a loose robe and sat on top of a chair. He was eating the breakfast which his attendants had brought him.

“Linley.” Doehring Cowart flew out by his side.

“Grandpa Doehring.” Linley looked at Doehring Cowart.

Laughing, Doehring Cowart said, “Linley, there’s two days left before the trial of the Debs clan’s case. Do you plan to go watch?”

“The trial?” Linley was startled.

This month, he had been absorbed in his bitter training. Linley had totally forgotten about everything else, including the Debs clan’s case. If it weren’t for Doehring Cowart’s reminder, Linley probably wouldn’t have remembered it at all.

“Yes, of course I’ll go.” A hint of a smile was on Linley’s face.

Year 9999 of the Yulan calendar. September 9th. Within the Blackwater Jail of Fenlai City.

The Blackwater Jail was the most famous jail in the Kingdom of Fenlai, and it was the most securely guarded jail. The cases awaiting trial at the Blackwater Jail were also the most important cases in Fenlai.

Within the Blackwater Jail’s courtyard, today there were many nobles congregating. Even his Majesty, King Clayde, had arrived, and was seated to the side, watching. Naturally, Linley came today as well.

“Lord Linley.” One noble after another greeted him warmly.

“Linley, come, sit with me.” Seated in front, Clayde gestured toward Linley. Linley smiled at Clayde, then walked over.

Linley sat down next to Clayde.

Merritt, his hair gleaming, sat at the judge’s seat. His waist and back were ramrod straight. He really did give the impression of being fair and impartial. “Everyone, please sit.” Merritt nodded and smiled towards the noble spectators who had gathered here. In particular, Merritt smiled modestly towards the direction of Linley and Clayde.

The noble spectators all sat down quietly. Today, more than ten people had come from the Debs clan. All of them were seated together, nervously watching the proceedings.

“Bring Bernard.” Merritt ordered directly.

Very soon, under escort by two soldiers, Bernard was dragged to the court, hands and feet both shackled.

Merritt glanced at a nearby official, who quickly strode forward. In a loud voice, he proclaimed, “Duke Patterson, when he was the Minister of Finance, acted in many ways against the benefit of the kingdom. In particular, he is suspected of colluding with the Debs clan in the smuggling of water jade. The scale of this smuggling operation is larger than any since the founding of our Kingdom of Fenlai. We have already discovered that the valuation of the smuggled water jade was greater than fifty million gold coins!”

In actuality, the Debs clan had just begun their smuggling program. Although the valuation was fifty million gold coins, in reality, the Debs clan had only spent a few million gold coins thus far. From this, one could tell what enormous profits lay in the smuggling trade.

But just as their smuggling activities had begun, Duke Patterson had died, resulting in this being revealed.

The official continued, “Based on our investigations, one of the main organizers of this smuggling activity jumped into the river, while the other two were the brothers Lanseer and Langmuir.”

Finishing, the official sat back down.

Merritt looked at Bernard. “Bernard, do you have something to say for yourself?”

Bernard nodded. “Yes, lord, I do. First of all, it was not our Debs clan which engaged in smuggling. Secondly, the Lanseer brothers had been expelled by our clan long ago. Thirdly, the primary mover behind this smuggling operation should’ve been that person you said jumped into the river. There is no link to our Debs clan at all.”

Merritt nodded and laughed. “The organizer of this smuggling operation was your third brother. And you say this has nothing to do with you?”

“Third brother? My third brother is still adventuring in the wilds. How would he have the chance to engage in smuggling?” Bernard continued to insist on this point.

“Your third brother is engaging in adventuring?” Merritt’s face grew cold. “Then let me ask you, if your third brother is outside adventuring, then why, despite me ordering your Debs clan to summon him back, hasn’t he returned after such a long period of time?”

Bernard said confidently, “My third brother is adventuring in other kingdoms. Most likely, he’s travelled too far. It is normal for us to need more than a year to find him.”

Merritt glanced at Bernard, chuckled, then said coldly, “Bring in Catson [Ka’te’sen] and the other two.”

“Catson?” Bernard was suspicious. Who was Catson and who were the other two?”

Very shortly, three very cowering youths entered the court, falling to their knees immediately as they said respectfully, “Greetings, Lord.”

These three youths clearly were peasants who had seen very little of the world before.

Merritt said calmly, “Catson, clearly explain what you saw happen.”

“Yes, Lord.” The leader of the youths said respectfully. “On June 28th, we three bros were fishing on the river, but suddenly, we saw a richly dressed noble lord clutching onto a dead tree trunk float by us. This noble was covered in blood and had already passed out.”

Upon hearing these words, the expression on Bernard’s face changed.

“The day that we pursued the leader of the smugglers was June 28th as well. As it just so happened, the leader jumped into the river.” Merritt looked at Bernard. “Bernard, are you willing to admit guilt yet?”

“My third brother is adventuring in distant lands. He definitely wasn’t organizing any smuggling activities. My Debs clan is definitely innocent.” Bernard still held his head up high and maintained his innocence.

Merritt laughed coldly, then said, “Bring Kanter [Kan’te] Debs.”

Hearing the name ‘Kanter Debs’, the faces of Bernard as well as the members of the Debs clan present all immediately turned white.
