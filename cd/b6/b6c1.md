#### Book 6, The Road to Revenge – Chapter 1, The Dusty Affairs of the Past

Hillman was being gripped so tightly by Linley’s claws that his clothes were torn open. Scarlet blood slowly leaked out, staining his clothes red.

But Hillman didn’t notice in the slightest.

Staring at Linley, Hillman said in a downcast voice, “Linley, calm down first.”

“Tell me.” Linley was staring at Hillman.

Hillman said solemnly. “The troop of Knights following you is about to arrive. For now, let’s not allow others to know about the affairs of your clan. Come with me first.” Hillman shook his shoulders loose of Linley’s claws, then grabbed Linley’s scaled arms and with the intention of pulling him to the ancestral halls…only to find that he was unable to budge Linley.

“Linley!” Hillman turned his head, a spark of anger in his eyes.

“Uncle Hillman, I know how to act.”

Linley’s face was deeply sunken, but he took a deep breath, retracting the scales on his arms into his body, returning to normal. Just as he once more returned the ‘Slaughterer’ to his case and held it, Linley could hear the sounds of hoof steps outside drawing near.

The troops of Knights of the Radiant Temple had finally arrived.

Linley turned, glancing at them coldly, but paid them no mind. He said directly to Hillman, “Uncle Hillman, lead the way.”

“Alright.”

Seeing that Linley was able to calm down, Hillman felt a little bit better. He immediately led Linley into the ancestral hall’s direction. Linley’s face remained sunken. At this moment, aside from Linley himself, perhaps nobody knew that beneath that calm expression, there lay hidden an incredibly deep, painful wound.

Neither the Shadowmouse Bebe nor Doehring Cowart made a sound.

They were connected to Linley’s soul. Naturally, they could feel the unimaginable grief and pain which Linley was currently suffering.

The wind rose, catching up and hurling into the air countless leaves which had been lying on the unimaginably ancient stone tiled grounds.

“Creaaaak.”

Hillman pushed open the door to the ancestral hall, then turned to look back at Linley. Holding the warblade ‘Slaughterer’, Linley stepped inside, his face calm. But his gaze was fixed upon those rows of spirit tablets placed in the middle of the ancestral hall. With Linley’s current vision, he could clear read the words on the newest spirit tablet, located at the front.

There were only two words on the front. “Hogg Baruch.”

Linley felt his mind growing dizzy, as though he were having a hallucination. But he still stood there, unmoving. And then, still carrying the ‘Slaughterer’, Linley stepped forward to the stone platform in front of the spirit tablets, placing the ‘Slaughterer’ on top of the platform.

Linley looked at the spirit tablet, a peaceful smile appearing on his face. In a soft voice, Linley said, “Father. I’m back.”

“I know that all your life, your greatest desire was that we recover our ancestral heirloom, as well as regain the bygone splendor of our clan, the Dragonblood Warrior clan.” Linley spoke very carefully, as though he were afraid to startle someone. His voice was so gentle, so careful.

Linley stared at the spirit tablet. “I didn’t disappoint you. I have already brought back to the Baruch clan, to the Dragonblood Warrior clan, our ancestral heirloom, the warblade ‘Slaughterer’.”

“Now…I have already brought back the ‘Slaughterer’. And very soon, I will restore our Dragonblood Warrior clan to glory. I will make sure the entire Yulan continent knows of the splendor of our Dragonblood Warrior clan, and will make sure everyone in the Yulan continent knows your name.”

“All of this, I will accomplish. I so swear.”

Suddenly, a fiendish look appeared on Linley’s face. “But of course, before I do all of these things. I will avenge you.”

There was no question at all in his mind. His father, Baruch, had been killed by someone.

Otherwise, based on his father’s prowess as a warrior of the sixth rank, as well as a man in the prime of his health, he couldn’t have died due to any ordinary illness. And what’s more, if he had died of illness, Hillman wouldn’t have acted so secretively. Linley’s intuition was telling him that his father’s death was no ordinary death!

“The person who caused you to die. I will make sure he dies as well!”

Within Linley’s eyes, once more there seemed to be a hint of that cold, dark gold color of the eyes of the Armored Razorback Wyrm. That terrifying dark golden color.

Linley fiercely turned to stare at Hillman. “Uncle Hillman, tell me. How did my father die, exactly? In addition, where was my father buried? Also, you said my father died three months ago? Why didn’t you tell me?”

Hillman opened his mouth, but did not speak.

“Linley, first calm down,” Hillman finally said slowly.

Calm down?

How could he calm down?

“I wish so much that my father could be here and personally see this warblade, ‘Slaughterer’, with his own eyes. I long to tell my father that I have become a Dragonblood Warrior. I deeply desire to see my father’s smile, hear his gratified laughter. See the pride on his face when I assume the Dragonform! However…all of this is now impossible.”

Linley felt as though his heart had been sliced by knives.

And Hillman was asking him to calm down?

Linley wanted to angrily rebuke Hillman, but he restrained from doing so. Taking in a deep, unwilling breath, he swallowed his rage. Staring at Hillman, Linley said, “Uncle Hillman, tell me everything which happened. I want to know everything.”

“Your father died three months ago. But before he died, his instructions to me were that only after you had the power of a warrior of the seventh rank could I tell you. Otherwise, I cannot tell you the circumstances surrounding his death.” Hillman said solemnly.

“A warrior of the seventh rank?”

“Yes.” Hillman nodded slightly. “This was the reason why I went to the Institute to look for you, but didn’t inform you of your father’s death or why he died. Your father’s dying wishes were that I was not to allow you to know of his death, so that you could calmly focus on your studies.”

Hillman looked at Linley. “Linley, it isn’t that I’m not willing to tell you. It’s that this was your father’s dying wish. I cannot go against it. Only if you are able to become a warrior of the seventh rank, would I be willing to tell you everything.”

Linley understood.

A warrior of the seventh rank?

Linley withdrew a leather-wrapped book from his clothes and handed it to Hillman.

“This is?” Hillman looked at it with surprise.

“A magus’ proof of rank.” Linley’s face was calm.

Every single magus, from the day he began to be evaluated, would be issued a certificate with his proof of rank. Each time he advanced a rank, there would be a record of it.

Hillman opened the book and saw that under the ‘wind-style’ and ‘earth-style’ entries, there were seven stars.

“Seventh rank…a seventh rank dual-element magus?” Hillman was stunned. He stared disbelievingly at Linley.

How old was Linley?

Only seventeen.

What did a seventeen year old dual-element magus of the seventh rank represent? Hillman wasn’t too clear on the specifics, but he knew that in the entire Kingdom of Fenlai, the most powerful magus present was a magus of the eighth rank. But that was an old man, well over a hundred years old.

Hillman remembered how, when he joined the army, there was a magus of the seventh rank who had arrived at the same time. He remembered the glory, the pomp of it all.

But now, little Linley, whom he had watched growing up, had become in the blink of an eye a dual-element magus of the seventh rank.

“This…this is real?” Hillman asked an extremely stupid question. Hillman knew very well that this certificate of rank definitely couldn’t be fake.

“Uncle Hillman. Now you can tell me what happened, right?” Linley stared at Hillman.

Hillman nodded, then headed for the private room behind the ancestral hall. A few moments later, he came out. Walking over to Linley, he withdrew an envelope from his clothes. Presenting it to Linley, he softly said, “This was left behind by your father, right before he died. Once you read it, you will understand.”

His hands trembling, Linley reached out and accepted the envelope.

There weren’t any words on the envelope.

He opened the envelope and withdrew the letter. The letter had two full pages of content.

“Linley: By the time you actually read this letter, I most likely would have died a long, long time ago.”

“Towards you and Wharton, my heart is filled with boundless remorse, but there is no way for me to do right by you two any longer. I only hope that you two will be able to live for a long period of time in peace, which is why I have instructed your Uncle Hillman to only provide this letter to you when you have become a warrior of the seventh rank.”

When he read this, Linley’s heart felt sour.

“Let me live for a long period of time in peace? I imagine that father never had expected me to become a magus of the seventh rank so quickly. After all, based on the normal rate of progression, from the sixth rank to the seventh rank would take a considerable amount of time.”

“Linley, within my heart, I have held a secret for many years. Your mother did not actually die when giving birth to Wharton.”

These words from his father caused Linley’s heart to shudder.

Ever since he was a child, Linley had known that his mother had died when giving birth to Wharton. But apparently…that was a lie.

“That year, when your mother was pregnant with Wharton, both of us were very happy. But the medical facilities at Wushan township were simply too poor, so I went with your mother to Fenlai City. Within Fenlai City, your mother safely gave birth to Wharton. Little Wharton was very adorable, and both of us were overjoyed. Shortly after he was born, filled with joy, your mother and I took young Wharton to the Radiant Temple to pray for Wharton to be blessed. That day, both your mother and I were extremely happy. Afterwards, we left the Radiant Temple and stayed overnight at a hotel in Fenlai City.”

“That night, a group of mysterious people came to the hotel and forcibly abducted your mother. Totally outnumbered, I was only able to protect young Wharton…but I did see that on the arm of one of the assailants, there was a red, spider-like birthmark.”

As he read this, Linley himself felt as though he had been transported back to that night, ten years ago.

Under the combined attack of many assailants, unable to ward them all off, his father had only been able to protect Wharton, and could only watch powerlessly, unable to save his beloved wife.

“I know that this group of people was definitely not an ordinary group of people. The weakest of them was a warrior of the fourth rank, while the strongest was even stronger than me. Fortunately, their target was only your mother, as otherwise I would’ve died long ago. Someone capable of mobilizing a squad such as this, definitely would be a major figure in Fenlai City. I didn’t dare to go public on this affair. I took little Wharton back home and told everyone else that your mother died in childbirth. Only your Uncle Hillman and Housekeeper Hiri know this secret.”

Seeing this, Linley’s mind was filled with questions.

Within that gang of people, the strongest was even stronger than his father, but they didn’t care about his father, only about abducting his mother. But why was his mother worth their time to abduct?

“I couldn’t let you know about this. During these past ten or so years, I have always buried this secret deep in my heart. I didn’t dare tell anyone…and I couldn’t even go by myself to investigate your mother’s whereabouts, or to find out if she was alive or dead, or who that group of people was. I didn’t dare.”

His father’s words caused Linley’s heart to feel so much pain that it clenched.

“I am the successor to the leadership of the clan of the Dragonblood Warriors. At the very least, I had to raise you until you were grown. I cannot allow the Baruch lineage to come to an end in my hands. Year after year, I could only secretly endure…but every night, I found it difficult to fall asleep. The question of whether your mother was alive or dead constantly tormented me. I have endured…I have endured eleven years!”

“Linley, you have made me incredibly proud. First, you became a student at the number one magus institute in the Yulan continent. And then, you became one of the top geniuses there, at the Ernst Institute. I am filled with confidence towards you. What’s more, even little Wharton’s density of Dragonblood in his veins has reached the requisite level. I am extremely proud. For both of my sons to be so outstanding…I feel that I have done right by the ancestors of the Baruch clan! But despite all of this, I still did not dare to investigate your mother’s whereabouts, because Wharton still needed a large amount of gold to sustain his costly studies.”

“And so I have endured for eleven years. But when you came back from the Mountain Range of Magical Beasts and gave me that large sack of magicite crystals, I knew…finally, I could give up everything and go investigate whether your mother is alive or not. Although your mother has not come back in the past eleven years, and there is a probably 80% to 90% chance that she is dead, I am unwilling to give up. Even if I die, I will avenge her.”

Seeing this, Linley’s hands began to tremble again.

Linley understood now. In the past, because he had to support the burden of Wharton’s tuition, his father didn’t dare to risk his life in investigating his mother’s whereabouts. But when he, Linley, had brought back that sack of magicite crystals worth 80,000 gold coins, his father no longer had any burdens left.

“Finally able to go investigating, I altered my appearance and put on a disguise as I snuck into Fenlai City. I began investigating what happened that year.”

“But too much time had passed. Knowing that one of the assailants had a red spider birthmark on his upper arm, I spent an entire year searching. Finally, I found that man with the red spider birthmark. Following up on this clue, I continued to investigate. Slowly…I found out who it was that had stood behind this group of assailants.”

“This group of assailants were directed by a member of the current royal clan of the Kingdom of Fenlai. And that person…is none other than the younger brother of the King of Fenlai: Duke Patterson [Bo’de’sen]!”
