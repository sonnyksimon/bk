#### Book 6, The Road to Revenge – Chapter 15, To Be Wronged

Have a private chat with Alice? Whether or not the Debs clan had engaged in the smuggling of water jade, what use would a private chat with Alice be to make that determination? Clearly, this Merritt had other designs. Nimitz was a person with significant worldly experience. Naturally, he knew exactly what was going on.

Nimitz’s eyes narrowed as he stared at Merritt.

But Merritt only casually reclined on his chair, even closing his eyes as he relaxed himself. He didn’t even look at Nimitz. Merritt’s attitude spoke for itself: If you want your family’s ‘grievance’ to be washed clean, then have Alice come talk to me about it.

Nimitz was quiet for a moment, then laughed. “So Lord Merritt is a fan of Master Linley’s ‘Awakening From the Dream’. It is understandable if you want to have a chat with Alice. Fine, I’ll go back and speak with her.”

Upon hearing these words, Merritt opened his eyes, smiling at Nimitz. “Haha, then Nimitz, you can go back now. If Alice is willing to have a good chat with me, I think I will have a better understanding of your Debs clan.”

Nimitz immediately stood up, bowing modestly. “Then Lord Merritt, I take my leave. I entrust the affairs of our Debs clan with you.”

Merritt nodded slightly.

Nimitz immediately departed.

Leaving behind Duke Merritt, alone in that living room.

Toying with his wine cup, Merritt mumbled in a low voice, “My goddess…Alice…” There was a look of satisfaction and anticipation on his face.

As the Right Premier of the Kingdom of Fenlai, and as a Duke, Merritt had an extremely exalted status. The number of people with a higher status than him in the Kingdom of Fenlai could be counted on one hand.

A person like him had experienced virtually any sort of woman he wished.

Merritt really was a lecher, despite being in his seventies. Warriors of his level could live to be over three hundred. Right now, he was only in his seventies and in the prime of his life. Merritt publicly had twelve wives to his name, but there was a common viewpoint amongst nobles; one’s own wives at home weren’t as interesting as having lovers outside, but having lovers outside weren’t as interesting as those you couldn’t get. Those whom you couldn’t get were the best of all.

But given Merritt’s status, there were very few women he was unable to get. At the same time, there were very few women who could truly move him.

But Alice was definitely one.

Ever since that sculpture, ‘Awakening From the Dream’ had become famous, in the hearts of many, the woman of the sculpture had become an untouchable, lofty goddess. For someone of Merritt’s stature, naturally he would deeply desire to get a goddess like Alice beneath his thighs. But this was really too difficult.

But now. An opportunity had come.

“Alice. The goddess?” Merritt was unable to repress his smile. Turning his head up, he drained all of the red wine from his glass.

Sitting within his carriage on the way home, Nimitz was frowning deeply.

Alice was Kalan’s fiancée!

If he were to ask Alice to get meet privately with Merritt, then he definitely would be essentially pushing Alice into a disaster. In the future, when faced with Kalan’s questioning, it wouldn’t be a big deal. But if word of this were to spread, the impact it would have on the Debs clan’s standing would be tremendous.

“Ugh. If the clan is finished, then what will its reputation matter?” Nimitz shook his head, sighing.

Right now, the Debs clan had reached a critical juncture. If the Debs clan was found to have been guilty of smuggling, then the entire clan would be exterminated, and all of its possessions would be taken by the King of Fenlai. Although the Debs clan had left behind some roots outside the kingdom, preventing it from being totally wiped out, almost all of its possessions were in the Kingdom of Fenlai.

If it was all lost, who knew how many years it would be before the Debs clan would return to its former glory?

Compared to the clan’s future, a little bit of mockery and humiliation wasn’t a big deal. After all, since when did the circle of nobles lack for embarrassing stories?

“But this has to be of Alice’s own free will.” Nimitz was a bit worried. “I can’t forcibly deliver her to the Right Premier’s manor, after all.”

Nimitz didn’t care at all about Alice’s purity. She was just a woman, after all!

But Nimitz knew…

“This Alice has a special relationship with Linley. If I were to force her, and then Linley found out…” Just thinking about it made Nimitz frightened. Linley had a very special status within the Kingdom of Fenlai.

Although he had the rank of Marquis, in actuality, Linley belonged to the Radiant Church. In the past, when Clayde had invited Linley to join the ranks of the nobles in the Kingdom of Fenlai, he had even said that between the two of them, there was no need to observe the normal protocols between king and subject.

Clearly, Clayde desired to pull Linley closer to him.

And all of the nobles of the Kingdom of Fenlai knew that if Linley were willing, he could probably easily become a Vicar of the Radiant Church. In a few dozen years, it would be quite natural for Linley to become a Cardinal.

The status of a Cardinal was even higher than that of the King!

“Can’t force her.” Nimitz felt a headache coming. He was worried that Alice would refuse. He pondered things from Linley’s point of view.

Alice was, after all, previously Linley’s first love! If he, Nimitz, were to force Alice to meet Merritt, and she were to lose her chastity, how could Linley not explode with rage?

Within the Debs clan’s manor.

The clan hall was filled with many members of the Debs clan. Alice and Rowling were there as well. All of them were awaiting the return of Nimitz.

They were all worrying about the future of the Debs clan!

“Second Uncle is back! Second Uncle is back!” A middle-aged man standing in the doorway saw Nimitz and began to call out.

Instantly, all of the members of the Debs clan rushed out towards Nimitz en masse. Alice and Rowling exchanged glances, then rose and went to welcome him as well.

“Second Uncle, what’s the situation?”

Nimitz looked at the group of people in front of him. Squeezing out a smile, he said, “The situation isn’t too bad yet. Everyone, go back to your residences. Alice, stay. I need to talk to you.”

Within the clan, Nimitz had a great deal of authority. Hearing his words, everyone departed.

Alice was somewhat confused, confused as to what Nimitz wanted to talk to her about.

“Big sister Alice, I’ll go back to my room now.” Rowling waved towards Alice and said in a quiet voice. A short period of time later, the person left in the hall was Alice.

Nimitz stepped into the hall.

“Second Granduncle, what’s wrong?” Alice stuttered.

Nimitz looked at Alice. Suddenly, he smiled warmly towards her. “Alice, don’t be nervous. Sit down first. Let’s have a good talk.” As he spoke, Nimitz sat down as well.

Why was Nimitz, who previously was so stern to her, who seemed to always look down on her, being so warm to her now?

Alice couldn’t help but feel suspicious.

“Come, sit.” Nimitz’s smile was so kind, so warm.

Alice nervously sat down.

Nimitz let out a long breath. Worry appeared between his brows. “Alice, we didn’t expect that this would happen so soon after you and Kalan got engaged. I don’t know who is secretly framing our Debs clan. If I did, I would kill him.” A baleful aura appeared on Nimitz’s face, but then it transformed into a look of helplessness. “But right now, the most important thing is to cleanse this stain from our name, and rescue Kalan and Bernard.”

Alice nodded.

But in her heart, Alice was suspicious. “Why is Second Granduncle saying these things to me?”

Staring at Alice, Nimitz said with sincerity, “Alice, there is something I must beg of you.”

“Beg me?” Alice was so startled, she rose to her feet.

Such as Nimitz’s standing within the clan that even the clan leader would be respectful to him. But now, Nimitz was saying that he had to beg her to do something. How could Alice not be shocked?

“Alice, Lord Merritt is in charge of investigating this allegation that the Debs clan was engaged in the smuggling of water jade. Lord Merritt is very intrigued by you and wants to meet with you privately.”

Nimitz said urgently to her, “Alice, this is a rare, wonderful opportunity to improve our relationship with him. Only by managing to have a good relationship with Lord Merritt would you be able to help our clan. Alice, you grew up alongside Kalan. You don’t want to see him in jail either, right?”

Alice was stunned.

A private meeting?

Alice was someone who had lived in a noble clan as well, and knew all too well about the shameful things which occurred amongst the nobility. She instantly could guess that this meeting with Lord Merritt would be more than a simple meeting.

“I…I…” Alice stuttered.

Nimitz begged, “Alice, our entire Debs clan is relying on you. I can even guarantee that so long as you can pull Lord Merritt to our side, you will be Kalan’s principal wife.”

Alice felt as though her mind was in shambles.

Alice was still pure and chaste of body.

She had refused to cross that last barrier with both Linley and Kalan. Even after getting engaged to Kalan, Alice still insisted on being married before she would enter the bridal bed with him.

But now she had to go deal with Lord Merritt…

“Alice, I’m begging you.” Nimitz gritted his teeth, leaving his chair and falling to his knees before her. “Alice, Kalan’s life is in your hands.”

“Kalan’s life?” Alice trembled.

Kalan had grown up alongside her. In recent days, in the face of ridicule and scorn from the other members of the Debs clan, it had been Kalan who protected her.

“Alright. I agree.” Alice gritted her teeth.

A look of surprised joy appeared on Nimitz’s face, then he hurriedly said, “Wonderful. How about this. Tomorrow at dusk, I’ll arrange for you to be brought to Lord Merritt’s manor.”

But right now, Alice’s face was extremely pale. She didn’t respond at all.

That next evening. Escorted by twelve knights, a carriage departed from the Debs clan’s manor, slowly rolling towards the manor of Lord Merritt. Within the carriage was only one person. Alice.

Alice quietly sat within the carriage, chewing on her lips. Her nervous hands were tightly gripping her dress.

The carriage continued to roll forward. Quite soon, it arrived at the main gate to Lord Merritt’s manor.

“Miss Alice, we’re here.” The voice of the carriage driver rang out from outside.

Hearing his words, Alice’s heart trembled. Her right hand drifted down to her waist. The firmness of the steel dagger by her side helped to slightly calm her mind down.

Taking a deep breath, Alice pushed open the carriage to the door and stepped out.

Within the welcoming hall of Lord Merritt’s manor.

Wearing a jacket on top and a skirt beneath, Alice was dressed relatively conservatively. Step by step, Alice managed to enter the hall relatively calmly. Alice looked around her, but saw nobody there within the hall.

“Hrm?” Alice couldn’t help but frown.

Just at this moment, a female attendant ran over. Respectfully, she said, “Miss Alice, the Lord Duke is in his study and would like to invite you there as well.”

“His study?” Alice shuddered slightly.

But under the urging gaze of the attendant, Alice still began to walk forwards with her.

The study was in a very quiet, secluded area. There were very few people here. Arriving at the door to the study, Alice saw a seemingly middle-aged, golden-haired man standing in front of a study desk, staring at some papers.

“This is Merritt?” Seeing Merritt, Alice’s first impression was that this was a very fierce person. Even when he sat down at his desk, his back was ramrod straight, and his eyes were sharp.

“Lord Duke, Miss Alice has arrived.” That female attendant said respectfully.

Only now did Merritt raise his head. Seeing Alice, he excitedly rose to his feet. “Haha, Miss Alice, you came? I’ve waited for quite a long time. Come, Miss Alice, please sit.” As he spoke, he left his seat and walked towards Alice.

Alice stepped into the study.

Alice looked around her. Towards the right side of the study, there were many bookshelves, covered with countless books. On the left side of the study, there was a bed.

“Often, when I’m reading or taking care of government affairs, I’ll get tired and will rest there.” Duke Merritt said with a smile. At the same time, he walked towards the study door and shut it.

Seeing the door to the study shut, leaving behind only her and Merritt in the room, Alice grew nervous.

“Lord Merritt, it’s better if we leave the door to the study open. I’m not accustomed to dark environments.” Alice hurriedly said.
