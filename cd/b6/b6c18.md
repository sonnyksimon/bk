#### Book 6, The Road to Revenge – Chapter 18, The Visit

Alice felt her heart suddenly tremble. A warm feeling suddenly rushed into her heart, a sensation of thankfulness mixed with a boundless regret.

“Big brother Linley, thank you. Thank you.” Alice couldn’t help but repeat herself. Her tears were already beginning to shimmer in her eyes. The tears of excitement.

Linley smiled. “Go back. This afternoon, I’ll pay a visit to his Majesty at his palace.”

Linley could feel that right now, his heart was very calm when he saw Alice. When seeing Alice, all he was seeing was a female friend whom he was on good terms with. Nothing more.

“Alright. Thank you.” Alice glanced at Linley one more time, then turned her head and left, her thoughts extremely complicated.

Originally, Alice was afraid that because in the past, she had hurt Linley, Linley would feel hatred for Kalan, which would cause Linley not to help save Kalan. But Linley’s reaction had been totally out of her expectations. Linley wasn’t agitated at all. He was very calm.

Watching Alice’s departing back, Linley sat down. Grabbing a fruit, he began to casually eat it. At this time, Bebe popped out as well.

“Boss, you’re gonna help that Alice? If it were me, I would’ve kicked her out long ago. Heck, it’s enough that you didn’t just slap her to death with one palm!” Bebe said unhappily.

Linley glanced at Bebe. “Bebe, humans aren’t magical beasts.”

At this time, Doehring Cowart flew out of the Coiling Dragon ring. Looking at Linley with an approving gaze, he said, “Linley, you performed very well. I was a bit worried that you’d have a child’s temper and shoo her away, throwing another stone into a drying well.”

“A child’s temper?” Linley was startled.

In Doehring Cowart’s eyes, such behavior was indeed that of a child.

“That’s right. Women, psh. They are all over the place.” Doehring Cowart chuckled.

Linley was instantly speechless. He was very much not in favor of Doehring Cowart’s viewpoint on women, which was rather similar to the viewpoints of Yale and Reynolds.

“Alright, enough chat. I need to continue my training.” Linley immediately rose and returned to the Hot Springs Garden.

As far as Linley was considered, Alice was nothing more than a side-episode, incapable of affecting his mood. Right now, the only thing Linley cared about was…avenging his father.

…..

“His Majesty is in his study, laboring over affairs of state. Lord Linley, please come with me to the study.” The palace attendant said respectfully.

Linley nodded.

Bebe standing on his shoulders, Linley followed the attendant towards the study. After a while, they finally arrived.

“Your Majesty! Lord Linley has arrived!” The palace attendant called out loudly from outside the door to the study.

Clayde, who had been absorbed in reading some texts, raised his head. When his tiger-like gaze landed upon Linley, his eyes shone excitedly. Laughing loudly, he said, “Linley, quick, come in. There’s no need for the two of us to stand on so much ceremony.”

“Yes, your Majesty.” Linley laughed faintly as he entered the study. Clayde, in Linley’s eyes, really was a bold, straightforward man, and was incredibly polite when interacting with Linley, never using his position as the king to try and bully him.

“If it wasn’t for my father’s death,” Linley mused to himself, “Perhaps you and I would’ve become friends. But there will come a day where I must kill you. Right now, the only thing I am lacking is an opportunity.” Linley had never hesitated in his determination to kill Clayde.

As soon as he had the opportunity, he would definitely kill him.

Clayde clinked wine cups with Linley in a toast, took a sip, then said, “Linley. It is quite rare that you voluntarily come pay a visit to the palace. What business do you, my Prime Court Magus, have to discuss with me today?”

Linley chuckled.

The Prime Court Magus actually had quite a few responsibilities, but Linley had never undertaken any of them. He allowed the other court magi to assume many of the responsibilities, and Clayde had never given him any pressure. After all, Linley was only a servant of the Kingdom of Fenlai in name. All he was doing…was showing that he, Linley, considered himself to be on Clayde’s side.

“It’s true that I came here today to discuss something.” Linley smiled as he looked at Clayde. “With the Debs clan under suspicion of smuggling water jade, your Majesty ordered that Kalan and Bernard be seized, right?”

“That is so.” Clayde frowned as he looked at Linley. “What, you’ve also come to speak on their behalf?”

Over this period of time, quite a few nobles had come to speak on behalf of the Debs clan. The reason they had done this was because the Debs clan had made use of their fortune.

“If you really want to save their clan, I can indeed give you face.” Clayde said forthrightly.

The only thing Clayde really wanted to do was to break the power structure that had been erected by his younger brother Patterson. As for the Debs clan, he was going to dispose of them just as a matter of course. He was totally willing to pardon the Debs clan in exchange for Linley now owing him a favor. After all, even if he were to pardon the Debs clan, he could also squeeze them for quite a hefty price in the process.

“No.” Linley only shook his head. “I haven’t come to speak on their behalf.”

“What?” Clayde looked curiously at Linley.

Linley said casually, “Your Majesty, the question of whether or not the Debs clan engaged in the smuggling of water jade naturally has to be handled in a fair, aboveboard manner.”

“Oh?” Clayde looked questioningly at Linley. “Then Linley, the reason you came today was because…”

Linley laughed. “I’m thinking that it’s enough for you to have seized the clan leader, Bernard, due to your suspicion that the Debs clan engaged in the smuggling of water jade. As for his son, there’s no need to seize him. After all, what’s the point of seizing a successor? If you seize the first one, they’ll still have a second one. As long as their clan isn’t exterminated, someone will continue the line.”

“Linley, you mean to say…” Clayde looked at Linley.

Linley looked back at Clayde. “Your Majesty, I hope you can release Kalan.”

“Oh, release Kalan. I heard that you and Kalan…?” Clayde had done a very thorough investigation on Linley. Naturally, he knew of the complicated history between Linley, Kalan, and Alice.

Linley let out a helpless laugh. “Your Majesty, that was a long time ago.”

Clayde reminded him, “Linley, I must remind you that based on my investigations, this Kalan fellow is a very vicious, narrow-minded person who can hold a grudge.”

“I know.” Linley nodded slightly.

Based on the few interactions he had with Kalan, Linley had already sensed that Kalan viewed him with hostility. And…Linley knew that during the seven day exhibition of his sculpture, ‘Awakening From the Dream’, someone had desired to destroy it.

Destroying a sculpture was an act which benefited nobody.

Aside from Kalan, Linley couldn’t think of anyone else who would want to destroy ‘Awakening From the Dream’.

“Then why do you help him?” Clayde continued.

“Your Majesty. Do you believe a narrow-minded man of limited vision such as him is someone I would be concerned about?” Smiling, Linley looked at Clayde. Clayde blinked, then laughed as well.

“Right. In the past, it could be said that you and Kalan were old acquaintances. But now, not only does he not wish to befriend you, he even harbors enmity towards you. It is his father who continues to try and befriend you. Compared to his father, Kalan’s vision really is very limited.” Clayde laughed loudly.

Clayde patted Linley on the shoulders. “Don’t worry. I’ll instruct Merritt to handle this case fairly and to investigate everything thoroughly. The Debs clan definitely won’t suffer any injustice. But if the Debs clan really was guilty of smuggling water jade, I won’t allow them to escape punishment either.” “Right. Handle the case fairly.” Linley nodded.

On the way back home in the carriage, Bebe was lying atop of Linley’s thighs.

“Wow, Boss, you are so evil. The leader of the Debs clan definitely engaged in smuggling. Later on, his clan will be finished. Even if Kalan is able to escape for now, in the future, he’ll still be in terrible straits!” Bebe said excitedly.

Bebe had wanted to destroy Kalan a long time ago. Linley shook his head with a laugh. “Whether or not the Debs clan really will be finished is hard to say. For example, they could give the majority of their clan’s fortune directly to King Clayde, and perhaps Clayde would give them a way out. But no matter what, now that they’ve fallen into Clayde’s hands, even if they don’t die, they’ll lose several layers of skin and flesh.”

Linley fully understood how dark the world of nobles could be. Although on the surface, they talked about handling things fairly, that was nothing more than a sham. “Compared to Clayde, the Debs clan is too weak.” Linley shook his head.

That puny little Kalan was someone Linley had never worried about. Kalan simply wasn’t even close to being on the same level as Linley. The one Linley wanted to deal with was Clayde!

“Milord, we have arrived.” The driver said respectfully.

Linley pushed open the carriage door and stepped out. With a leap, Bebe hopped onto Linley’s shoulders again. Just as Linley was about to enter his manor, a gate guard said respectfully, “Lord, a guest just came by. He’s currently in the main hall waiting for you.”

“A guest? In the main hall?” Linley felt suspicious.

There often would be nobles coming to visit Linley, but without his permission to come in, all of them would quietly wait outside. Only people with a very high status, such as Duke Patterson or King Clayde, or Cardinal Guillermo, would directly head to the main hall, instead of waiting outside.

“Who is it?” Linley couldn’t help but ask.

“No clue, but in his hands, he was holding the medal of a Cardinal.” The guard said respectfully. As a Knight of the Radiant Church, he was very familiar with the insignias of the Cardinals.

Each Cardinal only had a single medal. Naturally, some extremely powerful Ascetics had medals as well. Possession of a medal implied a certain status, representing that this person’s position was no less than that of a Cardinal.

“An insignia?” Linley was startled.

Without hesitating at all, Linley immediately went towards the main hall. By the time Linley passed through the walkway and reached the main hall, he was shocked by who he saw.

Within the main hall was a middle-aged, black-haired man wearing a long, loose robe. Judging from appearances, he was in his thirties or forties. He gave off an indolent, lazy aura.

When Linley saw this middle-aged man, that middle-aged man seemed to sense him as well. He immediately looked over towards Linley, a look of excitement in his eyes. “Master Linley, you came?”

“Master Linley?” Linley’s mind was full of questions, but he quickly entered the main hall.

“You are…oh, I remember now. You were that one who made the bid of ten million gold coins.” Linley remembered now. During the sculpture auction of ‘Awakening From the Dream’, this middle aged man was the one who had bid ten million.

The middle-aged man nodded excitedly. “I didn’t expect Master Linley to remember me. This makes me so excited. Oh, right. Let me introduce myself. My name is…Cesar [Xi’sai].”

“Cesar?” Linley had never heard this name before.

“Cesar?!” Doehring Cowart’s voice suddenly boomed out in Linley’s mind. “I didn’t imagine that little freak Cesar would still remain on this plane, in the Yulan continent.”

Linley was startled.

Grandpa Doehring knew this Cesar? Grandpa Doehring was from a long gone era! If he knew this man, then how old would this Cesar be?

“Linley, this Cesar is a total freak. His rate of improvement in strength is extremely fast, and he kills without blinking. When I was alive, he had already entered the Saint-level. Although back then, he was only an early-stage Saint-level, after five thousand years, based on his rate of improvement, he is most likely far more powerful now.”

Linley’s heart clenched.

The man in front of him appeared to be only thirty or forty, but was actually already a Saint-level combatant during Doehring Cowart’s era. Doehring Cowart had only lived for a thousand years before dying, but this Cesar, if one were to count accurately, had been alive for nearly six thousand years now.

A six thousand year old freak!

“Master Linley, what is it?” Cesar said with concern. “Your face seems to have a rather unpleasant look.”

“Nothing, Mr. Cesar. Please, sit.” Linley forcibly calmed himself down, but whenever he thought of who this person in front of him was, he couldn’t help but be stunned.

A six thousand year old freak, a super-combatant who had survived from the era of the Pouant Empire until the modern era. He had already been a Saint-level combatant back then. And now?

“Master Linley, I am very much in awe of your sculpting skills. If it weren’t for the fact that Delia, that little girl, begged me, that day I definitely would’ve bought your sculpture.” Cesar pursed his lips as he spoke, but then his eyes lit up. “So Master Linley, when are you and that Delia girl getting married?”

“Married?”

No matter how stunned Linley had been by Cesar, upon hearing these words, Linley’s eyes bulged out of his sockets as he stared speechlessly at Cesar.
