#### Book 6, The Road to Revenge – Chapter 16, Limits

As he closed the door to the study, Merritt heard Alice’s words. He couldn’t help but turn to Alice with a smile. “Miss Alice, we’re going to discuss the affairs of the Debs clan. We can’t discuss those openly and publicly, can we? If his Majesty were to find out, then I would be in serious trouble. You should know that I’m taking on serious risks on behalf of your Debs clan. Best we leave the door closed.”

Alice was stunned.

In terms of wordplay, how could Alice match this Lord Merritt, who had engaged in the highest levels of court intrigue for so long?

Smiling, Merritt walked past her. In front of the bookshelf, there were two chairs around a round table. Merritt would often chat with some of his friends here.

Merritt first sat down, then looked at Alice. “Alice, you should sit.”

“Thank you, Lord Merritt.” Alice secretly let out a sigh of relief, then sat down on the opposite chair. The thing which made Alice the most nervous in this study was that bed.

“Please wait a moment.”

Smiling, Merritt rose to his feet, then pulled out a bottle of red wine and two wine cups. He poured himself and Alice a cup of wine each.

“Alice, this is the Bluerain red wine from the Yulan Empire, a sixty-year old vintage. The flavor isn’t bad. Have a taste.” Merritt smiled as he raised his glass to her.

Alice was somewhat afraid that some sort of knockout drug had been mixed into the wine. But, under Merritt’s gaze, Alice was forced to raise her own glass as well. Only, she just barely touched the wine with her lips.

Merritt didn’t force her. Changing the topic, he said, “Alice, you and Kalan have already become engaged. I expect you know quite a bit about the affairs of the Debs clan. Did you know they were engaged in smuggling?”

“No, I didn’t. I think Kalan wouldn’t engage in smuggling.” Alice hurriedly said. “Lord Merritt, the Debs clan is quite powerful. I think they wouldn’t engage in this smuggling business.”

With a smile that wasn’t a smile, Merritt looked at Alice. “Hard to say.”

“Ah!”

Merritt seemed to have seen something, and all of a sudden, he moved next to Alice, so close that his face was mere centimeters away from Alice’s face.

Startled, Alice hurriedly retreated.

“Don’t move.” Merritt’s shouted carried a hint of a command.

Born from long years of being accustomed to power, Merritt’s commanding voice froze Alice in her tracks, as ill at ease as she was. Merritt carefully inspected Alice’s hair, then looked down at Alice.

Upon lowering his head, his face was now only a few centimeters away from Alice’s. This made Alice hurriedly bend her head away from him.

Seeing this, Merritt laughed, then returned to his original seat. He let out a helpless sigh. “Just then, I saw a single white hair on your head, but after you moved, I couldn’t see it anymore.”

A strand of white hair?

In her heart, Alice began to grow irritated. She lived together with Rowling now, and every morning, when they were bored, they would comb each other’s hair. Often, she would find some white hairs on Rowling’s head. But Rowling often expressed envy towards Alice, as she could never find white hair on Alice’s head.

Rowling couldn’t find any white hair despite combing Alice’s hair every day. How could Merritt have found any?

But Alice didn’t dare to say this.

“Alice, you are still young. Don’t be too upset. If you are upset, you’ll age faster, and thus have white hair.” Merritt said solicitously.

Alice only quietly listened to him as he spoke.

Merritt nudged his chair in Alice’s direction, then fixed his gaze upon Alice. “Alice, you are quite beautiful, you know. Your charm and aura of refinement is really quite mesmerizing to behold.”

Alice couldn’t help but feel shy and nervous.

Merritt leaned forward slightly, staring intensely at Alice. “Alice. Those wives of mine, all they care about are superficial things like money and glory. They seem so vulgar, so low. But you are totally different. Truly, you are, you know. The very first time I saw you, I was stunned.”

“I very much regret that I ended up marrying women such as them.” Merritt suddenly reached out and held Alice’s hand. Alice’s eyes suddenly widened. Merritt continued to look at Alice. “Alice, if I…if I were to tell you that I love you from the bottom of my heart, that I am smitten with you, would you believe me?”

Alice hurriedly stood up…but Merritt maintained his tight grip on her hand.

“Lord Merrit, Lord Merritt. I’m the fiancée of Kalan!” Alice struggled, and only after three attempts was she able to break free from Merritt’s grip.

Merritt looked at Alice with a smile. “As you say, you are only a fiancée, which means you aren’t married yet. You totally can marry another. As for Kalan, what does a kid like him know about having fun?”

As he spoke, Merritt once more moved nearer to Alice, while Alice continued to move back.

But in her nervousness, Alice didn’t notice in the slightest that Merritt was pressuring her into the direction of the bed.

“Alice. I really have fallen for you. I swear!” Merritt stared soulfully at Alice.

Merritt wasn’t lying. Over the course of admiring the sculpture ‘Awakening From the Dream’, and then upon seeing Alice herself, he really did fall for Alice. But this sort of ‘falling for’ was only a desire to possess.

“Lord Merritt!” Alice was growing frantic.

Suddenly, Alice’s back legs collided with the bed. Knocked off balance, Alice fell backwards onto the mattress.

A hint of a smile appeared on Merritt’s face. He immediately threw himself on top of Alice, all but pressing his body against hers. “Alice, my goddess, please satisfy the desires of this mortal who has been mesmerized by you. If you satisfy my desires, I’ll satisfy yours as well and clear the unjust stains from the Debs clan.”

Clear the stains of the Debs clan?

Staring at Merritt who was right on top of her, Alice couldn’t help but suddenly think back to a night she had been with Linley at a small hotel. The two of them had entangled themselves lustfully, but at the very end, she had stopped Linley.

How could she give up her chastity to this man in front of her?

“My goddess, come to me.” Merritt’s voice was very soft, as though he was trying to hypnotize her.

“No. No!”

Alice suddenly pulled the dagger from her waist and thrust it at Merritt. At the same time, the stones on the floor flew at Merritt.

Alice was an earth-style magus, after all!

But Merritt himself was a powerful warrior. His reflexes were very fast, and he quickly dodged to one side while at the same time slapping the dagger out of Alice’s hand.

Alice instantly dodged towards the other side, running for the door.

But with a flicker of his body, Merritt appeared between her and the door. With a smile that was not a smile on his face, he looked at Alice. “Alice. Do you still want to resist? Based on your prowess as a magus and that little knife, you want to resist me?”

“Lord Merritt, let me leave.” By now, Alice was firm in her resolve.

“You no longer wish to save the Debs clan? You don’t wish to save your fiancé, Kalan?” Merritt asked.

Alice’s eyes were determined. Gritting her teeth, she said, “Although I do wish to save them, this is not the way to do it. You beast!”

“Beast?” The expression on Merritt’s face changed. He coldly said, “Originally, I wanted for the mood to be a bit more romantic, but since you refuse to cooperate, then I’ll show you what a beast really looks like.”

Alice’s face turned pale.

“Merritt. Don’t go too far.” Frightened, Alice quickly retreated, grabbing the chair next to her and smashing it at Merritt.

With a single fist, Merritt easily broke the chair apart.

“Don’t resist. This place…is my manor.” Merritt said with a soft laugh.

Watching Merritt draw step by step closer to her, Alice gritted her teeth and said wildly, “Merritt! You’d best not forget that I once was Linley’s woman!”

These words halted Merritt in his tracks, stunning him.

Alice really did not want to say these words. She knew that her actions of the past had wounded Linley very deeply, and she didn’t want to have anything more to do with him. But at this point in time, she could think of no other way.

“Linley?” Standing there without moving, Merritt frowned.

Biting her lips, Alice stared at Merritt. “Merritt, I can pretend that nothing at all happened today. But if you go too far, then don’t blame me when I also go all-out afterwards. I trust you know how influential Linley is now.”

Merritt looked at Alice.

He really had been enchanted by Alice, but Merritt knew very well that Linley’s relationship with Alice was very special. Just from looking at that sculpture, ‘Awakening From the Dream’, one could tell how deep Linley’s affection for Alice had been.

“Linley’s feelings towards Alice really were in the realm of true love. If Linley were to find out…” Merritt’s head began to hurt.

Linley.

Very hard to deal with!

The current Linley already possessed incredible influence. Although he, Merritt, was powerful, in the end he was only the Right Premier of a single kingdom. To the Radiant Church, perhaps deposing one of the rulers of a kingdom was something it would do only after serious consideration, but they wouldn’t even think twice before dealing with the Right Premier of a kingdom.

All Linley had to do was to ask the Radiant Church for their assistance. Dealing with him, a Right Premier, wouldn’t be a problem.

But in the future, Linley would only be more formidable. This was one of the reasons why not a single member of the nobles of the Kingdom of Fenlai had dared to plot against Linley or make attempts against Linley’s life, which was why, in front of Linley, they all behaved so courteously.

“Alas…” Merritt let out a long sigh. “Alice, I really, truly, have fallen for you from the depths of my heart, so much so that I lost my sense of rationality.”

Merritt smiled apologetically at Alice. “I apologize. I’ve come back to my senses now. Since you aren’t willing or able to have feelings for me, of course I cannot force myself on.”

“Lord Merritt, I’ll take my leave, then.” Alice quickly scurried to the door, opened it, then rushed out.

Seeing Alice depart, the apologetic look dropped from Merritt’s face, and his gaze grew vicious and cold. With a cold sneer, he spat out the word, “Bitch!”

By the time Alice had returned to the Debs clan manor, it was now totally dark.

Right now, all of the members of the Debs clan were in the middle of the main hall, eating dinner. Only, the atmosphere wasn’t very good. The clan could be exterminated at any time, after all.

“Alice. You returned?” Rowling suddenly saw Alice running inside.

Nimitz and the others all stood up as well.

“As fast as that?” Nimitz frowned. Alice had come back far too early, much earlier than he had expected.

“Alice, eat dinner with us.” Rowling immediately called to her.

On the walkway past the main hall, Alice glanced at the people inside and said apologetically, “I’m not feeling well. I’ll go back to my room and rest first.” Alice’s voice was very low and hoarse.

Rowling felt that Alice wasn’t acting normally.

“Let me go see how Alice is doing.” Rowling smiled at everyone, then left the hall, leaving behind Nimitz, who was frowning with suspicion.

Alice and Rowling, in their room.

Upon entering the room, Alice had immediately thrown herself into her bed. She could no longer hold back her tears, which poured out. Her heart was filled with wrongs and injustices.

“What did I do wrong? Lord, why must you punish me so?”

Alice was howling with rage in her heart.

“I never asked for much, only that I could have a simple, peaceful life. I want my parents to have a peaceful life, for myself to have a peaceful life. Why, why must you punish me so?” Alice’s heart was filled with misery. True, the Debs clan perhaps was going to be finished.

But what did that have to do with her?

Why did they have to send her to deal with Merritt?

Why did she have to be forced to the point where she had to shout out the words, “I once was Linley’s woman?” How difficult had it been for her to force these words out! Alice truly hadn’t wanted to say that!

“Big sister Alice, what happened?” Rowling ran into the room. Seeing Alice sobbing to the point where there was a huge wet spot on the bed, Rowling grew frantic with worry.

Rowling immediately went over and began to stroke Alice’s back. “Don’t cry, don’t cry. Whatever it is, you can tell me. Tell me.”

Alice immediately turned and threw herself into Rowling’s arms, bawling even fiercerly. It wasn’t as bad without anyone there to comfort her, but now that someone had come, Alice felt all the more aggrieved and wronged.

Rowling comforted Alice for more than half an hour before Alice finally became somewhat calmer.

“Big sister Alice, what exactly happened? Tell me.” Rowling looked at Alice.

Alice took a deep breath, then slowly explained the injustice that had been done to her. “Little Rowling, you are also aware of the current situation with the Debs clan. Yesterday, Second Granduncle came and wanted to have a private chat with me. He wanted me to…”

The more she heard, the more fury Rowling felt.

She was angry at Nimitz’s behavior. She was angry for what Alice had suffered. And she felt rage towards that beast-like Merritt’s behavior. At the same time, she felt sympathy for Alice.

“I don’t want to get involved, get involved anymore. I just want to live out a peaceful life.” Alice said, sobbing sporadically.

Over these past few days, Rowling had been considering what the best way to help the Debs clan was. But upon hearing Alice’s story, she suddenly understood a few things.

“Big sister Alice, don’t be sad. No matter what, you definitely cannot let that Merritt destroy your chastity.” Rowling comforted her.

Alice nodded.

“But we still have to come up with a way to save Kalan and the others.” Rowling said. “Big brother Kalan is our fiancé, after all.”

Alice also wanted to save him, but she didn’t know how.

“We still have an option.” Rowling looked at Alice. “But…I don’t know if you would be willing to take it, sister Alice.”

“Rowling…” Looking at Rowling, Alice had already guessed what she was going to say.

Rowling nodded. “Right. Go ask Linley for help. Today, as soon as you mentioned his name, that Merritt no longer dared to touch you. Clearly, Linley is extremely influential. Based on what I know, not only does Linley have a relationship with the Radiant Church, he also has a relationship with the Dawson Conglomerate. Even his Majesty, King Clayde, treats Linley as he would a friend, rather than an ordinary subject. If Linley is willing to speak out, we would have a much greater chance of rescuing big brother Kalan.”

Currently, in the Kingdom of Fenlai, without question, people were more willing to defer to Linley than to anyone else.

Even the Left Premier and the Right Premier couldn’t compare with him.

Because, as one could easily tell, in the future Linley would be a high level person within the Radiant Church. Even right now, he was viewed as an extremely important potential talent who needed to be cultivated and trained. For the sake of Linley, those two Cardinals of the Radiant Church had even gone to Hogg’s funeral and paid their respects to him. From this, one could easily see how important they viewed Linley as being.

“Big brother Linley?” Alice’s emotions were very mixed.

In truth, in Alice’s heart, she knew this was a possibility long ago, but she didn’t want to confront it. She truly didn’t wish to go beg Linley. She felt that she didn’t have the face to see him again.

She knew that she had wounded Linley too heavily. That moment when she had seen that sculpture, ‘Awakening From the Dream’, Alice understood how deeply Linley loved her. Or at least, how deeply he had once loved her.

She was ashamed to meet him!

“Big sister Alice, I understand your feelings.” Rowling tightly gripped Alice by the hands. “But, big sister Alice, big brother Kalan and his father are very likely to lose their lives. I beg you, please just suffer a bit on our behalf. At least Linley won’t act the way that Merritt did.”

Alice’s heart was filled with pain.

“No face? Is my self-respect more important, or are the lives of big brother Kalan and his father more important?” Alice asked herself this question. She had no other choice.

“Big sister Alice.” Rowling stared beseechingly at Alice.

Alice took a deep breath, forcing herself to calm down. Looking at Rowling, she nodded. “Alright. I’ll go see big brother Linley tomorrow.”
