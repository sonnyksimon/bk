#### Book 6, The Road to Revenge – Chapter 8, An Excessive Desire to Kill

During the recent assassination attempt, Linley’s side suffered the losses of eighteen Knights of the Radiant Church, four female attendants, and two male attendants. As a result of this, the Radiant Church further strengthened and enlarged the security detail within the estate.

That same night of the assassination, within the manor.

“Linley, are you okay?” King Clayde asked solicitously.

“I’m only slightly wounded, your Majesty.” Linley’s arm was wrapped with medical gauze.

Actually, Linley hadn’t been injured at all during this attack, but he didn’t want others to know exactly how powerful he was. Thus, he lightly injured himself on purpose, using his straight chisel to cut himself on his arm.

To Linley, who had previously suffered the pain of the initial Dragonform transformation, this sort of pain was nothing.

“As long as you are fine, Linley.” Duke Patterson, who was by King Clayde’s side, laughed.

Linley looked at Duke Patterson.

Tonight should have been the night for the meeting between Linley and Duke Patterson, but because of the assassination attempt, the two of them no longer would have the chance to have a private conversation tonight.

“Second brother, it’s best that we don’t disturb Linley any further. Let’s allow him to have a good rest.” Clayde turned his head and said.

“Yes, your Majesty.” Patterson glanced at Linley, and then followed King Clayde out.

Linley felt as though there were a hint of helplessness in the look Patterson had given him. Clearly, per Patterson’s original plan, there were some things he wished to discuss with Linley in private during their scheduled one-on-one meeting.

But clearly, this was no longer an appropriate time.

In the next few days, the estate once more returned to normal.

“Boss, today is May 18th, right?” Bebe, who was enjoying lunch alongside Linley, suddenly spoke mentally to Linley.

“Right. What is it?” Linley looked at Bebe.

Bebe wrinkled his little nose. Quirking his mouth, he mentally said, “Boss, have you forgotten? That Bernard fellow, the leader of the Debs clan, told us that June 18th would be the date of his son’s engagement ceremony. He invited you to attend as well. It’s now May 18th. You only have a month left.”

“Engagement?”

Linley was startled.

A month from now, Alice and Kalan would be getting engaged.

“That’s none of my business.” Linley quickly returned to his usual calm demeanor, lowering his head and continuing to eat.

Bebe’s beady little eyes rolled around three times, and then he used his tiny little paws to rub at his chin. A look of suspicion on his face, he said, “Could it be that I, Bebe, am mistaken? Shouldn’t be the case. I’m so awesome, after all. My judgment is excellent. In his heart, the Boss certainly cares about this affair. If it were me, Bebe, I would smash that little Kalan’s skull in with a single paw.”

“Lord Linley.”

One of the guardian knights entered the main hall. “Lord Linley. Cardinal Guillermo has come.”

“Guillermo?” Linley hesitated for just one moment, then he immediately put down his utensils and went to the door.

In the entire hierarchy of the Radiant Church, the person whom Linley was most familiar with and had the best relationship with was probably Cardinal Guillermo. When someone treated Linley as courteously as Guillermo did, Linley naturally wouldn’t act in a high, arrogant manner, as though he thought himself better.

“Linley, there’s something I must tell you.” Upon seeing Linley, Guillermo began to chuckle with joy as he spoke.

Linley looked at Guillermo questioningly. “What is it?”

Beaming, Guillermo said, “Linley, are you aware that within our Radiant Church, we have a special group of people known as…Ascetics?”

“Yes, I am.” Linley nodded.

Previously, when he had been kidnapped by those experts from the Cult of Shadows, it was the Deputy Arbiter of the Ecclesiastical Tribunal as well as an Ascetic and several Executors who had scared the opponents away. Only then had he been able to return to the city of Fenlai.

“Within our Radiant Church, there have been many people obsessed with magic or fighting skills who have enlisted within the ranks of the Ascetics. Put another way, neither the Knights of the Radiant Temple, nor the Ecclesiastical Tribunal, have as many experts amongst their ranks as the Ascetics do.”

Guillermo beamed as he patted Linley on the shoulders. “What I am about to tell you is that you have the chance to become the disciple of a legendary Ascetic.”

“A legendary Ascetic?” Linley frowned.

Guillermo smiled faintly. “This legendary Ascetic is considered to be at the highest levels, even amongst the Ascetics. He also possesses an extremely high status within our Radiant Church. As for his power, even if we look at the Yulan continent as a whole, there are perhaps only those three freaks of nature who can surpass him in power.”

“Three freaks of nature?” Linley instantly grew curious. “Lord Guillermo, who are these three freaks of nature that you speak of?”

While chatting, the two of them walked back to the main hall.

Guillermo didn’t reply right away. He glanced at the Vicar next to him, and the Vicar instantly escorted everyone present away, then obediently stepped out himself, closing the door.

In the entire main hall, only Linley, Guillermo, and Bebe were now present.

“Linley, in the future, it’s possible that you will meet with these people, so it isn’t a big deal if I tell you about them now.” Guillermo said, putting on a mysterious air.

Linley looked at Guillermo curiously.

Guillermo sighed. “Here in the Yulan continent, there are three individuals who have surpassed the existence of the Saint-level combatants. The three ‘freaks of nature’ I talked about, are precisely those three freaks.”

“Those who ascended past the level of Saints? That would make them Gods?” Linley was shocked.

“Right. You can refer to them as Gods.” Guillermo nodded.

Linley immediately perked up his ears to listen closely.

Guillermo slowly said, “Across the entirety of the Yulan continent, there are only three such freaks. The first freak is the ‘High Priest of the Living Temple’ of the Yulan Empire. Many people simply refer to him as the ‘High Priest’. I, at least, have no idea how old the High Priest is. He has been alive for simply too long.”

Linley nodded.

“This second freak has been alive an extremely long time. He is the true ruler of the third most dangerous place in the Yulan continent, the Forest of Darkness. This freak is supposedly a magical beast in nature, but he has already reached the level of being able to transform into a human. Linley, you should already know that when a magical beast reaches the Saint-level, he can transform his body enough to speak in human tongues, but is not able to transform into a human form. You can imagine for yourself how terrifying a magical beast who can transform into a human must be.”

Linley nodded slightly.

He had previously heard Doehring Cowart speak of these two individuals. Even back when Doehring Cowart was alive, these two had been invincible presences.

“And the third person?” Linley asked.

Guillermo sighed. “This third person is also someone who I revere greatly. He was the founding Emperor of the O’Brien Empire, the most militarily powerful empire in the Yulan continent. People call him the ‘War God O’Brien’.”

“O’Brien?” Linley memorized this name.

Given that the O’Brien Empire was named after this person, one could imagine how amazing he was.

“Five thousand years ago, the War God quickly rose to prominence, defeating one Saint-level combatant after another. In that era, there were many super-combatants, such as the Four Supreme Warriors, who appeared during that time period.” Guillermo smiled at Linley.

Linley thought back to his own ancestor, Baruch.

The first leader of the Baruch clan had appeared almost exactly five thousand years ago as well.

“Back then, the Four Supreme Warriors were extremely powerful, but their brilliance was totally eclipsed by the War God. The War God defeated one powerful Saint-level combatant after another, and in the end, even engaged in a great battle with the High Priest, in the air above the Yulan River. During the course of their battle, the shockwaves alone killed over ten thousand people. In the end, both the O’Brien Empire and the Yulan Empire gave up a large amount of territory, allowing it to form into three independent kingdoms which served as buffer zones between these two great Empires.” Guillermo sighed emotionally.

“Linley, in the minds of many, the High Priest is the most powerful human alive. But the War God was actually able to fight to a stalemate with the High Priest. But how few years had the War God been alive for? This is why so many people are in awe of him. Who knows what level of power the War God is now at, after five thousand years of training.” Guillermo sighed with praise.

Linley secretly nodded as well.

“This War God. He fought the High Priest to a stalemate?” Doehring Cowart’s voice rang out in Linley’s mind. “How is that possible?”

Back in Doehring Cowart’s era, the High Priest’s brilliance eclipsed everyone in the world.

In Doehring Cowart’s heart, the High Priest was invincible and undefeatable.

“Grandpa Doehring, every era will see super-combatants emerge. If you, Grandpa Doehring, hadn’t died back then and had continued to train, perhaps one day you would’ve also broken past the Saint-level and become an expert on the same level as the High Priest.” Linley mentally said.

Doehring Cowart let out a low sigh and no longer spoke.

“Enough talk about those three freaks. The person I am about to have you meet is only inferior to those three. If you can become his disciple, it will be of great benefit to you as you attempt to increase your power in magic.” Guillermo said.

Linley laughed inside.

As far as someone who was only inferior to those three freaks…wasn’t his own Grandpa Doehring someone who was at the peak of the Saint-level?

“What is the name of this Ascetic?” Linley asked.

“His name is…Fallen Leaf.”

Within one of the slums of Fenlai City. Only now did Linley realize that within Fenlai City, one of the largest, most prosperous cities in the Yulan continent, there was such an impoverished, desolate place. It was far worse off than even his own hometown of Wushan township.

At this moment, Linley and Guillermo were walking shoulder-to-shoulder within a foul, dirty alley.

“Lord Guillermo, the Lord Fallen Leaf that you spoke of lives here?” Linley couldn’t believe it.

“Right.” Guillermo nodded. “Linley, remember, this Lord Fallen Leaf detests those nobles who think themselves better than others. Thus, you must be modest and courteous, even towards these poor people.”

Linley glanced at the poor people lining the streets.

Not too far away, he saw a seven or eight year old child, malnourished to the point of being skin and bones, who wore a foul, oily black rag as his clothes. This child was staring at Linley with fear in his eyes.

Due to his skinniness, his sunken eyes seemed particularly large.

Those innocent eyes made Linley’s heart tremble.

Linley didn’t do anything, just continued to walk forward alongside Guillermo. On the road, Linley saw one poor child after another. None of them wore any proper clothes, and all of them were extremely poor.

“Here we are.” Guillermo suddenly said.

Linley couldn’t help but turn his head to look.

They were standing in front of a casually erected metal frame-like dwelling. An old man who looked like a beggar sat in the middle of the building. The old man was so skinny that it made one’s heart quiver, and all the skin on his body was sagging down. His hands were like the claws of a chicken, only skin and bone.

This old fellow was looking at Linley with curiosity.

“Lord Fallen Leaf.” Guillermo said respectfully.

“He really is Lord Fallen Leaf?” Linley wasn’t sure in his heart, but seeing Guillermo behave in such a manner, he was forced to believe it.

But could this old man in front of him, who looked like a beggar that could be blown down by a good gust of wind, really be the high Saint-level combatant, Lord ‘Fallen Leaf’?

“Guillermo, this is the one you mentioned to me, the so-called kid with talent?” The old beggar asked.

“Yes, Lord Fallen Leaf.” Guillermo said respectfully.

“Grandpa Fallen Leaf, Grandpa Fallen Leaf, quick, help save my mother. She was beaten and injured by someone!” A youthful voice rang out, then a girl came running in, carrying her skinny mother on her back.

The old beggar immediately turned around and stretched his right hand out.

Surrounded by a holy light, that heavily wounded woman began to heal at an astonishing speed.

The old beggar turned back to look at Linley. “I will only teach those with kind hearts and pure souls. But you…your heart is filled with an excessive desire to kill. I will not teach you.”

Guillermo couldn’t help but be astonished by these words.

“An excessive desire to kill?” A hint of a smile appeared on Linley’s face.

The need to seek vengeance on behalf of his parents had caused unspeakable pain and torment to Linley. Every minute, he desired to kill Patterson, but he continued to force himself to be calm and to not be rash. But this sort of constant self-repression did indeed cause Linley’s killing urge to only grow greater and greater.

“Then, Lord Fallen Leaf, I take my leave.” Linley bowed slightly, then turned and left.

The old beggar had originally wanted to say a few extra words. Upon seeing Linley turn and leave so cleanly and bluntly, he couldn’t help but be startled. But then, a hint of a smile appeared on his face.
