#### Book 6, The Road to Revenge – Chapter 24, Breakthrough

Linley was silent for a moment, then smiled and nodded apologetically at Yale. “Boss Yale, sorry for the hassle.”

“It’s no hassle.” Yale chortled. “It’s just sending someone to make a delivery is all. No big deal. Our Dawson Conglomerate often sends people to deliver letters to the headquarters. We’ll get several things done.”

Linley nodded.

“Third Bro.” Yale’s voice became solemn as he looked at Linley. “Tell me the truth. Why are you in such a rush to get these herbs?”

If it were someone else asking him, Linley totally could’ve lied and claimed that he was using it to make a medical lotion which would help him increase the speed at which his body gained strength. After all, it wasn’t unheard of to bath in medicinal waters as part of training. But facing one of his bros, Linley didn’t wish to lie.

“Boss Yale, right now, I can’t tell you yet. When the time is right, I will tell you.” Linley patted Yale on the shoulders as he spoke.

The bros of dorm 1987 had been together since they were young. They ate together, lived together, played together. They were as close as real brothers.

“Understood, Third Bro. But if you need anything at all, make sure you let me know.” Yale didn’t ask anything else.

The next day, Linley’s housekeeper brought over the herbs which Linley had asked for, except he hadn’t been able to find any fog grass. Based on what the housekeeper said, there was no fog grass available on the market at all. If they wanted to buy some, they would have to send someone to buy it from the Four Great Empires.

After all, fog grass was cultivated from the great plains to the far east. Some of the market centers of the Four Great Empires fairly close to the great plains did have a small amount of fog grass for sale.

“Right now, of the eight ingredients I need to produce Bloodrupture poison, seven are ready. All I’m missing is cloud fungus.” Within his secret study, Linley had put all of the various herbs in front of him on a table, pondering what to do. Of the eight ingredients, there were three that were rare. Fog grass had been procured by the Dawson Conglomerate, while he already had enough Blueheart Grass.

“If I wait three months, then at that time, the people from the Dawson Conglomerate will come and deliver the cloud fungus.” Linley felt very confident.

At most, three months. At that time, he would have all the ingredients that he needed, and would thus be able to prepare a few mixtures of Bloodrupture poison.

But Linley wasn’t the sort of person to sit around waiting.

“Help me spread the word. Let it be known that I am preparing to begin a period of training with the usage of herbal baths, and need cloud fungus as one of my components. I’m willing to pay up to a million gold coins for it.” Linley instructed his housekeeper.

Although Linley wouldn’t lie to his bros, he had to give a good excuse to the rest of the world.

Cloud fungus, in and of itself, was not a poisonous plant. It actually was greatly beneficial to the body. But all herbs possessed their own wondrous properties. When these eight herbs were all refined and processed together, they would be able to produce a poisonous powder like the Bloodrupture poison.

“Yes, Lord Linley.” Upon hearing the words, ‘a million gold coins’, the housekeeper’s heart trembled.

To Linley, a million gold coins really wasn’t much. When he had auctioned off his sculpture, ‘Awakening From the Dream’, the price was twelve million gold coins. Afterwards, when Patterson had secretly met with him, he had gifted Linley another ten million gold coins. After Linley’s rise to prominence and appointment to the rank of Prime Court Magus, the Radiant Church, King Clayde, and many other nobles had all given Linley many valuable gifts.

And just a short while ago, the Debs clan had gifted Linley with a magicrystal card that had one million gold coins on it.

Linley’s current net worth was well over twenty million gold coins.

And this wasn’t even counting the Saint-level magicite core that Linley had acquired from the Saint-level Violet Tattooed Bear. That core, which Linley was keeping hidden, was a priceless treasure which probably was worth more than even a hundred million gold coins.

The news that Linley was seeking to buy cloud fungus for a million gold coins originally only spread amongst herbal merchants, but shortly afterwards, all the various nobles of the Kingdom of Fenlai learned of it as well. All of those nobles now knew that Master Linley needed cloud fungus.

If they could provide Linley with the cloud fungus, not only would they receive a million gold coins, they would also have a chance to build up a relationship with Linley.

Many nobles began to wrack their brains for methods by which they could locate cloud fungus.

But alas, cloud fungus was far too rare, and far too expensive.

After ordering this news to be spread out, Linley continued his life of solitary, pitiless training within his manor. In the blink of an eye, November arrived, and with it the temperature began to drop as well. The leaves of the trees within the Hot Springs Garden began to turn yellow and fall, filling the grass with sallow leaves.

“Haaaaa!”

Linley, who had been engaging in one-finger vertical push-ups suddenly exerted strength through his fingers, flipping himself into the air. Somersaulting easily through the air, Linley landed on the ground, his bare upper chest covered in sweat.

Aided by the Supergravity Field, after having trained for so long, even Linley’s powerful body was beginning to feel tired.

“Whew.”

Standing normally again, Linley felt the muscles in and near his fingers, arms, and shoulders all feel numb and sore. He found this feeling to be very comfortable, as he knew that in this situation, his muscles and bones were slowly strengthening.

The way to train one’s body was to exceed one’s limits time and time again, so long as one didn’t exceed the limits by too much each time.

Seating himself cross-legged, Linley immediately began to train in accordance with the ‘Secret Dragonblood Manual’, allowing the liquefied Dragonblood battle-qi in his dantian to begin to rush out. In a short while, the mighty Dragonblood battle-qi had filled Linley’s entire body.

Training, time and time again. Each time, the Dragonblood battle-qi would become a bit more pure, and Linley’s bones and flesh would become a bit stronger.

The azurish-black Dragonblood battle-qi entered his dantian again, then spread out again. The dantian was the nucleus for a Dragonblood Warrior. Linley had reached the late-stage of the sixth rank long ago, and in September and October, he had reached the peak of the sixth rank.

Right now, Linley had reached a plateau. He could break through any day now.

“Crack. Crack.” All sorts of strange sounds began to emit from Linley’s body. Linley’s muscles seemed to have a mouse buried beneath them, as they began to ripple up and down nonstop. Even his veins were popping out, and throughout Linley’s body, beads of sweat and beads of blood were beginning to come out!

“I’m finally about to break through.” Linley was shocked and pleased.

He had waited far too long for this day.

“Bubble, bubble.”

That azurish-black Dragonblood battle-qi began to roil about strangely, filling Linley’s entire body with pain. But within his dantian, that liquefied Dragonblood battle-qi began to condense itself yet again, increasing in density by several factors. The Dragonblood battle-qi was being drawn back into the dantian nonstop. And then, it would once again be emitted from the dantian yet again, forming a circle.

Whenever the Dragonblood battle-qi entered the dantian, it would transform.

After roughly an hour’s time had passed, all of the Dragonblood battle-qi in Linley’s body had undergone this transformation. Although there was theoretically only a thin barrier between the peak of the sixth rank and the early seventh rank, Linley’s strength was now several times greater than it had been in the past.

Linley opened his eyes, a look of uncontrollable excitement within them.

“Haha, I’ve finally entered the realm of a warrior of the seventh rank.” Linley was extremely excited.

As long as he were to agitate the Dragonblood battle-qi in his body, he would be able to assume the Dragonform. The training speed of the Dragonblood Warriors was extremely high, especially in the earlier stages. Linley had spent just about half a year before advancing from the sixth rank to the seventh rank. This sort of advancement was extremely astounding.

But Linley estimated that to progress from the seventh rank to the eighth rank, he would need several years, most likely.

The farther along one was, the harder the road would become. But nonetheless, most Dragonblood Warriors only needed a few decades to reach the Saint-level in power.

Bebe, who had been sleeping nearby this entire time, opened his sleepy eyes, which suddenly brightened. Excitedly, he spiritually said to Linley, “Boss, you reached the seventh rank?”

“Yeah.” Linley nodded happily.

“Then doesn’t that mean, once you Dragonform, you have the power of an early-stage ninth rank?” Bebe was excited. “Looks like your power is gonna be more than mine now, Boss!”

Linley began to laugh as well.

In the early stages, the boost to power provided by the Dragonform was quite dramatic. For example, as a warrior of the seventh rank, in the Yulan continent, he could only be considered an unremarkable fellow. But upon using the Dragonform, he would be an early-stage ninth rank warrior, who was qualified to be considered a notable figure in the world.

However, the more powerful one grew, the weaker the boost provided by the Dragonform would be.

Dragonform, after all, was nothing more than forcibly drawing out the Dragonblood which a weak Dragonblood Warrior hadn’t been able to fully absorb.

“Early-stage ninth rank, and your Dragonform was influenced by the Armored Razorback Wyrm. The Armored Razorback Wyrm specializes in speed and defense, while you also possess strong defense and unquestionably high speed.” Doehring Cowart appeared from the ring at this time.

Linley was very confident in his own speed.

Because after taking on the Dragonform, not only did he have the natural high speed of a Dragonblood Warrior, he could also utilize wind-style magic and boost himself with a Supersonic spell of the seventh rank, which would increase his speed by a good amount.

Linley was so pleased that he just stood there, grinning stupidly.

“Boss, stop laughing like an idiot. Look at yourself, you’re filthy. Take a bath, jeeze.” Bebe intentionally put a disgusted look on his face while covering his nose and jumping up and down as he bared his fangs at Linley.

Linley looked at himself.

At this moment, his body was covered in both sweat and blood. He really did look dirty.

“Splash!”

Linley jumped directly into the hot springs pool. The water in the hot springs was constantly flowing, so Linley didn’t worry about getting it dirty. After having experienced the sensation of his entire body transforming, then having the hot springs water rush against it, Linley felt so comfortable that he lay within the hot springs pool, eyes closed.

He fell asleep.

He felt so comfortable that he actually fell asleep.

Just as Linley was enjoying a beautiful dream, a voice rang out from outside. “Lord Linley. Lord Linley.” The female attendant’s voice clearly sounded rather anxious.

Linley’s eyes suddenly opened. Hearing the voice, he couldn’t help but frown. “Come in.”

Only then did that female attendant dare to enter the gardens. Standing at the side of the hot springs pool, she snuck a few looks at Linley’s naked body, then respectfully said, “Lord Linley, a herald from the palace is waiting outside. He says that he has come at the command of his Majesty, who is inviting you, Lord Linley, to make a trip to the palace.”

“By command of his Majesty?” Linley hesitated slightly, then directly clambered out of the pool.

“You can leave now.” Linley always dressed himself, as he didn’t like the female attendants helping him dress.

“Yes.” Her cheeks scarlet red, the female attendant quickly lowered her head and fled the Hot Springs Garden.

…..

Seated in a carriage, headed for the palace. Outside the carriage, aside from sixteen palace soldiers, there were sixteen knights from the Radiant Church. Linley’s entourage was larger than that of even the Left Premier or the Right Premier.

“Lord Linley, his Majesty is currently within the East Flower Garden.” The shrill voice of the palace attendant rang out.

“Lead the way.” Linley said abruptly.

The palace attendant was very deferential towards Linley, smiling at him the entire way.

“Who else has his Majesty invited this time?” Linley asked.

“Just you, Lord Linley.” The palace attendant replied.

“Just me?” Linley began to feel suspicious, but he didn’t ask anything further. Under the guidance of the palace attendant, Linley finally arrived at the palace’s East Flower Garden. As it was now already November, there were very few flowers which were still in bloom. But the countless flowers in the East Flower Garden of the palace were still vibrant and beautiful.

And that ‘Golden Lion’, King Clayde, was currently chatting with his Queen in the garden.

“Haha, Linley, you came.” Clayde greeted Linley in a very friendly manner. “Come, sit.” “Your Majesty. Queen.” Linley paid his respects, then sat down.

Clayde and the Queen exchanged glances, and then he grinned at Linley. “Linley, I heard that you have been looking for cloud fungus in order to create a medicinal bath for yourself?”

“Yes.” Linley nodded.

Suddenly, Linley had an idea as to why Clayde had specially requested his presence at the palace. But Linley didn’t quite dare to believe it. He was searching for this cloud fungus for the sake of dealing with Clayde. Could it be that Clayde was going to…

“Haha, I knew you were searching for this cloud fungus, so I sent my men out to do a search. By a stroke of good fortune, my palace storehouse just so happened to have a single clump of cloud fungus.” Clayde glanced at a nearby female attendant, who immediately presented a golden brocade box she was holding to Linley.

Linley was really, truly stunned.

The cloud fungus that he had been so desperately seeking, had been provided to him by King Clayde!
