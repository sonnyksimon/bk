#### Book 7, Heaven and Earth Turned Upside Down – Chapter 3, Would Definitely Die?

All of the powerful people in the highest floor of the Radiant Temple were stunned. Linley’s soul was a huge distance away from the level of crystallizing. He was nothing more than a magus of the seventh rank. Even an Arch Magus of the ninth rank wouldn’t be able to resist the Divine Baptism of the Radiant Sovereign.

“How is this possible?” The Ascetics, Executors, and Deputy Arbiters all began to mumble amongst themselves, unable to believe what they had just seen.

“It actually failed. The divine boon actually failed to successfully create a new Blessed One. Then…how should we deal with this Linley?” Heidens stared at Linley, suspended in mid-air. “An absolute genius such as him will definitely be a peak-stage Saint-level combatant within a hundred years. He might even become more powerful than me. By that time, the glory of our Radiant Church will be able to spread across an even wider territory.”

Heidens really couldn’t bear to just kill Linley.

“Your Holiness?” Guillermo called out softly.

Heidens’s lost, confused gaze suddenly sharpened. He had made his decision.

“Your Holiness, Linley hasn’t become a Blessed One. Then we…?” Guillermo asked.

Heidens looked at Linley. Under his control, Linley’s body slowly drifted down to the floor. At this point in time, Linley pushed himself to a standing position with his hands. Right now, Linley’s body was totally uninjured. It must be said that receiving a divine boon had its benefits.

Linley looked at the mighty people surrounding him.

“These people are all combatants of the ninth rank at least. If I were to struggle against them, I wouldn’t have any chance at all.” Linley coldly stared at Heidens and the others. “Your Holiness, what exactly are you intending to do with me?”

Suddenly, a smile appeared on Heidens face. “No need to ask too much. Executors, return Linley to his private room.” “Yes, Your Holiness.” Those six special Executors nodded.

Without giving Linley any chance to react, they immediately headed towards Linley, as one of them barked out, “Move! Or do you want us to drag you?”

They were forcing him by their actions. Linley had no options.

“Fine.” Linley opened the door and began to walk downwards. Those six Executors followed directly behind Linley. As Linley went down the stairs one level at a time, he saw that all of the guards, upon seeing those six Executors, were all extremely respectful.

Those six special Executors all wore bluish-violet robes. Those icy eyes of theirs stared at Linley, making him feel as though…if he acted untowardly in any way at all, they would immediately kill him.

After the six Executors had escorted Linley away, the female Cardinal, Melina, asked, “Your Holiness, that Linley didn’t become a Blessed One. Although we don’t know the reason why not, the decision we must come to right now is, what should we do with Linley?”

Guillermo and the others all looked at Heidens.

Linley was a genius. They all knew this. But Linley hadn’t become a Blessed One, and his mother had been killed by the Radiant Church. The Church had to come to a decision: Would they accept the risk of recruiting Linley and hide the truth behind the death of his mother? Or would Linley be put to death?

Although it would be possible to hide the truth behind his mother’s death for a time, once Linley entered the highest ranks of the Radiant Church, it would most likely be impossible to hide it any longer.

Heidens’ face was cold. In a cold voice, he said, “Kill.”

Guillermo and the others felt their hearts quiver.

“In a few more days, it will be the 10000 year anniversary of the Yulan Festival. Let’s arrange for Linley’s execution to be after the festival.” Heidens announced.

Guillermo, who had the closest relationship with Linley, sighed in his heart.

A genius who would have dominated the entire continent would now see his fate cut short. Guillermo knew very well that with Linley imprisoned in the Radiant Temple, there was no way Linley would be able to escape. Linley wouldn’t even be able to leave his cell.

“That Cesar has some sort of a relationship with Linley, but even Cesar doesn’t have the ability to break into the Radiant Temple to rescue Linley.” Guillermo sighed secretly.

Linley would definitely die!

On the ninth floor of the Radiant Temple, within the private cell.

“Get in.”

Linley entered the cell, and the six Executors closed the door behind him.

As the six Executors turned and immediately left, one of them, a silver-haired man, turned to look at Linley. “Kid, let me give you a reminder. Although you have recovered your strength, don’t dream about breaking out of this cell.”

The other five Executors halted as well, and a bald old Executor laughed, “Break out of the cell? Kid, if you are able to break out of this cell, that would mean your power is on the level of his Holiness himself.”

“What do you mean?” Linley asked.

Linley himself couldn’t see anything special about this cell. Given his power as a warrior of the ninth rank when Dragonformed, an ordinary stone cell would be shattered as easily as paper.

“The Radiant Temple is the most incredible edifice the Radiant Church possesses. The entire Temple itself hides a massive magical formation within it known as the Glory of the Radiant Sovereign. It’s impossible for you to do the slightest bit of damage to it, whether from the inside or from the outside.” That silver-haired man said proudly. “Kid, let me tell you, the only chance you have of breaking out from this cell is by breaking the lock on the cell door. I can also tell you that the lock is made from metals that were alloyed with some adamantine.”

Finished speaking, the six Executors laughed loudly amongst each other, then left.

Linley was silent.

When he heard the words, ‘adamantine’, Linley understood that it was probably impossible for him to break out. According to legend, when the earth-style spell Earthguard reached the Deity level, the Earthguard armor would be composed of adamantine. Its power and durability was enough to be able to withstand several blows of even a Deity-level combatant. As for a Saint-level combatant, there was no way at all for them to break it.

Linley was an earth-style magus, and so naturally he knew about the legends regarding the Earthguard armor at its peak power.

Upon becoming a Saint-level Grand Magus, the Earthguard armor would be composed of diamonds, and upon breaking through to the Deity-level, the armor would be of adamantine.

“Linley, I expect that this cell is used for the Radiant Church to imprison combatants of the ninth rank, or perhaps even the Saint-level.” Doehring Cowart spoke. “Although this lock only has a trace amount of adamantine and isn’t pure adamantine, it would probably be hard for even a Saint-level combatant to break it.”

Linley nodded.

From the words of the Executors, he had already figured out that he would not be able to break out, as they had said that breaking out would demonstrate Linley’s power was at least on par with the Holy Emperor.

That next afternoon.

Monroe Dawson, Yale, Reynolds, and George were all seated together around a table covered with breakfast items. During this period of time, Yale, Reynold, and George had never stopped being worried about Linley. But even Monroe Dawson making a personal appeal had failed. What could they possibly do?

Break into the Radiant Temple to rescue Linley? Even Monroe Dawson wouldn’t dare to do such a thing.

“Yale, in two days, it’ll be the Yulan Festival. This Yulan Festival will be the 10000th Yulan Festival, which we’ll only see once in our lives. You three kids can have a nice, rowdy time.” Monroe Dawson chortled.

Monroe Dawson had treated these two dear bros of his son Yale with the utmost friendliness.

This was because all three of Yale’s bros were quite out of the ordinary. Linley, George, and Reynolds. Reynolds’ clan possessed an astonishing amount of power in the O’Brien Empire’s military. George’s clan held tremendous influence within the Yulan Empire, and wasn’t much weaker than the Leon clan.

As for Linley, although his clan was now weak, it was still the clan of the Dragonblood Warriors. And Linley’s own potential was limitless.

Suddenly, the sound of footsteps.

“Milord Chairman, an emissary of the Radiant Church have arrived.” A servant said respectfully.

Hearing the words “Radiant Church”, the eyes of Yale, George, and Reynolds all lit up, and they turned to look at the servant. Monroe Dawson knew what his son was thinking, and he immediately instructed with a laugh, “Let them in.”

“Yes.”

A short while later, a Vicar walked in. He said respectfully, “Chairman Dawson. His Holiness instructed me to deliver this letter to you.” As he spoke, he withdrew from within his clothes a beautifully, lavishly decorated letter.

The servant immediately accepted the letter, then gave it to Monroe Dawson.

Monroe Dawson immediately opened the letter. But upon seeing the contents of the letter, his face changed. He said coldly, “You can leave now.”

That Vicar bowed slightly, then left.

“Father. What is in the letter?” Yale asked urgently. “Does it have to do with Third Bro?” Reynolds and George all looked hopefully at Monroe Dawson.

Monroe Dawson nodded.

“The Holy Emperor informs me that the internal deliberations of the Radiant Church have concluded. They will execute Linley in secret.” Monroe Dawson’s words were like thunder, ringing in the ears of Yale, Reynolds, and George, whose faces immediately turned white. They were stunned for a long moment.

“No, no way.”

Yale was the first to begin shouting. He snatched the letter from his father’s hands, and with shaking hands held it as he began to read. By his side, Reynolds and George both craned their necks to take a look as well. But when the three of them saw the contents, they all turned frantic with fear.

“No!!!”

Yale leapt out of his seat, intending to rush directly out of the hall. “Yale!” Monroe Dawson frowned, shouting coldly.

“Stop him.” Monroe Dawson ordered.

Yale turned his head to stare at his father. Frantic, he said, “Father, I beg you, lead some men to rescue Third Bro. If necessary, the Conglomerate can give up something valuable. I refuse to believe that the Radiant Church won’t care whatsoever about our Conglomerate. Father, I beg you.”

“Hmph, what do you know? If there really were terms that could be negotiated, the Holy Emperor would’ve started negotiating with me long ago. The grievance which Linley has with the Radiant Church clearly isn’t what we thought it was. Otherwise, the Radiant Church wouldn’t decide to execute a genius like him. Enough. Men, escort your young master to his room. Let him spend a good period of time calming down.”

Immediately, the guards escorted Yale back to his room. No matter how frantically or how angrily Yale protested, it was of no use.

Reynolds and George could only maintain their silence.

They didn’t have any special relationship with Monroe Dawson, after all. But in their hearts, they were frantic on Linley’s behalf.

A visitor had arrived at Linley’s cell. It was Guillermo.

“Guillermo.” Linley looked at Guillermo with some surprise.

Guillermo had brought with him an extremely lavish meal, and delivered it through the small opening in the cell door.

Guillermo looked at Linley. He let out a sigh. “Linley, I really viewed you very favorably. But…alas. Perhaps it was meant to be, that you couldn’t become a member of our Radiant Church. Alright, have a good meal. You won’t have many meals left.”

Hearing these words, Linley was stunned.

“Lord Guillermo, what do you mean by saying this?” Linley looked at Guillermo.

Guillermo let out a sigh. “In two days, which is to say, January 2nd, the last day of your existence will arrive.” Guillermo really did like this young man, Linley. Especially after finding out the reason why Linley attempted to assassinate Clayde, Guillermo felt all the more regretful for how Linley’s fate had turned out.

He could’ve had a glorious future, but for the sake of his parents’ deaths, he was willing to forsake everything in order to gain revenge.

Although he, Guillermo, would never have acted in such a way, in his heart, he still felt admiration for Linley.

“January 2nd?”

Linley’s facial expressions changed several times, but finally he closed his eyes. He already completely understood. Clearly, in two days, he would be put to death.

“Thank you, Lord Guillermo. If it wasn’t for you, I would’ve clung to the hope of surviving.” Linley laughed calmly.

Guillermo looked at Linley. With a low sigh, he shook his head, then turned his head, leaving Linley alone in his cell.

“January 2nd. They had to wait until after the Yulan Festival to kill me, eh? Tomorrow will be the Yulan Festival. I believe it will also be the day of Kalan and Alice’s marriage as well, right?” Knowing that he was about to die, Linley somehow felt calmer and more at peace than he ever had before.
