#### Book 7, Heaven and Earth Turned Upside Down – Chapter 4, A Giant Foot

The night between year 9999 and year 10000. The snow flew about as the temperature in Fenlai City dropped to an astonishing low. Within a cold cell in the Radiant Temple, Linley was resting against one of the icy stone walls of the cell.

Linley didn’t notice the cold at all.

“I know that I am about to be put to death, but I don’t have any ability to resist at all.” Linley lowered his head, sighing softly.

He had made attempts, had tried.

But this cell was exactly as the Executor had described. It possessed incredible endurance, and even in Dragonform Linley was not able to break the lock or the room in the slightest. All he could do was quietly wait for the sentence which was soon to be carried out.

The dark night went by quickly, and that great blizzard finally came to an end as well. Both the nobles as well as the commoners were celebrating, in their own ways, the arrival of this 10000th Yulan Festival on this glorious, cloudless day. In particular, the Radiant Temple.

On this day, in the air above the Radiant Temple, countless beautiful mirages and illusions created by magical formations were on display.

In the Holy Capital, Fenlai City, today was a day for a sea of celebrations. That massive plaza in front of the Radiant Temple was filled to the brim with people who hailed from all sorts of places. Everyone was calling out in excitement to each other over this 10000th Yulan Festival, and the Radiant Temple organized many lively activities as well.

Yale, Reynolds, and George were within the third floor of a hotel. They stared at the far-away Radiant Temple Plaza. The plaza was covered densely by people, a veritable sea of people.

“Boss Yale, are we going to go to the wedding ceremony of the Debs clan today?” George asked.

The wedding of Kalan of the Debs clan was on Yulan Day. Today was an extremely propitious day, and there were many families in Fenlai City holding wedding ceremonies on this day. These sorts of weddings would start at noon, and continue until nightfall.

“Yes. Of course.” Yale had an ugly look on his face.

Due to Linley’s affairs, Yale, George, and Reynolds were all in low spirits.

“Hmph, Third Bro was too soft-hearted towards this b*tch and that punk Kalan. But now, Third Bro is going to be executed, while that b*tch and that punk Kalan are going to be enjoying themselves and hold a wedding ceremony.” Yale was burning with rage.

He had never looked kindly upon Alice and Kalan.

Especially right now, with Linley on the verge of being executed, and himself unable to save him. He had no place to vent his frustrations and anxiety. This only made him now view Alice and Kalan even more unfavorably.

“Right. They want their wedding to go smoothly? In their dreams!” Reynolds ground his teeth as well.

Even George felt a desire to wreck this wedding.

Yale, George, and Reynolds had all been consumed by worry for Linley for days now. Knowing that Linley was about to die, but not having the ability to rescue him, they couldn’t help but think back to all the years they had spent together growing up. They hated themselves all the more for not having the power to save him.

And right at this time, Alice, who had discarded Linley, was now going to get married to Kalan.

How could these three just let it slide?

On Greenleaf Road, the Deb’s clan’s manor.

At noon, one noble or magnate after another arrived at the Debs clan’s manor. Although after the smuggling case, the Debs clan was no longer one of the topmost clans of the Kingdom of Fenlai, they were still a clan with some influence. At least, in the Kingdom of Fenlai, they could still be ranked amongst the top twenty. “Lord Count Juneau has arrived!”

“Lord Baron Prey [Pu’lei] has arrived!”

Nobles, noble ladies, affluent girls, all entered the manor of the Debs clan. The leader of the Debs clan, Bernard, welcomed them all in a very friendly manner. The Debs clan’s power had shrunk dramatically, but within the Kingdom of Fenlai, they were still able to remain standing on fairly stable footing.

“Lord Duke Bonalt has arrived!”

Hearing the words ‘Duke Bonalt’, many nobles turned to look at the door. Even Bernard immediately hurried over to welcome him. Right now, the highest ranking person attending this wedding ceremony would be Duke Bonalt. Last time, at the engagement ceremony, even King Clayde, the ruler of Fenlai, had come. But this time, for the wedding proper, his Majesty did not come.

Everyone knew the reason why.

“Lord Duke, your attendance brings great honor and joy to our Debs clan.” Bernard said humbly.

Duke Bonalt nodded.

After the assassination attempt at Linley’s manor, the Right Premier Merritt had died. Although Clayde had promoted another important minister to the rank of Right Premier, in terms of influence, there was no way he could compare for now with Merritt, who had been Right Premier for decades.

What’s more, the Minister of Finance, Patterson, was also dead. Right now, in the entire Kingdom of Fenlai, perhaps the most powerful, influential figure aside from the ruler was this Left Premier, Duke Bonalt.

“Kalan, come and pay your respects to Duke Bonalt.” Bernard immediately called out.

Kalan was dressed very handsomely today. The pure black tailored suit he was wearing made him the most outstanding-looking young man present today. Kalan very modestly bowed in front of Duke Bonalt. “Duke Bonalt, welcome to my wedding.”

“Congratulations, Kalan.” Duke Bonalt said with a casual laugh. But just at this moment…

“Young master Yale of the Dawson Conglomerate has arrived!”

When these words rang out, Bernard’s eyes immediately lit up, and even Duke Bonalt headed over alongside him. These other nobles of Fenlai naturally couldn’t compete with the Dawson Conglomerate. The Dawson clan of the Dawson Conglomerate was one of the most powerful clans within the entire Yulan continent.

Yale, dressed in a sleek black suit.

Reynolds, dressed in a handsome blue suit.

George, dressed in a faintly checkered white suit.

The three walked in, shoulder by shoulder, causing all the watching nobles to stare at them with bright eyes. Generally speaking, magi would have a certain aura about them. This was because magi often entered the meditative state, resulting them being very much in sync with natural elemental essence. That, combined with their powerful spiritual energy, gave them a certain aura.

In addition, Yale, George, and Reynolds all belonged to ancient clans. Clearly, their refinement and aura could not be matched by the likes of most nobles in Fenlai.

“Young master Yale, welcome! And these two are?” Bernard could tell at a single glance that these two definitely weren’t from any ordinary clans either.

Yale laughed calmly. “These two are my two dear bros from the Ernst Institute.”

Reynolds courteously said, “Mr. Bernard, I am from the Dunstan [Deng’si’tan] clan of the O’Brien Empire. You can address me as Reynolds.”

“The Dunstan clan?”

Both Bernard and Duke Bonalt were startled. Everyone present with some experience knew of the fame of the Dunstan clan. The Dunstan clan was an extremely powerful clan within the O’Brien Empire, a clan which controlled an exceedingly powerful military force.

“Young master Reynolds, our Debs clan warmly welcomes your arrival!” Bernard said excitedly.

The arrival of a young master from the Dunstan clan naturally gained a great deal of face to Bernard. Nearby, Kalan also bowed very courteously. But it was clear that the difference between him and a descendant of one the great clans was extremely large.

“And this?” Bernard looked towards George.

George smiled. “Mr. Bernard, I come from the Walsh [Hua’shi] clan of the Yulan Empire.”

“The Walsh clan?” The hearts of all the nearby nobles thumped hard. The Walsh clan was an ancient clan with thousands of years of history. In the Yulan Empire, they possessed tremendous influence, and was pretty much on the same level as the Leon clan of Dixie and Delia.

“Young master George, your arrival today brings exceptional honor to our Debs clan.” Bernard was extremely humble.

Both the Walsh clan and the Dunstan clan were extremely powerful clans from the Four Great Empires. They were clans which could influence the internal strategies of their respective empires. Even before the fall, the Debs clan couldn’t compare with the likes of these clans, much less the current Debs clan.

The wedding ceremony of the Debs clan was a very lively affair. Many nobles as well as many young noble ladies wanted to strike up conversations with Yale, George, and Reynolds. In the eyes of those young noble ladies, even if they abandoned the thoughts of becoming a principal wife, if they could become even just a secondary wife to one of those three, their clans would receive countless benefits.

As for the original center of attention, Kalan, much less attention was now paid to him.

But there were three people whose attentions were focused on him. Yale, Reynolds, and George.

“Look. Miss Alice and Miss Rowling have arrived.” Suddenly, a voice rang out in the hall. Right now, the two female leads had appeared, dressed in beautiful wedding gowns. They entered from a side door, and Kalan immediately went to go welcome them. Very naturally, both Alice and Rowling slipped their arms around Kalan’s.

At this time, Yale, Reynolds, and George finally acted.

“Haha, Kalan, these two must be your wives, right? They really are beautiful!” Reynolds was the first to laugh and walk over.

Seeing them walk over, Kalan immediately headed towards them with his two wives. “Rowling, Alice, pay your respects to these three young masters. This is young master Reynolds of the Dunstan clan, and this is…” But halfway through his words, George let out a cry of surprise, shouting out loudly, “Alice?! You’re getting married to this Kalan?”

George’s shout was very loud. These words caused the entire hall to fall silent.

To say something like this at someone’s wedding ceremony was far too impolite.

“Right, Alice, aren’t you dating our Third Bro?” Reynolds added.

It was Yale’s turn to speak. “Second Bro, Fourth Bro, you two didn’t know this, but this Alice has already broken up with Third Bro. She’s going to get married with this Kalan now.”

“She broke up with Third Bro?”

George and Reynolds both shook their heads, sighing.

Reynolds then immediately said, “Alice, since you abandoned our Third Bro to be together with this Kalan fellow, then you definitely will be his principal wife, right?” “Actually, no. The principal wife is this Miss Rowling. This was already proclaimed at the engagement ceremony.” Yale immediately said.

These two sentences made Alice’s face turn scarlet, while the look on Kalan’s face was extremely awkward as well. But not a single person in the entire hall dared to berate Yale, Reynolds, or George for their discourtesy. Given their statuses, who would dare?

“Three young masters, we have to toast our guests. Please excuse us.” Kalan forcibly suppressed the rage in his heart and spoke modestly.

“Alright.” Reynolds nodded as well.

Kalan immediately led Rowling and Alice towards other tables. Yale, George, and Reynolds only coldly watched him depart. Thinking about how Linley was probably going to be executed soon, their hearts were filled with even more rage at the injustice of it all.

Suddenly…

“Bam!” “Bam!” “Bam!”

A terrifying series of sounds could be heard from outside. It was a low, somber sound that made the earth tremble with each vibration, and all of the utensils in the hall were knocked to the floor.

“What’s going on outside?” A nobleman in the hall stood up in surprise.

“Rowling, Alice, stay put.” Kalan immediately ran out of the main hall with his father, and many other nobles ran out as well. They wanted to see what exactly was going on outside, for such a huge ruckus to be caused.

Reynolds, Yale, and George also headed outside, curious.

But right at this moment…

“BAM!”

A giant foot suddenly descended from the heavens, landing directly in the front courtyard of the Debs clan’s manor. That giant foot just happened to land directly on Kalan and Bernard, who had just entered the front courtyard. The sound of bone splintering could be heard as the two of them, father and son, were immediately smashed into a meaty paste. The ground was stained with their blood.

That foot was over four meters long, and was covered with thick golden fur.

“Ah!” Many people raised their heads to stare at the monster.

This was an enormous golden-furred ape, at least twenty or thirty meters tall, the size of an eight-floor tall building. This gigantic golden ape’s eyes were like a pair of giant purple carriage wheels. The giant golden ape’s body seemed to be brimming with power, causing the very air around it to shudder.

“Violet-Eyed Goldfur Ape! A Saint-level magical beast, the Violet-Eyed Goldfur Ape!” Seeing this magical beast, Yale couldn’t help but stare at it, his jaw slack.

That Violet-Eyed Goldfur Ape raised its head, letting out an excited howl, and spoke in the human tongue, “Haha, kill, haha, kill them for me! Kill them all! The more you kill, the greater the rewards the King will give you! Haha, kill!”

“Bam.” “Screech!”

Yale, George, and Reynolds could suddenly hear the howls and cries of magical beasts from all directions, as though the entire world had suddenly been filled with them. Suddenly, Yale, George, and Reynolds saw that the entire sky had been filled with countless, innumerable flying magical beasts!

“Dragonhawks! These are Dragonhawks! This…” Reynolds was stunned and slackjawed as well.

From far away, an enormous flock of Dragonhawks had appeared, covering the entire sky with their presences. The density of Dragonhawks was so high that there was no way to count their number.

Suddenly, everyone felt as though the day of the apocalypse had descended upon them. Right now, no one could be bothered to grieve or feel pity for Kalan and Bernard, who had been crushed to a pulp by that giant foot of the Violet-Eyed Goldfur Ape.
