#### Book 7, Heaven and Earth Turned Upside Down – Chapter 8, One Hand

In a secluded courtyard within the palace, the most important members of the royal clan of Fenlai were gathered, including Clayde, his wives, and his many children.

“The entire city of Fenlai is swarming with magical beasts. We definitely cannot all travel together in a large group, as that would attract some extremely powerful magical beasts.” Clayde said solemnly. This reasoning was something everyone understood, and was the reason why Director Maia and Menlo Dawson were travelling in small convoys.

Convoys of a few dozen people were everywhere in Fenlai City, and weren’t remarkable at all.

But a convoy of several hundred people would draw the attention of magical beasts of the ninth rank, and perhaps even result in an attack from a Saint-level magical beast.

The most dangerous thing one could do right now was to attract attention from magical beasts.

“Carre [Ka’lei], you and your mother shall lead a division of the Wildthunder squad soldiers. Here are five un-imprinted magicrystal cards. Remember, this represents thousands of years of accumulated wealth of our clan!” Clayde looked solemnly at his son.

There were too many people in the royal clan. They had to go in separate packs.

Clayde didn’t want for his clan to be annihilated. By going in separate packs, the chances of at least some surviving would be greater.

“Yes, father.” Carre was overjoyed.

Thousands of years of accumulated wealth…how much would that be worth?

“Shaq [Sha’ke], you, your mother, and your younger sister will also lead a division of the Wildthunder squad soldiers. Here are five magicrystal cards for you as well.” Clayde withdrew another five magicrystal cards and handed them to his second son. Both of the princes were extremely excited.

Clayde’s face was very solemn. He said, “The elite soldiers of our clan will be divided into these three divisions. Carre’s, Shaq’s, and my own. No matter who manages to survive in the end, at least our clan will continue. Enough, let’s head out!”

“Kaiser, as the instructor for the Wildthunder Regiment, you will come with me.” Clayde looked at Kaiser.

“Yes, your Majesty.” Kaiser nodded.

The Wildthunder Regiment was the most powerful defensive regiment within the Kingdom of Fenlai. The entire regiment, including Kaiser, only consisted of a hundred people, giving each squad only 33 soldiers. But although they were small in number, they were high in quality. Even the weakest member of this regiment was a warrior of the seventh rank.

Divided into three squads, the royal clan immediately began to flee in three separate directions.

….

“Swish!” Linley leapt up at a high speed. At the same time, there was a violet flash of light, and the Thunderwing Pegasus that was harassing Linley suddenly split into two halves. Linley continued to run forward, making his way towards the palace at high speed.

On the way, he passed by far too many human and magical beast corpses.

“Arrived at the palace.” Linley was leaping forward so fast that his body was naught but a blur, and with each movement, he travelled dozens of meters. This sort of astonishing speed made it impossible for magical beasts of the fifth and sixth ranks to stop him.

“Whew.”

Linley easily leapt up over ten meters in the air, flipping into the interior of the palace.

“Roaaaar!” The sound of magical beasts roaring could be heard from within, as well as the battle cries of soldiers. Right now, there were no longer any guards at the palace gates. The only things present were corpses, blood, and rent flesh. And, occasionally, a massive corpse of a magical beast.

Like an agile treecat, Linley leapt his way through the tops of the various palace buildings.

But when Linley arrived on top of one particular roof, he suddenly saw a mounted squad far away. Right now, virtually no one was using carriages anymore. Carriages were simply too slow for fleeing.

“That is…”

Linley instantly was able to recognize that golden-haired man in the center of the squad. It was the ‘Golden Lion’, Clayde. Clayde was currently issuing orders to his soldiers to kill the magical beasts besieging them. This squad’s teamwork was really quite marvelous.

When a group of elite warriors of the seventh and eighth ranks worked together as one, they were actually more powerful than a group of the same size consisting only of warriors of the eighth rank that had no teamwork.

“Clayde.” Linley’s eyes lit up.

“Boss, let’s make our move.” Bebe was excited as well.

“Wait. We can’t afford any mistakes this time. Wait for his squad to get closer to us, and then we will launch a sudden ambush.” Linley remained on top of the roof, his cold eyes focused on that distant mounted squad.

….

“Don’t waste any time. Quick.” Clayde swung the giant warsword in his hands, chopping down a Dragonhawk from midair.”

During this past half month, Clayde had managed to purge a small amount of Bloodrupture poison from his system, allowing him to recover 10% of his battle-qi. Although it was just 10%, he once more had the power of a warrior of the eighth rank.

But Clayde recognized that he would most likely need another half year to purge the remaining 90% of Bloodrupture poison from his body.

“Where the hell did all these magical beasts come from. Bastard.” Clayde was growing more and more furious.

These magical beasts had destroyed his capital, and now they were threatening his life. How could he not be angry?

“Quick.”

After killing all of the attacking magical beasts, Clayde immediately pressed his men to hurry on, and the troop of knights once more sped forward. As Clayde and his men travelled at high speeds through the pathways between the palace buildings, they didn’t notice at all that someone was lying in wait on the roofs above.

Watching Clayde and his men draw nearer and nearer, Linley narrowed his eyes.

All the fur on Bebe’s body was standing straight up.

“Now is the time!”

Linley’s voice rang out in Bebe’s head, and the two of them, man and magical beast, flew at high speeds towards Clayde. In that split second before launching, Linley’s entire body was suddenly covered with a layer of black scales, and spikes sprouted from his forehead, his elbows, and his knees. From behind, that draconic tail sprouted out as well.

Dragonform!

That squad of knights lived up to their reputation of being elites. As soon as Linley and Bebe flew towards them, they immediately noticed and tried to react. But Linley and Bebe were simply too fast!

“Ah! It’s you!” Immediately seeing that terrifying creature, Clayde knew without a doubt that Linley had come!

He didn’t have time to wonder why Linley wasn’t dead yet, because Linley’s draconic tail had already arrived, viciously slapping at him from just two meters away. Behind him, Kaiser had already become caught up dealing with that black Shadowmouse and wasn’t going to be able to save him.

“Whap!”

Linley’s draconic tail slapped down mercilessly, and Clayde quickly dodged by tumbling to the ground. Linley’s tail thus landed on the horse, and the animal was split into two halves by the sheer force of that vicious blow. The warhorse let out a pain-filled whinny before collapsing.

Fallen on the ground, Clayde pressed down on the ground with his fists and quickly retreated.

But now, Linley came chasing after him.

“Swish swish!” At the same time, eight spears gleaming with battle-qi were thrust at Linley.

“Haaargh!”

The Dragonblood battle-qi in Linley’s body burst forth, and he used his right leg to viciously kick at the ground. He instantly reached an extremely high momentum as he shot forward like a boulder that had been catapulted forth in anger. Linley’s body smashed fiercely against those eight spears.

The eight spears shuddered at almost the exact same time, and those eight streams of power essentially managed to cancel out with the power of Linley’s charge.

“This will be troublesome.” Linley frowned.

He didn’t expect those eight knights would be able to block his attack so effortlessly.

But what Linley didn’t know was that those eight knights were shocked and terrified as well. These eight knights were Clayde’s personal bodyguards, the most elite of the elite Wildthunder Regiment. All of them were warriors of the eighth rank. Working together, the eight of them would even be able to hold off a warrior of the ninth rank.

However, not even a warrior of the ninth rank would dare to forcibly ram into their spears. But Linley had.

“What a freakishly strong defense.” Hiding far away and protected by the remaining knights, Clayde’s heart trembled.

“Shkreeeee!”

Bebe let out a piercing screech, then swept his fierce claws at Kaiser again and again, while sometimes using his fangs to bite at him as well. But Kaiser rather effortlessly managed to use his greatsword to block each of Bebe’s attacks. Kaiser’s sword techniques seemed very simple but were highly effective.

One step back, then a piercing stab with the sword that seemed incredibly hard to block.

“Clayde, who is going to rescue you today?” Linley looked at the mighty warriors in front of him and sneered. “Fine, you want to engage in group attacks?” As soon as Linley finished speaking, he immediately charged at one particular knight.

Linley didn’t fear or pay attention to the attacks of the other knights, simply aiming himself at that one knight.

Now, their combined attacks were useless.

“Whoosh!” Linley was simply too fast. In the blink of an eye, he arrived by the side of that warrior of the eighth rank. Balling his fierce claws into a fist, he slammed it towards that warrior. The warrior leaned back to avoid it, but at this time, Linley’s draconic tail suddenly swung forward and crushed the warrior’s skull in.

“Thrall[Sa’er]!” Many of the knights howled in fury.

The Wildthunder Regiment had always trained together, and their affection for each other was no less than that of blood brothers. Many warriors furiously aimed their attacks at Linley, and despite their anger, they were still able to coordinate their attacks very well, as greatswords and long spears attacked in perfect sequence.

“Pew!” The Bloodviolet Godsword suddenly appeared in Linley’s hands. Ignoring the attacks aimed at him, Linley flew to another knight while thrusting Bloodviolet directly towards his eyes. The sword went straight through his skull. The man died immediately.

“Die!” Instantly, another one of the knights pierced at Linley’s head with his own spear.

Linley flipped Bloodviolet around and struck a counterblow. Just as the knight was about to attempt to block it, Bloodviolet suddenly curved in midair and effortlessly cut the knight’s head off. Even without being activated by battle-qi, the Bloodviolet Godsword in Linley’s hands could easily kill a warrior of the seventh rank. And now, suffused by Linley’s Dragonblood battle-qi, the Bloodviolet Godsword was more than capable of killing a warrior of the eighth rank as well.

Bizarre attacks!

Three of the warriors of the eighth rank had died in the blink of an eye.

“I want to see how you’ll block me!” Linley once again charged towards Clayde, the devilish Bloodviolet Godsword flashing nonstop in the air. None of the knights dared to close with Linley, because that Godsword in Linley’s hands was simply too bizarre.

“Grooooowl!”

Suddenly, from far away, a roar could be heard.

“Rumble, rumble, rumble.” Ponderous, heavy footsteps shook the earth. The deep sounds and vibrations made it more than clear that this was an enormous magical beast headed their way, and it was drawing closer.

But Linley didn’t care about anything at this point.

“Block him, block him!” Clayde shouted loudly, while continuing to retreat.

Linley suddenly leapt into the air, launched himself off a wall, and flew towards Clayde at high speed. Seeing this though, Kaiser instantly kicked off and launched himself backwards as well, transforming into a blur and sweeping the greatsword in his hands directly towards Linley.

“Come.” Linley didn’t attempt to block the sword at all, aiming the Bloodviolet Godsword in his hands directly at Clayde.

“Last time, you had a Saint-level Fateguard to protect you and Heidens to save you. I want to see who will rescue you this time.” Linley’s dark gold eyes spat death at Clayde, and the Bloodviolet Godsword in his hands struck out towards Clayde’s throat like a vicious snake. Right now, Clayde had almost gone crazy as he began to wave the greatsword in his hands in an attempt to block.

“Haaah!” Very suddenly, Kaiser released his grip on his greatsword, letting it fly.

“Bam!” Linley didn’t manage to react in time, and his right arm was struck heavily by the greatsword. Right at that moment, the burning battle-qi contained within the greatsword burst forth. Linley felt his arm suddenly grow numb. Due to this smashing blow, the Bloodviolet Godsword in his hands was now more than a meter away from his target, Clayde.

“Hmph.”

The Bloodviolet Godsword suddenly curved in midair, wrapping itself around the greatsword in Clayde’s hands, and then slid down until it was wrapped around Clayde’s wrist.  Chop!

“Whap!”

Clayde’s right hand was cut off, and it fell to the ground with a thud. The fingers on the hand were still extended, and the sword fell to the ground as well. In addition, that severed hand had a ring on it. That ring was the most precious item of all to the royal clan of Fenlai – the interspatial ring.

“My hand! Get it back, get it back!” Clayde’s face had turned white from the pain, but he still shouted furiously.

This interspatial ring contained 22 magicrystal cards with a total value of 2.2 billion gold coins! In addition, it had several dozen precious treasures that the royal clan had accumulated over thousands of years. Clayde would rather die than allow this interspatial ring to be lost. This was the accumulated wealth of countless generations of his clan!

“Swish!”

A black blur suddenly flashed by and made off with the severed hand, then leapt onto Linley’s shoulders.

“Boss, the more Clayde wants something, the more we will work to prevent him from getting it.” Standing on Linley’s shoulders, Bebe mentally spoke to Linley. “But Boss, why would he want this severed hand so much? There’s nothing special about this hand. Could it be that it is this ring that he wants?”
