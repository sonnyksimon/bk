#### Book 7, Heaven and Earth Turned Upside Down – Chapter 1, A Hope of Living

Upon hearing the words, “What a waste of a genius”, Clayde exulted mentally.

He already knew that the Holy Emperor’s choice was.

“You can leave now.” Heidens waved his arm and said calmly.

“Yes, Your Holiness.” Clayde bowed respectfully, then turned and left the top floor of the Radiant Temple. In the entire hall, only Holy Emperor Heidens now remained. Walking to a window, Heidens stared down at the city of Fenlai, maintaining a long silence.

After a long time…

“Knock!” “Knock!” “Knock!” The sound of knocking on the door.

“Enter.” Heidens said calmly.

The person who entered was Cardinal Guillermo. Guillermo glanced at Heidens’s back. Able to sense that Heidens was in a foul mood, he respectfully lowered his voice. “Your Holiness, how should we attempt to persuade Linley?”

“Persuade? No need.” Heidens said calmly.

Guillermo couldn’t help but raise his head to stare at Heidens in astonishment. If they wanted Linley to be of use to them in the future, at the very least they would have to speak with him and persuade him. After all, not only had Heidens severely injured Linley, Linley had a deep grudge against Clayde to begin with.

“Guillermo, do you know who Linley’s mother was?” Heidens turned his head to stare at Guillermo. Guillermo was startled. Curiously, he said, “Linley’s mother? Didn’t she die while giving birth to Linley’s younger brother?”

“No.”

Heidens shook his head. “When you investigated Linley’s background and information regarding his mother, you weren’t able to uncover the truth. Linley’s mother was actually that woman we acquired twelve years ago.”

That woman from twelve years ago!

Guillermo instantly remembered, because that woman had had a huge impact on the upper levels of the Radiant Church.

“But if we’ve already killed his mother, then…” Guillermo instantly understood why the Holy Emperor was now in such a foul mood.

A genius such as Linley was extremely enticing. But in the future, once Linley discovered the truth about his mother, he would be a huge threat to the Radiant Church.

“Guillermo. The 28th of this month will be the day when the glorious aura of the Radiant Sovereign will be the strongest, is it not?” Heidens said suddenly.

“Yes.” Guillermo was somewhat perplexed by Heidens raising this question.

“Make the preparations. That night, I intend to beg the Radiant Sovereign for a divine boon.” Heidens said calmly.

“Divine boon?” Guillermo was greatly shocked, but then he quickly understood Heidens’s plan. He secretly sighed to himself, “The Holy Emperor is most likely requesting this divine boon on behalf of Linley. Although this will limit Linley’s future potential, given his talent, he will still be an incredible figure. Only, what a waste of his talent.”

A Divine Boon was in reality a manifestation of the divine power of the Radiant Sovereign in the material world.

The Radiant Sovereign, as a Sovereign, one of the most powerful entities in existence, could extend a thread of his divine, faith-based power to totally cleanse a person’s soul, causing them to be wholly devoted and faithful to the Radiant Sovereign. Only a person who had already reached the Saint-level and was able to crystalize his soul would be able to resist the effects of this Divine Baptism.

Everyone else…definitely could not resist!

But after his soul had been affected by the Divine Baptism of the Radiant Sovereign, Linley’s natural talent would be impacted as well. His future accomplishments would definitely be a bit lower.

“What a waste. What a waste of a talent.” Heidens sighed again. This was the reason why earlier, in front of Clayde, he had said the words ‘what a waste’. Heidens was, however, very confident. Once he had been affected by the Divine Baptism, even if he later found out about his mother’s death, Linley would still be loyal and faithful to the Radiant Sovereign.

Because the faith this Divine Baptism created would go deep within a person’s soul!

In the blink of an eye, ten days passed. The city of Fenlai was as calm as it had always been, but all the major noble clans in Fenlai felt a strange, oppressive atmosphere. For example, his Majesty, King Clayde, was always in a terrible mood these days, and several major ministers and nobles had run afoul of his temper and been executed.

On the Fragrant Pavilion Road, behind a lavishly decorated hotel, a group of people were gathered together within a quiet, three-story building.

Yale, George, and Reynolds had been here this entire time.

Ever since they had found out about what happened to Linley, the three of them had continued to worry for Linley. They knew very well what a huge disaster Linley had dragged down upon himself. Not only had he openly attacked King Clayde and killed over a thousand elite warriors of the kingdom, he had even forced the Holy Emperor himself to subdue him in the end.

“Boss Yale, have your people heard any news of Linley yet?” George asked, and Reynolds looked at Yale as well.

Yale shook his head.

All of them had ugly looks on their faces. They had grown up alongside Linley. At the Ernst Institute, they had eaten together and roomed together. Although they weren’t actual siblings, they were as every bit as close to each other as real brothers were. There was no way they could just stand by and watch as Linley was executed.

“There’s no way. I don’t have any means of reaching the high level people in the Radiant Church.” Yale was somewhat frantic. “Wait a few more days. My father will arrive soon.”

Yale’s father.

Monroe [Men’luo] Dawson!

The Chairman of the Dawson Conglomerate, and the controller of the enormously powerful Dawson clan, whose wealth made even the Four Great Empires and the two major alliances envious. Their mercantile web had already encompassed every city in the Yulan continent, and was totally able to determine whether a nation’s economy flourished or collapsed.

Each of the three major trading unions possessed tremendous power.

Neither the two major alliances nor the Four Great Empires were willing to be openly hostile against them, because once one of the trading unions was openly at war with an empire, it could very well trigger an economic collapse, wiping out decades of progress and causing chaos within its domain.

“Boss Yale, you told us to wait a few more days two days ago! If we keep on waiting, I’m afraid…” Reynolds was frantic as well.

There was nothing Yale could do.

Fortunately, his father had been engaging in some tourism in a kingdom not too far from the Kingdom of Fenlai. Upon getting the news, Yale had immediately gotten in touch with his father and expressed the hope that his father could come to Fenlai City as quickly as possible. Given his father’s status as the Chairman of the Dawson Conglomerate, most likely Heidens would personally welcome him to the city.

Once his father appeared, the chances of rescuing Linley would be exponentially greater.

“Young master, young master!” A skinny, tall youth came running in, excited. “Young master, the Chairman has arrived!”

“Father!”

Yale leapt to his feet in joy. In the eyes of Reynolds and George as well, a hint of hope appeared.

Within the VIP reception hall of the Radiant Temple.

A two-meter tall, bald, pudgy man stepped into the hall, grinning merrily. This bald fatty was two meters tall and of enormous girth, most likely weighing 300-400 pounds.

This was the Chairman of the Dawson Conglomerate – Monroe Dawson! From another door, in walked the Holy Emperor – Heidens.

Heidens was also nearly two meters tall, but he was quite thin. The two of them together, both tall, both bald, but one fat while the other was skinny, made for a very interesting sight.

Behind Monroe Dawson, there were two middle-aged men. One was a golden-haired man with cold, hawk-like eyes, while the other was a powerfully built red-haired man. These two followed solemnly behind Monroe Dawson. Without question, the two of them were both combatants of the ninth rank!

Behind Heiden, as well, there were two red-robed Cardinals, one male, one female. These two were Guillermo and Melina [Ma’li’na].

“Oh, Your Holiness.”

Monroe called out in an exaggeratedly loud voice as he attempted to bow. However, that large belly of Monroe’s made bowing an extremely difficult thing to do. “Monroe, please sit.” Heidens was still quite friendly to him.

Monroe immediately sat down, as did Heidens.

Monroe’s enormous butt was simply too big. Most chairs wouldn’t be a good fit for him. Fortunately, the Radiant Church had prepared a special chair for him in advance. Upon sitting down, a delighted grin split his rotund face, and he laughed loudly. “Thank you, your Holiness. On this trip, I had only intended to do some sightseeing near Greenstone Lake, but who would’ve thought that my son would insist on me hurrying over here? Alas, you should understand that as a father, I had no choice.”

“Monroe, you really do pamper little Yale.” Heidens said with a smile.

Monroe nodded helplessly. “Hehe, that little tyke. But I’ve heard Yale say that he has an extremely incredible bro by the name of Linley. Not only is he a master sculptor, he is a genius magus, and also a very powerful warrior. When I heard this, I was very much impressed. But from what Yale says, this Linley has now been imprisoned within the Radiant Temple.”

“This is indeed the case.” Heidens nodded in acknowledgment.

Monroe chortled, “Your Holiness, can you give me some face and free Linley? Young people are always so impetuous. Although I know he attempted to assassinate Clayde, in the end, Clayde didn’t die, right? I’m sure that your Holiness wouldn’t care too much about a small matter like this.”

Monroe spoke casually and simply.

But Heidens couldn’t respond to him in as casual a manner.

This Monroe Dawson had gone so far as to explicitly ask Heidens to give him face. If Heidens refused, wasn’t that the same as directly refusing to give Monroe face? Although Monroe was grinning cheerfully, Heidens knew very well how powerful the Dawson Conglomerate standing behind Monroe was.

“Monroe.” Heidens shook his head. “It isn’t that I won’t give you face. It’s that it’s really not convenient for me to free him. Because…Linley killed several people from the Ecclesiastical Tribunal, including students of Osenno [Wu’sen’nuo] himself. Osenno is extremely angry this time.”

“Osenno?” Monroe Dawson frowned.

Osenno was one of the other pillars of the Radiant Church – the Praetor of the Ecclesiastical Tribunal.

In truth, it should be said that the Radiant Church actually had two leaders; the public leader known as the Holy Emperor, and the hidden leader in charge of killings, slaughters, and eliminating heathens and apostates – the Praetor of the Ecclesiastical Tribunal.

“This is going to be difficult.” Monroe immediately knew that this was not good.

Perhaps Heidens would care somewhat about Monroe’s status, but that cold fellow Osenno was nothing but a crazed killer.

But Monroe Dawson could also guess something.

“Linley killed the students of Osenno? These words are most likely a lie spun by Heidens, but there’s no way I’ll be able to verify this with Osenno.” Monroe felt helpless. He could tell that clearly, Heidens did not wish to let Linley go that easily.

The Dawson Conglomerate really did have its eyes set on Linley.

This was especially true after discovering that Linley was capable of Dragonforming. In terms of both his potential as a magus as well as a warrior, Linley’s potential was incredible. Once the Dawson Conglomerate acquired Linley, when Linley entered the Saint-level, the influence of the Dawson Conglomerate would instantly supersede that of the other two trading unions.

“If that’s the case, then I’ll leave now.” Monroe Dawson immediately stood up.

Heidens smiled calmly. “I truly am sorry, Monroe. Right now, the Radiant Church has not internally decided on how we should punish Linley. After we have decided on how we should deal with Linley, I’ll send someone to inform you.”

“Sure. During this period of time, I’ll stay in Fenlai City. I really want to see the upcoming Yulan Festival. This 10000th Yulan Festival is sure to be an amazing spectacle. In a man’s entire life, he might only see such a spectacle this one time.” Monroe Dawson beamed as he spoke.

After speaking, Monroe Dawson departed with his two bodyguards.

Heidens quietly watched as Monroe Dawson departed. By his side, Guillermo said quietly, “Your Holiness, that damn fatty foolishly thinks he can claim Linley for his own. After the 28th, Monroe can abandon all of his hopes.”

Heidens turned to glance at Guillermo. Smiling, he left the hall as well.

Right now, the only thing to do was to wait for December 28th.
