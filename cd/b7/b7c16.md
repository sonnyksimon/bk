#### Book 7, Heaven and Earth Turned Upside Down – Chapter 16, The Furnace

Linley was in a dire need of a good weapon, and so Monroe Dawson decided to immediately take Linley to Master Vincente. Monroe Dawson, Linley, Yale, George, and Reynolds all went in a group to a dwelling not too far away.

“Lord Chairman!” The guard at the entrance immediately bowed respectfully upon seeing Monroe Dawson.

The servants and guards of the Hyde clan had been personally arranged for by Monroe Dawson. They all belonged to the Dawson Conglomerate to begin with.

“Lord Dawson has arrived?” A middle-aged man who had been quietly lying in rest in the front courtyard instantly scrambled to his feet and walked over. His face filled with gratitude, he said, “Lord Dawson, if you want to see me, all you have to do is send someone for me. I would just come to your place.”

This Vincente truly did feel grateful towards Monroe Dawson.

In this past half year, Monroe Dawson had been extremely friendly and courteous towards his Hyde clan, but hadn’t required anything of them. In particular, when they had fled for their lives, if the Dawson Conglomerate hadn’t assisted them while they were in the Kingdom of Hanmu, perhaps many more members of the Hyde clan would have died.

“Haha, let’s talk inside.” Monroe Dawson slapped Vincente on the shoulders.

“Alright.”

Other members of the Hyde clan, such as Vincente’s father, and Vincente’s two sons all came out.

“Come, Mr. Vincente, let me make some introductions.” Monroe Dawson beamed as he pointed at Linley. “The three of you should already know my son, but this one here is that genius magus I have often mentioned to you. He is…”

“Linley of the Baruch clan, a master sculptor and a genius magus.” Vincente continued.

Vincente turned his eyes to Linley. Even Vincente’s father and his two children turned to stare at Linley in awe.

“Linley, I imagine you know about our Hyde clan.” There was a very special sentiment visible in Vincente’s eyes. Although both the Hyde clan and the Baruch clan had decayed in power over the years, in their heart, they were filled with pride and a certain type of arrogance.

The clans of the Four Supreme Warriors had five thousand years of history!

No matter how far they had fallen, this sort of innate pride and arrogance sprung from their hearts.

Two descendants of two Supreme Warrior clans looked at each other, sharing a very special moment.

“The Violetflame Warrior clan.” Linley said modestly. “In the books passed down within our Baruch clan, there are careful descriptions regarding the Hyde clan, one of our fellow Supreme Warrior clans.”

Hearing these words, Vincente couldn’t help but feel as though he had been given quite a bit of face, and felt all the more well-disposed towards Linley. “Linley, let me introduce you. This is my eldest son, Yotian [Yu’xing] Hyde. This is my second son, Trey [Te’lei] Hyde.” Vincente clearly was very proud of his sons. “Linley, my two sons are quite talented as well. But of course, compared to you, they have quite a distance to go.”

Yotian and Trey only nodded, but from the fierce look in their eyes, they clearly didn’t submit to their father’s claims that the two brothers were a bit inferior to Linley.

“Haha, alright, Mr. Vincente. I’ve come today to ask you for your help.” Monroe Dawson said directly.

Vincente immediately said magnanimously, “Lord Dawson, if you need anything, just tell me. As long as I am capable, I will definitely do my best.” In this past half year, the Dawson Conglomerate had helped the Hyde clan out in many matters. But the Hyde clan hadn’t been able to repay them at all. After all, the Dawson Conglomerate hadn’t asked them to do anything.

The feeling of owing someone wasn’t a good one.

Monroe Dawson laughed while gesturing at Linley. “Linley wants a good weapon. I want to ask you to be the one to forge it for him.”

“Forge a weapon?” Vincente looked at Linley. “Linley, a weapon for yourself?”

“Yes.” Linley nodded.

A gratified look was in Vincente’s eyes. Nodding, he said, “Right. We descendants of the Four Supreme Warriors can’t be physically puny and weak. We must train as warriors, and naturally we must have a fine weapon. Tell me, what sort of weapon do you desire!”

Both Vincente, and his two sons, upon hearing that Linley was a magus genius, felt a bit of disdain towards him in their hearts. In their eyes, the Four Supreme Warriors descendants should be powerful, invincible warriors. Now that Linley was asking them to help him make a weapon, they felt very happy.

“A heavy sword.” Linley said slowly. “Mr. Vincente, I am 1.9 meters tall. You decide how long the heavy sword should be. You should know what length would be most suitable to someone of my height.”

Vincente was a bit surprised. “A heavy sword? Not a greatsword or a warblade?”

Greatswords and heavy swords were two different types of weapons. “A heavy sword.” Linley said with certainty.

“Alright. Any other requests?” Vincente was the leader of the Hyde clan. The descendants of the Hyde clan weren’t just powerful warriors; they were all extremely skilled blacksmiths as well.

Linley removed the bag he was carrying. “The materials for the heavy sword must include this.”

From within the bag, Linley withdrew that fist-sized chunk of adamantine ore.

Just by looking at it, Vincente couldn’t tell that this was adamantine. After all, even Vincente had never seen adamantine before. He immediately asked curiously, “What is this ore called?”

“Adamantine.”

Linley replied directly.

“Adamantine?!” Vincente, his father, and his two sons all stared in astonishment at the fist-sized chunk of black rock in Linley’s hands.

Vincente suppressed the excitement in his heart. Looking at Linley, he said, “Can you let me take a look?”

“Yes.”

Vincente carefully accepted the chunk of adamantine. Although he had never seen adamantine before, Vincente knew that adamantine was extremely heavy, and so he had prepared himself for it. Indeed…

“At least a thousand pounds.” Vincente’s eyes shone. “Indeed. Adamantine is over a hundred times heavier than gold. The legends are true.”

Vincente suddenly came to his senses, and he stared at Linley in astonishment. “Linley, you want to use this entire chunk of adamantine in the forging of your heavy sword?”

“Right. All of it.” Linley replied.

Vincente shook his head repeatedly. “Linley, this adamantine ore is a thousand pounds by itself. Using adamantine as the base, the other materials you will need to alloy it with will have to be of high quality as well. Given the size of your heavy sword, it will most likely weigh nearly three thousand pounds. This will be my first time forging such a heavy sword. Three thousand pounds! You want it for yourself? Even most warriors of the seventh rank won’t be able to use it freely. Even a warrior of the eighth rank will be slowed down by it, despite being able to wield it with ease.”

“Mr. Vincente, just worry about the forging.” Linley laughed.

Dragonblood Warriors were immensely strong, physically. Comparatively speaking, in terms of battle-qi, Dragonblood battle-qi was a bit weaker.

Of the Four Supreme Warriors, the Dragonblood Warriors and the Undying Warriors possessed greater strength. The founder of the Baruch clan, Baruch, was someone who had dared to fight head on against a Nine Headed Serpent Emperor and win, killing it in the end.

A Nine Headed Serpent Emperor was an incredibly large creature, with strength to match. It could be considered one of the most powerful Saint-level magical beasts in existence. But Baruch still dared to fight it head on and killed it. From this, people learned how powerful and strong the Dragonblood Warriors were. Vincente glanced at Linley, then nodded. “Within my clan, we do indeed have secret methods for forging adamantine. But it will be hard for me to acquire all of the other rare ingredients right now.”

“Let me handle that.” Monroe Dawson said.

Vincente nodded. Given the power and influence of the Dawson Conglomerate, procuring some ores should be very easy. Vincente looked at Linley. Solemnly, he said, “Linley, adamantine weapons are indeed very formidable. If you only use a small amount of adamantine ore in your weapon, I’ll still be able to sharpen and put an edge on it. But if you want to use this much adamantine, I’m afraid that at most, I’ll be able to make the edges of the sword slightly thinner. But I won’t be able to put an edge on it.”

A thousand-pound chunk of adamantine ore! Vincente had never even heard of such a thing.

The sturdiness of the weapon it was used to forge would be incredible. To put an edge on and sharpen such a weapon? Vincente knew his own limits.

“Unable to put an edge on it?”

Linley suddenly thought back to the records of his clan. The first Dragonblood Warrior had used a warblade to do battle, but the later Dragonblood Warriors did not. One had even used a massive warhammer, relying purely on weight and power.

A three thousand pound heavy sword would totally be a match for that warhammer of his ancestors.

“If you can’t put an edge on it, so be it.” Linley was very confident. Such a heavy sword with such weight would be able to smash magical beasts to death with sheer kinetic force when wielded by the terrifying strength of a Dragonblood Warrior.

“Good. As long as we have the other ores needed, I can immediately begin the forging for you. A single weapon won’t take more than half a day of work.” Vincente said confidently. He, Vincente, had forged countless weapons, and he was very confident in the secret forging methods of his clan.

Monroe Dawson laughed. “Vincente, then can you provide me with your secret recipe for forging adamantine now?”

“Fine. I’ll go get it now.” Vincente immediately left.

The Dawson Conglomerate’s efficiency level was terrifyingly high. Before nightfall, they had procured a large piled of quality ores. In truth, the secret forging methods of the Hyde clan didn’t require any specific ores, as every material had possible replacements as well.

But the materials provided by the Dawson Conglomerate were the best of the best.

That night.

“The quality of these materials is extremely high, and all of these ores are high value ores.” Staring at the ores, Vincente was so excited that his face had a ruddy glow. Laughing loudly, he said, “Linley, with such good materials to work with, I’m afraid that the heavy sword will be slightly heavier than I anticipated.”

“That’s fine.” Linley laughed.

A weapon weighing just a bit over three thousand pounds could still be easily wielded by most warriors of the ninth rank, much less the astonishingly strong Dragonblood Warriors.

“Alright. Tomorrow morning, I’ll begin.” Vincent said heroically.

That night, Linley didn’t go back to his own manor. He chatted mentally with Bebe, who very obediently stayed home and didn’t come over. As far as Bebe was concerned, right now his life consisted of eat, sleep, eat, sleep. This was the type of life he liked.

Early morning. The sky slowly brightened.

Those three Hyde clan members, father and two sons, were bare-chested as they began the forging process. Vincente was the primary worker, while Yotian and Trey assisted on the sides. The flames spat forth by the bellows were at an incredibly high temperature.

“Hiss, hiss.”

Vincente Hyde’s body began to emit a blue flame, which quickly merged with the flames in the furnace. The color of the flames in the furnace actually changed as well, and those other ores began to slowly liquefy. Only the adamantine ore didn’t change at all.

Vincente picked up a cup of greenish herbal liquids and poured it directly over the adamantine ore. “Hiss, hiss.” The green liquid actually began to transform the adamantine ore somehow, as it actually slowly began to melt as well.

Finally, the general shape of a sword could be seen.

“Clang!” “Clang!” “Clang!”

The forgehammer smashed down again and again, the speed of the blows coming at a terrifyingly fast rate. The hammer danced in Vincente’s hands, giving everyone present the sense of watching an artistic performance. Clearly, Vincente’s hammer strokes had a certain rhythm to it, and the form of the heavy sword began to become more and more clearly defined.

“Hiss, hiss.”

Vincente’s body was constantly emitting that blue flame, keeping the heavy sword under high temperatures at all times. He continued hammering away at it for three hours. The heavy sword, which originally had been all sorts of colors, gradually turned into a pitch black color. Vincente was covered in sweat, and his face was turning a bit white. This was perhaps the most tiring forging project he had ever done.

“Give me mountain spring water.” Vincente shouted loudly.

His elder son, Yotian, immediately brought over a nearby barrel of water, then mixed into it a cup with a different, pre-prepared liquid inside. Using the secret liquid ingredients of their clan along with mountain spring water would definitely produce optimal tempering results.

“Hiss, hiss.” The heavy sword was placed within the barrel.

Watching by the side, Linley and Monroe Dawson’s eyes lit up. After being tempered, the heavy sword would more or less be complete. But just at this time, the gloomy, overcast sky suddenly boomed with thunder, catching everyone off-guard.

“Success!” Vincente pulled the heavy sword out, his face filled with excitement. He raised it high in the air, laughing loudly, “Haha, Linley, success! This is the finest creation I have ever made!”

“BOOOOM!”

A terrifying sound could be heard as a bolt of blue lightning suddenly forked down, striking directly on top of the heavy sword!
