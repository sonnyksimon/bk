---
layout: chapter
prev: /cd/b1/b1c19
next: /cd/b1/b1c21
---
#### Book 1, Chapter 20 – Earth-Style Magic (part 1)

Linley’s anticipation was about to erupt like a volcano as he immediately became suffused with excitement.

“Grandpa Doehring, can you really teach me to become a magus?” Linley excitedly looked up at old man Doehring.

Doehring Cowart, seeing the state Linley was in, stroked his white beard. “Linley, your Grandpa Doehring is a Saint-level Grand Magus. Even if you don’t have much natural talent, I can still teach you magic. Of course…if your talent is low, your accomplishments will be low as well.”

If any other magus had been present and heard his words, they would have been astonished.

Amongst the society of magi, the most important thing is talent. No talent meant no possibility of becoming a magus. Many people believed this!

But Doehring Cowart dared to claim that even if his student’s talent was poor, he still had the ability to make a magus out of the student. If anyone else had made this claim, they would be viewed as just wildly boasting…but the man who said these words was a five-thousand year old Saint-level Grand Magus!

“Low talent, low accomplishments?” Linley felt his heart tremble.

The reason he wanted to become a magus was because he wanted to restore glory to the Baruch clan. Even if he couldn’t accomplish this, he hoped to at least accomplish the one task which generations of clan elders had strove to achieve for centuries – reclaiming their ancestral heirloom. If he could accomplish this, it would be enough.

But to do so, power was an important component.

“Linley, don’t be worried. Your aptitude for magic hasn’t even been assessed yet. Who knows if it will be high or low? Perhaps you will have a tremendous talent for magic.” Doehring Cowart stroked his white beard as he smiled.

Grandpa Doehring’s tranquility brought calm to Linley as well.

“Grandpa Doehring, how does one test for magical aptitude?” Linley couldn’t help but grow eager.

“It is actually quite easy to test for magical aptitude.” Just as Doehring Cowart spoke, suddenly –

Footsteps could be heard from outside the door. Hearing them, Linley immediately grew nervous. He quickly said to Doehring Cowart, “Grandpa Doehring, quick, hide. Someone is coming.” If this five-thousand year old Saint-level Grand Magus of the bygone Pouant Empire was discovered, it could be disastrous.

Doehring Cowart only smiled, not moving at all.

“Grandpa Doehring!” Linley was beginning to grow impatient.

“Creaaak.” The bedroom door swung open, and Housekeeper Hiri stuck his head inside. Seeing that Linley was awake, he couldn’t help but smile. “Young master Linley, I didn’t expect that you would have already awoken. How do you feel, young master?”

Linley immediately forced out a smile. Nodding, he said, “Thank you for asking, Uncle Hiri. I’m much better now.”

Linley felt extremely agitated. He couldn’t help but turn to look in the direction of Doehring Cowart, but Doehring Cowart was still standing there, grinning. “What’s going on with Grandpa Doehring? Ugh. We’re about to get discovered. It’s going to be so annoying to have to explain.”

“Young master Linley, it’s time for dinner. Since you are already awake, come eat dinner with us.” Uncle Hiri smiled as he spoke.

“Oh. Got it.” Linley snuck another peek at Doehring Cowart, his heart filled with questions. “What’s going on. From Uncle Hiri’s expression, it seems as though he can’t see Grandpa Doehring at all.”

Seeing Linley constantly glance at the corner of his bed, Uncle Hiri asked curiously, “Young master Linley, why are you staring at the side of your bed? Did you drop something? I can help you look for it.”

“No-, nothing.” Linley immediately crawled out of bed. “Uncle Hiri, let’s go eat dinner.”

Although he found Linley’s reaction to be a bit odd, Uncle Hiri didn’t think too much of it, just nodding and smiling. Linley dressed himself, but still couldn’t help but sneak a peek at Doehring Cowart. But just as he did so, Doehring Cowart, who was still grinning at him, suddenly disappeared from Linley’s field of vision.

“He entered the Coiling Dragon Ring.” Linley could now clearly feel that a spirit was now residing within the Coiling Dragon Ring.

Unlike in the past, Linley had now soulbound the ring with his own blood, giving him a deeper level of understanding.

“Linley, no need to speak aloud. Just speak to me mentally. As the master of the Coiling Dragon Ring, you can directly engage in spiritual communication with me, as I am a spirit within the ring.” Doehring Cowart’s voice rang out in Linley’s mind.

This greatly surprised Linley.

“Grandpa Doehring?” Linley tested the mental link.

“I hear you.” Doehring Cowart’s voice rang out in Linley’s mind as well.

Linley’s heart was immediately filled with joy. But as he engaged in conversation with Doehring Cowart, he didn’t pay attention to where he walked, and he tripped over the doorway. Uncle Hiri, walking ahead of him, turned and laughed. “Young master Linley, watch where you walk.”

“Got it, Uncle Hiri,” Linley laughed in reply.

While excitedly engaging in mental conversation with Doehring Cowart, Linley entered the dining room and sat down. Today’s dinner was actually quite sumptuous, including a fragrant smelling roasted sheep. Hogg glanced at Linley. Smiling, he said, “Linley, have some.” As he spoke, Hogg personally tore off a strip of meat from the sheep’s lower hindlegs for Linley.

“Thank you, father.”

Linley felt quite surprised. His family was in poor economic straits, so normally their dinner was quite spartan. But today, they even had roast sheep?

What Linley didn’t know was…when the rain of stones descended on the town, aside from men and women, even many animals were killed. The Baruch clan aside, even some poor families who rarely ate meat were enjoying an extravagant meal today.

“Grandpa Doehring, why didn’t Uncle Hiri see you just then?” Linley mentally asked Doehring Cowart.

“Linley, I must inform you that aside from you, nobody can see me. Because right now, I’m just a spiritual projection, which has no matter. I’m invisible to the eye. Only you, as the master of the Coiling Dragon Ring, can see me.” Doehring Cowart explained in detail.

Linley suddenly understood.

Previously, Grandpa Doehring had said that he had died long ago, and only his spirit now remained.

“Grandpa Doehring, in the future, doesn’t that mean you can always appear by my side?” Linley felt extremely happy.

Just as Linley spoke, he saw that next to him, a white-haired old man suddenly appeared out of nowhere. It was Doehring Cowart. But Hogg, Housekeeper Hiri, and his younger brother Wharton still continued to eat and chat, not noticing Doehring Cowart’s existence in the slightest.

“Wow…”

Hearing and seeing were two different things. When he personally witnessed all the other people at the dinner table be unaware of Grandpa Doehring’s presence, Linley felt deeply astonished.

“There’s still some people who can sense my presence. Those whose spiritual presence are on par with me can feel my presence. But naturally…if I hide within the Coiling Dragon Ring, they definitely won’t be able to sense me.” Doehring Cowart’s voice sounded within Linley’s head.

“On the same spiritual level as Grandpa Doehring?” Linley chewed and thought at Doehring Cowart at the same time.

“Those who have the same spiritual power as me are most likely Saint-level combatants. Only Saint-level combatants can sense my presence, if barely. But of course, the prerequisite is that I appear outside the Coiling Dragon Ring. Once I enter the ring, there is no way they can find me.” Doehring Cowart smiled as he spoke.

Linley mentally nodded as he grabbed a roasted leg of mutton and chewed on it.

“Linley, eat more slowly.” Hogg saw how fast Linley was eating and couldn’t help but laugh.

Linley grinned at his father, but continued to devour his food with haste. In the twinkling of an eye, he had stripped the leg of mutton of all flesh. Linley let out a comfortable burp, then used the napkin to wipe his lips. Standing, he said, “Father, Uncle Hiri, I’m done eating. I feel like my head is still a bit dizzy, so I’m going to go and get some more rest. Wharton, see ya.” Linley was the first to finish eating.

“Still feeling dizzy? Then go and get some rest.” Hogg hurriedly said.

The earlier events of the morning had left a lasting impression on Hogg. There was a moment when he even thought Linley had been crushed to death. After experiencing such an event, Hogg’s attitude towards Linley clearly had improved substantially.

“Big brother, see ya.” Chubby little Wharton waved at Linley with a grease-covered hand.
