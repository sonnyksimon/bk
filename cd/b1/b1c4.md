---
layout: chapter
prev: /cd/b1/b1c3
next: /cd/b1/b1c5
---
#### Book 1, Chapter 4 – Growth (part 1)

The spring wind came, turning green the poplar trees near the empty space outside of Wushan town. On the empty ground, a group of youths were ardently training. Almost a year had passed since the Dragonblood test, and Linley was eight years old now. Over the course of this period of time, Hillman clearly saw that Linley had only become even harder working!

“Well done, Linley! Hold it, hold it!” Hillman encouraged from the side.

Right now, Linley was only wearing trousers. His upper body was covered with sweat, and his body, as taut as a drawn bowstring, was lying on the ground. His hands were pressed fiercely to the ground, as straight as tree trunks, while the rest of his body was motionless. He was supporting himself from a push-up position, with just his hands and the tips of his toes! His entire body was taut!

The ‘Static Tension’ training exercise!

A very simple yet very effective training exercise. If a person could reach the level of being able to maintain this pose for an hour, then his body would no longer fear ordinary swords or sabres.

Drip, drip!

Beads of sweat rolled down from Linley’s forehead. The sweat entered Linley’s left eye, and he couldn’t help but wince at the pain.

“Ley is really amazing. Just eight years old, but he’s able to match the thirteen year olds in doing the ‘Static Tension’ exercise.” Some of the children who had already given up were sprawled on the ground, chatting as they watched Linley.

“Ley, keep it up! Keep it up for the rest of us! Beat those thirteen year olds!” The golden-haired Hadley shouted from the side.

“Yeah, keep it up, Ley!” The other children started to chant as well.

Linley was on extremely good terms with the other kids. Although Linley was the child of a noble house, he was extremely kind to the children of commoners, and often helped them train as well.

“Gotta hold it. Gotta hold it.” Linley constantly said to himself.

In the back of Linley’s mind, the words his father said a year ago constantly echoed. “Linley, we are the family of the Dragonblood Warriors. As a member of the Dragonblood Warriors clan, you have an advantage, but also a disadvantage! The advantage is, even though the density of Dragonblood in your veins hasn’t reached a sufficient level, your body will still be much stronger than those of most ordinary people. It might be very difficult for others to become a warrior of the sixth rank through training alone, but for you, it will be somewhat easier.”

“However, your disadvantage is this. The descendants of the Dragonblood clan are not able to train battle qi according to normal manuals. This is because the blood in our veins is only suited to the training method inside the ‘Secret Dragonblood Manual’. It conflicts with all other types of battle qi cultivation methods. Unfortunately, only those who have reached a certain density of Dragonblood are able to practice using the method within the ‘Secret Dragonblood Manual’. Therefore, you will not be able to cultivate battle qi at all.”

“Also, although in theory, anyone training the body can reach the sixth rank, that’s just in theory. In practice, the number of people who accomplish this is very low. But for us, it is different. Even if the amount of Dragonblood in our veins is low, our starting level will be higher than others. Just from training alone, we can become warriors of the sixth rank. Your great grandfather, based on training alone, managed to become a warrior of the seventh rank!”

Linley remembered his father’s words very clearly.

Linley growled to himself, “I’m stronger than everyone else now, only because of the Dragonblood in my veins. But since I can’t practice battle qi, my only options are to work hard, and to work harder! Since great grandfather was able to become a warrior of the seventh rank, then I shall…I shall become a warrior of the eighth rank. Or even the ninth rank! Nothing is impossible!”

A warrior of the eighth rank!

A warrior of the ninth rank could be considered the most powerful expert in the entire country of Fenlai. A warrior of the eighth rank, although unable to restore the Baruch family to its former glories, would be able to dramatically improve its current situation.

“Gotta hold!” Linley gritted his teeth.

By this point, his muscles felt like they were being chewed on by countless ants. His entire body was quivering, and every single muscles on his entire body trembled. Every single trembling muscle could be seen visibly.

After a long time, in the end…

Thud!

Linley, exhausted, collapsed to the ground.

“That feels wonderful.” Flat on the floor, his entire body relaxed, Linley could clearly feel how numb his entire body was. All the muscles on his body, after undergoing that training, were slowly growing. Although the growth wouldn’t be noticeable from just one or two exercises, after a long period of time, the effects would be pronounced.

Hillman, off to the side, nodded with satisfaction.

And then, Hillman’s face grew cold as he turned to look at the fourteen and fifteen year olds. “All of you had better hold on! Linley’s only eight years old, while all of you are almost adults. Don’t let an eight year old get the better of you!”

…………….

After morning exercises ended, Linley bid farewell to his group of friends and went towards the Baruch clan manor. If a stranger had seen him, the eight year old Linley surely would have been assumed to be eleven or twelve years old, and not just a mere child of eight.

The descendants of Baruch truly were different from other men.

“Big brother!” Upon seeing Linley, the healthy-looking Wharton rushed over.

“That’s enough, Wharton. My entire body is covered with sweat. Let me wash myself first.” Linley patted Wharton on the face and laughed.

Wharton hmphed. “I know that as soon as you wash up, you’ll go take lessons from father.”

As a member of a noble house, Linley’s education began from a young age. The five-thousand year old Baruch clan was even stricter regarding educational matters than even the royal families of most kingdoms were.

“Enough, Wharton. I’ll play with you around noon.” Linley laughed.

Wharton was only a child, while Linley was much more mature.

After washing up and changing into some fresh clothes, Linley entered the study. At this moment, his father, Hogg Baruch, was sitting in front of a desk, his back ramrod straight. In front of Hogg were three thick tomes.

“Father!” Linley respectfully bowed.

Hogg coldly nodded, and Linley quickly walked next to him.

“Yesterday, I explained the history of the countries of the Yulan continent to you. Repeat it back to me.” Hogg said coldly.

This was the real Hogg.

Instances like the time when he was holding the crying Linley in his arms were extremely rare. Normally, Hogg’s attitude towards Linley could be summarized in one word: ‘Strict’. In all things, Hogg strove for perfection. He wouldn’t let Linley get away with any mistakes.

“Yes, father.” Linley said calmly.

“In the Yulan continent, there are three dangerous areas. The number one mountain range, the ‘Mountain Range of Magical Beasts’. The second mountain range, the ‘Mountain Range of the Setting Sun’. And, the number one forest, the ‘Forest of Darkness’. The space these three dangerous regions take up is incomparably large. The ‘Mountain Range of Magical Beasts’ runs across the entire continent, from north to south, covering over ten thousand kilomters. Within it are countless magical beasts, including Saint-level beasts which have the power to ‘destroy the heavens and ravage the earth’. Because of the ‘Mountain Range of Magical Beasts’, the Yulan continent has been divided into different regions.”

“West of the ‘Mountain Range of Magical Beasts’, there are twelve kingdoms and thirty-two duchies. Within these kingdoms and dukedoms, there are two major divisions. The first is the Holy Union, with the kingdom of Fenlai being the principal kingdom. The second is the Dark Alliance, with the kingdom of Heishi being the principal kingdom. These two alliances are opposed to each other and constantly battle because one is controlled by the Radiant Church, while the other belongs to the Cult of Shadows.”

“East of the ‘Mountain Range of Magical Beasts’, there are four empires, six major kingdoms, and countless duchies! These four empires are enormous, and are not influenced by the Holy Union or the Dark Alliance. In these four empires, the rule of the emperors is absolute. Any of the four empires are comparable to the Holy Union.”

“The four empires are the central Yulan Empire, the southeastern Rhine [Lai’yin] Empire, the eastern Rohault [Luo’ao] Empire, and the northern O’Brien [O’Bu’Lai’En] Empire.” After having said all this at one go, Linley let himself relax slightly.

“Just this?” Hogg frowned.

Linley was about to immediately continue, but Hogg cut him off. “Let me ask you, within our Holy Union, how many kingdoms and duchies are there?”

“Within our Holy Union, there are six kingdoms and fifte…sevente…” Linley suddenly frowned.

How many duchies were there in the Holy Union? Linley’s memory was a bit hazy. He wasn’t sure if it was fifteen, or if it was seventeen. He couldn’t be sure.

“Hmph!”

His face cold and harsh, Hogg pulled out a wooden stick, and Linley obediently stuck his hand out.

His eyes narrowing, with a ‘WHAP’ sound, Hogg whacked Linley’s hand with the stick. A red line immediately appeared on Linley’s hand, but Linley could only clench his teeth, not making a sound.

“Linley, you must remember, we are currently living within the Holy Union. You must know everything about the Holy Union!” Hogg coldly looked at his son. “In the entire Yulan continent, the most important entities are the four empires and the two alliances.”

Linley nodded.

Although his father’s words were simple, Linley clearly understood the deeper meaning.

“At the far northern end, the Holy Union shares a border with the O’Brien Empire. While at the southern end, the Dark Alliance intersects with the Yulan Empire. Under the guidance of the Radiant Church, the unity of our Holy Union isn’t one whit inferior to that of the empires.”

Listening to his father’s words, Linley agreed.

Yesterday, he had read many books. Clearly, the Holy Union could be considered the ‘cultural center’ for the entire Yulan continent. At the same time, in terms of economic strength, it was on par with the Yulan Empire, making the two of them the most economically powerful entities in the world.

In addition, it had the support of the Radiant Church.

The Holy Union truly was very formidable.

“Today, we will study art,” Hogg said coldly. “As the descendent of a noble family, you must have a thorough understanding and appreciation of art. Art is what gives noblemen an aura of gravitas!” Hogg pulled out a large tome as thick as a fist, immediately opening it.

“In the year 3578 of the Yulan calendar, the grandmaster stone-sculptor Proulx [Pu’lu’ke’si] was born….”

Off to the side, Hogg solemnly taught while Linley strove hard to memorize. He wanted to meet his father’s requirements.
