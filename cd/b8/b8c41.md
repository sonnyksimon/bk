#### Book 8, The Ten Thousand Kilometer Journey – Chapter 41, The Power of the Hundred Layered Waves

Surrounded by the foggy mist, Linley, the Arch Magus necromancer Zassler, Rebecca, Leena, Barker’s four brothers, Bebe, and Haeru all stared at the Saint-level Undying Warrior in front of them with a mixture of astonishment and delight.

Those powerful, muscular arms and legs…

Just by looking at the Saint-level Undying Warrior, one could almost physically see the warrior’s aura of power and might. In particular, that white, marble-like armor made Barker look as though he truly was a war machine.

Although the other four siblings were 2.2 meters tall, compared to their big brother Barker, they now seemed like under-age children. They only reached Barker’s chest in height.

“The Saint-level. Big bro, how do you feel?” The eyes of Hazer, the third brother, were shining.

Standing in mid-air, the Saint-level Undying Warrior emitted a deep rumbling noise, and then allowed his voice to reverberate through the air. “The feeling…is of power. Unbelievable power. What’s more, I can fly easily, as though it were a natural ability.”

Most Saint-level combatants needed to reach a certain level of understanding and insight to fly.

But the Four Supreme Warriors were different. As long as they had enough power, the exalted, mysterious bloodlines of the Four Supreme Warriors would allow them to fly as though it were second nature to them.

It was similar to how some Saint-level magical beasts would immediately and naturally know how to fly upon reaching their age of adulthood and maturity.

This was an innate gift!

“Haha, second bro, third bro, fourth bro, fifth bro. Don’t be too stressed out. All of you are at the peak of the eighth rank, right? With just a single extra step, you’ll be at the ninth rank, and by then, after transforming, you will be like me.” Barker tried to keep his sonorous voice quiet, but he couldn’t help but express his excitement.

Seeing this, Linley also felt great excitement and joy on behalf of these five brothers, who were Supreme Warriors like him.

The Barker brothers had trained for much longer than Linley had. When they had been captured and then escorted by Stehle, they had already been over thirty years old. At that time, they had already entered the eighth rank for quite some time.

They were warriors of the eighth rank who had never trained using the ‘Secret Undying Manual’.

As soon as they did, it was only natural that they then developed at an astonishingly fast pace. After all, the power of one’s body was what determined how much battle-qi could be generated, and those powerful bodies of theirs…the five of them had all reached the peak of the eighth rank. And today, Barker had broken through the last gate and reached the ninth rank.

An Undying Warrior of the ninth rank in human form, an early-stage Saint-level after transforming.

“Breaking through from the peak of the eighth rank to the ninth rank isn’t hard, but it isn’t easy either. I might still need several years.” The fifth brother, Gates, pursed his lips.

It was hard to say when one would break through to another level.

For example, Linley was currently at the peak of the eighth rank as well. He might break through tomorrow, or he might break through in three to four years.

Barker suddenly looked at Linley. With excitement, he said, “Lord Linley, use that ‘Profound Truths of the Earth’ technique to attack me again.”

“You want to give it a try?” Linley laughed with resignation.

One of the reasons why Linley was able to make the five of them willingly address him as ‘Lord’ and accept him as their leader was because Linley had totally outclassed the five of them in terms of martial force. In recent years, the five brothers had trained in accordance with the ‘Secret Undying Manual’, and after transforming into Undying Warriors, they had sparred a few times with Linley.

Undying Warriors did indeed possess an astonishingly high defense.

But the strange vibrational attacks of the ‘Profound Truths of the Earth’ were able to pierce through the armor and the muscles of the Undying Warriors, suffering only a slight loss of power before attacking their internal organs.

At that time, Linley had only used his weakest ‘Triple Layered Waves’ against them, and at a reduced level of power. But despite that, the five brothers still suffered some light injuries.

“The Profound Truths of the Earth is an extremely dangerous technique. Barker, if you really want to give it a try, then it has to be like it was in the past. I’ll start at the weakest level of power, then slowly ramp it up one level at a time. I don’t dare use my most powerful attack at the very start.” Linley said sincerely.

Profound Truths of the Earth – Hundred Layered Waves.

The power of this attack was dozens of times greater than the Triple Layered Waves. According to Linley’s calculations, it shouldn’t be too difficult for him to use this attack to kill an early-stage Saint-level combatant.

“Don’t worry, Lord. Let’s do this one step at a time. I won’t try to show off too much.” Barker’s deep voice rumbled out.

“Fine, then.” Linley nodded. “You are already at the Saint-level. I’ll transform into a Dragonblood Warrior as well.” Linley removed his upper body clothes, then allowed his body to become fully covered with black draconic scales, with the sharp spikes coming out as well.

In the blink of an eye, Linley had totally transformed into his Dragonblood Warrior form.

“Each time I see his Lordship’s eyes, my heart trembles.” The fifth brother, Gates, said in a low voice. The other three nodded.

The dark golden eyes Linley had inherited from the Armored Razorback Wyrm were cold and utterly remorseless.

“Barker, first, I’ll use my fist to execute the Profound Truths of the Earth. If you can totally withstand it, then I’ll switch to using the adamantine heavy sword.” Linley said in a deep voice.

Through using his fists, he could still put the power of the Profound Truths of the Earth on full display.

Only, in terms of actual force, it would be about half of that which the adamantine heavy sword could generate.

“Alright, come. Don’t take it too easy on me.” Barker was full of excitement as well. Right now, his blue eyes had a hint of gold in them.

Launching off from the ground, Linley shot upwards like a vicious blur towards the mid-air Barker.

“Ten Layered Waves.” Linley let out a growl.

Like a thunderbolt, his right fist smashed through the air, landing directly against the white armor covering Barker’s chest. But Barker felt nothing at all as that seemingly titanic punch slammed against his body.

“Boom!” “Boom!” “Boom!” ….

The strange attack penetrated through his armor and his powerful muscles, then pierced through the Undying battle-qi surrounding his organs. Finally, like a warhammer, it smashed against his heart and his other organs.

His internal organs all quivered.

But then, the Undying battle-qi in Barker’s body once more covered his organs.

“Haha, I’m fine. Again.” Barker’s eyes were shining. Linley’s punch using the Ten Layered Waves had actually not been able to injure him at all. The only thing he felt was a slight tremble from his internal organs.

Linley nodded.

Indeed, if a Saint-level Undying Warrior, with their incredibly strong defense, wasn’t able to take an empty-handed Ten Layered Waves blow, then Undying Warriors wouldn’t be worthy of being praised as the Supreme Warriors with the greatest level of defense.

“Fine. I’ll begin to gradually increase my attack power.” Linley didn’t waste any more words, immediately beginning to attack.

Barker knew very well that the weak point of the Undying Warriors lay in their low speed. In truth, even if he were to engage in a genuine battle against Linley, given Linley’s superior speed, Linley could land one punch after another on him. The result wouldn’t be too different from what he was doing right now; just standing there and letting Linley hit him.

The number of vibrations each blow caused slowly began to increase.

From ten layers, to twenty layers, to thirty layers…

“His defense truly is powerful. He’s even managed to withstand ninety layers of vibrations.” Linley’s eyes were shining. He immediately called out loudly, “Barker, prepare to take my most powerful bare-handed blow!”

Barker waited for him there in mid-air.

Barker had to admit that just then, the ninety layered waves had caused him some injury. But due to the astonishing healing power of his Undying battle-qi, he had already pretty much recovered from that light injury.

“Hundred Layered Waves!”

Like a tempest, Linley shot into the air, his fist drawing closer and closer to Barker before finally smashing against his chest.

“Boom!” “Boom!” “Boom!” “Boom!” ….

Barker felt as though he had been smashed by an enormous meteor as both his body as well as his internal organs began to vibrate with a strength which he had never experienced before. A hundred vibrations occurred in the blink of an eye.

Barker felt his internal organs shudder, and he could already taste blood in his mouth.

He wanted to swallow it, but then another stream of blood was forced into his mouth by his organs. He could no longer repress it, and he spat out a mouthful of blood.

“Big bro!” Barker’s four brothers immediately ran over in astonishment.

“Barker.” Linley was surprised as well.

“I’m fine.” After spitting out that mouthful of blood, Barker actually felt much better. “I’m not injured too badly. My Undying battle-qi should be able to totally cure this sort of minor wound in just three days or so.”

Barker looked at Linley with admiration. “Lord, in terms of understanding and insight, you are on a much higher level than me. Although my body is more powerful than yours, I’m still unable to defeat you.”

The Linley of four years ago definitely wouldn’t have been a match for the current Barker.

But over the course of these four years, Linley had deepened his understanding of the Profound Truths of the Earth. By enhancing his original Triple Layered Waves to the current level of a Hundred Layered Waves, he had increased his attack power by several dozen times.

“If I were to use my adamantine heavy sword, the power of the Hundred Layered Waves would be doubled.” Linley said to himself. The power of the Profound Truths of the Earth, when executed by a heavy sword, was extremely great.

“If I used all of my power with the adamantine heavy sword and executed the Hundred Layered Waves attack, I could most likely heavily injure or even kill an early-stage Saint-level Undying Warrior.” Linley was now very certain.

The defensive abilities of the Undying Warriors were legendary.

If even an early-stage Saint-level Undying Warrior was unable to take this attack, how could an ordinary early-stage Saint-level combatant do so?

“Any early-stage Saint-level who encounters the Hundred Layered Waves attack will most likely die.” Linley felt extremely confident.

Raw power and level of insight were mutually supportive.

Compared to four years ago, Linley’s raw power had not increased much. But in terms of the effectiveness of his level of understanding, he had improved by dozens of times. An ordinary peak-stage combatant of the ninth rank, upon reaching the Saint-level, would generally only increase in power by around ten times or so.

“Barker.” Zassler said with a smile. “In terms of raw power, the bodies of the five of you are not one whit weaker than Linley’s. Your body, Barker, is in fact stronger than Linley’s. But in terms of insight and understanding, you are too inferior. Linley has already told you that his levels of understanding can be divided into four levels; ordinary attacks being the first, ‘wielding something heavy as though it were light’ as the second, ‘impose’ as the third, and the ‘Profound Truths of the Earth’ as the fourth. But the five of you are still at the most basic level of using raw force. Your level of understanding and insight is far too low.”

Barker deactivated his transformation, returning to his normal appearance.

“In the past four years, Lord Linley has already taught us much. But we five brothers truly…” Barker laughed awkwardly.

“Old man, do you think we are geniuses? His Lordship is around twenty five years old, but has already reached the peak-stage of the Saint-level in terms of insight and understanding.” The fifth brother, Gates, didn’t treat Zassler with any respect at all.

Zassler glanced at Gates unhappily.

“You are physically powerful and all use heavy weapons. You should easily be able to understand the level of ‘using something heavy as though it were light’. But don’t be too impatient. As long as you focus on your training, one day, you will perhaps understand it.” Linley said encouragingly.

In truth, Linley had a large, unfair advantage.

His elemental affinity was, after all, exceptional. As a magus, he naturally was able to more easily attune with nature and commune with it. Pairing his inborn elemental affinity with his proficiency with the sword, it was very natural for him to be able to quickly deepen his level of understanding.

“Yes, Lord.” The five brothers all nodded.

The five Barker brothers all knew that right now, Linley was also at the peak of the eighth rank. As soon as he broke through to the next level, Linley would also be at the early-stage of the Saint-level in his Dragonform. Given his already-high level of understanding, by then, the difference between Linley and them would be even greater.

“We can’t allow ourselves to become a hindrance to him.” The five proud brothers all decided to work even harder from now on.

In the blink of an eye, yet another year passed.

The autumn wind was still howling drearily, the same as before.

Staring into the distance at the Barker brothers engaged in their training, Linley couldn’t help but grin. All five of the Barker brothers were physically stronger than Linley, and Linley had paid for the Dawson Conglomerate to produce weapons for them.

Five long-handled greataxes.

Those long-handled greataxes were at least two meters long, and were astonishingly thick. In addition, the axeheads were extremely large as well. The greataxes themselves were made from the finest and rarest of materials, with each long-handled greataxe weighing an astonishing 5300 pounds.

“The fifth brother, Gates, has a relatively higher talent for insight. He was the first to understand ‘wielding something heavy as though it were light’. The other four have yet to grasp it.”

Although over a year had passed, aside from Barker, the others remained stuck at the peak-stage of the eighth rank and had not broken through. The only pleasant surprise had been Gates coming to understand ‘wielding something heavy as though it were light’.

Zassler had spent this year in tireless training as well. This 800+ year old man had been somewhat embarrassed by the rapid increases in power by Linley and the Barker brothers, causing him to become hard working as well.

Watching the dried leaves fall from the trees, Linley suddenly felt very much at peace.

“Five years. It has been five years. I should go fulfill my end of the five year agreement as well.” Linley looked towards the northwest, in the direction of the prefectural city of Cerre.
