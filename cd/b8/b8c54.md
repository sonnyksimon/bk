#### Book 8, The Ten Thousand Kilometer Journey – Chapter 54, Personal Disciple

The Jacques clan’s castle was extremely large, but virtually everyone in the castle knew that the ‘quiet park’ that took up nearly a third of the castle was a restricted area.

Because that was where McKenzie lived. Aside from McKenzie and his wife, only three attendants as well as McKenzie’s disciples were permitted to enter. Normally, even the clan leader or his sons had to be granted entry before entering.

The quiet park was extremely large, and more than half of it was taken up by trees and flowers. The buildings inside the park were both simple and unadorned. But despite that, it would be easy for over a hundred people to live within this place.

Linley’s group had been invited to enter the quiet park.

A jade-haired, beautiful, virtuous looking woman who seemed to be in her thirties guided Linley’s group through the park, helping arrange places for them to live.

“Bliss [Bi’li’si], prepare a banquet, just like last time when Haydson came to visit.” McKenzie said to the beautiful attendant.

“Yes, milord.”

The jade-haired woman was very shocked. McKenzie, when receiving guests, was very particular about how he treated them. Generally speaking, this high-class banquet which McKenzie was now instructing to hold was generally only for Saint-level combatants.

“Can this youngster be a Saint-level expert?” Bliss glanced at Linley, guessing silently.

McKenzie laughed towards Linley. “Linley, although you’ve been in my Northwest Administrative Province for quite some time, I’ll wager you have yet to try some of the true delicacies of the Northwest Administrative Province.”

“True delicacies?” Linley raised an eyebrow.

When he was staying in the hotels, the dishes Linley had ordered were all very famous. After all, for someone at Linley’s level, money was of no concern.

“Of course, the provincial capital has many restaurants with fine dishes. But there are some special dishes which even those finest of restaurants only prepare a single portion of each week. Those special delicacies are something that you can’t simply buy with money.” McKenzie said proudly.

Throughout his life, McKenzie had only two hobbies; the first was training, and the second was sampling the various delicacies of the world.

McKenzie had even once said that if one didn’t have the chance to eat rare foods, then one’s life would have no flavor.

“Then today, I must have a good sampling of what you have to offer.” Linley chuckled.
Right now, only Linley and McKenzie were in the main hall, as well as Bebe, who was standing on Linley’s shoulders. As for Barker and his brothers, all of them had retired to their rooms.

“Hrm?” Seeing the Shadowmouse on Linley’s shoulders, McKenzie seemed to be slightly surprised. “Linley, I have the feeling that this magical beast of yours seems to be quite extraordinary. But he looks like a black Shadowmouse. This…” Black Shadowmice were the weakest type of Shadowmice. But McKenzie was certain that given Linley’s status, there was no way he would have such a weak magical beast companion.

Bebe had reached the Saint-level already.

Currently, however, Bebe was totally suppressing his aura. If a Saint-level combatant were to suppress their aura, unless the opponents were far stronger, they wouldn’t be able to sense the exact power.

“Bebe is a peak-stage magical beast of the ninth rank.” Linley laughed.

On Linley’s shoulders, Bebe flashed his fangs disdainfully towards McKenzie. As Linley planned it, Bebe having reached the Saint-rank was one of his most valuable hidden trump cards.

Bebe was already extremely terrifying before reaching the Saint-rank. Now that he had reached the Saint-rank, if Linley didn’t use the Profound Truths of the Earth, he would be absolutely ravaged by Bebe in their sparring matches.

But amongst Saint-level experts, how many possessed such a strange attack as Linley’s Profound Truths of the Earth? Generally, Saint-level experts weren’t a match for Bebe at all.

“A black Shadowmouse which is a peak-stage magical beast of the ninth rank?” McKenzie was still very surprised.

“Enough about that. McKenzie, in a few days, I plan to head off to the imperial capital. What do you think would be a good time for us to hold our sparring match?” Linley asked.

“Leaving so soon?” McKenzie was a bit disappointed. “I was hoping to celebrate with you for quite a while, brother Linley. That way, when we sparred together, we would learn more as well. But since you have business to attend to in the imperial capital, then…how about this? In three days, let’s have our sparring match in that small desolate mountain outside the city.”

“Works for me.” Linley nodded in agreement.

“Come, come take a look at my training yard.” McKenzie said warmly, and Linley followed McKenzie over to take a look.

While Linley was enjoying the warm hospitality of McKenzie, Wharton and Nina had left the imperial capital and were headed for the War God’s College.

The War God’s College was built on top of a tall mountain. The mountain was thus named, War God Mountain.

“It has already been over two hundred years since the last time the War God accepted a personal disciple. A few years ago, the Prodigy Sword Saint, Olivier, refused the War God’s invitation. I didn’t expect that he would suddenly declare that he would accept another personal disciple.”

“If one day, I could become his personal disciple, even if it were just for a day, I would die a happy man.”

The road outside the imperial capital was filled with people, all chatting and calling out to each other. The War God’s College accepting new honorary disciples was no longer an issue of major interest; accepting a new personal disciple, however, was an earth-shaking event. The importance of such an event was not one whit less than a new Emperor assuming the throne.

After all, in the past five thousand years, the War God O’Brien had only accepted a total of 20 or so personal disciples. Many of them were already deceased.

As for Emperors?

In the past five thousand years, there had been over a hundred of them.

Although in the hearts of the commoners, this was a huge affair, the War God’s College method of carrying the recruiting of a personal disciple was very simple. When the time came, they would simply send out a public announcement of who the next personal disciple would be.

The designated time was today at noon.

And thus, early this morning, a large number of people had come to congregate outside War God Mountain. Wharton and Nina naturally went to watch this momentous event as well.

Within their carriage.

“Big lunk, who do you think will become the next personal disciple of the War God?” Nina asked. Even in the eyes of an imperial princess, the War God was high and far above them, someone who they could never approach. Since she was born, Nina had never seen the War God once.

In fact, not even the current reigning Emperor, Johann [Qiao’an], had ever met the War God.

But the personally taught disciples of the War God were qualified to meet him. From this, one could see the extremely elite status the War God’s personally taught disciples held. In the past, when that Prodigy Sword Saint, Olivier, had refused the enticing offer to become a personally taught disciple of the War God, everyone was shocked and filled with admiration.

“The personally taught disciple of the War God would definitely be a person of enormous talent. At the very least, he would be a warrior of the ninth rank, and one with the possibility of reaching the Saint-level.” Wharton’s words were based on historical precedent.

“However, there are too many experts of the ninth rank in the Empire, and talent level is difficult to determine as well. It is very hard to say who the War God will accept as his personal disciple.”

Suddenly, the carriage came to a halt.

“Princess, we’ve already reached War God Mountain. There are too many people up ahead. The carriage can’t pass through.” The driver called out.

Wharton immediately helped Nina off the carriage.

“There are so many people here.” Seeing the sea of people in front of them, Nina couldn’t help but be afraid.

At the base of the cloud-topped War God Mountain, people were densely clustered everywhere. Earlier, carriages might have been able to advance, but now, none would be able to. The mountain roads were filled with people.

“Nina.” Wharton smiled towards Nina.

“Groooowl.” The Saber-Toothed Tiger, who had been following the carriage the entire time, leapt over. Wharton put Nina on top of it. “Have a good seat and take a firm grip. We’ll take a shortcut.”

Nina was both a warrior and a magus. Although she wasn’t very powerful, she was able to clutch quite tightly to the Saber-Toothed Tiger’s neck.

“Let’s go.” Nina was very excited.

The Saber-Toothed Tiger immediately soared into the air, with Wharton travelling at high speed by its side. Wharton and Nina didn’t take the main road; rather, they took some hard-to-traverse side roads from the back of the mountain.

Even the toughest, steepest of mountain paths were as easy for the Saber-Toothed Tiger to traverse as flat land. Wharton was extremely agile as well.

The two of them clambered up at high speed. On the way, they encountered quite a few powerful experts who were using the same method as they were. After all, if they had to squeeze in through the main road, who knew how long it would take?

“Here we are.” With a final leap, Wharton and the Saber-Toothed Tiger arrived at the main plaza.

“Wow. I’m so scared that my entire body is covered with sweat now.” Nina’s little face was very red. She hopped off the Saber-Toothed Tiger’s back.

The neat, flat stone plaza in front of them was extremely large. There already were over ten thousand people present, and yet it didn’t seem crowded at all. In fact, to the contrary; it seemed rather empty.

“Big lunk, did you know that this huge training school’s foundation was originally created by the War God himself? That year, he used one stroke of his sword to slice off the main peak of War God Mountain, then had the War God’s College built on the now-flat land.

Wharton was astonished at the War God’s power.

In truth, War God Mountain actually had several mountain peaks, with one being the primary peak. But the War God effortlessly chopped it off with one blow of his sword, creating a flat surface, upon which these various buildings of the War God’s College were erected, becoming the place where the honorary disciples of the War God’s College would stay.

According to legend, the personally taught disciples of the War God lived at another mountain peak.

“It isn’t time yet. Let’s have a rest.” Holding Nina’s hand, Wharton headed to a nearby stone bench and took a seat.

The plaza began to fill up with more and more people. Finally, the appointed time came.

On the tall dais in the front part of the plaza, there were a large number of people, all of whom were the honorary disciples of the War God’s College. Wharton’s ‘competitor’, Lamonte, was there as well.

“Look. A Saint-level expert.”

“Someone is flying over.”

Wharton and Nina all looked upward into the sky. They saw three human forms dressed in blue robes flash through the air, flying shoulder-to-shoulder towards the dais. Finally, they landed.

“Three Saint-level experts!” Everyone present felt their hearts tremble. Normally, even a single Saint-level expert was a rare sight, but now, three had appeared.

After landing, one of the three Saint-level experts, a middle-aged man who appeared to be the leader, said in a loud voice, “Everyone, today, we three fellow apprentices have come at our master’s instruction to announce who the 27th personal disciple will be.”

Everyone grew quiet.

“All three of them are the personal disciples of the War God.” Wharton suddenly felt as though he couldn’t breathe. The War God’s College was simply too powerful. All three of these personal disciples were Saint-level experts. No wonder the O’Brien Empire was named the most militarily mighty Empire in the world.

That middle-aged man continued, “The last time a personal disciple was accepted was in year 9723 of the Yulan calendar. This is now year 10008 of the Yulan calendar. 285 years have passed.”

Everyone below began to murmur. Such a long time had passed between accepting new disciples. 285 years. Many people didn’t even live that long.

“I announce that my master’s 27th personal disciple will be….Blumer Akerlund [Bu’lu’mo A’qi’lun]!”

Hearing this name, everyone in the plaza immediately let out a roar of joyous approval. At the same time, from within the group of honorary disciples who were standing on the dais, Blumer quietly walked out.

Blumer was rather skinny, and his eyes were slightly sunken. He gave the appearance of being a resolute, cold person.

“Respectful greetings to you, senior fellow apprentices.” Blumer bowed as he walked in front of those three men.

Those other three personal disciples of the War God all nodded slightly. Their leader, the middle-aged man, withdrew a scarlet interspatial ring from within his clothes.

Blumer knew that the emblem of one’s status as a personal disciple of the War God was always an interspatial ring, and a scarlet red one at that.

“So it’s him.”

Watching from below, Wharton shook his head slightly. Last time, when he had tried to join the ranks of the honorary disciples, the one who had won in the end was this Blumer.

Unexpectedly, after just a year had passed, Blumer had suddenly become the personal disciple of the War God!

Nina nodded as she said, “The seemingly common and ordinary Akerlund clan actually produced two geniuses in a row. The Prodigy Sword Saint, Olivier, was an absolute genius who even the War God wished to take on as his disciple. And now, Olivier’s younger brother, Blumer, has himself become the personal disciple of the War God.”

However, Wharton’s heart was filled with self-confidence despite seeing Blumer’s success.

So what if Blumer was able to join the War God’s College? Wasn’t the point of it all to reach the Saint-level? He, Wharton, upon reaching the Saint-level as a Dragonblood Warrior, would definitely be a powerful expert amongst the Saint-levels.
