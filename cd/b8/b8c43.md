#### Book 8, The Ten Thousand Kilometer Journey – Chapter 43, The Gathering in Basil

That night, Linley had dinner at the main hall of the governor’s castle.

“Keane, Jenne. Come outside a moment.” After finishing dinner, Linley called out to them, then walked out of the main hall to the quiet rear gardens.

Keane and Jenne exchanged glances, then followed Linley to the gardens as well.

The gardens at night were very peaceful and quiet. Looking at Jenne and Keane, Linley smiled. “Jenne, Keane, there’s something I must inform you of.”

Keane and Jenne stared at Linley, puzzled.

“The Radiant Church and I have a deep hatred between us. We will not rest until one or the other is destroyed.”

These words from Linley immediately stunned Jenne and Keane. They knew that Linley was no ordinary man, but they had no idea that he was diametrically opposed to the Radiant Church.

The Radiant Church was, without question, an enormous entity.

Lowering his voice, Linley said, “Five years ago, when I fought with the Radiant Church, it most likely resulted in them becoming aware that I am in the O’Brien Empire. Five years ago, the forces of the Radiant Church became aware of Haeru’s existence. I believe that just based on this alone, they should have discovered how I had followed the two of you to the prefectural city of Cerre.”

Many people knew that back then, a mysterious expert with a black panther companion had protected Keane and Jenne on their journey to Cerre, allowing Keane to assume the position of city governor.

This wasn’t a secret. It wouldn’t be strange at all for the Radiant Church to find out about this.

“I suspect that the Radiant Church has definitely hidden quite a few people within the prefectural city of Cerre.” Linley said calmly.

From the moment he had decided to come to the prefectural city of Cerre, Linley had already made certain plans.

That Stehle had exchanged blows with him before. After that fight, the Radiant Church would certainly have realized how dangerous Linley was to them. If they didn’t send people immediately to kill him as soon as possible, then the Radiant Church really would be a pack of fools.

“Then what should we do?” Keane and Jenne were both rather bewildered.

“Jenne, first of all, let me ask you. Do you still want to follow me?” Linley stared at Jenne.

Jenne nodded without any hesitation.

Linley nodded slightly. “I’m afraid that within your castle, the Radiant Church has spies here as well. That’s what I want to let you know…that I plan to leave the prefectural city of Cerre tonight.”

“What?” Jenne looked at Linley in astonishment. “Big brother Ley, you plan to leave by yourself?”

“Don’t worry. I’ll just head out slightly before you do. I’ll head to the provincial capital of Basil first. I’ll take up residence in the eastern side of the city’s Nile [Nai’er] Hotel. When the time comes, you can find me there.” Linley was very confident in his ability to deal with the Radiant Church’s men.

However, he couldn’t take Jenne and Keane along with him.

If he brought such a large group of people, he would be as good as harming Jenne and Keane.

“The Nile Hotel of the eastern city. This is a very famous hotel. I know where it is.” Keane nodded. Over these five years, he had paid quite a few visits to the provincial capital.

Linley had made these plans long ago.

Right now, whether or not he killed the Radiant Church’s forces wasn’t important. After all, killing those people didn’t make a huge difference to the Radiant Church.

If he encountered them, he would kill them. If he didn’t, then forget it.

As for Jenne, by the time they reunited with the five Barker brothers and Zassler, Linley would no longer be concerned about any schemes the Radiant Church might have to play.

“Then I’ll leave now.” Linley laughed.

“Immediately?” Jenne and Keane were startled.

“Immediately. That way, the Radiant Church’s men wouldn’t have any idea.” Linley chuckled, then transformed into a black blur, flying through the air and disappearing from the rear gardens.

At the same moment, the Blackcloud Panther, Haeru, as well Bebe also departed at high speed.

Three black blurs flashed over the prefectural city of Cerre’s twenty meter tall walls, easily crossing over to the other side. Although the city walls were useful against ordinary combatants, to experts on Linley’s current level, they were nothing more than a fairly high door stop.

Riding on Haeru’s back, the night wind howled past Linley.

“I’ve discovered that I rather like the feeling of travelling by night.” Feeling the cool wind blow against his face, Linley felt very much refreshed.

The light of the moon seemed to make the world covered by a layer of thin gauze, making everything seem so dreamlike.

This night, there were people riding on horses at high speed heading to other places as well. These were the people who were rushing towards Lyndin to give her the good news. However, there was a distance of over a hundred kilometers from the prefectural city of Cerre to the town Lyndin was staying in.

Linley had only arrived in the prefectural city of Cerre at nightfall. The supervisor for the prefectural city of Cerre only received the news at around 6 o’clock at night. By the time he sent someone out, it was already 7 o’clock.

At around 8 o’clock at night, Linley had left the prefectural city of Cerre.

At this time, that messenger was still on the road. By around 9 o’clock, the messenger finally managed to arrive at the town where Lyndin and the others were staying. The town was lit by fires. The poor man who had been blown on by the cold wind of November finally felt a hint of warmth.

“Lord Lyndin.” The messenger man arrived at Lyndin’s residence. Seeing Lyndin at the doorway, he immediately jumped off his horse. “Lord Lyndin, something important has happened. We’ve already discovered that Linley has arrived at the prefectural city of Cerre.”

The eyes of Lyndin, who had been standing there coldly, suddenly lit up.

“Linley?” Lyndin was both shocked as well as overjoyed.

She had waited for five full years, to the point of being numb. And then tonight, this report had come out of nowhere.

“Syke [Sai’ke], Syke! All of you, come out.” Lyndin’s cold voice rang out a few times, and the other five Angels immediately rushed over.

These six Angels were all wearing human bodies, and thus their power was limited to that of a warrior of the ninth rank.

But their essence was still that of the Angels.

They would definitely obey orders. For the sake of the glory of the Lord, they would be willing to sacrifice their lives at any time.

Upon hearing that there was news of Linley’s return to the prefectural city of Cerre, the other five Angels grew excited as well. Their mission was to kill Linley.

“Let’s go, we head out immediately.” Lyndin immediately ordered.

“Yes.” The other five didn’t hesitate at all.

Lyndin and the others didn’t bother about the messenger. The six of them, relying on their legs, immediately began racing towards the direction of the prefectural city of Cerre. As combatants of the ninth rank, without question, the speed they could reach was much faster than that of horses.

The next morning.

Within a very ordinary manor in the prefectural city of Cerre. The previous night, Lyndin and her people had taken up residence here upon reaching the prefectural city of Cerre.

“What? Linley disappeared?” Lyndin stared coldly at the white-robed man in front of her.

The white-robed man immediately said, “Lord Lyndin, the people we stationed within the city governor’s castle didn’t know either. They only found out this morning that Linley and his two magical beasts disappeared. Most likely, they’ve left the prefectural city of Cerre.

“Bam!”

Lyndin angrily smashed a fist against the stone desk in front of her, smashing it into tiny pieces. The other five Angels were extremely angry as well.

The six of them had spent over five years here. They had just received word of Linley’s arrival, but then in the blink of an eye, he had disappeared again.

The white-robed man was somewhat nervous now. He knew that the six people in front of him were very powerful. Even the Northwest Administrative Province’s supervisor had to obey the orders of these six people.

However, the white robed man didn’t know that these six were actually angels.

Only in the moments before the deaths, when Lyndin and the others chose to go all out, would their true power as Angels be put on display.

“Investigate. Go investigate. Find out where Linley has gone. Also…activate every resource we have in the entire Northwest Administrative Province. We must find Linley. Linley must be somewhere within the Northwest Administrative Province.” Lyndin said in a cold, deadly voice.

“Yes.” The white-robed man immediately assented.

They hadn’t been able to find Linley for five years. Lyndin had even begun to worry if Linley had perhaps left the O’Brien Empire. After all, given they had found no trace of him, there was no way they could be certain as to where he actually was.

But at least they now knew for sure that Linley was in the Northwest Administrative Province.

Just as Lyndin was feeling furious at her helplessness on the third morning, they received word from the provincial capital of Basil.

“Linley has appeared within the provincial capital of Basil.”

As soon as they received this news, Lyndin’s other colleagues grew excited.

“Lord, shall we head out now?” The five looked at Lyndin expectantly. Lyndin was the captain of their squad. In fact, amongst the Descended Angels, Lyndin could be considered a fairly famous person.

The Angels that would descend into bodies that could only support the ninth rank were almost all Two-Winged Angels. Only three of them were Cherubim, Four-Winged Angels, and of the three, Lyndin was the only female one.

“That McKenzie is in the provincial capital.”

Lyndin frowned. “McKenzie has reached the Saint-level nearly sixty years ago. From our reports, his power can be considered a mid-stage Saint-level. If he were to interfere, things would become complicated.”

“Lord, if we were to go all out, killing McKenzie shouldn’t be too hard.” Another nearby Angel, the one known as Syke, spoke out.

“Right. When going all out, we can allow our bodies to collapse and utilize all of our true power. The five of us are all Two-Winged Angels, while you, Lord, are a Cherub. Although it will only be for a short period of time, it should be enough to kill Linley.

Hearing her subordinates words, Lyndin hesitated.

Indeed. If Angels were to ignore their physical collapse, they could indeed use all of their real power for a short period of time. But most likely, after just two or three attacks, their bodies would have turned to ash.

When a Cherub and five Two-Winged Angels used the Angel Battle Formation and allowed their bodies to collapse from using their full power, even a mid-stage Saint-level combatant might die in their hands.

“No rush.” Lyndin said calmly. “Everyone, calm down. Going all out is our last resort. After all, pre-transformation, Linley isn’t that impressive. We can instead find an opportunity where Linley is in his human form and directly kill him.”

“Lord, then your intention is to…” The five looked at Lyndin.

“That Linley doesn’t recognize the six of us.” A hint of a cruel smile was on Lyndin’s face.

That day, Lyndin’s group, led by the white-robed man, rode fine horses out of the prefectural city of Cerre.

“Lord, the military carriage up ahead belongs to the soldiers of the city governor of Cerre.” The white-robed man reported in a quiet voice to Lyndin and the others as soon as he saw them.

“Oh? Is it Jenne and Keane?” Lyndin glanced at the distant caravan.

Jenne and Keane’s relationship with Linley was something that Lyndin knew quite a bit about.

“Have your subordinates been mixed into their caravan?” Lyndin lowered her voice.

“Yes, Lord.” The white-robed man nodded. Smiling, Lyndin said, “That’s fine. For now, we don’t need to pay them any attention.

Lyndin’s group clearly travelled at a much faster pace than Keane and Jenne’s group. In the blink of an eye, they passed them by. The reason Keane and Jenne were making this trip
out to the prefectural city of Basil was because they needed to attend the annual dinner party.

Lyndin’s team and Jenne’s caravan were both headed towards the provincial capital of Basil. As for Linley, quite some time ago, he had settled down in the hotel in the east side of the city.

There was a small manor located right off behind the hotel. Linley was staying there.

“I came to the provincial capital of Basil in such grand fashion. Most likely, the Radiant Church’s men recognized me. I wonder who the Radiant Church will send out next time?

Linley wasn’t worried in the slightest. He was actually quite eager.

“I haven’t encountered anyone who could fight me head on yet, or force me to use the ‘Hundred Layered Waves’ level of the ‘Profound Truths of the Earth.’
