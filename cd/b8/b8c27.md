#### Book 8, The Ten Thousand Kilometer Journey – Chapter 27, Secrets of the Church

Zassler knew that for a twenty one year old to reach such a level meant that in the future, he would eventually leave Zassler far behind in the dust.

“We can be considered to know something about each other’s abilities now. Didn’t you want to know about the Radiant Church?” A look of self-confidence was on Zassler’s face. With regards to the secrets of the Radiant Church, he, Zassler, probably knew as much as the high level members of the Church itself.

“Speak.” Linley immediately began to listen carefully.

Zassler nodded. “Simply put, the Radiant Church’s power, on the most superficial level, includes Missionaries, Priests, Bishops, Vicars, and Cardinals. They also have the eight ace regiments of knights, as well as powerful Knights of the Radiant Temple. This can be considered their second military force. In addition, they also have the servants of the Ecclesiastical Tribunal as well as a large number of Ascetics.”

Hearing this, Linley was silent. He knew all this already.

“But aside from these overtly visible forces, they also have two hidden forces.” These words immediately aroused Linley’s interest.

Ascetics and Executors of the Ecclesiastical Tribunal were considered their ‘overtly visible’ forces?

“These two hidden forces are extremely formidable, more powerful than any of their other forces. The first hidden force is known as the Zealots!” Zassler frowned. “These Zealots are very terrifying. They have a very strange power which is not light-style power. I can’t explain it either.”

This was the first time that Linley had heard the term ‘Zealot’.

“And the second force?” Linley asked.

Zassler’s face was solemn. “The second force is the most powerful force the Radiant Church has to offer, their true trump card. They will never use this force unless things reach the final, most critical point. These are…Descended Angels!”

“Angels?!” Linley’s heart shook.

In the past, at the Ernst Institute, Linley had read quite a bit regarding Angels. The impression he had of Angels was that they were powerful, extremely powerful.

“Because of the restrictions of having fleshly bodies, Descended Angels will not be at the peak of their power. However, even the weakest Descended Angel will be a combatant of the ninth rank. Many are Saint-levels. Descended Angels are the true, most terrifying force available to the Radiant Church.” Zassler sighed.

Linley’s heart was filled with shock.

“Zassler, I’ve read about Angels before. The descriptions of the most powerful Angels say that they have the power of Deities. If the Radiant Church has a large number of powerful Angels, they shouldn’t be in their current state.” Linley probed.

Zassler shook his head. “No. The power of the Descended Angels will depend on the human vessels the Radiant Church provides.”

“Human vessels?” Linley looked questioningly at Zassler.

“Right. Angels are unable to create dimensional rifts and directly descend into our world. Their only option is to use some special methods and descend into the body of a human. The strength or weakness of this human body will determine how much power the Angel can wield.” Zassler explained.

“Linley, although this world has ninth-rank combatants and Saint-level combatants…if it weren’t for their battle-qi, their physical strength would be quite a bit weaker. Normal humans can only reach the sixth rank based on their muscular strength.”

Linley agreed with this assessment.

“When an Angel descends into a body with muscular strength of the sixth rank, they can at most wield power of the ninth rank. Thus, the Radiant Church needs bodies of the seventh rank, or even higher.” Zassler said with certainty.

“Even more powerful bodies?” Linley frowned.

“Although normal human bodies can generally only reach the sixth rank, there are still some geniuses who are extremely powerful. Since youth, they possess boundless strength. It can be said that they are inherently powerful. These people with special natural gifts might reach the limit of the seventh rank based on muscle power alone. And a body which can naturally reach the seventh rank in power should be enough to allow an Angel to wield power of the Saint-level.”

Hearing Zassler’s words, Linley couldn’t help but frown.

Because Linley’s great grandfather had been able to train to the seventh level just based on his muscular strength. But afterwards, Linley’s great grandfather had died in battle. In the past, Linley had never questioned this, but now…

“Could it have been possible that my great grandfather was actually taken away by the Radiant Church for his body?” Linley was guessing.

In truth, all of the Four Supreme Warriors possessed tremendous innate physical gifts. All of them could train to an extremely powerful level just based on muscle strength.

Zassler continued, “This has caused the Radiant Church to scour the entire world for people with powerful bodies. The more powerful the body, the more powerful the Descended Angel will be. But it’s of no use. In this era, the Yulan continent has four Deity-level combatants. Faced with these Deity-level combatants, Saint-level combatants can do nothing but die.”

“Four Deity-level combatants?” Linley stared at Zassler in surprise. It seemed as though Zassler knew about the existence of that expert from the Mountain Range of Magical Beasts.

Zassler saw Linley’s surprise. Laughing, he said, “The four Deity-level combatants are humanity’s War God and High Priest, the magical beast who is the King of the Forest of Darkness, and the magical beast King of the Mountain Range of Magical Beasts who appeared on Apocalypse Day.”

“Linley, when I was being taught the secrets to Necromantic Magic, I learned…that the bodies of Deities are at the Saint-level in terms of physical strength alone.” Zassler said with certainty.

A Deity-level combatant could be said to be composed of his divine body, his divine spark, and the divine power he wielded. There was no way a Saint-level combatant could injure them at all.

“Thus, in order to wield the power of a Deity, the body alone must be at the Saint-level in physical strength. Most likely, the Radiant Church is not able to manifest a Deity-level Angel. Even if high class Angels were to descend, they wouldn’t be able to use their deific power, due to being restricted by their physical bodies.” Zassler said confidently.

The teachings of Necromantic Magic were abstruse and profound. In addition, Zassler was over eight centuries old. He truly knew many things.

“Deity-level combatants!” Linley’s heart swelled with amazement.

Any of these four most powerful experts of the Yulan continent could shake the world with their might. On the Apocalypse Day, the appearance of Dylin had caused both the Radiant Church and the Cult of Shadows to flee and avoid him.

The Radiant Church had their Descended Angels. But then, what did the Cult of Shadows possess, for them to be equal to the Radiant Church for countless, untold years?

Despite that, both powers combined still didn’t dare to offend that Dylin, the King of the Mountain Range of Magical Beasts.

From this, one could clearly tell how ineffable the power of a Deity-level combatant was.

“Who knows when I will have power like that.” Linley was filled with eagerness and hope towards this sort of power.

….

Zassler continued to tell Linley a great deal of information regarding the Radiant Church.

“The Radiant Church cares the most about two things. The first is finding extremely powerful bodies. The second is to find extremely pure souls.” As Zassler said this, Linley’s face changed.

Pure souls?

His own mother had died as a result of this.

“Supposedly, the ‘Radiant Sovereign’ which the Radiant Church worships only needs two things. The first is the worship of his followers. The second is pure souls. The purer the souls offered by the Church, the greater the gifts that the Radiant Sovereign will bestow upon them.”

By now, Linley had a good understanding of the Radiant Church.

The reason why the Radiant Church sacrificed pure souls to the Radiant Sovereign was the same reason they searched for powerful bodies. It was because they wanted to acquire powerful Descended Angels.

“Linley, in the Yulan continent, the Radiant Church has hidden reserves of power in every location. After all, the power of a religion is extremely formidable.” Zassler said with a sigh. “But in the Four Great Empires, the Radiant Church is fairly weak. In the Anarchic Lands, however, their influence is quite powerful.”

“Anarchic Lands?”

A map drifted to the forefront of Linley’s memories.

East of the O’Brien Empire, there was an area which was slightly larger than the O’Brien Empire itself. In the center of this area was an enormous forest – the Forest of Darkness.

The Forest of Darkness was thousands of kilometers wide, and thousands of kilometers long as well. This enormous forest took up half of the land in this area.

North of the Forest of Darkness, were the Eighteen Northern Duchies, roughly the same size as one of the O’Brien Empires administrative provinces.

South of the Forest of Darkness were the 48 Anarchic Duchies. The total area of these duchies was roughly half the size of the O’Brien Empire. This could be considered the most politically chaotic area in the Yulan continent, as the 48 Anarchic Duchies engaged in constant warfare.

“The Radiant Church and the Cult of Shadows are the two most powerful religions in the Anarchic Lands.” Zassler said.

Linley could imagine.

In the war-torn Anarchic Lands, it was only natural for those poor commoners to turn to religion for solace.

“Alright, I’ve talked so much that my mouth is dry. Let’s eat breakfast.” Zassler laughed loudly.

Zassler and Linley were both in possession of interspatial rings, and both their rings contained fine wine. Drinking wine while eating freshly plucked fruit, the two of them continued to discuss their plans for dealing with the Radiant Church.

“Oh, right. I suddenly remember something.” Zassler suddenly said.

“What’s that?” Linley looked at Zassler

Zassler chuckled. “This time, when I was being escorted under guard, we ran into another squad of the Radiant Church’s men. This squad was also escorting a group of people.”

“Who? An expert like you?” Linley asked.

If they were experts, then he and Zassler would go rescue them. After all, each of them had enmity with the Radiant Church. If they banded together, they would only be stronger.

“No. It was two adorable girls.” Zassler shook his head. “Originally, when that squad and Lampson’s squad met up, I saw those girls. I must say, those two girls were as innocent and pure as angels. Based on my familiarity with souls, I am quite certain that these two girls have extremely pure souls.”

Practitioners of Necromantic Magic, compared with the other types of magi, were undoubtedly the most experienced when it came to souls.

“However, in the eyes of the Radiant Church, my importance far outweighed the importance of those two girls. Lampson and the others took me away at high speed, while the two girls were taken away by another squad, which was moving quite a bit more slowly.” Zassler said.

“So your intention is…?” Linley looked questioningly at Zassler.

Zassler chuckled, “My intention is for us to go rescue those two girls. After all, that squad didn’t have many experts in it. It only had a single combatant of the eighth rank.”

In the eyes of Zassler and Linley, an expert of the eighth rank really was nothing.

“How is it that an Arch Magus necromancer like you would be so kind-hearted as to go rescue two girls?” Linley looked at Zassler.

Zassler laughed. “I delight in disrupting the affairs of the Radiant Church whenever I can. And what’s more, with such extremely pure souls, the two of them might be suitable for training in Necromantic Magic.”

The requirements for learning Necromantic Magic were terrifyingly high.

This was why in the entire Yulan continent, the number of necromancers was extremely, extremely low. The soul was a person’s most important quality, and even the Radiant Sovereign desired to acquire pure souls. From this, one could tell how important a pure soul was. In order to learn Necromantic Magic, an extremely pure soul was needed.

“You should know what trajectory they were on, right?” Linley asked.

Zassler nodded his head. “The path they took should be identical to the path that I had been taken on, unless this squad has already received word of the deaths of Lampson and his men. Only then might they suddenly change their direction.”

“Then let’s go.” Linley immediately rose.

“Groooowl.” Bebe and Haeru, who had been lying on the nearby grass, both stood up. These two magical beasts were very excited. By their nature, magical beasts were violent and barbaric, loving to do battle.

“Right now?” Zassler was a bit startled. “We’ve destroyed all trace of Lampson and his men. Even if the Radiant Church’s people discovered that the manor was empty, they probably would only think that Lampson and his men had left. They wouldn’t discover that Lampson is dead this fast. Even if they found out that Lampson and his men were dead, they wouldn’t be able to send the message to the other squad so quickly.”

“Leave nothing to chance at all. We will immediately set out on the same path that you were taken on and trace our way back.” Linley said immediately.

Zassler, helpless against Linley, could only shake his head, let out a sigh, then rise to his feet as well.
