#### Book 8, The Ten Thousand Kilometer Journey – Chapter 14, Repeated Assassination Attempts

“Very dangerous?” Linley began to laugh. “How dangerous, exactly?”

Seeing Linley’s reaction, Jenne couldn’t help but nod frantically. “Extremely dangerous. My aunt is currently in control of Cerre City, and her authority is on par with that of a city governor right now.”

Jenne said somewhat awkwardly, “Big brother Ley, I am so sorry. I didn’t tell you these things earlier. There’s no need for you to risk yourself for me. It isn’t worth it.”

“Haha….”

Linley laughed. “Not worth it? I don’t have anything else to do right now either. Escorting you along the way is just a matter of course. As far as the ‘danger’ is concerned? I have a much better understanding than you of whether or not it will be dangerous. Alright, Jenne, go back and get some rest.”

“Big brother Ley.” Jenne stared at Linley, somewhat stunned.

“Go back.” Linley said with a faint smile.

Jenne cast a grateful glance at Linley. “Thank you, big brother Ley.” But then, Jenne looked solemnly at him. “However, big brother Ley, I really don’t want you to risk yourself for my sake.”

“Go back to sleep.” Linley intentionally hardened his face, ‘barking’ at her.

“Oh.” Like a scolded child, Jenne nodded obediently, then turned and left via the door. Actually, in her heart, Jenne was feeling quite happy right now. She was, after all, an eighteen year old child. When such a girl saw such an outstanding young man treat her so well, of course the girl would feel happy. Jenne didn’t truly want to separate from Linley.

After walking outside the door, Jenne suddenly turned her head.

Jenne smiled beautifully. “Big brother Ley, when you harden your face like that, you look really grim and scary.” And then, like a playful child, Jenne fled down and away from Linley’s room.

Watching her flee, Linley didn’t know whether to laugh or to cry.

Taking a deep breath, Linley calmed himself down, then returned to his bed, quietly seating himself in the meditative position as he began to train his spirit. No matter when or where he was, Linley would always seize every possible moment for training.

Linley would never forget about seeking vengeance for his parents.

Could never forget about the death of Grandpa Doehring!

Could never forget that right now, he had a goal set for himself – Destroying the entire Radiant Church, root and stem!

“There will come a day…” Linley’s resolve was extremely firm. Right now, he desired neither authority nor status. All he wanted was to be able to train in peace.

…..

In another stand-alone residence facing this hotel complex, there was a room where a lamp had been lit the entire night. The grim red-haired man sat alone in that room, six others surrounding him.

“If we succeed with this initiative, everyone will benefit. But if we fail…you all know how cruel Madame Wade can be.” The red-haired man said calmly.

The six men’s hearts were all filled with fear.

Madame Wade was heartless and vicious. When Count Wade had been alive, virtually everyone in Cerre City knew that although Count Wade was the city governor in name, in reality, the true governor was Madame Wade.

Even Madame Wade’s son always felt frightened and cold when facing her.

Unfortunately, her son was dead now.

Per the rules, the successor to Count Wade as city governor should be his son. But how could Madame Wade so easily allow those two countryside-dwelling siblings to take the position?

“Captain, don’t worry. We definitely won’t fail this time. Although that expert is very powerful, he can’t always be protecting them.” One of the six men said with force and determination.

The others all nodded as well.

“Fine. I’ve already arranged for this hotel’s owner to be bribed. On the third floor of the hotel, there are two rooms which are facing the siblings’ residence. When the time comes, the four of you shall take up those two rooms. The other two will come with me. Remember, we will make our move as soon as we see the opportunity to, but our primary target is the boy.” The red-haired man reminded.

After all, right now, Keane was the first in line for succession.

Jenne was a girl. It would be much harder for her to become the city governor.

“When the boy comes out, we move. After killing him, if we have the chance, we can kill the girl as well.” The red-haired man said coldly. “Alright. Let’s go wait. Perhaps the boy will need to make a trip to the bathroom at night. That will allow us to complete our mission easily.”

“Yes, Captain!”

Per the red-haired man’s orders, four of the six men immediately left the residence, heading directly for the hotel and for the two rooms on the third floor that had been prepared.

A curved moon was hanging in the sky tonight, and moonlight cast a gentle glow upon the world.

The archers that the red-haired man had brought on this trip were the elite archers of Cerre City. They should have been able to easily shoot a weak, unprepared boy from the distance of fifty or sixty meters.

“Captain, what should we do?” The other two men asked, standing by the red-haired man’s side.

The red-haired man said calmly, “Your mission is…if those four do not have a chance to kill the boy, dress up as hotel attendants and deliver breakfast to them. When you near the boy, immediately kill him with one hit.”

“Captain!” The two immediately became frantic.

Order them to dress as attendants to go assassinate the boy? But that powerful combatant with the black panther companion was right there. Even if they succeeded, would they be able to survive?

“Hmph.”

The red-haired man looked coldly at them. “The two of you have no options. When the eight of you came with me, your families were all taken into custody by Madame Wade. Once your mission fails, not only will you be doomed, your families are finished as well. But if you succeed, even if you die, your families will be treated well.”

Both men’s faces turned white.

“The two of you should know what type of person Madame Wade is, and what type of person I am.” The red-haired man said mercilessly.

Although this red-haired man was nominally their captain, in reality, he was nothing more than Madame Wade’s loyal hound. He was merciless when killing people.

“But of course, if the other four succeed, then there’ll be no need for the two of you to risk your lives.” The red-haired man said calmly, “Right now, you two should pray. Pray that the War God blesses you.”

Both of them were silent.

They were so-called ‘elite’ soldiers from the army. But how could small figures like them possibly struggle against Madame Wade? And what’s more, the red-haired man was keeping his eyes on them.

……

Right now, there were four archers based in the third floor of the hotel. All of them were lying in ambush in their separate rooms. In each room, one was resting, while another was on watch. They had to stay in top condition, and once Keane stepped out, they would immediately awaken the other person.

The night slowly passed on.

This night, Keane didn’t take a single step out of his room. The sky began to brighten, and the fresh morning air freshened the minds of the four archers considerably.

“Squeak.”

The door opened.

“He’s coming out.” The archers on watch in each room reminded their partners.

The four archers in the two rooms all felt their heart-rates speed up. All of them secretly looked out the window in the direction of Jenne and Keane’s residence.

“It’s the girl. Don’t be impatient. Wait.” The archers were waiting quietly.

……

Pushing the door open, Jenne’s face was wreathed in smiles. After knowing that Linley wouldn’t leave and would continue to protect them, although she knew the path ahead was still perilous, Jenne still felt very happy.

“Ah. What nice, fresh air.” Jenne closed her eyes, taking a deep breath of the fresh morning air.

And then, Jenne began to walk in the direction of her younger brother’s room. In a clear voice, she called out, “Keane, time to get out of bed. Don’t be lazy-a-bed’.” As she spoke, Jenne knocked on the door.

Hearing Jenne’s voice, Linley opened his eyes, ending his training. As for Haeru, Linley’s Blackcloud Panther who was sleeping at the foot of Linley’s bed, he didn’t even bother to open his eyes.

….

Still wearing his sleepwear, Keane opened his door. Rubbing his eyes sleepily, he muttered, “Sis, why are we getting up so early? I haven’t woken up yet. It’s been a long time since I’ve had a good sleep.”

Right at this moment, the eyes of the archers in the third floor of the hotel lit up.

“Target acquired.”

The four archers simultaneously nocked their bows, preparing to fire.

…..

“Young miss, young master. You two have gotten up quite early.” The old servant, Lambert, pushed his door open as well.

“Good morning, Grandpa Lambert.” Jenne said warmly.

Keane just pouted, still rubbing at his eyes. “Grandpa Lambert, it isn’t that I got up early, it’s that big sis woke me up.”

Right at this moment.

“Fire!”

From one of the rooms in the third floor, an archer let out the order in a quiet voice. Simultaneously, two of the archers rose to their feet, their bows appearing in view of the window.

“Swish!” “Swish!”

Two sharp arrows shot out simultaneously. At the same time, the two archers from the other room shot their arrows as well.

“Swish!” “Swish!”

Two arrows in front, two arrows behind. In the blink of an eye, they ripped through the air, arriving directly in front of Jenne. Two of these arrows were aimed at her, while the other two were aimed at Keane.

At this moment…Linley was still in his room. The old servant, Lambert, was over ten meters away from the two siblings. Given his speed, there was no way he would be able to block in time.

“Young miss!” Lambert could only cry in alarm.

Jenne and Keane both felt the danger coming and turned their heads to look. But all the two siblings saw, as though in slow motion, were those arrows growing closer and closer to them.

The metal arrows sliced through the air with a ear-piercing hissing sound.

“Clang!” “Clang!” “Clang!” “Clang!”

Four sounds in a row.

…..

Jenne and Keane both stood there, frozen with shock. Next to them, Lambert was also frightened stiff. With a ‘squeak’ sound, the door to Linley’s room swung open.

Linley left his room.

“Bebe, all yours.”

Bebe was standing directly in front of Jenne and Keane. Just then, in the blink of an eye, Bebe had easily blocked four arrows in a row.

After the ambush attempt yesterday, Linley had expected this band of assassins to try again today. Thus, he had ordered Bebe to stand guard all night outside, just to be safe.

Given Bebe’s physically small size, when he hid amidst the grassy areas in the courtyard, not even Jenne and Keane would notice him, much less the archers.

“Boss, just watch.” Bebe excitedly licked his lips.

“Swoosh”

A cruel black shadow suddenly flashed through the air. A height of ten or so meters was nothing to Bebe, who jumped directly through the open windows. When the archers who had just failed with their sneak attack saw the little black Shadowmouse, their hearts shook and they immediately attempted to flee.

But before they had a chance to leave their rooms, Bebe had entered.

His two claws flashed forward, and two archers immediately collapsed in pools of blood. Bebe then smashed hard against the wall, going straight through the hole he had created into the other room.

The two remaining archers were hurriedly fleeing as well.

Turning, they saw a black blur flying towards them. The two of them didn’t even have the chance to call out. “Slash!” “Slash!” The sounds of two claws ripping through jugulars could be heard.

Bebe disdainfully looked at the two corpses on the ground, then immediately turned and left via the window, returning to the courtyard. From start to finish, only a few seconds had passed.

“Bebe, nicely done.” Linley praised with a laugh.

Bebe delightedly raised his head up high. At this moment, the Blackcloud Panther, Haeru, growled unhappily towards Bebe. “Hmph, if I had gone, I would’ve been even faster.”

Bebe immediately growled unhappily back at the Blackcloud Panther.

Linley couldn’t be bothered trying to placate the two of them. Instead, he walked towards Jenne, Keane, and Lambert, who were still in states of shock. They had escaped from life-and-death encounters twice in two days. Although in the past, the two siblings had often been bullied, they had never been in such danger.

“Everything’s fine now, everything’s fine now.”

Linley lightly patted Jenne on her shoulder. With a “Wah!” sound, Jenne suddenly burst into tears, hugging Linley. Next to her, Keane began to blubber as well, also charging forward to hug Linley.

Linley had no choice but to console these two siblings.

After the two of them had calmed down, Linley asked the nearby Lambert, “Lambert, you made our breakfast arrangements already, right?”

“Yes. In a bit, the hotel will probably send people with our breakfast.” Lambert looked at Linley with the utmost gratitude in his eyes.
