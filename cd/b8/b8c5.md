#### Book 8, The Ten Thousand Kilometer Journey – Chapter 5, The Mysterious Black Panther

Linley was beginning to frown.

He, too, had never heard of such a creature. There were several types of panther-type magical beasts, but one which was entirely pitch-black and covered with dense black wavy lines which formed decorative patterns, was totally unheard of.

Generally speaking, creatures that one had never heard of must not be underestimated.

The one-eyed bald man said, “That monster decided to eat the meat on the face and the legs of our magus right in front of us, one large bite at a time. Watching this, we were all furious, and we instantly charged forward to attack it.

“However…”

The bald man shook his head. “What we didn’t expect was that the monster was far too powerful. We originally thought that the reason it attacked from ambush, then slipped away was because it was weak. However…when our entire group attacked it, it only heavily wounded us.”

“Heavily wounded?” Linley questioned suspiciously.

“Right.” The one-eyed bald man said in fear and anger. “That monster definitely was capable of killing us all, but it didn’t. It only heavily wounded us.

“We originally thought that we still had a chance of living, only to find that the monster was focused on us now. Each day, it would take away two of our people. Sometimes, it would take them away, while other times, it would just eat our friends not too far away from us.”

Linley’s heart trembled.

He knew that magical beasts were highly intelligent. The magical beast that this one-eyed bald man had encountered clearly was extremely powerful and extremely intelligent. Most likely, this magical beast was a perverted creature.

“We wanted to flee back, but each time we tried to head away from and out of the Mountain Range of Magical Beasts, that monster would come again and heavily injure us again.”

That one-eyed bald man laughed bitterly. “We simply weren’t able to escape the Mountain Range of Magical Beasts. Each day, that monster would come and take away one or two people. In the blink of an eye, our twelve person squad only had six left.”

“Having already seen this happen several times, my wife finally broke down mentally when the monster once again began to eat our friends in front of us. She begged me. Begged me to kill her.”

The one-eyed bald man laughed bitterly. “You have no idea the terror we felt after those three days. All of us were at the point of collapse. My wife was fairly weak, even weaker than me. Faced with this terrible choice, in the end, I finally made the choice to put my wife out of her misery.”

“You killed your wife?” Linley frowned.

“Yes. I killed her with my own hands.” The one-eyed bald man said painfully. “But the very day I killed my wife, we encountered several other people, one of whom was a major figure in our Southwest Administrative Province. A combatant of the ninth rank named Pruitt [Pu’lu’te].”

“We had been at the brink of despair and collapse. I killed my own wife, but right afterwards, a combatant of the ninth rank appeared. How do you think I felt?”

The one-eyed bald man’s entire body was shaking. “I almost went mad. Truly. I almost killed myself, I was in such pain.”

Linley could totally imagine how, when overcome by despair and mentally broken down, one would personally kill one’s wife, so as to not allow one’s wife to suffer the fate of being eaten alive, one bite at a time. But then, after killing one’s wife, a combatant of the ninth rank appeared?

This sort of contrast was definitely capable of driving someone insane.

“I was filled with pain, but my other friends were very happy, because they knew that we now had a chance. A combatant of the ninth rank! That was someone whom only the Saint-level would surpass. We told our story to him, and Lord Pruitt immediately promised to dispose of this beast for us.”

“When that monster once more came for us, Lord Pruitt immediately made his move.” A strange expression was on the face of that one-eyed bald man. “Just one blow. The monster took a blow from Lord Pruitt head on, then smashed Lord Pruitt’s head open with a blow from its paws.”

Linley’s heart shook.

It was actually able to take a blow from a combatant of the ninth rank head on? Its speed and defense were both incredibly terrifying. A monster like this definitely couldn’t be underestimated.

“This time, the monster was extremely excited. Right before our very eyes, it suddenly transformed, increasing in size from two meters tall to nearly five meters tall and ten meters long. It devoured Lord Pruitt with one gulp.” The one-eyed bald man said in terror.

The look on Linley’s face changed.

“Able to change its size?” Linley was truly shocked.

All Saint-level magical beasts were capable of changing their size. They could easily make themselves much larger or much smaller. But of course, a very small number of magical beasts of the ninth rank with extremely high natural talent could do this as well.

For example, Bebe was capable of changing his size slightly.

In other words…

This magical beast was either a Saint-level magical beast, or an extremely talented magical beast of the ninth rank.

“It wouldn’t be a Saint-level, would it?” Linley’s heart was somewhat unsettled. Although Linley was very self-confident, he still didn’t have any hopes of dealing with a Saint-level magical beast at all.

That one-eyed bald man laughed painfully. “Just like that, the monster continued to torment us, eating two of us each day. In the end, only my younger brother and I were left. We continued to flee along the core regions, hoping in vain that this monster would engage in battle with some other powerful magical beast, giving us a chance to flee. But clearly, no magical beasts were capable of stopping that monster.”

Linley nodded.

He now totally understood.

But this one-eyed bald man didn’t have any good intentions towards Linley, insisting on following Linley. Clearly, this was out of the hopes that Linley would protect him. Acting like this showed that this man didn’t care about whether Linley lived or died at all.

The expression on Linley’s face grew hard.

“Milord, I…I had no other choices.” The one-eyed bald man knew what Linley was thinking. He hurriedly said, “I have kids. My second brother had kids as well. We didn’t want to die.”

“Do you think I want to die?” Linley said coldly.

Just based on what that one-eyed bald man had said, Linley had a general sense of how powerful this monster was.

It was faster than Bebe, and wasn’t hurt from a sword blow from a combatant of the ninth rank.

Just based on these two points, Linley couldn’t help but feel nervous. What’s more, that was only the power that had been revealed. What was the true level of power possessed by this monster?

Was it a Saint-level magical beast?

Linley couldn’t be certain. If it was a Saint-level of magical beast, then even if he and Bebe joined forces, they still wouldn’t be a match at all.

“You didn’t want to die, so you pulled us under water as well?” Linley felt extremely dissatisfied.

“Bebe, let’s go.”

Linley immediately sped up his footsteps, heading forward. The one-eyed bald man continued to follow Linley. Linley couldn’t help but turn his head and stare at him coldly.

This bastard was still following?

Clearly, that monster had its mind set on that one-eyed bald man.

“Milord, you…please save me.” The one-eyed bald man’s eyes were filled with a beseeching look.

But his actions only made Linley dislike him more and more. This man was selfish, only caring about himself. He didn’t care about others at all.

“Even a ninth rank combatant died. Do you think I’m a Saint-level combatant?” Linley suddenly drew the adamantine heavy sword from his back, and the one-eyed bald man was frightened into beating a hasty retreat.

“If you continue to follow me, then don’t blame me for being merciless to you.” Linley said coldly.

Linley was now a peak-stage warrior of the seventh rank, and a middle-stage warrior of the ninth rank in Dragonform. Although he was somewhat more powerful than when he was in Hess City, in Hess City, Linley was only capable of fighting that warrior of the ninth rank, Kaiser, to a draw.

Right now, it would be very difficult for Linley to be able to kill a combatant of the ninth rank in one blow.

But that monster had easily done just that, killing a ninth ranked combatant.

Risking his own life for a person he didn’t even know? Was that worth it?

Linley returned his adamantine heavy sword to its sheath, then left by himself. The one-eyed bald man just stood there, not daring to follow. He only stared with despair and hatred at Linley’s back.

“Ah!!!”

After walking less than a hundred meters, an agonized scream came from behind him. Linley immediately turned to look back.

On the snow ground, there was a black panther that was two meters tall and nearly four meters long. The black panther had, in its maws, the body of that one-eyed bald man.

“Save…save me!” The one-eyed bald man was still alive.

Linley’s attention was totally focused on the black panther. The black panther’s body was covered with a large number of wavy, patterned lines. It was quite beautiful, actually. And right now, that black panther’s cold eyes were currently looking at Linley with curiosity.

Clearly…

The black panther was playing a game. The previous game had just come to an end, and now, Linley had become the next victim in its game.

“Save me!” The one-eyed bald man stared at Linley, begging Linley with his eyes.

But that black panther just bit down viciously. With a crunching sound, half of the one-eyed bald man’s waist was bitten off, and his intestines began to slide out. The one-eyed bald man spasmed on the ground a few times, not dying right away.

The black panther walked forward gracefully, stepping on the one-eyed bald man’s chest with its sharp paws.

“CRUNCH!”

The one-eyed bald man’s chest caved in, and seconds later he stopped moving.

The black panther looked at Linley with interest, and then it began to slowly, gracefully move towards Linley. It must be said that its graceful stride was indeed quite beautiful to behold.

“Bebe. Prepare to ambush him. This time, we’re going all out.” Linley could tell that this unidentifiable panther-type magical beast now had its eyes set on him. Instead of allowing this creature to ambush him as it pleased, it was better to engage it head on.

Linley drew the adamantine heavy sword from its sheath, staring at the black panther.

“Hmph.” Linley’s body began to transform. Cold, sharp horns erupted from his forehead, while black scales quickly covered his entire body. That sturdy tail erupted from behind him as well, and his knees, elbows, and spine became lined with sharp spikes.

In the blink of an eye, Linley had totally Dragonformed.

The black panther, seeing this human suddenly transform into a strange, human-shaped aberration, couldn’t help but be startled. Its sleek, glossy hair immediately rose up in caution.

One was a Dragonblood Warrior.

The other, a mysterious panther-type magical beast.

“Come.” Wielding the adamantine heavy sword in his hands, Linley didn’t move at all, just standing there on the snowy ground. As stable and unmoving as a mountain.

The black panther’s body began to crouch down slightly. It was gathering its power!

“Whoosh!”

His dark golden eyes locked onto the black panther, this time Linley was just barely able to see the black panther’s movements. In the blink of an eye, the black panther had crossed the hundred meters distance between them and arrived in front of him.

“WHAP!”

Moving as fast as lightning, Linley’s draconic tail swung at the black panther’s body. In terms of speed, the attack speed of Linley’s tail was actually much faster than the black panther’s movement speed.

The black panther was knocked back over ten meters onto the snowy ground.

But immediately upon landing, the black panther let out a deep growl as it stared at Linley with its cold eyes. This time, the creature was clearly going to attack at full power. With a leap, the black panther charged at incredible speed, so fast as to make one’s heart tremble.

Linley could clearly tell that there wasn’t a single hint of blood on the black panther’s body.

The draconic tail of a middle-stage Dragonblood Warrior of the ninth rank wasn’t able to injure it at all.

The adamantine heavy sword of Linley swung downwards, chopping as fast as lightning. Black light seemed to flow off the blade of the adamantine heavy sword. The black panther actually dared to swing a paw to directly claw at Linley’s adamantine heavy sword.

“CLANG!” Linley’s adamantine heavy sword was actually deflected to the side by the black panther’s paw.

“Slash!”

The other paw slashed against Linley’s arm. On the black scales covering Linley’s arm, a rather deep scratch could be seen, and two scales had been split open as well.

The man and the magical beast had each exchanged a blow. They immediately separated.

“Growl…growl…” Standing in the middle of the snow, the black panther stared coldly at Linley. He now saw Linley as a serious opponent. Just now, his attack hadn’t been able to totally rip apart that scaled defense and tear off Linley’s arm. This made the black panther very surprised.

Linley stared at the damage done to his scaly armor.

Most magical beasts of the ninth rank were not capable of breaching Linley’s defense. But just now, that panther had been able to rip two scales apart.

The black panther’s body suddenly increased in size, transforming from two meters tall to five, and lengthening to ten meters as well. That black tail of the panther was waving around like a whip. The panther continued to stare coldly at Linley.

“Growl…”

This enormous creature once again charged towards Linley.
