#### Book 8, The Ten Thousand Kilometer Journey – Chapter 6, Another Transformation

“Boss.” Bebe’s voice rang out in Linley’s mind.

Holding the adamantine heavy sword with one hand, Linley leapt backwards in an arcing dodging pattern. At the same time, he mentally said, “Bebe, don’t panic. Let me first have a good fight with this mysterious black panther. If I can’t beat it, you can make your move against it. You are my secret weapon.”

Bebe, understanding, rapidly retreated to one side.

Right now, Linley had been filled with a growing urge to do battle. Despite having spent this much time in the Mountain Range of Magical Beasts, he hadn’t yet encountered an opponent who required him to truly use all of his power. Saint-level beasts were too powerful, while Linley could now totally dominate ordinary magical beasts of the ninth rank through his higher speed.

Only, this mysterious black panther was even faster than Linley.

“Growl!” The enormous black panther landed on the ground, cold gaze fixed upon Linley.

But Linley only had a hint of a smile on his lips.

“It increased in size, but no doubt its speed is now slower.” Linley could clearly tell that this black panther’s speed had dropped by 20% to 30% just now. With the support of the wind-style Supersonic spell, Linley was totally confident in his ability to deal with it.

But Linley also understood something.

With greater size came lesser speed…but most likely, the black panther’s offensive abilities had just greatly increased. Even in its normal form, the black panther had been able to rip open two of Linley’s scales. Linley no longer dared to allow the black panther to land any more claw attacks against him.

With a ‘swish’, that mysterious black panther once more pounced towards Linley at high speed, arriving in front of Linley in mere moments.

“Whoosh.”

Right at this moment, Linley suddenly slid down on the snow beneath the black panther, passing below it while simultaneously stabbing at the black panther’s chest with his adamantine heavy sword.

“CLANG!”

Linley’s heavy sword once again slammed against the sharp claws of the panther. Although the black panther’s speed had decreased with its increased size, the attack speed of its paw strikes was still astonishingly fast.

“Swish!” That seven or eight meter long black panther tail ripped through the air, viciously slashing towards Linley.

Linley kicked off powerfully against the ground with his right foot, launching himself towards an enormous nearby tree. As he arrived, Linley kicked viciously against the massive tree with both legs.

“CRACK!” The tree was broken in half and fell down, while its dense array of branches also smashed everywhere.

With astonishing speed, Linley used the bounce-back force to dive back towards the mysterious black panther, while at the same time, gripping the adamantine heavy sword with both hands in a vicious downward stroke against the black panther.

“Slash!” The adamantine heavy sword moved so fast that it ripped through the air, creating an ear-piercing, howling sound.

But right at this moment, the black panther turned its head to stare at Linley, staying there without moving, allowing Linley to strike it at will. Clearly, this black panther understood that after having transformed to a larger size, it would no longer be able to rely on its speed to suppress Linley.

“Swish!”

The adamantine heavy sword in Linley’s hands suddenly seemed to lose all weight and force, floating gracefully downwards at an astonishing speed. The tip of the sword, however, was beginning to tremble.

“Bam.” The adamantine heavy sword collided against the black panther’s body.

A look of surprise appeared in the cold, arrogant eyes of the black panther, because this sword blow seemed to have no force behind it at all. Without hesitating in the slightest, it sent its seven or eight meter long tail slashing fiercely towards Linley.

“Thunderbolt.” Linley’s formerly calm eyes suddenly seemed to spit lightning bolts.

The black panther suddenly felt as though that adamantine heavy sword which had just touched its back suddenly exploded with a terrifyingly powerful blast of force. The force was like the eruption of a volcano, blasting out power wildly and at high speed.

“BAM!”

The black panther felt its limbs grow soft, and its body was pressed down by a significant amount. Its glossy black fur suddenly began to ripple like the waves of the sea.

“Growl!!!!” A small amount of blood leaked out from the corner of the black panther’s mouth.

This level of ‘wielding something heavy as though it were light’ required perfect coordination between physical strength and battle-qi. It wasn’t just raw, brute force; rather, it was concentrating all of the rushing power and unleashing it at one blow. Although the black panther possessed astonishing defensive capabilities, with its fur neutralizing more than half of the offensive power, a significant amount of power still entered its body, causing the black panther some internal injuries.

“Whap!”

That whip-like black tail of the black panther landed viciously on Linley’s body, smashing apart the armored scales on Linley’s waist and sending Linley flying.

Just as Linley was about to smash into the top part of the trunk of another large tree, Linley suddenly stretched out his right hand and plunged his claws into the tree trunk like a grappling hook. Hanging onto the trunk, Linley looked down from his position at the upper trunk of the tree.

“As I thought. Once it transforms to a large size, its offensive power increases greatly.” Linley looked at the shattered scales on his waist and the fresh blood leaking from beneath it. He now understood much more about this mysterious black panther. “As for its defense, however, it didn’t increase that much.”

When it transformed to a larger size, the black panther’s defense didn’t change much. Its speed dropped, and its attack power increased.

“It seems as though my ‘Thunderbolt’ technique is still effective against it.” Linley was very satisfied with the effect of his ‘Thunderbolt’ attack.

This black panther possessed a terrifyingly powerful defense. Even the explosive power unleashed by ‘Thunderbolt’ was largely blocked by its extremely tough black fur, and the fur itself seemed to be totally undamaged.

If Linley were only to use raw, brute force and battle-qi against this black panther, he probably wouldn’t be able to wound it at all.

“Time to use my magic.”

Linley began to mumble the words to a magical incantation. Right now, Linley was hanging around thirty or so meters up above the ground off that tree trunk, while the black panther was staring up at him coldly from below. Seeing that Linley didn’t come down, this peak-stage, highly intelligent magical beast of the ninth rank, came to a snap decision.

If you aren’t coming down, I’m coming up!

“Swoosh!” That five-meter tall, enormous black panther suddenly flew into the air, leaping directly towards Linley. With its astonishing springing force, it cleared thirty meters with a single bound.

Linley’s heart was as tranquil as water.

Despite seeing the enormous black panther fly upwards towards himself, he still continued to chant the words to his spell. Only, he slapped the trunk of the tree with his right hand, sending himself flying upwards at an incline at high speed.

The tree which Linley had just slapped instantly split apart by the force of that blow.

“Crash!” The tree toppled to the ground towards the panther.

This tree was enormous enough that when it was falling, it took up half of available space. To the physically small Linley, it didn’t prove a problem at all, but the enormous panther was forced to slash at it with its paws and rip it in half.

Seizing this moment, Linley finally completed the magical incantation he was chanting.

“Swiiiish.” On Linley’s back, a pair of translucent, blue wings suddenly appeared. Flashing with azure light, the translucent wings seemed extremely beautiful. With a gentle flap of the wings, Linley’s body rocketed into the air.

Wind-style spell of the eighth rank: Airwings!

Seeing this, the enormous black panther instantly howled with fury. It actually pounced once more towards Linley at high speeds, as Linley flew higher.

“Bam!” Although the black panther had increased in size, it was still extremely dexterous and agile, capable of leaping dozens of meters at a single bound. Borrowing force against the tree trunk, it continued to leap higher and higher up the trees.

But after five or six leaps, the enormous black panther had reached the top of the tallest tree, while right now, Linley flew high above the Mountain Range of Magical Beasts with his translucent wings.

“Now, time for me to thrash you.” Linley saw that the enormous black panther had already leapt towards him from the top of the tallest tree. But now, with nothing to grab on, the black panther had no choice but to allow its body to fall down.

Just as its body began to fall…

“Whoosh!” Linley suddenly spread his wings and rocketed downwards at an astonishing speed.

Through using the astonishing downwards speed granted to him by the Airwings spell, Linley quickly arrived next to the falling enormous black panther. The enormous black panther glared angrily at Linley, but in mid-air, it had nothing to latch onto.

“Haaaaaaaaargh!” Linley suddenly activated all of the Dragonblood battle-qi in his body.

Reaching the absolute maximum limit of Dragonblood battle-qi power in an instant, and with both hands gripped tightly around the adamantine heavy sword, Linley delivered a vicious mid-air chop against the falling black panther, which had nowhere to dodge.

“CLANG!” The black panther’s sharp claws once again clashed against the adamantine heavy sword.

But Linley only confidently swung his adamantine heavy sword against it again at high speed. At this moment, the dancing adamantine heavy sword in Linley’s hands had seemed to become one with the wind, slashing down more than ten times against the falling black panther in the space of one second.

With each sword blow, he executed the ‘Thunderbolt’ technique.

“Bam!” “Bam!”

After blocking the very first strike, the black panther’s body had begun to accelerate its downward falling speed. But using his Airwings spell, Linley was still able to match the black panther’s rate of descent. One sword, then another, then another…

The black panther felt as though each sword stroke of Linley’s was more forceful and heavier than the last, and each sword stroke unleashed the same explosive, flood-like burst of power, causing its internal organs to shake.

After taking over ten blows, the body of the enormous black panther was smashed all the way into the ground by Linley.

“BOOM!”

An enormous crater appeared, and cracks appeared in every direction on the ground. The roots of the massive trees around them began to emerge from the ground, uprooted from the force of this collision.

In the middle of the crater, the enormous black panther spat out a large mouthful of fresh blood, and even a hint of blood could be seen coming out of its fur. These repeated blows by Linley’s heavy sword had caused even the black panther’s fur to be unable to withstand all of the attack force.

“Black panther.” Linley stood in midair, over ten meters above it. His translucent wings fluttered. “I know that you understand the human tongue. I’ll give you a chance. As long as you submit to me, I’ll spare your life.”

Right now, Linley really wanted to tame and acquire this magical beast.

Linley had been in sore need of a good mount this entire time. And, even more importantly, this black panther was an extremely superior creature, especially after it transformed in size. Its enormous, two-story tall body, combined with its astonishing speed and defense made it an absolute war machine.

“Growl!”

The enormous black panther stood up, staring coldly at Linley. Its deadly eyes were filled with boundless wrath. Its head was still raised proudly. How could it possibly submit so easily? But right now, the black panther understood that this human warrior in front of it wasn’t the prey it had thought he was. For a warrior to possess such terrifying power and also be able to use a high level wind-style spell such as ‘Airwings’ was an expert which was extremely rare in the human world.

“Are you willing to submit?” Linley shouted from up high.

As far as magical beasts were concerned, only martial force could make them submit and subdue them. And the higher the rank of a magical beast, the more difficult it was to make them submit.

“Groooowl!” The enormous black panther let out an angry roar.

“If you won’t submit, then I’ll beat you until you do!” Linley was very confident.

When combining his magic with his warrior abilities, his power could rise to an astonishing level. Right now, due to the pair of translucent Airwings on his back, Linley was in total control of the battlefield.

“Swish!” Linley once more dived downwards.

The movement speed of the pair of translucent wings was higher than that which four limbs provided. In the blink of an eye, Linley appeared in front of the enormous black panther as he once more smashed downwards viciously against it with his adamantine heavy sword.

But the black panther only retreated over ten meters at high speed, then pounced forward again.

Flexing his translucent wings, Linley began to dodge about very agilely in the air while constantly chopping downwards with his adamantine heavy sword. Every sword carried with it a terrifying force, capable of flattening a hill.

“Bam!”

The enormous black panther’s body was once more struck by the adamantine heavy sword and knocked flying. Blood had matted its glossy black fur with a red color. Linley stood confidently in midair, ready to strike another blow at the black panther at any moment with his adamantine heavy sword.

“Will you submit?” Linley said in a solemn voice.

The black panther once more rose to its feet, staring coldly at Linley. Suddenly…the black panther’s body began to shrink. It once more shrunk down to a height of two meters and a length of four meters…but the strange thing was, this time, the black panther’s entire body began to glow with a hazy black and white light.

“What on earth?” Sensing danger, Linley quickly flew a bit higher using his translucent wings, cautiously staring down.

That black and white light disappeared. The black panther’s body, previously covered with a large, dense amount of black stripes, now only had a few thick black stripes on its upper body, while the fur on its four limbs had turned as white as snow.

Seeing this, Linley sucked in a cold breath. “Blackcloud Panther? The legendary Blackcloud Panther?”
