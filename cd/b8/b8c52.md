#### Book 8, The Ten Thousand Kilometer Journey – Chapter 52, The Beirut Clan

“In terms of lineage as well as personality, Caylan is a fine man.” Wharton held Nina in his arms, speaking softly. “I’m afraid that your Imperial father will give your hand in marriage to Caylan.”

Nina nodded. “It is true that Imperial father values Caylan due to his high talent for magic. In the future, he has a high chance of becoming an Arch Magus of the ninth rank, and even has a chance to become a Saint-level Grand Magus. The Empire has many Saint-level experts, but most of them are Saint-level warriors. There are extremely few Saint-level Grand Magi.”

Wharton sensed that Caylan was a threat.

Although Lamonte belonged to the War God’s College, he was just an honorary disciple. In addition, his clan wasn’t particularly powerful either. He, Wharton, was a Dragonblood Warrior after all. As long as the Emperor wasn’t a fool, he would definitely select Wharton.

But if Caylan were to compete against Wharton, things would be different. His father was, after all, the powerful and influential Left Premier of the Empire.

“Nina.” Wharton became very solemn.

“Hrm?” Nestled in Wharton’s arms, Nina looked up at him.

“I am preparing to request an audience with His Imperial Majesty, and to personally ask for him to give me your hand in marriage.” Wharton said with a very solemn expression on his face.

Nina started, and then a look of wild joy appeared on her face.

“Truly?” Nina was very excited.

“Yes.” Wharton nodded. “Nina, before I do so, you can chat with your Imperial father and get a sense of which way the wind is blowing.”

Nina shook her head helplessly. “I thought I told you already. My Imperial father himself has yet to make up his mind. The only thing he says is, ‘no rush’, ‘no rush’….but my Imperial father does have a very favorable impression of you, and he values you as well. If you really were to ask for my hand, I think your chances would be very high.” Nina was very hopeful.

Only one of her older sister’s had married someone whom she loved. For the rest of Nina’s sisters, their marriages were marriages of political convenience, and not very happy ones.

Wharton nodded slightly.

“Don’t worry, Nina. I won’t let anyone take you from me.” Wharton tightly embraced Nina, who placed her head against Wharton’s massive, sturdy chest.

The Northwest Administrative Province. Cloudpeaks Village, outside the provincial capital. On the west side of Cloudpeaks Village, there was a forest. The already Dragonformed Linley was currently sparring with Bebe.

“Bebe, don’t force me.” Linley said helplessly as he wielded the adamantine heavy sword. “If you keep doing this to me, then I’ll be forced to use the Profound Truths of the Earth.”

“Heh heh, Boss, I know you care about me too much to do that to me.” Bebe was hovering in midair, speaking in the human tongue.

Upon reaching the Saint-level, magical beasts could freely alter their size, and also speak in human tongues. But only a Deity-level magical beast could transform into a human shape.

In the entire Yulan continent, only the King of the Forest of Darkness and the King of the Mountain Range of Magical Beasts, these two powerful Deity-level experts, could transform into a human form.

As for Bebe, who knew how long it would be before he could reach such a level?

“You rascal.” Linley sighed. “I reached the Saint-level and finally caught up to you in terms of speed and defense. But you, you immediately entered the Saint-level as well. Your speed became even more ridiculous.”

But movement speed and attack speed remained two different concepts.

The speed of swinging a sword was far faster than movement speed. Although in terms of dodging and agility, Linley was no match for Bebe, his adamantine heavy sword was still able to block Bebe’s attacks. Thus, facing Bebe, Linley usually just stood there, using his sword to defend himself.

“Heh heh.” Bebe laughed proudly.

Actually, Linley’s Profound Truths of the Earth still posed a real threat to Bebe. After all, the Profound Truths of the Earth all but ignored external defenses, rendering Bebe’s powerful defense useless.

But how could Linley bear to use such a vicious attack against Bebe?

Thus, Bebe continuously teased and taunted Linley in their spars.

“Enough. My body has two claw marks on it now. Are you happy?” Linley laughed as he rubbed Bebe’s little head. “Let’s go back. It’s time to eat.”

As he spoke, Linley reverted to his human form, then put on a new set of clothes.

“The Boss is always the best.” Bebe flew to Linley’s shoulders and giggled.

In Linley’s current squad, if Linley were to avoid using the Profound Truths of the Earth, there was no one here who was a match for Bebe. Bebe was a truly powerful Saint-level magical beast, through and through.

“Bebe, what sort of magical beast are you, exactly?” Linley walked while chatting with Bebe.

“I really don’t know either.” Bebe rapidly shook his little head.

Linley suddenly remembered something, then looked towards Bebe in astonishment. “Bebe, do you remember back when we initiated our ‘bond of equals’, I asked you what your name was? At that time you said, ‘Bei’, ‘Bei’. You didn’t say whatever it was you wanted to say very clearly.”

Linley remembered that scene very clearly.

“Little Shadowmouse, what is your name?” Linley had mentally asked him.

The little Shadowmouse had said, somewhat excitedly, “Bei….bei….”

Linley had stared at the little Shadowmouse.

“What’s the little Shadowmouse saying?” Linley didn’t really understand.

His white beard flowing, Doehring Cowart had floated next to him and mentally said,”Linley, this little Shadowmouse is still an infant. He can’t form precise sounds yet. Even when engaging in mental communication with you, for now, he can only communicate simple intentions.”

Due to their spiritual link, Linley had been able to feel the little Shadowmouse’s excitement, but the little Shadowmouse simply couldn’t speak at all.

“Okay. You were saying ‘Bei’….’Bei’….then I’m going to call you ‘Bebe’. How’s that?” Linley had grinned as he watched the little Shadowmouse.

The little Shadowmouse had seemed to ponder for a while, and then had happily nodded.

And just like that, Linley had named him ‘Bebe’.

“Did I say that?”

Bebe was startled.

“Oh, right.” Bebe remembered. “I remember now. When I was very, very young, so young that I couldn’t even open my eyes, I heard a very close, very warm voice speak to me.”

Linley immediately looked at Bebe. He had never heard Bebe speak of this before.

It was normal for magical beasts to be unable to open their eyes soon after they were born. At that time, Bebe most likely had just been born not long ago. That was a very distant memory. If Linley hadn’t brought it up, Bebe wouldn’t have recalled it either.

“That voice told me that I belonged to some clan. It instructed me to hide in the back courtyard of your clan’s manor and to not run around. And then, the voice disappeared.” Bebe was very puzzled.

“The Bei-something clan?” Linley said questioningly.

“I don’t recall very well. It seems to have been Bei…Bei…oh!” Bebe’s little eyes lit up. “‘Beirut’ [Bei’lu’te]. Right. It seems to have been ‘Beirut’. That voice told me that I was a member of the mighty Beirut clan. It told me not to run around, because it was dangerous outside. That’s why I stayed in your manor’s back courtyard the entire time, Boss, as I slowly grew up there.”

Linley now understood.

“The Beirut clan?” Linley was puzzled. “Magical beasts have clans?”

Bebe shook his head in confusion as well. “I don’t know either. I never met my parents after I was born. I just stayed at the back courtyard of your clan’s manor, and all I had to eat were those pieces of rubble.”

Linley firmly imprinted this name into his memory – the Beirut clan!

Linley was absolutely certain that he had never heard of any powerful clan in the Yulan continent named ‘Beirut’. But this clan was most likely a magical beast clan.

A magical beast clan?

Linley didn’t know about it because he was not a magical beast.

But Bebe didn’t know either, because he had no parents.

Ten more days passed for Linley within Cloudpeaks Village. Per Linley’s agreement with McKenzie, all he had to do was make a single trip to visit the Jacques clan within thirty days.

“Big brother Linley, you have a letter.” Jenne ran in excitedly from outside.

“Oh, it should be from the Dawson Conglomerate.”

The Dawson Conglomerate sent a letter every month. Linley immediately walked out. There was a young man leading a horse outside. Upon seeing Linley, the young man immediately bowed and said courteously, “Lord Ley, here is your letter.”

Linley accepted the letter and laughed. “Next month, there will be no need for you to come here.”

The young man looked at Linley questioningly.

“By this time next month, I will no longer be here.” Linley had made the decision long ago that in the next few days, he would head to the Jacques clan.

His wounds had healed long ago, and after Dragonforming, he was a Saint-level combatant. It was time to go visit his little brother.

It had been a long, long time since he had met with Wharton. In his heart, Linley had always missed this one and only sibling of his.

“Yes, Lord Ley.” The young man said respectfully, and then he mounted his horse and left.

As for Linley, he opened the letter and read it. The letter had quite a good amount of general information regarding the current state of affairs for the Radiant Church and the Yulan continent as a whole. It also had some information about Reynolds, George, and Yale. At the bottom was information regarding Wharton.

“George is really formidable.” Linley mentally sighed in praise.

With the support of the Walsh family, George had continued on his upward trajectory within the Yulan Empire. He himself was very talented as well, but more importantly…

The Third Prince of the Yulan Empire had successfully inherited the imperial throne, becoming the Emperor of the Yulan Empire.

Prior to the Third Prince assuming the position of Emperor, George had been on very close terms with him. The two of them were politically of one mind. Now that the Third Prince had succeeded his father as Emperor, George had become the youngest Grand Secretary in the Yulan Empire.

The entire Yulan Empire only had twelve Grand Secretaries. Each of them possessed extraordinary power and authority. What’s more, George was also the Deputy for the Right Premier of the Yulan Empire.

“By comparison, Reynolds hasn’t done as well as George.” Linley chuckled, then he closely read the information regarding Wharton. Linley had a general idea of what Wharton was up to.

But upon reading the letter…

“What?!” Linley was shocked. “Wharton has asked the Emperor for the Seventh Princess’s hand in marriage?”

The Dawson Conglomerate had just transmitted this news to the provincial capital of Basil not long ago. After all, this event only happened a few days ago.

“The Emperor didn’t agree?”

Frowning, Linley continued to read. “Fortunately, although he didn’t agree, he didn’t refuse too harshly either.”

According to the letter, the Emperor was continuing to delay.

That Lamonte had gone long ago to ask the same question, and the Emperor hadn’t agreed then either. Now that Wharton had gone, the Emperor still declined to agree. What he said was, “Nina is still young. There is no rush.”

Nina was already twenty one years old. She wasn’t that young.

But Nina was both a magus and a warrior, and her affinity as a magus was to water magic, which was of exceeding benefit to one’s body. Nina’s lifespan would definitely be very long. It would be easy for her to live for three or four hundred years. Given this, it was true that she did not need to be in a rush to marry.

“One is the son of the Left Premier of the Empire, while the other is an honorary disciple of the War God’s College.” Linley could immediately tell who his younger brother’s greatest adversary was. It was the Left Premier’s son, that magus named Caylan.

“It seems as though the situation isn’t looking good.” Linley’s forehead was furrowed. A cold light flashed in his eyes. “No matter what, I can’t let Wharton walk the same road that I did. Tomorrow. Tomorrow, I’ll go pay a visit to the Jacques clan. After satisfying the agreement, we’ll head directly to the imperial capital.”

Linley had made his decision.

But right at that moment…

“Lord, Lord!” The familiar voice of Gates rang out. Gates was probably the most lively of the five brothers.

“Lord!” It wasn’t just Gates; the others were shouting as well.

Puzzled, Linley returned to the courtyard. As he did, Gates and the others immediately rushed to him, their faces filled with wild joy.

“All of you are so happy. What’s the good news?” Linley laughed.

“Second brother, second brother has already reached the ninth rank!” Gates was the first to speak.

“Ankh, our second brother, is at the Saint-level as well after transforming.” The third brother, Hazer, said with joy.

Linley was startled.

Of the five brothers, Barker was the first to reach the ninth rank. After he possessed the power of the Saint-level, the other four brothers, all at the peak of the eighth rank, continued to work hard. Unexpectedly, another one had reached the ninth rank so soon.

“Myself. Bebe. Barker. Ankh. All of a sudden, four of us have reached the Saint-level.” Linley had never heard of a clan possessing four Saint-level combatants. The scariest part of it was…the other three brothers could break through at any moment as well.

Linley had no idea, but he was grinning so widely that his lips threatened to split apart.

Perhaps the very next day, someone would come running over to tell him that another one of the five brothers had broken through. They would then have yet another Saint-level in their ranks.

Linley now felt all the more convinced that his decision to go rescue Barker and his brothers was an absolutely genius decision. By now, aside from Barker and Ankh, the other three brothers could be considered Saint-levels in the making.
