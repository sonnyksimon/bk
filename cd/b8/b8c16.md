#### Book 8, The Ten Thousand Kilometer Journey – Chapter 16, The Yulan River

The greatest river within the Yulan continent was, without a doubt, the Yulan River. The Yulan River’s main stream flowed through the O’Brien Empire, the Yulan Empire, the Rhine Empire, and the Rohault Empire. Its countless tributaries were densely spread across each of the four empires.

It would be fair to say that the Yulan River nourished and gave life to over half of humanity.

“What a wide river.” Seated on the deck of a multi-level ship, Linley stared with awe at the vast, turgid waters of the Yulan River.

This ship had been employed by Linley for his usage alone.

He spent ten thousand gold coins to have it take the group directly to the harbor nearest to Cerre City. That harbor was less than a hundred kilometers from Cerre.

As Linley had explained it, if they continued on their originally planned route, who knows how many more assassination attempts they would have to endure? It was better for them to directly commission a boat to take them southwards through the Yulan River.

This boat had been commissioned by Linley on the spot. Linley didn’t believe that the people who worked on this ship all belonged to Madame Wade’s forces. Madame Wade’s influence did not, after all, hold much sway near Blackrock City.

“Big brother Ley.” Jenne came out of the ship’s cabin.

In the middle of this river, the wind was very strong. It blew against Jenne’s long hair and long dress. Smiling, Jenne looked at Linley. Walking next to him, she sat down as well. “Big brother Ley, to think that originally, I had wanted to employ you for ten thousand gold coins.” Jenne said these words with quite some embarrassment.

To Jenne and Keane, ten thousand gold coins was an enormous sum of money.

But how could they have imagined that Linley would go ahead and specially commission the services of this ship? The amount of money it cost to specially commission a large ship such as this was quite high. Although the distance between Cerre and Blackkrock was not that far, the cost was ten thousand gold coins. And what’s more, this was an extremely discounted price that they had given Linley as a show of respect to him, a powerful combatant who had a black panther for a companion.

So far, Linley had taken only a single gold coin out of the ten thousand gold coins he had been promised as his ‘hiring fee’.

But by now, Linley himself had already spent ten thousand gold coins. It wasn’t strange for Jenne to be embarrassed. Jenne and her brother had wanted to pay for the boat themselves…but of course, they currently had no money.

“Jenne, don’t you think that the scenery here is quite beautiful?” Linley walked to the end of the deck, which was surrounded by protective steel chains.

Linley rested his hands against the steel chains, looking at the surroundings.

The rolling waves of the Yulan River could be seen for kilometers about. At its widest, the Yulan River was several kilometers wide; at its narrowest, it was still hundreds of meters wide. This was the ‘mother river’ for the entire Yulan continent. Who knows how many people it had given life to? The recorded history of the Yulan continent had stretched back for hundreds of thousands of years.

“This Yulan River must have existed for hundreds of thousands of years as well.”

Gazing at the turgid river waters, Linley couldn’t help but imagine what it would’ve been like, hundreds of thousands of years ago. As he lost himself within this massive, boundless river, Linley felt his heart become unbounded as well.

“The people and kingdoms from hundreds of thousands of years back have turned to dust long ago. Compared to the endless march of history, where kingdoms and empires rise then collapse, personal grudges and enmities are so meaningless and small.”

Facing this vast river, Linley had a very strange feeling.

“Right now, the Yulan continent has six major political entities. The Four Great Empires, the Holy Union, and the Dark Alliance.” Linley’s heart was extremely calm.

Ever since he was young, Linley’s goals had been to realize his father’s dreams, and to stand at the highest levels of training and power.

But after his father died, Linley’s heart had fallen into a dark abyss. He had embarked on a road to revenge, a road of slaughter…and on this road, Linley had lost his Grandpa Doehring.

The three years of training he had spent in the Mountain Range of Magical Beasts and his communing with nature had allowed nature to cleanse his soul. His heart was now as calm as still water, and he had transformed, like a butterfly emerging from the cocoon.

“Only by reaching the pinnacle of power can one realize one’s dreams. Despite being such an enormous organization, when the Holy Union came face to face with that Dylin, didn’t they choose to retreat?”

Linley had total confidence in himself.

“There will come a day when I, too, will reach those heights.” Staring at the raging waves, Linley felt nothing but great ambitions, as boundless as the river.

….

The captain of this ship had an extremely easy life. Although the rapids of the Yulan River were rather fast, it was still far safer than the sea. The captain even had time to casually chat with his sailors.

“Hey, did you guys see that black panther?” The captain said delightedly. “That’s a magical beast. You just wait and see. My own son will tame a magical beast of his own soon.”

“Captain, that’s a panther-type magical beast. Do you think your son could tame one of those?” The nearby sailors began to laugh. There wasn’t too much of a social stratification between a captain and his sailors. Both were men who made their livings on the sea.

The captain sighed emotionally. “High-class magical beasts. I really admire those people who can tame one. I remember how last year, when we went to the imperial capital, I saw the War God’s College accept new honorary disciples. Wow. You have no idea how many experts were there. Some were mounted on enormous magical beasts, while others were seated on flying magical beasts…so many experts all rushed there, struggling to be the one to qualify for that sole slot. Those battles and those movements between the experts…all I saw were blurs. They were too fast, too fast.”

The sailors all began to make wild boasts about the experts they had seen before.

In the O’Brien Empire, every single child wanted to become a powerful combatant, with being recruited by the War God’s College being their ultimate goal.

….

Linley was seated meditatively on the wooden deck, allowing the wind to blow against him. His adamantine heavy sword was on his legs. His eyes closed, Linley was quietly attuning with the boundless vastness of the Yulan River’s waters.

“The power to impose is the power of the heavens, the power of the earth, the power of the boundless oceans.” Linley’s spirit had totally become one with the wind. He almost felt as though he could sense the vast riverbed of the Yulan River as well as the boundless land surrounding it.

Naturally, he could also sense that rushing river as well.

The ship continued to sail forward. They did stop occasionally in their journey so as to allow everyone to have some food, but Linley remained in the meditative posture on the deck, not eating at all.

In the blink of an eye, six days had passed.

“Sis, is big bro Ley gonna be ok? He hasn’t eaten or drank anything.” Keane pointed at Linley, who was still in the meditative posture, as he worriedly asked Jenne.

Jenne was somewhat worried as well, but she shook her head helplessly. “I don’t know either. That Bebe won’t let us get near him though.”

“Don’t worry.” The captain of the ship walked over, chuckling. “Those high-level experts aren’t like us ordinary folks. To them, even traversing a precipice ten thousand fathoms deep is of no issue. Not even a million man army can stop them. I’ve heard of people who, in the course of their meditative training, neither ate nor drank for months. At their level, not eating or drinking for months is actually quite normal.” Although the captain used the word ‘normal’ when he spoke, a trace of envy was in his eyes.

Hearing the ship captain’s words, Jenne and Keane began to feel even more astonished.

“Can it be?”

Suddenly, a murmur could be heard. Jenne, Keane, and the captain all turned their heads towards Linley, and when they did, they were shocked.

Holding the adamantine heavy sword in his hands, Linley jumped directly into the river.

“Big brother Ley!” Jenne shouted in alarm.

The three of them immediately ran over to the deck. Running to those locked steel chains, they stared down. To their amazement, they saw that Linley was currently standing on top of the water, the adamantine heavy sword in his hands. He floated up and down with the waves, but didn’t sink down at all.

This sight stunned them all and left them gaping in shock.

Mid-air flight was something only a person at the Saint-level could do.

“Earth…fire…water…wind…” Linley murmured in a quiet voice, and then suddenly, he thrust his adamantine black sword towards the sky. As the adamantine heavy sword shot up, it seemed as though a hole had been pierced in the sky, as a dreadful, screeching howl could be heard from the air.

At the same time, all the water surrounding Linley suddenly erupted skywards like a geyser.

“Haha.” Linley laughed loudly and happily, and then his body could be seen constantly moving and spinning about amidst the waves. The river water seemed to follow Linley’s movements, as the heavy sword constantly shrieked and howled with each stroke.

All the river water in an area of a hundred meters around Linley had gone wild.

Sometimes, the water would all rise tens of meters into the sky, while at other times, they would form a giant whirlpool. Other times, the water would shoot out like sharp arrows in every direction, while at other times, it would just circle around Linley….

“Clang.” A crisp, clear sound rang out from the heavy sword entering its sheath.

Those wild waters suddenly calmed down. In the blink of an eye, the Yulan River once more returned to its ordinary state, with just a few lingering effects. Striding on the waves, Linley didn’t sink down at all.

But this time, Linley wasn’t using his wind-style magic to counteract the effects of the weight of the adamantine heavy sword.

Rather, he was using his new insights on how to ‘impose’.

“This ‘imposing’ force was the force of the heavens. It is also the force of the enormous earth and the boundless seas.” A hint of a smile was on Linley’s face. With a gentle leap, Linley vaulted back onto the deck of the ship.

This entire time, Linley had been focusing on understanding ‘impose’ through his affinity to earth and wind. But over the course of these six days of meditation, Linley was able to sense the movements of the waves, and he also remembered the blazing passion of the fire elemental essences in fire-style magic.

Dense, graceful, pliable, and passionate.

When these aspects of these four elements were merged with each other in a sword stroke, they could make the universe move. This was what ‘impose’ truly meant. In the past, Linley’s understanding of ‘impose’ was nothing more than the most rudimentary of understandings.

“Big brother Ley, just now, what were you, what was…?” Keane was very excited, but he didn’t know what to say.

Jenne was looking at Linley with awe as well.

What Linley had just done had truly stunned them. Even the captain, who was well-travelled and worldly, had never seen such an awesome spectacle.

“Just training.” Linley said with a calm smile.

Although in the records of his clan, the highest level of using heavy weapons was this third level of ‘impose’, Linley suddenly had a certain feeling.

‘Impose’ was not the end of the road.

There was something even greater than it.

After reaching the ‘impose’ level, and in particular, after his soul could become attuned to nature, Linley always had this feeling…that there were even more profound truths awaiting him. Linley could dimly sense them, but he had no way of actually comprehending them.

“Battle-qi and brute strength are only the most basic of building blocks. In order for one’s attacks to become more powerful, having a deep grasp of these profound principles is extremely important.”

You might possess the power to lift something that weighed a million pounds, but if your movements were too stupid and clumsy, you might only be able to unleash 10% of your total power.

After training hard, you might be able to unleash 30%.

Experts would be able to unleash 70%.

But what Linley wanted to do was to unleash 100%. And, borrowing from the ‘imposing force’ of the universe itself, strike blows that were more powerful than he himself was physically capable of.

“Jenne, Keane, how far are we from the shore?” Linley asked.

“We are another day off,” the nearby captain replied.

Linley nodded, then instructed, “How about this. Let’s not get off too close to Cerre City. Let’s get off at the harbor one stop removed from Cerre City.”

“Yes, Lord Ley.” Although the ship captain didn’t understand the reason, he still agreed.

…..

Linley’s choice to travel by river had thrown all of Madame Wade’s forces into a state of confusion. That red-haired man, Kerde, in the end had managed to learn that Linley’s group had travelled by ship and were advancing through the Yulan River.

No matter how powerful Apothecary Holmer was, he couldn’t just leap past a river that was hundreds of meters across at its narrowest and get onto the opponent’s boat, right? Even if he was able to get on the boat, they would no doubt be highly suspicious of his intentions.

Thus, they could only lie in ambush at the port, as if they were waiting for a hare to fall into their snare.

However…

Based on their calculations, the ship should’ve already arrived by now.

“What’s going on? Shouldn’t they have arrived yesterday?” Apothecary Holmer was resting in a commoner’s house in a town that was located quite near the port.

“Master Holmer, please wait a bit longer.” Madame Wade’s subordinates were extremely frantic as well.

Suddenly, the door to the residence swung open, and one of Madame Wade’s subordinates rushed in. He angrily said, “Master Holmer, they didn’t stop at this harbor; they stopped at the previous one. They have already reached a small city named Redsand which is quite near Cerre. Most likely, they will reach the prefectural city of Cerre by tonight.

“They are arriving tonight?” Apothecary Holmer was startled.

“Quick, we need to head out immediately.” Apothecary Holmer immediately ordered, and the entire group frantically hurried back in the direction of the prefectural city of Cerre.
