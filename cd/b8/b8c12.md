#### Book 8, The Ten Thousand Kilometer Journey – Chapter 12, Blackrock City

Right. A kid who had lived in a small village after the age of six would naturally be very innocent. Linley felt that Jenne was quite innocent as well.

Through that short conversation, Linley had already learned a great deal about this little fellow, Keane.

At the same time, Linley more or less also understood what was going on with him and his sister.

“Assume the position of city governor? I’m afraid it won’t be that easy.” Linley thought to himself. Compared to these two innocent siblings, Linley could see much more deeply.

The highest level of city in the O’Brien Empire was the imperial capital, followed by the provincial capitals of the seven provinces. Beneath the level of the provincial capitals were the prefectural cities, then ordinary cities, and then countryside villages.

The status of a governor of a prefectural city was actually quite high.

How could the position of governor of a prefectural city be so easily acquired by an innocent countryside-raised child?

…..

After training the entire night, when Linley next opened his eyes, it was already dawn.

“Lord Ley, by nightfall tonight, we should be at the border cities of the Empire.” Lowndes chortled. “Lord Ley, let’s eat breakfast together.”

“Alright.”

Linley and Bebe headed over to them. As for Haeru…the food there wasn’t nearly enough for him. Late last night, Haeru had entered the Mountain Range of Magical Beasts and only returned after having eaten a good, full meal.

Within a carriage not too far away from Linley.

“Sis, I’ll get off first.” Keane happily hopped off the carriage.

Lambert looked at Keane, who had not a care in the world. He shook his head mentally, and then looked at Jenne. Lambert knew very well how innocent and how kind Jenne was.

“Miss, don’t rush off just yet.” Lambert squeezed out a smile.

“Grandpa Lambert, what is it?” Jenne looked questioningly at Lambert with her big eyes.

Lambert said, “Miss, you saw as well how we met with bandits on the way. When we reach the border cities, we’ll have to separate from the caravan. By then, I, an old man, along with you and the young master will be all alone on the road. If we meet with any bandits on the way, I might not be able to overcome them.”

Jenne couldn’t help but to think back to that bloody scene of attacking bandits from the previous night.

“Right. Then what should we do?” Jenne was a bit nervous.

Lambert laughed. “Miss, didn’t you notice that Lord Ley? Even the leader of those bandits was killed by Lord Ley with a single sword stroke. As long as Lord Ley is willing to protect you, you definitely won’t be in any danger.”

Jenne was eighteen years old, after all. She wasn’t as irresponsible as Keane.

“Grandpa Lambert, if I try to invite a powerful combatant like that to assist us, do you think he will agree?” Jenne looked at Lambert.

Lambert laughed encouragingly. “Don’t worry. Just tell him that you and Keane are the children of the governor of the prefectural city of Cerre [Chi’er], and that this time you are returning for the purpose of Keane assuming the governorship. If he can guard you on your way back, once you arrive at Cerre, you will definitely thank and reward him heavily. Remember…don’t tell him too much. Don’t tell him that in the past, you were living in a small village. Just tell him what I told you now.”

Lambert knew very well that if Linley became aware of the details of their situation, he probably wouldn’t agree.

“Oh.”

Jenne didn’t even notice that there were some slight differences between the truth and what Lambert had just instructed her to say.

“Go, and remember what I told you. Act sincerely.” Lambert encouraged.

“Okay.” Jenne nodded. Taking a deep breath, she summoned her courage and descended from the carriage.

Watching Jenne leave the carriage, Lambert secretly sighed. “Alas. Madame, even on your death bed, you weren’t willing to swallow your anger. You insisted on having Jenne and Keane go assume the position of city governor. Lord Count Wade [Wei’de] is already dead, but the senior madame probably won’t so easily allow Keane to assume the position of governor.”

“If we had a combatant of the ninth rank protecting us though, then we will have a good chance.” Late at night, Lambert had heard others whisper that McKinley had already reached the eighth rank as a warrior. But Linley had been able to easily kill him in one blow. As Lambert saw it, Linley should therefore be a warrior of the ninth rank.

…….

The wind was blowing. After eating to his content, Linley was relaxing comfortably for now, as they would depart again soon.

“The O’Brien Empire. Mm. We should be there tomorrow.” Linley was reclining on his cart, lazily awaiting their departure. But right at this moment, from the corner of his eyes, Linley suddenly saw someone approaching.

“Jenne?” Linley sat up curiously.

Somewhat cautiously, Jenne was walking over to him. Seeing Linley sit up and look at her, Jenne forced out a small smile. “Lord Ley, hello.”

“Miss Jenne, hello.” Linley was a bit confused. Why had this Miss Jenne come?

Jenne just stood there hesitantly for a moment, not knowing how to start.

“Miss Jenne, is there something I can help you with?” Linley asked preemptively.

Jenne’s face turned slightly red. Clearly, she was very nervous. “Lord Ley, it’s like this. My younger brother and I are journeying to my father’s prefectural city. My younger brother is going to assume the position of city governor. But we’re afraid that the journey to the city will be dangerous. Therefore, we were hoping…hoping to ask you, Lord Ley, to protect us.”

Getting this all out in one breath, Jenne began to stammer a bit.

Linley had a basic understanding of the general geography of the O’Brien Empire. His younger brother, Wharton, was in the southernmost administrative province of the O’Brien Empire, known as the O’Brien Administrative Province.

Linley himself currently was in the Northwest Administrative Province of the O’Brien Empire.

From the northwest province to the southernmost province was a journey that would most likely take a year and a half or so. But of course, if Linley hurried along the way by riding on the Blackcloud Panther, he could cross a thousand kilometers per day and arrive within ten days.

But Linley was in no rush.

His younger brother was in school at the O’Brien Academy. Why the need to rush over there? Right now, the most important thing for him was training and raising his own strength as much as possible.

“Protect you? For how long?” Linley asked with a laugh.

“Not too long,” Jenne hurriedly said. “The city of Cerre is in the Northwest Administrative Province. From here to there, it should only take us around ten days or half a month or so. When we get there, I will definitely thank you and reward you heavily.”

“Thank and reward me?”

Linley was sighing to himself. Based on Linley’s experience, he knew very well, how could the position of city governor of a prefectural city be so easily taken by a pair of innocent siblings who had no powerful backers at all?

“We’ll give you lots of gold coins.” Jenne looked hopefully at Linley.

Jokingly, Linley said, “Oh? How many gold coins?”

Jenne gritted her teeth. “Ten thousand gold coins? What do you think?” Jenne had been living in the village since she was ten. Normally speaking, one or two gold coins could last for quite a while in a place like that. She knew that the prefectural city was a wealthy place, and she believed that although ten thousand gold coins was an astronomical figure, the prefectural city should be able to support it.

“Ten thousand gold coins?”

That previous night, the mercenary captain had wanted to offer Linley ten thousand gold coins as a token of his thanks as well. But frankly, even aside from the wealth in Linley’s interspatial ring, each of Linley’s sculptures, given his status as a master sculptor, would be worth over a hundred thousand gold coins.

“Is that not enough?” Jenne stuttered.

Linley looked at Jenne. “Miss Jenne, generally speaking, how much did you and Keane spend each year in the village?”

“In the village?” Jenne was startled. Lambert had just instructed her repeatedly not to say that in the past she had lived in a village, but Linley had already known about it.

Jenne said honestly, “A few dozen gold coins each year. After all, we had to pay for my mother’s medical treatment. Right. Lord Ley, I don’t have that much money on me right now, but in the future, I will.”

Linley had to admit that she really was an innocent girl.

“So, um, actually, you know, it should be fairly safe inside the Empire’s borders. Grandpa Lambert probably was just over-thinking things. Um. I should leave.” Jenne felt rather awkward, and began to just blurt out random things.

“No. I just wanted to ask, right now, how many gold coins can you pay up front?” Linley asked.

After hearing that her prefectural city was in the Northwest Administrative Province, Linley had already made up his mind to help them, as it was on the way for him. After all, he was going to pass through the Northwest Administrative Province enroute to the O’Brien Administrative Province.

“Right now? I have around ten gold coins on me.” Jenne withdrew a small pouch in her purse. “Uncle Lambert has a few more coins on him also.”

Linley accepted the pouch, retrieving a single gold coin from it.

“Done.” Linley placed this gold coin into his own pouch. “From this moment forward, I’ve accepted this escort mission. But of course, this gold coin is just your down payment. When your younger brother becomes the city governor, I’ll collect the remaining 9999 gold coins.”

Jenne was wildly overjoyed at her success.

“Thank you, thank you.” Jenne was so excited that her little face turned pure scarlet.

…..

The caravan began to move forward once more, and the Blackcloud Panther once more began to lope alongside Linley’s cart. At the same time, Haeru looked suspiciously at Bebe and growled, “Bebe. Master accepted an escort mission for just ten thousand gold coins?”

Even a hundred thousand gold coins wouldn’t be enough to invite an expert like Linley to help out.

Just by killing a magical beast of the eighth rank, Linley would be able to procure a magicite core of the eighth rank that was worth 500,000 gold coins. Generally speaking, it was difficult for combatants of the eighth rank to kill magical beasts of the eighth rank. Only combatants of the ninth rank were able to kill magical beasts with confidence.

“Haeru, what do you know? The Boss is being benevolent, get it?” Bebe growled back to the Blackcloud Panther.

Growling to each other, the two magical beasts conversed in the language of magical beasts. Seeing them chatting to each other, Linley chuckled, continuing to sit quietly in the cart.

“Squeak, squeak.”

The cart’s wheels rhythmically squeaked, constantly moving forward. By the time the sun went down past the mountains, this caravan finally arrived at a border city of the O’Brien Empire.

Riding on the cart, Linley’s body swayed back and forth as he watched the distant city grow closer.

This was a pitch-black city that looked as if it were an enormous magical beast that had taken the land for itself. The walls of the cities were over thirty meters tall. Only powerful combatants would be able to scale such heights.

“Blackrock City. The ‘wall’ of the O’Brien Empire in the Northwest Administrative Province.” Linley had long since heard of this famous city.

Historically, there were quite a few major battles that had been fought at Blackrock City. Although many years had passed by, when they drew near Blackrock City, they could still see the dark red color staining many of the enormous black stones making up the walls of the city. These were dried bloodstains that had accumulated over countless years and battles.

“Everyone, we’ll part ways here.” Malone shouted loudly from outside the city walls.

Based on their mission requirements, their mercenary company was only responsible for delivering the caravan to this location. Immediately, the various merchants and travelers began to drive their carriages or carry their bags towards the city gates.

“Big brother Ley!” Keane called out from his carriage.

On the journey over, Keane had learned that Linley was going to escort them. Immediately, he grew even closer to Linley, and Linley, in turn, told Keane to just address him as ‘big brother’. After all, Linley was only 21 years old.

“Let’s go together.”

Linley led his two-meter tall, four-meter long black panther directly towards the city gates. The previously lazy-looking guard, seeing Linley’s black panther, was so scared that he immediately took a few steps back.

Panther-type, tiger-type, and lion-type magical beasts were all high-class magical beasts. Even the weakest panther-type magical beasts and lion-type magical beasts were generally of the seventh rank.

Right now, in a time of peace, the security at the gates wasn’t too strict.

The gate guards didn’t even inspect Linley, directly allowing him entrance.

“My heavens, what rank of magical beast is that black panther? When it looked at me, my heart almost stopped from fear.”  A gate guard cried out loudly in fear.

An older gate guard next to him lowered his voice and said, “Lower your voice. From what I know, the weakest type of panther, the Golden Tattooed Panther, is a magical beast of the seventh rank. This black panther is at least a magical beast of the eighth rank.”

….

“Wow! Blackrock City is so developed!” Keane’s eyes were shining.

On the main streets of Blackrock City, Linley, Keane, and Jenne were walking side by side. Jenne was wearing a peaked cap on her head, pressed down firmly and with a veil in front of her face. After all, Jenne’s beauty could cause a great deal of trouble.

“He thinks THIS is developed?” Bebe squeaked on Linley’s shoulders.

Blackrock City was a city meant for war. Although it was fairly developed due to traders, there was no way it could compare to the now-lost Holy Capital, Fenlai City. Even when compared to Hess City, the capital of a kingdom, there was quite a big difference.

“Careful.” Linley’s body suddenly turned into a blur as he flashed in front of Jenne and Keane.

“Swish.” “Swish.”

With a wave of his right hand, Linley snatched two arrows out of the air.

“You think you can run?” With a wave of his hands, Linley sent the two arrows going back the way they came, piercing through the throats of the two distant men who were preparing to flee.

“Urk…”

Those two men clutched their throats in shock, and then collapsed, dead.

“Ah!” The previously calm street became filled with screams, and many people began to run about in a panic.  “Let’s go.” Linley said to the stunned Jenne and Keane.
