#### Book 8, The Ten Thousand Kilometer Journey – Chapter 48, The Four-Winged Angel

Tonight, the moon was in the sky. The moon was very bright, covering the wilderness with its desolate glow.

And in this desolate wilderness, four white Angels were gliding down through the air coldly, like an illusionary mirage, drawing ever closer to Linley.

“What incredible speed.” Linley was surprised.

Right now, Linley’s offense was powerful, but his defense was weak. His offense was powerful enough to kill an early-stage Saint-level combatant. But his defense was poor; although he could take blows below the Saint-level of power, he still couldn’t take blows from early-stage Saint-level combatants.

“Hrmph.” Linley launched himself off the ground. With the aid of the Supersonic spell, Linley very agilely began to dodge. In terms of speed, however, Linley was still slightly slower than these four Angels.

“Shkreeeee!”

With an ear-piercing screech, a black blur suddenly appeared, moving even faster than those four Angels, colliding against the Angel nearest Linley.

“Die.” That Angel coldly smashed his fist against the black blur.

“Bam!” The fist, glowing with holy light, smashed viciously against the black blur. The black blur was knocked to the floor, but with a ricochet from the ground, it quickly charged up again.

“Swish!” Two fierce claws extended out, swiping viciously against the Angel.

One claw smashed against the Angel’s fist, while the other struck the Angel’s body. The Angel’s body was already at the point of collapse; struck by such a vicious claw, the body actually trembled, a layer of muscle being ripped open and blood pouring out.

“Bam!” Circling around once, the black blur smashed viciously against the Angel a second time.

This strike only hastened the collapse of the Angel’s body.

With a “boom” sound, the Angel’s body directly disintegrated. The white wings disappeared. Just like that, a Two-Winged Angel had died in battle.

Lyndin, who was watching the battle from behind, stared in astonishment at the black blur.

She could tell that the black blur was Linley’s pet, that adorable Shadowmouse. But by now, the Shadowmouse was already a meter long, no longer just that twenty-centimeter long, hand-sized critter. And the black Shadowmouse was astonishingly fast…even faster than Two-Winged Angels.

Bebe was simply too astonishing.

“Six years ago, in the Mountain Range of Magical Beasts, Bebe was roughly on par with Haeru in their battle. Six years later, Haeru hasn’t improved at all. He’s at his limit. But Bebe has continued to grow these six years…in terms of speed, Bebe is now far faster than Haeru. As for defense…perhaps even Barker, a Saint-level Undying Warrior when transformed, doesn’t have much higher defense than Bebe.” Linley knew exactly how formidable Bebe was.

Right now Bebe’s speed was simply too high.

Every day, Linley was absorbed in his bitter training. Bebe did nothing but eat, sleep, sleep, eat. And yet, the pace of his strength growth was faster than Linley’s.

One could tell how powerful Bebe had become just from looking at his transformation. From half a meter in the past to 1 meter long now.

“Squeeeeeak.” Bebe let out an excited cry, while mentally transmitting, “Boss, let me handle these Two-Winged Angels. Their attacks can’t hurt me at all.”

Linley was speechless.

Two-Winged Angels, early-stage Saint rank. Couldn’t harm Bebe.

What sort of freakishly powerful magical beast was Bebe?!

The other three Two-Winged Angels, seeing how this black blur’s lightning-fast claws had destroyed one of their comrades with two swipes, couldn’t help but be filled with both shock and fury.

Not giving them the time to react, Bebe immediately charged towards another one of them.

“Ignore him. Kill Linley.” Lyndin’s cold voice rang out.

The three Angels paid no more attention to that terrifying black blur, charging towards Linley. But although they paid Bebe no heed, Bebe himself wouldn’t let them off.

After all, Bebe was slightly faster than them.

“Whoosh.” A black blur flashed by, with Bebe arriving next to one of the Angels.

Linley had run far away, as he was not confident in his ability to deal with the group attack of the Angels. Only in single combat was he confident of success. After all, Linley wasn’t like Bebe, with his freakishly tough defense.

“Slash!”

Bebe opened his maw wide, chomping towards one of the Angels.

“Bam!” The Two-Winged Angel slammed his fists against Bebe, but Bebe actually wrapped his twin claws around the Angel’s right fist, and then bit at it.

“Crunch!”

The right hand was bitten off.

Resisting the pain, the Two-Winged Angel smashed his left fist against the black Shadowmouse angrily. This attack carried with it virtually all of the power available to the Two-Winged Angel, and his left hand shone like the sun.

“Baaaam!” The left hand smashed against the black Shadowmouse, but at the same time, the black Shadowmouse thrust its claws fiercely against the Two-Winged Angel’s chest.

Skin and flesh ripped open. Blood sprayed everywhere.

Bebe was smashed to the ground, but the Two-Winged Angel’s body trembled. The vessels in its body totally collapsed, and even its heart had imploded, unable to sustain that amount of power any longer. As blood leaked everywhere, the Two-Winged Angel collapsed from the skies.

Yet another Angel had fallen.

“Boss.” Bebe was looking anxiously at Linley.

“Bam!”

Linley was sent flying by a fist, but the Two-Winged Angel’s body shuddered, and then crumbled, falling from the skies. The last remaining Two-Winged Angel immediately chased after Linley.

“Boss!” Bebe’s speed reached its limit. With Linley constantly dodging as well, Bebe managed to interpose himself between Linley and the Angel, just before the Angel would have struck Linley again.

Bebe stared angrily at the Two-Winged Angel.

“Boss, you okay?” Bebe mentally transmitted.

“I’m fine. But if I took more of those blows, I wouldn’t be able to take it.” Linley wiped the blood away from the corner of his lips. Part of the scales around his chest were smashed apart, with blood leaking out from behind them.

Linley couldn’t help but be frightened.

Just then, the two Angels had pincer-attacked him. Linley was slightly slower than them to begin with. In the end, his only option was to block one attack with his own, while accepting the second blow.

“Still not fast enough. If I could match Bebe in terms of speed and defense, I wouldn’t have cut such a sorry state.” Linley sighed to himself.

Six years ago, Bebe was roughly as fast as he was, while Bebe’s defense was a level higher.

But six years later, Bebe’s speed was nearly double his own. In terms of defense, Bebe’s was multiple levels higher now. The most irritating thing was, Bebe remained at the ninth level. He had not reached the Saint-level.

No wonder the Blackcloud Panther, Haeru, had submitted to him.

Haeru was a proud magical beast of the ninth rank with extremely high natural talent, but compared to Bebe, his so-called talent was far weaker.

Using his astonishing defense and speed, Bebe dealt with the final Two-Winged Angel. In the blink of an eye, the four Angels had all died. Their leader, Lyndin, remained in her human form, watching from afar.

“Boss, are Two-Winged Angels of the early-stage Saint-level? Why did I feel that they weren’t that powerful?” Landing on the ground, Bebe mentally spoke to Linley.

Linley chuckled, casting a glance at Lyndin.

“Bebe, didn’t you notice that after they utilized their Angelic power, blood began to flow from their bodies? Clearly, their bodies couldn’t withstand that level of power. They weren’t truly early-stage Saint-levels; although they had the power, their bodies were still as weak as before.” Linley had immediately seen the truth of the matter.

Those bodies had been at the breaking point already. A few good blows to those bodies would cause them to totally collapse.

“What a powerful magical beast.”

Staring at Bebe, Lyndin said with surprise, “Linley, I only heard that you had a Shadowmouse, but it seems he isn’t a Shadowmouse. He seems more like the legendary ruler of the rat race….”

“What’s that?” Linley looked at Lyndin.

Linley had always been curious as to exactly what sort of magical beast Bebe was.

“The type of magical beast with the greatest defense and the highest speed…could he really be that type?” Lyndin had lived in the realm of the Radiant Sovereign for many years. As a Four-Winged Angel, she had seen many things.

There were quite a few magical beasts that would reach the Saint-level upon becoming an adult.

But even amongst those, there were still a few extremely rare and outstanding types of magical beasts. This was the first time Lyndin had seen any of the legendary rulers of the rat race.

“Boss, what’s this woman saying?” Bebe looked doubtfully at Linley.

“She’s saying you are a ruler amongst the rats.” Linley chuckled.

Even the likes of Doehring Cowart and the Holy Emperor didn’t know what kind of magical beast Bebe was, but it seemed as though this Lyndin had a bit of a clue. Only, from the sound of it, Lyndin was just guessing, and wasn’t certain.

“Linley, you should feel proud.”

Just now, Lyndin had only been briefly surprised by Bebe’s performance. Now, she had totally calmed down again. “For the sake of killing you, a Cherub, a Four-Winged Angel, is about to die alongside you.”

Lyndin’s entire body began to shine with white light, and then four white wings sprouted forth from Lyndin’s back, stretching and spreading out as Lyndin took to the skies.

A Cherub!

“Not good.” The look on Linley’s face changed. The more wings an Angel had, the more powerful they were, and as the number of wings increased, the power increased at a rapid geometrical rate.

“Boss, let me go!”

Bebe excitedly let out a sharp screech, then transformed into a blur as he charged towards the Cherub.

Lyndin smiled coldly. Her four wings fluttered slightly, and she suddenly transformed into a white blur. Her astonishing speed was actually not one whit inferior to that of Bebe’s.

“Boom!”

Lyndin’s fist, clad in holy light and appearing like white jade, smashed against Bebe. This time, Bebe was smashed down, flying into the ground like a meteor and even creating a deep crater in the ground. Bebe’s body had been smashed deep into the earth.

“Bebe.” Linley was shocked.

Linley had guessed at how powerful this Cherub was, but he didn’t expect the Cherub to be so terrifyingly strong.

“Bo-, Boss, I’m fine.” Bebe’s weak voice rang out in Linley’s mind. Linley could guess at how heavily injured Bebe currently was.

The power of a Cherub was far greater than that of a Two-Winged Angel.

“Linley. It is your turn.” Lyndin’s body was already beginning to be covered with blood, but Lyndin didn’t care about her collapsing body at all.

Lyndin knew that she had, at the very least, ten seconds of life left. These ten seconds were more than enough for her to kill Linley.

Those four white wings of light fluttered slightly, and then Lyndin transformed into a white blur. Linley couldn’t see her clearly, as she appeared almost like a mirage, suddenly appearing in front of him.

The only thing Linley could see were Lyndin’s cold, remorseless eyes, now silver in color.

“Time to go all out!”

“Ah!!!”

Linley launched himself off the ground, rapidly retreating while at the same time, the Bloodviolet Godsword appeared in his hand. He immediately activated that terrifying baleful aura hidden within Bloodviolet.

This terrifyingly baleful aura had influenced even the peak-stage Saint-level expert, Stehle, much less Lyndin.

Trembling slightly, that strange bloody light covered and began to flow on the surface of Bloodviolet.

That baleful aura entered Lyndin’s mind, attacking her soul.

“This…” A hint of fear suddenly appeared in Lyndin’s cold eyes. She only sensed that she seemed to have returned to that time when she was with the army of Angels engaging in warfare in other planes, and had suddenly encountered within the depths that terrifying demon. She still remembered how that demon had easily butchered so many of the Angels. An entire army of hundreds of thousands of Angels had been butchered.

That full-power punch of hers, under the influence of Bloodviolet, began to grow weaker.

At the same time as he activated Bloodviolet, Linley fiercely swung his adamantine heavy sword forward, chopping mercilessly against Lyndin’s body.

Profound Truths of the Earth – Hundred Layered Waves.

“Boom!” Linley was struck by Lyndin’s fist as well, which had been reduced to roughly half-power. His black scales immediately split apart, and Linley’s chest caved in as a large volume of blood poured out of Linley’s mouth.

Like a ripped sandbag, Linley smashed against the ground, kicking up a huge cloud of dust.

Lyndin stood there disbelievingly.

“How could he possess such a terrifying baleful aura?” And then, Lyndin suddenly felt herself bound by the Laws of the universe. Her soul, not resisting in the slightest, was drawn forth by the Laws, disappearing from the plane of the Yulan continent.

As for Lyndin’s corpse, it gently slumped down, fresh blood leaking from her mouth and nose.
