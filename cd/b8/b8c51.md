#### Book 8, The Ten Thousand Kilometer Journey – Chapter 51, Wharton

The imperial capital of the O’Brien Empire. Channe [Chi’yan]. In the entire Yulan continent, perhaps only the capital of the Yulan Empire could match Channe in terms of size.

As for the name ‘Channe’, the War God O’Brien himself had chosen this name.

The imperial capital, Channe. There were millions of residents living here.

As a capital with over five thousand years of history, Channe had many ancient clans. In a place such as the imperial capital of Channe, even experts of the ninth rank were quite common. No one dared to act rashly in the imperial capital, because there were far too many powerful clans here.

But of course, the number one power of the imperial capital of Channe was, without a doubt, the War God’s College.

Although the personally taught disciples of the War God virtually never showed their faces, even the weakest of the honorary disciples were at least warriors of the eighth rank, while most were warriors of the ninth rank. From this, one could tell how astonishingly powerful the War God’s College was. And of course, there was the master of the War God’s College. The War God himself.

It must be understood that in the O’Brien Empire, all other religions were outlawed. Even the commoners prayed to the War God. The War God had become the object of their faith!

From this, one could tell how important the War God was in the hearts of the commoners.

The east part of the imperial capital of Channe was a place covered with palaces and noble residences, with the imperial palace located within the east city as well. Within East Channe there was a street named Boulder Street, and on each side of Boulder Street there were meticulously constructed manors. These were all built by the order of the imperial clan of the Empire, and were given as rewards to the nobles and government officials who had rendered great deeds unto the Empire.

One of the manors on Boulder Street was the residence of the newest rising star of the Empire, Count Wharton. Two sturdy guards stood at each side of the gate to his residence, their waists stiff. And right now, within the main hall of the manor, there were four people.

All four of them were standing, but one of them was pacing about, a hint of a frown appearing on his brows.

He seemed to be roughly twenty one or twenty two years old. He wore a simple warrior’s outfit, with the sleeveless outfit totally revealing his bulging muscles. He had a straight nose, thick black eyebrows, and a blocky, angular face, making him look very courageous and fierce.

But the most astonishing thing about him was his body.

He had the astonishing height of 2.2 meters. He had massively broad shoulders, a comparatively narrow waist, and two toned, powerful legs.

“Just by looks, Wharton does seem to be more astonishing than Linley.” Hillman said to himself.

Compared to Wharton, Linley appeared to be more reserved and understated.

“Young master Wharton, are you still worrying about the Seventh Princess?” Housekeeper Hiri, his nose red from drinking wine, begin to chortle. Wharton turned to look at him helplessly. “Grandpa Hiri, you know who those people chasing after Nina [Ni’na] are.”

The other young man in the group of four laughed. “Young master Wharton, why has a bold, forthright man such as yourself become so squeamish and nervous when it comes to matters of love? Why don’t you just go with her to meet with His Imperial Majesty? Isn’t that simple?”

“Just go directly?” Wharton raised an eyebrow.

Hillman encouraged as well, “Nader [Na’de] is right. You are already a warrior of the eighth rank, and the scion of the Dragonblood Warrior clan. His Imperial Majesty surely knows that for a scion of the Dragonblood Warrior clan to reach the eighth rank means that he definitely has been able to train in Dragonblood battle-qi, and has the ability to transform.”

As Hillman saw it, for someone to reach the eighth rank without training in battle-qi was virtually impossible.

But Hillman had no idea that right now, by Linley’s side, there were five brothers who had reached the eighth rank just based on physical training.

“Wharton, as a Dragonblood Warrior, you are a fit and qualified match to wed the Seventh Princess. I trust His Imperial Majesty will agree.” Housekeeper Hiri laughed as he spoke. “But as for asking for her hand, I think it might be better if you let the Seventh Princess to sound His Imperial Majesty out first. That way, you’ll have a better idea going in.”

Housekeeper Hiri and Hillman glanced at each other, then both of them began to laugh.

In the past year or two, the relationship between Wharton and the Seventh Princess of the Empire had become quite well known throughout the entire imperial capital. Only, the other young nobles of the imperial capital had refused to give up. What’s more, two of them were quite competitive.

“Enough of that for now.” Wharton shook his head.

He trusted the Seventh Princess. The Seventh Princess had already told him long ago that aside from him, she wouldn’t marry anyone else. But Wharton also knew that the marriage of an imperial princess of the Empire was not up to her alone to decide. In addition, Wharton didn’t want the Seventh Princess to be too frustrated and unhappy. If he could openly wed her, that would be for the best.

“Oh, right. Grandpa Hiri, any news of my big brother?” Wharton asked.

Housekeeper Hiri nodded. “The Dawson Conglomerate has sent word that your big brother remains hidden in seclusion, where he continues to train. There’s no special news.”

“Big brother is as hard working as ever.” In his heart, Wharton admired Linley very much.

Many of the weighty responsibilities of the Dragonblood Warrior clan, such as the reclaiming of their ancestral heirloom, or the avenging their parent’s deaths, had been shouldered by Linley alone. As for him, Wharton, he could remain here in the imperial capital and quietly train.

Even from afar, Linley continued to shield him from the wind and the rain.

“Big brother…” Wharton still remembered how when he was young, when those two Saint-level combatants were doing battle outside Wushan Township, those boulders had rained down densely from the skies. His big brother had ignored his own safety to cover Wharton with his own body.

Wharton could clearly remember that dangerous moment….

“Get down!” Linley had angrily shouted at Wharton, while charging towards him with no regard for his own safety. Linley had used his own weak, frail body to shield Wharton.

After leaving home at the age of six, Wharton was now twenty two years old. In another month, he would be twenty three.

It had been almost seventeen years.

He hadn’t seen his sibling in seventeen years.

“Young master Wharton, don’t worry too much. Young master Linley will come find you once his training reaches a certain level. After all, he knows exactly where you are living.” Housekeeper Hiri said consolingly.

Wharton nodded, then chuckled at himself. “When big brother sees me, I wonder if he’ll still recognize me.”

“The little six year old kid has changed quite a bit. Haha…it’s true that your big brother might not recognize you.” Hillman began to laugh.

Nader nodded as well. “When I came along with my father from the Holy Union, I initially couldn’t recognize you either, young master Wharton. It was only after I saw Housekeeper Hiri did I realize that this big fellow who was even taller than me was actually that little kid I used to know.”

“Nader, you punk.” Wharton glared at him.

Nader was Hillman’s son. However, Nader didn’t have much talent as a warrior; although he was already twenty five years old, Nader was only a warrior of the fourth rank. But Nader was extremely discreet and careful, and so alongside his father Hillman, he managed and oversaw the work of all the guards of the manor.

“Whoah, it’s getting late.” Wharton took out a pocket watch and cast it a glance. “Grandpa Hiri, Uncle Hillman, I need to head out.”

“He must be meeting up with the Seventh Princess again.” Nader snickered, intentionally putting a smirk on his face.

Wharton laughed confidently towards Nader. “Naturally. What, are you jealous?” As he spoke, Wharton chortled as he walked out of the manor.

Watching Wharton leave, Housekeeper Hiri felt very moved.

“When we came, young master Wharton was just a child. But now, he’s all grown up. I have fulfilled the task Lord Hogg gave me.” When he thought of Hogg, Hiri couldn’t stop sighing.

“The Baruch clan has been slumbering for many years. But now, it has finally begun to awaken. In another ten years, most likely the entire Yulan continent will once again be filled with people discussing the legendary Dragonblood Warriors.” Hillman said confidently.

Carrying the warblade ‘Slaughterer’, Wharton rode on a Saber-Toothed Tiger on the streets. Saber-Toothed Tigers were magical beasts of the eighth rank, and thus their aura would make ordinary magical beasts cower away from it. What’s more, Wharton was so physically huge himself. Together, they posed such a terrifying sight that everyone who saw him felt dread.

Thus, the pedestrians on the street all made way for him.

“That’s the genius student of the O’Brien Academy, Wharton. Look. He’s riding a magical beast of the eighth rank.”

“Saber-Toothed Tiger. How fierce! If I had a magical beast of my own, how great that would be.”

Many people on the streets chatted about Wharton as he passed by. In the past, when Linley had seen that Velocidragon for the first time, he too had dreamed of having a powerful magical beast like a Velocidragon for his companion. In the eyes of many youths, Wharton was their role model.

Saber-Toothed Tigers were extremely fast. Even when travelling on the streets, it moved forward very rapidly and very nimbly.

“Here we are.” Wharton saw that magnificent hotel from far away. This was the appointed meeting spot for him and the Seventh Princess. The receptionist for the hotel recognized Wharton as well, and immediately opened the door for Wharton to enter.

Leading the Saber-Toothed Tiger behind him, Wharton entered the hotel.

Wharton looked around the hotel, his gaze finally settling on the person he cared about the most. He immediately called out happily, “Nina.” But just at this moment, Wharton suddenly frowned…because he also once again saw the person who irritated him.

“Wharton.”

Nina had a head of full, lustrous blonde hair, and her pale face was as charming as ever. Her brilliant, shining eyes didn’t have a single hint of impurity in them.

Nina ran over happily towards Wharton, who immediately stepped forward, taking Nina by the hand.

“That guy is bothering me again.” Nina whispered to Wharton.

Wharton glanced at the distant man, saying in a low voice, “Nina, don’t pay any attention to that guy.” But just at that moment, the handsome young man walked over. With a calm laugh, he said, “Wharton, I really didn’t expect to see you here. Why is it that you always appear wherever Nina is?”

“Shut your mouth, Lamonte [Lan’mo].” Wharton frowned. “Remember. Nina’s name isn’t for the likes of you to call out. And also. The question you asked me, I should be asking you. Why is it that wherever Nina is, you always appear?”

Lamonte glanced at Wharton, a smile that was not a smile on his face.

Although on the surface, he didn’t seem to care much, in his heart, Lamonte really disliked this Wharton. After all, it was Wharton who had taken Nina away from him.

“Oh, a Saber-Toothed Tiger.” Lamonte looked at Wharton’s Saber-Toothed Tiger. Laughing, he said, “Wharton, any interest in letting my Blue-eyed Tiger Mastiff have a fight with your Saber-Toothed Tiger? I’ll wager that my Blue-eyed Tiger Mastiff would definitely win.”

Blue-eyed Tiger Mastiffs and Saber-Toothed Tigers were both magical beasts of the eighth rank.

However, there were differences in power amongst magical beasts of the eighth rank as well. For example, Goldmane Mastiffs and Blue-eyed Tiger Mastiffs were considered one of the top kinds of magical beasts of the eighth rank. Blue-eyed Tiger Mastiffs were particularly effective against tiger-type magical beasts.

“Not interested.” Wharton paid his suggestion no heed at all. Looking coldly at Lamonte, Wharton said, “Lamonte, if you really want to have a competition, I wouldn’t object to having a sparring match against you. As for having magical beasts, compete? Hrmph.”

“A competition between men?”

Lamonte chuckled, then no longer said anything.

He, Lamonte, was an honorary disciple of the War God’s College, and he was a warrior of the ninth rank. He was indeed qualified to be arrogant. But right now, virtually all of the ancient clans of the imperial city knew that Wharton was of the Baruch clan, which in turn was the clan of the Dragonblood Warriors. And Wharton was clearly able to use battle-qi.

A scion of the Dragonblood Warrior clan who could use battle-qi was definitely capable of Dragonforming as well.

Lamonte knew very well that although Wharton appeared to be only a warrior of the eighth rank, when using that unique, special warblade of his to attack, he could fight on par with ordinary warriors of the ninth rank. But once Wharton transformed, he, Lamonte, wouldn’t be a match at all.

“Let’s leave.” Gently stroking the head of his Blue-eyed Tiger Mastiff, Lamonte chuckled lightly.

And then, Lamonte left along with his magical beast, just like that.

Nina and Wharton headed directly to a private deluxe room on the second room of the hotel. As for Nina’s female attendant, she stayed outside the room.

“You big lunk, tell me, what should we do about that Lamonte? He is so annoying.” Nina nestled in Wharton’s arms, asking in a soft voice.

‘Big lunk’. This was how Nina had addressed Wharton the first time they had met. Whenever they met in private, this was how Nina would address him.

“It is your own fault for being so charming, Nina.” Wharton grinned as he tweaked Nina’s nose. “Actually, I don’t care too much about that Lamonte fellow. The one I’m worried about is Caylan [Kai’lan].”

“Big brother Caylan?” Nina said with resignation, “I only think of him as a big brother, but he…sigh.”

Caylan was twenty three years old, but was already a magus of the seventh rank.

There were quite a few twenty three year old warriors of the seventh rank, but very few twenty three year old magi of the seventh rank. Moreover, Caylan had reached the seventh rank as a magus when he was twenty one years old.

If Linley hadn’t sculpted ‘Awakening From the Dream’, most likely it would’ve taken him until the age of twenty to reach the seventh rank.

In the imperial capital, Caylan was considered a genius magus. He had been childhood friends with Nina. And more importantly, Caylan’s father was the Left Premier of the Empire, an extremely powerful man. Caylan himself was, simply put, a very good person as well. It could be said that he was a nearly perfect individual.
