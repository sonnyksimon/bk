#### Book 8, The Ten Thousand Kilometer Journey – Chapter 33, The Four Bros’ Paths

Zassler walked out from the back as well. Hearing the middle-aged man address Linley by his name, he immediately grew wary. But after he reached Linley’s side, he saw that upon reading the letter, a smile appeared on Linley’s face. A very happy smile.

Zassler could tell that although Linley wasn’t a sinister fellow, he was rather callous, focused utterly on training.

He had never seen Linley smile in such a happy, brilliant manner.

“Zassler.” Linley laughed. “You stay here for now. I need to meet a friend.”

“Sure.” Zassler nodded.

“Bebe.” Linley shouted towards Bebe, who was sleeping on the ground. Bebe opened his bleary eyes, staring questioningly at Linley.

“Come, make a trip with me.”

“Haeru, you can stay here.”

Bebe delightedly raised his head up at Haeru arrogantly, then scampered onto Linley’s shoulders. Happily, he mentally spoke to Linley. “Boss, what are we going to do?”

“You’ll know when we get there.” Linley laughed.

“Lead the way.” Linley said to the middle-aged man.

Within fifteen minutes, Linley and the middle-aged man reached a lavish, large mansion. From far away, Linley could recognize the figure standing in the middle of the main hall.

“Third Bro!” That familiar voice called out excitedly.

“Boss Yale.” Linley was laughing as well.

“Squeeaaaak!” Bebe squeaked out delightedly as well. When they were at the Ernst Institute, Bebe had gotten along very well with Yale, Reynolds, and George as well. Naturally, they were quite familiar with each other.

Yale had matured quite a bit compared to three years ago. Right now, Yale was roughly as tall as Linley, nearly two meters tall. But Yale was slightly thinner than Linley, making him appear like a tall, skinny man.

That form-fitting black gentleman’s suit, combined with a faint cologne, made Yale seem to have a very magnetic charisma.

“Third Bro, I’ve been worried to death over these past three years.” Yale bear-hugged Linley.

Hugging his dear friend, Linley felt very happy as well.

In the past three years, he hadn’t seen his dear friends a single time.

“I didn’t expect that you would grow to be about as tall as me. These three years really have changed you.” Yale sighed. Compared to three years ago, Yale didn’t change that much, but Linley had.

Linley laughed loudly. “You were a year older than me to begin with. You just had a head start. Now that you are no longer growing, it’s very normal that I caught up.”

Bebe squeaked off from the side.

Bebe was very happy as well. It had been a long time since Bebe had seen Linley laughing and joking like this.

“Wow, Bebe!” Yale hugged Bebe, affectionately rubbing his little head. “I knew that you’d come. I’ve prepared some fine foods for you!”

Yale turned his head and glanced at the attendant, who understood what Yale desired. Very shortly afterwards, over ten attendants pushed food carts laden with food over.

“This is roasted meat delicacies from around the world. Bebe, have a taste.” Yale laughed loudly.

Bebe’s little nose sniffed the air, then his eyes immediately began to shine. Transforming into a black shadow, he charged towards those food carts. Watching this, Linley and Yale both began to laugh.

“Boss Yale, let’s chat inside.” Linley said with a laugh.

The two bros entered the main hall, which had been covered with all sorts of delicacies and fine wines. The two bros began to eat and chat.

“Right, Yale, what happened to the Ernst Institute?” Linley suddenly asked.

“It’s finished.” Yale shook his head and sighed. “The Ernst Institute was very close to Fenlai City and came under heavy attack by the magical beasts. You know, even the instructors in the Institute were only of the eighth rank at most. Most of the students were very weak. Facing all those magical beasts…how could they resist them?”

Linley nodded.

Students of the sixth year, the highest year, were just magi of the sixth rank. But the magical beasts possessed quite a few beasts of the fifth, sixth, seventh, and eighth ranks. When a large number of magical beasts charged over, it really was a disaster.

“There is no longer an Ernst Institute in the world.”

Yale sighed. “I, Reynolds, and George left the Holy Union three years ago. These three years, I’ve been running around between the O’Brien Empire and the Yulan Empire. As for Reynolds, naturally he returned to his clan, while George returned to the Yulan Empire as well. I hear that George has done quite well for himself. He’s managed to enter the imperial government of the Yulan Empire.”

“Entered the imperial government?”

Linley wasn’t too shocked. George was, after all, a very good person at organization, and behind George was the mighty Walsh clan. Success wouldn’t be too hard for him.

“And Fourth Bro?” Linley laughed as he asked.

“Fourth Bro? He returned to his own clan and was delivered to the army by his father.” Yale laughed loudly. “Third Bro, just imagine. Fourth Bro in the army. Isn’t that unbelievable?”

Linley began to laugh as well.

Their Fourth Bro, Reynolds, was a very lively and rebellious person. But now, he was entering the army? One could imagine how miserable he was there.

“But last year, when I saw Fourth Bro, he seemed to have changed quite a bit. He’s much more mature than before, and he does indeed look like a soldier now. But as soon as he started to drink with me, he returned to his old self.” Yale roared with laughter.

“Boss Yale, what about yourself? I feel that compared to before, you have even more of a nobleman’s aura than in the past.”

Indeed. Dressed in that black gentleman’s suit, Yale’s nobleman’s aura could be clearly sensed by anyone.

“Nothing for it.” Yale laughed bitterly. “After leaving the Ernst Institute, aside from normal magical training, I’ve been focused on managing some of my clan’s affairs. Naturally, I had to sit through countless noble banquets. After so long, I’ve learned some of their mannerisms.”

Linley nodded.

His three dear bros had all embarked on path which belonged to them.

Government. Military. Market.

“And what about myself?” In his mind, Linley knew exactly what his path was. “Advance on the path of training until I reach the level of the High Priest, the War God, and Dylin. Stand at the very peak of the Yulan continent!”

The absolute peak-level experts possessed all the true power in this world.

To a Deity-level combatant, everything was but a joke. No one dared to offend a Deity-level combatant. They were the ultimate forces in existence in the Yulan continent.

Linley wouldn’t permit any obstacles to prevent him from advancing on this path.

Nothing would stop him!

“Third Bro, three years ago, when I went to the imperial capital, I saw your little brother.” Yale suddenly said.

“Wharton?” Linley’s eyes lit up.

Yale nodded with a laugh. “When I saw Wharton, he was very worried about you, since he didn’t know what your situation was. I told him you were fine, and that you were just training by yourself.”

“How is Wharton doing?” Linley asked.

“Don’t worry, he’s doing very well.” Yale said with surprise, “I didn’t expect that your little brother was even more muscular than you. Three years ago, he was already a bit taller than me. By now, he should be even taller. Those arms, those muscles. Damn!”

Linley laughed while nodding.

Wharton’s growth was totally within his expectations. After all, every single Dragonblood Warrior in the history of his clan was extremely physically muscular. The weapons they used were the likes of the first Dragonblood Warrior’s warblade ‘Slaughterer’, the second one’s heavy pike, or the third one’s heavy warhammer.

“Linley, your little brother, Wharton, really knows how to conceal himself. In the past, he had been hiding his power the entire time. But after knowing about your affairs, your little brother stopped doing so and began to slowly reveal his strength. A while ago, at the annual tournament for the seventh grade students, he shocked everyone when he defeated a warrior of the eighth rank.” Yale sighed in amazement.

Linley smiled calmly.

A warrior of the eighth rank?

Right now, Wharton was of the seventh rank, and he could also Dragonform. Once Dragonformed, he could reach the ninth rank in power.

“After becoming famous, how has Wharton been doing?” Linley asked.

“Wharton was conferred the rank of Imperial Count. Right now, he’s a rising star in the O’Brien Empire. In a few years, perhaps he will be recruited into the War God’s College.” Yale sighed. “In the future, he has a high chance of entering the Saint-level.”

“War God’s College? Saint-level?” Linley didn’t actually wish for his younger brother to enter the War God’s College.

To the venerable Dragonblood Warriors, entering the Saint-level was something which would happen without fail.

Linley chatted with Yale for an entire morning. Linley was now totally at ease, knowing that all of his bros were living good lives.

After lunch.

“Third Bro. This is a talisman of the Dawson Conglomerate. It represents your status as an elder. Take it.” Yale withdrew a black talisman.

Linley was a bit shocked. “An elder?”

When Linley was at the city of Fenlai, he had already displayed the power of an early-stage warrior of the ninth rank. At that time, Linley was only seventeen. Given his natural ability as a magus, and as well as the fact that he could transform into a Dragonblood Warrior, the elders of the Dawson Conglomerate had come to the conclusion that he would sooner or later enter the Saint-level.

Since that was the case, allowing Linley to become an ‘elder’ of the Dawson Conglomerate was definitely a worthy investment.

“Just take it, on account of us being bros.” Yale laughed.

Linley glanced at Yale. He understood that by accepting this talisman, it signified that if in the future, the Dawson Conglomerate ran into any difficulties, he would have to help out. After all, this talisman represented both power and responsibilities.

“Alright. I’ll accept it.” Laughing, Linley took the talisman. Even if he didn’t have this talisman, if the Dawson Conglomerate really ran into any difficulties, for the sake of his dear bro Yale, Linley of course couldn’t just stand by and watch.

“Thanks.”

The two bros were very close. Thus, there were many words that did not need to be said.

“Third Bro. I feel as though your aura, compared to three years ago, is much more restrained. Over the course of these three years, what level of power have you reached?” Yale lowered his voice and whispered the question with curiosity.

Linley didn’t hide the truth. “Beneath the Saint-level, I should be invincible.”

Yale stared with slight amazement at Linley.

“Enough for now, I have to get back. I’ll come visit you in a few days.” Linley laughed.

The North Sea Administrative Province. Within an ordinary little city.

Within a quiet, secluded courtyard.

“Lord Stehle.” A powerfully built warrior called out softly from outside a door. “It’s time for us to move.”

A moment later, with a creak, the door swung open. Stehle swept the man with his cold stare. “Then let’s move.”

“Yes.” The man didn’t even dare to breathe loudly.

Stehle left the courtyard. Only then did the people nearby let out relieved sighs. A glance from a peak-stage Saint-level combatant was enough to make a man’s heart quail.

“Quickly.” The man immediately urged.

The other men, escorting those five hugely brawny warriors, began to move as well. Those five huge warriors were 2.2 meters tall, and astonishingly muscular. Only, they were tightly bound by a dark golden rope. No matter how powerful they were, they couldn’t break free from these bounds.

Their mouths had been sealed as well.

“Mumble, mumble.”

The five siblings angrily tried to curse.

“Do you want to die?” One of the black-robed guards landed a vicious whip-blow on the body of one of the five siblings, but only left behind a faint white mark. “F\*ck, their bodies are incredibly durable.”

While Stehle’s group was busy traversing one city after another in the North Sea Administrative Province, Linley was entrusting Rebecca and Leena into the care of the Dawson Conglomerate’s forces in Basil. And then, Linley, Bebe, and Haeru set out in the direction of the prefectural city of Deco.

“This troop escorting these prisoners only have two warriors of the ninth rank. This will be easy.” Travelling on Haeru’s back, Zassler laughed. “I wonder who this squad is escorting.”

“Zassler, the news of Perry’s death should have reached the general supervisor of the Church’s affairs in the O’Brien Empire by now, right?” Linley suddenly said.

“Yes, he should know by now.” Zassler said. “However, they definitely wouldn’t be aware that I can Soulscour.”
