#### Book 8, The Ten Thousand Kilometer Journey – Chapter 36, True Experts

This sword attack by Stehle, in terms of level, had surpassed that of the ‘impose’ level. If the ‘Profound Truths of the Earth’ of the adamantine heavy sword was one sort of special attack, then this attack by Stehle could be summarized using a single word: Fast!

“I’m going to die?” Linley was filled with resentment and an unwillingness to die. He wanted to live. He hadn’t yet attained his goals.

But unfortunately, in this world, many people died at times and places not of their choosing. After all, the world didn’t revolve around any person. Many events would not cater to their desires.

“Boss.”

Bebe’s tears had already begun to flow.

But suddenly, Bebe was stunned.

Not just Bebe. Haeru, Zassler, the Black Knight Captain, the five Barker brothers, and even the far away group of onlookers were all stunned.

“What’s going on?” Everyone was flabbergasted.

Linley was standing on the ground right now, while Stehle was stabbing down towards Linley from the sky. His sword was very, very close to Linley’s forehead.

But the two of them didn’t move; they were frozen in position.

Even the drop of blood dripping down from Linley’s injured right hand had frozen in mid-air.

At this moment, it was as though the entirety of spacetime around Linley and Stehle had suddenly frozen. Objects, bodies…everything was paralyzed.

Not just them. Bebe, Haeru, Zassler, the five Barker siblings. All of them were frozen.

Silence!

A gloomy feeling. A terrible sense of loneliness and quiet.

A look of astonishment was in Stehle’s eyes.

“Master Linley. Long time no see.”

A gentle, playful voice rang out. A seemingly thirty-something year old man with long black hair, dressed in a loose robe, walked over. He looked the same as he always did; as though he had just woken up.

“Stehle, right? All of you young fellows have reached the peak of the Saint-level. If I still didn’t advance, I really would feel too ashamed to meet anyone.” The lazy man dressed in the loose robe waved his hand. As though struck by a mountain, Stehle was sent flying backwards as though he were a meteor.

“Bam!” “Bam!” “Bam!” “Bam!” ……

Stehle’s body slammed through over ten stone walls before finally hitting the ground.

“Linley, I haven’t seen you in around three years, yes?” The indolent man beamed at Linley. At this moment, Linley suddenly felt as though he could move again. Bebe, Haeru, Zassler, and the five Barker brothers all regained their movement ability as well.

That terrifying suppressive aura had vanished.

“Lord Cesar.” Linley immediately paid his grateful respects. Linley felt more gratitude towards Cesar than he ever had before. Just now, he had truly felt it was totally hopeless. The man had just saved his life. How could he not be grateful?

The person who had come was indeed Cesar. The King of Killers.

Zassler and the others all stared in astonishment, their mouths hanging open. What they had seen just then was simply too bizarre. And, faced with this man, Stehle was totally unable to resist at all.

The sound of stones rumbling could be heard. Stehle climbed to his feet. Although his face was covered in dust and dirt, he still walked over, staring with disbelief at Cesar.

“You…you…this….this…” Stehle was in total shock.

“This what? Haha, tell me. This what?” Cesar grinned evilly at Stehle.

Stehle had totally lost the demeanor and poise of an expert, only staring in Cesar in utter astonishment. He stammered, “God…God….Godrealm?!”

“Godrealm?”

Linley and Zassler were both astonished as well.

No wonder Stehle had been so astonished. Just now, when everything had suddenly been frozen in place, was the legendary power of a “Godrealm”. Only a Deity-level could utilize this power.

Right now, the Yulan continent had four supreme experts – War God O’Brien, the High Priest, and the Kings of the Forest of Darkness and the Mountain Range of Magical Beasts.

But now…this King of Killers, Cesar, had his own Godrealm?

“Haha…” Cesar laughed.

“Lord Cesar.” Linley and the others stared at Cesar in astonishment.

Cesar beamed as he stroked his mustache. “Don’t be surprised. Stehle, you and the others have been too arrogant. Hell, your old man, Cesar, reached the peak of the Saint-level over five thousand years ago, and my speed of training was far faster than yours. I’m a genius, you know.”

Cesar spat out a bit of saliva, harrumphing as he continued. “But your old man was stuck at the peak of the Saint-level for over five thousand years. If I still couldn’t find a way to break through, I really would feel ashamed. Thus, two years ago, I finally broke through that tiny little barrier.”

Stehle, Linley, Zassler, and the others all remained silent.

Good heavens.

Just like that, another Deity-level combatant had been born.

Linley found it understandable, actually. According to what Grandpa Doehring had said, Cesar was a person from Doehring Cowart’s era, and even back then, he was a Saint-level expert. To break through after five thousand years and finally reach the Deity-level wasn’t exactly something which happened out of nowhere.

“Lord Cesar.”

Stehle bowed respectfully.

Any Deity-level combatant was worthy of respect. Upon reaching the Deity-level, one could ignore the existence of even empires. They were the true, highest powers of the land. It wouldn’t be too hard for a Deity-level combatant to wipe out the entire Holy Union, at most risking some serious wounds.

“What is it?” Cesar looked at Stehle.

Stehle said respectfully, “Lord Cesar, all these years, the relationship between the Radiant Church and you, Lord Cesar, has been quite excellent. I wonder if Lord Cesar would be willing to join us in the Radiant Church. As long as you are willing, Lord Cesar, I believe His Holiness, the Holy Emperor, would be willing to accede to any request.”

This was a Deity-level combatant.

Most likely Heidens would even be willing to resign the Holy Emperorship to him. After all, with a Deity-level combatant overseeing the Church, the status of the Radiant Church in the Yulan continent would be totally different.

“Not interested.” Cesar snorted. “Hell, over these years, your old man hasn’t even been willing to manage the affairs of my own ‘Sabre’ organization. And you want me to work on your behalf?”

Stehle let out two awkward laughs.

Right now, most likely Cesar could stand in front of the Holy Emperor, wag his finger in the man’s nose, then curse at him, and the Holy Emperor wouldn’t dare make a sound. This was the prestige of a Deity-level combatant.

“Lord Cesar, if you are unwilling, we won’t force it. But as for this Linley…he’s killed many people of our Radiant Church. Lord Cesar, would you be willing…”

“Bullshit.”

Cesar kicked Stehle in the stomach, but clearly, Cesar didn’t use any force with the kick. “Linley is a master sculptor on the same level as master Proulx and the others. I don’t have many hobbies. One is beautiful women, the other is sculptures. You want to kill Master Linley in front of me? In your dreams.”

Stehle no longer dared to say anything.

Stehle was extremely frustrated, because this mission of his had been to escort these five siblings back to the Radiant Church. Those five siblings all had bodies that were of the eighth rank in muscle power alone. Once the Angels descended into them, they would transform into five peak-stage Saint-level combatants.

“Lord Cesar, that’s fine. The Church will of course give you face, Lord Cesar.” Stehle squeezed out a smile. “However, those five over there are people which our Church absolutely must have. No matter what the cost, we must take them back with us. I hope, Lord Cesar, you will agree.”

“Oh, those five? Take them. I don’t know them anyhow.” Cesar said casually.

The Radiant Church had indeed treated him quite well over the years. Thus, Cesar would give the Radiant Church face as well.

The five Barker brothers were astonished.

“Lord Cesar!” Linley said frantically.

“Linley, do those five people have some sort of very important relationship with you?” Cesar twisted his lips. “Doesn’t seem to be the case. Don’t bother with them, then. Just enjoy your own life. Why bother about theirs?”

This was Cesar’s temperament. He travelled alone, and acted as he pleased.

“Thank you, Lord Cesar.” Stehle was overjoyed.

Cesar beamed at him, then turned to look at the five Barker brothers. “Let me take a look and see who you are, that the Radiant Church would value you so highly.” Cesar swept the five Barker brothers with his gaze.

The five Barker brothers were indeed very eye-catching. Those 2.2 meter tall bodies and terrifyingly muscular forms. All of them looked like enormous bears.

“The five of you had best not resist.” Stehle walked over. Zassler and Linley wanted to stop him, but under Stehle’s cold gaze, Zassler and Linley could only laugh bitterly inside.

How could they stop a peak-stage Saint-level combatant?

Linley had just used both the baleful aura of the Bloodviolet sword as well as the most powerful attacks of the adamantine heavy sword. Despite that, he had only given the opponent the most superficial of injuries.

“Linley, no matter what, we five brothers would like to thank you.” Barker, the oldest of the five brothers, said loudly.

“These five fellows are pretty large, aren’t they.” Cesar’s playful voice rang out.

Stehle immediately responded, “Yes, they are quite muscular.”

Cesar looked at the five men. His expression, originally playful, suddenly slowly sank into a brooding look. He even began to slowly walk towards the Barker brothers, one step at a time.

“Why are you coming over?” The third of the five brothers, Hazer [Hei’sha], growled.

“Third bro, don’t be rude!” Barker growled back.

“Big bro.” The muscular man said unhappily.

Cesar quietly stared at the five siblings. By his side, Stehle was beginning to grow surprised. In a low voice, he asked, “Lord Cesar, what are you doing?”

“Stehle, you can leave now.” Cesar said calmly.

“Then Lord Cesar, I bid you farewell.” Stehle said respectfully. Then he immediately shouted towards the Barker siblings. “The five of you, walk in front.”

“I said you can leave now. The five of them will remain behind.” Cesar said in a cold voice.

Stehle was startled.

Behind them, Linley and Zassler were both stunned as well. Even the five Barker brothers were shocked by these words.

“Lord Cesar, you…?” Stehle stared at Cesar in astonishment. Just moments ago, Cesar had agreed to let him take the five of them away. But in the blink of an eye, things had changed.

Cesar’s expression was colder and grimmer than it had ever been. He stared coldly at Stehle. “Stehle. Listen clearly. Go back and tell Heidens this. If in the future, the Church’s men make any attempts on these five brothers, then don’t blame me, Cesar, for not giving you face when I slaughter my way to your Sacred Isle.”

Hearing these words, Stehle was totally shocked.

“If you leave now, I’ll pretend nothing happened today. Otherwise…” Cesar’s eyes glittered with a cold light, and a terrifying murderous aura began to emanate from him.

Cesar was the King of Killers to begin with. He specialized in assassination.

And now, Cesar was a Deity-level combatant.

Once Cesar made the decision to go against the Radiant Church, just by engaging in assassinations, he could probably kill all the Saint-level combatants of the Radiant Church without suffering a single injury.

No matter what, the Church could not afford to offend a Deity-level combatant, much less a Deity-level combatant who specialized in assassinations.

“Alright.” Stehle’s heart was filled with bitterness.

It was also filled with rage. Rage at how overbearing and domineering Cesar was being. But Stehle knew that the person in front of him was a Deity-level combatant. He was qualified to be overbearing and domineering. He didn’t dare to show his anger or to retaliate.

“Then Lord Cesar, I bid you farewell.” Stehle bowed slightly, and then transformed into a blur, disappearing from the scene.

Linley, Zassler, and the five Barker brothers stared at Cesar in puzzlement.

“In the past, Cesar was always so lazy and lackadaisical. So why did he grow so solemn upon seeing the five Barker brothers?” Linley was extremely puzzled as well.

Cesar glanced at Linley and his group. “Come with me and leave this place. There are quite a few onlookers here. And…I expect Saint-level combatants have already detected the powerful ripples generated by this battle.”

There actually were no Saint-level combatants in the prefectural city of Deco.

The closest Saint-level combatant was over a thousand kilometers away. Even Saint-level combatants would take quite a while to travel that sort of distance when flying.

Linley and the others immediately followed Cesar away from the battlefield. That very night, they left the prefectural city of Deco and entered the mountain wilderness. Only then did Cesar have everyone come to a rest stop.

“We’ll spend the night here for now.” Cesar sighed.

Right now, Cesar didn’t seem as carefree and unrestrained as he usually was. On the contrary, he seemed rather heartsick. Linley had the feeling that Cesar must have some sort of connection to those five siblings.
