#### Book 8, The Ten Thousand Kilometer Journey – Chapter 8, Leaving the Mountains

Virtually all magi knew how to set up a soul-binding magic array. But in terms of actually setting one up, there were certain requirements. Generally speaking, only upon reaching the seventh rank as a magus did one have sufficient spiritual strength to set it up.

A nearly translucent pentagram was floating in mid-air.

And then, the pentagram magic formation flew towards the head of the Blackcloud Panther, who didn’t resist at all, allowing the magical formation to enter his mind. Suddenly, both Linley and the Blackcloud Panther could feel that their spirits were now interconnected.

This was not the same as the ‘bond of equals’ which Linley and Bebe shared.

In the ‘bond of equals’ between Linley and Bebe, both of their souls had become intermingled. With this soul-binding magic array, however, was formed solely from Linley’s spiritual energy. When the Blackcloud Panther accepted the soul-binding compact, naturally Linley was the master.

“Master.” The Blackcloud Panther was extremely respectful.

Linley looked at the Blackcloud Panther. “What is your name?” Linley knew that some high-class magical beasts had names of their own. For example, that Armored Razorback Wyrm which Linley had encountered in the Mountain Range of Magical Beasts’ Foggy Valley had been named Sartius.

The Blackcloud Panther’s voice sounded in Linley’s mind. “Master, my name is Haeru [Hei’lu].”

“Haeru?” Linley memorized the name.

“Haeru, tell me about your transformation abilities.” Towards this topic, Linley felt quite a bit of interest.

The Blackcloud Panther nodded. “Master, it is because I am a dual-element magical beast of both darkness and wind elements. In my brain, I have two magicite cores; one is darkness element, the other is wind element. Normally, I am in my first form, where my defense, offense, and speed are all equal.”

“When I rely primarily on the energy from my darkness magicite core, my body will increase in size and my attack power will go up, at the expense of speed. When I rely primarily on the energy from my wind magicite core, I will be in this form, the wind-style form, with great speed but weaker defense.”

The Blackcloud Panther, Haeru, said honestly.

Linley now understood.

So Blackcloud Panthers were dual-element magical beasts of wind and darkness, and this current form was the wind-style form. The giant form was the darkness-style form, and only the original form was the ‘normal’ form.

“I had originally thought that Haeru’s current form was his normal form.” Linley snickered to himself.

Linley suspected that the person who had written the records which Linley had read regarding Blackcloud Panthers had only seen this wind-style form, and thus mistook this as the only form of the Blackcloud Panthers.

“Growl!” Bebe ran over, growling in a low voice towards the Blackcloud Panther.

The Blackcloud Panther began to chat with him as well.

“Looks like our journey will be more interesting in the future.” A hint of a smile was on Linley’s face.

…..

Alongside his two magical beast companions, Bebe and Haeru, Linley continued his daily training regime. Linley immersed himself in the world of sword-training. Every so often, Linley would have some new insights regarding how to use his heavy sword.

Spring left, autumn came.

In the blink of an eye, another year had passed.

That fall in the second year, the temperature had dropped to a murderous low. Linley was seated cross-legged beneath an ancient oak, training. The Dragonblood battle-qi had suddenly begun to boil, causing his blood vessels and his heart to once again begin to change and transform.

In addition, within Linley’s dantian, the Dragonblood battle-qi had finally begun to change as well. Excited, Linley let out a laugh. He had finally broken past the late-stage of the seventh rank and reached the eighth rank. He had become a warrior of the eighth rank!

As a warrior of the eighth rank, upon totally Dragonforming, Linley’s power was now at the peak-stage of the ninth rank.

There was a significant difference between a peak-stage ninth rank warrior and an early-stage ninth rank warrior.

“When I was in Hess City, it was hard for me to even break past the armor of an ordinary magical beast of the ninth rank. But now, even without using the adamantine heavy sword, I can kill most magical beasts of the ninth rank.” Linley was extremely confident.

A peak-stage Dragonblood Warrior of the ninth rank could definitely vanquish a peak-stage magical beast of the ninth rank.

Aside from Saint-levels, perhaps there was nobody in the world who could threaten him anymore.

“Only, the higher level of using this adamantine heavy sword, this so-called ‘impose’ level…what is it?” Linley began to frown. Right now, Linley had completely mastered the technique of ‘wielding something heavy as though it were light’.

He walked barefooted on the ground.

Linley continued on his path of training, constantly harmonizing himself with the pulsing thrum of the earth and the indistinct ebbs and flows of the wind. In turn, Linley’s spirit became purified by nature, becoming more agile and graceful.

….

Winter arrived.

In the morning, a great blizzard had descended, covering the entire world with blankets of falling snow. Standing in the middle of the snowstorm, Linley stared up at the snowflakes falling from the sky. His heart was very peaceful.

Suddenly, Linley sat down cross-legged, placing the adamantine heavy sword across his lap. His upper body was still bare, and he still wore that ragged pair of hempcloth pants.

The snow settled on top of Linley’s body, but Linley didn’t notice it at all.

Time passed. The snow continued to fall from morning until nightfall, covering the entire area with a layer of snow as thick as one’s foot.

Bebe and Haeru had hidden themselves underneath a large pine tree, where they watched Linley.

“Impose.”

Linley’s eyes opened. Within them, there was a hint of a smile. Raising his head to stare in front of him, he saw that the snow had ceased to fall. Although it was almost dark, the entire world had been painted a light white color by the snow.

“Grooooowl!” From far away, the roar of a magical beast could be heard.

A Glacial Snow Lion was striding on the snow. Apparently having discovered Linley, it began to draw close to Linley, one step at a time. Watching the Glacial Snow Lion draw near, Linley didn’t seem to react at all.

“Swoosh!” With a mighty leap, the Glacial Snow Lion pounced towards Linley.

Linley watched as the Glacial Snow Lion pounced towards him. Very casually, he grabbed the adamantine heavy sword that had been lying in his lap and chopped directly towards the Glacial Snow Lion.

“Rumble!” The moment Linley swung the adamantine heavy sword, space itself seemed to suddenly be compressed in the area around the sword, in the direction of the Glacial Snow Lion.

Terrified, the Glacial Snow Lion wished to flee, but the entire area around it was compressed by that pressuring force. It had nowhere to run.

Facing this heavy sword, it had no choice but to take it head on.

“Bam!”

The heavy sword slammed against the Glacial Snow Lion’s body. The Glacial Snow Lion’s entire body trembled momentarily, then suddenly disintegrated into a pile of flesh and blood.

“So ‘impose’ refers to ‘imposing’ one’s will on the heavens and the earth, to the point where even space itself can become used to constrict someone. Haha…” Linley laughed.

After having experienced that huge blizzard, Linley finally entered the third level of wielding heavy weapons; the ‘impose’ level. Only, Linley understood that he had just barely begun to grasp this level.

“To be able to so quickly grasp the ‘impose’ level, I really must give thanks to my training as a stonesculptor as well as my insights as a magus.” Linley felt very happy.

Because he was a magus, Linley’s soul could more clearly sense the throbbing pulse of the earth as well as the flows of the wind. His soul was capable now of becoming one with nature. In addition, this entire time, Linley had been extremely focused on his training and had accumulated a great deal of experience. This allowed Linley to finally surpass that initial barrier and enter the ‘impose’ level of wielding the heavy sword.

In terms of power, the ‘impose’ level was far more terrifying than the level of ‘wielding something heavy as though it were light’. It was also far more profound and mysterious.

….

Spring. Year 10003 of the Yulan calendar. The northernmost edge of the Mountain Range of Magical Beasts was only a few kilometers away from the North Sea. In fact, from the northernmost point in the Mountain Range of Magical Beasts, one could see the vast, endless expanse of water known as the North Sea.

Between the Mountain Range of Magical Beasts and the North Sea, there was a corridor which linked the Holy Union and the O’Brien Empire together. Almost every day, large numbers of people passed through this wide road.

Virtually everyone, be it the merchants of the O’Brien Empire, the merchants of the Holy Union, or others, passed through this corridor.

However, the citizens of the O’Brien Empire, when facing the citizens of the Holy Union, felt a sense of natural superiority. This was because the O’Brien Empire was the most powerful Empire in the entire Yulan continent. What’s more, it possessed the ‘War God’. In the war-loving O’Brien Empire, virtually every citizen was proud to belong to the O’Brien Empire.

Right now, on the wide corridor, there was a merchant caravan with hundreds of people that were camping and resting. Many people were currently eating.

“Old Hett [Hei’te].”

A young man riding on a carriage chuckled at a chubby man next to him. “You’ve made a fortune on this latest deal.”

“Haha.” That middle-aged fatty laughed contentedly. “Petrie [Pi’te’li], you are a smart young fellow. If you continue to work for me, in three years time, you’ll be able to buy a manor in your hometown, then buy a few beautiful serving maids and hire a few manservants. You’ll be able to live a happy life as an estate owner.”

“Three years? Shit, in three more years I probably will have lost my life.” The youngster swore. “A new person like myself is always assigned the most dangerous tasks. Alas…in one year, I’ll go back home, buy a beautiful girl, and enjoy life. Estate owner? That’ll depend on whether or not I have that good fortune.”

The middle-aged fatty began to laugh. “You are a newcomer. Of course it falls on you to take on the most dangerous tasks. However, that means you get a large share as well. Oh, right. Petrie, this time in our caravan, there’s a very beautiful girl. As we are headed the same way, we are escorting her.”

“Are you talking about Miss Jenne [Zhan’en]?” The young fellow’s eyes instantly lit up. “If I had a woman like that, I’d be willing to work for ten more years. That figure. That aura. Oh, man…”

“But she clearly is a noble, and that old servant of hers isn’t weak either.” The middle aged fatty chortled.

“Can’t I at least fantasize?” The youngster said unhappily.

The middle aged fatty began to laugh, but then he suddenly looked towards the south. “Hrm? Petrie, look. Someone is coming out from within the Mountain Range of Magical Beasts.” Petrie immediately looked south towards the Mountain Range of Magical Beasts.

Dressed in an ordinary blue warrior’s uniform, a man carrying a heavy sword on his back was walking out from within the Mountain Range of Magical Beasts. His long brown hair just barely reached his shoulders. By the looks of it, he was nearly two meters tall.

By his side was a black panther that was nearly as tall as he was, and on the back of that black panther was a black Shadowmouse.

“What is that black panther?” Petrie said in astonishment.

Staring with wide eyes, the middle-aged man said, “Don’t cause a ruckus! I’ve heard that all panther-type and lion-type magical beasts are very powerful. Generally speaking, they are at least magical beasts of the sixth rank, or even higher.”

Immediately, Petrie no longer dared to make a sound.

Right at this moment, the brown-haired man began jogging towards their caravan with long strides. The caravan guards immediately became alert. The person coming towards them was clearly a powerful warrior.

…….

Right now, Linley was in an excellent mood. After three full years of hard training, he had finally left the Mountain Range of Magical Beasts.

“The Northern Sea is indeed vast.” This was the first time Linley had seen the North Sea, and the sight of that enormous, boundless sky-blue sea stunned Linley, filling him with awe.

Seeing the resting caravan in front of him, Linley jogged in that direction.

“Hey, friend, what do you want?” A heavily bearded guard shouted out loudly. Smiling, Linley replied, “I’m headed for the O’Brien Empire. I hope you can take me along with you.”

The heavily bearded guard looked at Linley, then turned towards a middle-aged, golden-haired man next to him. After exchanging a few words, he said to Linley in a loud voice, “That’s easy. Twenty gold coins, and we’ll take you with us.”

“Fine.” Linley agreed very readily. He immediately pulled out a small sack of gold, counted out twenty gold coins, and handed it over.

This outfit Linley was currently wearing had been stored in the interspatial ring, ready for just an occasion such as this. In his interspatial ring, Linley naturally had prepared quite a few things.

“Hey, friend, since you already have a mount, do you plan to ride in a carriage, or on this panther?” The heavily bearded man asked warmly.

“In a carriage, I suppose.” Linley said.

“Fine. You can go get inside that cart in the back. That one right there, the flat cart with two people in it.” The heavily bearded man pointed as he spoke. Actual covered carriages were rather expensive, and in this caravan, the majority of the soldiers all rode in flat carts.

“Sure.” Linley agreed quite casually.

As he walked over to and reached that flat cart, the two men already in the cart, previously engaged in conversation, were immediately terrified by Haeru, who was walking alongside Linley. Panther-type magical beasts were generally high class magical beasts, after all.

“Ah, friend, please, sit.” The two men were incredibly friendly.

Linley entered the cart. The cart had mattresses made of hay inside, which were covered by a thick cotton cloth. As Linley sat on top of a hay mattress, Bebe jumped right onto Linley’s shoulders as well.

“Come, friend, have some wine.” The slightly older one of the two men warmly offered.

“Thanks.” Linley accepted the wineskin and took a large gulp.

“Hey, everybody, get ready. We’re about to start moving again!” A loud voice rang out, and all the people who had got off their carriages for a rest immediately got back into their carriages.

The caravan began to move forward again, embarking once more on its journey towards the O’Brien Empire…
