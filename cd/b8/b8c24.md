#### Book 8, The Ten Thousand Kilometer Journey – Chapter 24, Zassler

The sky slowly grew dark. Linley remained hidden outside the walls of this residence the entire time, but up till now, he still hadn’t found any opportunity or method by which he could stealthily get near that mysterious old man.

“Based on their conversation in the hotel, the Radiant Church seems to have sacrificed several powerful experts for the sake of catching this person.” Linley frowned as he considered the question. “This old man is at least of the ninth rank in power.”

“But he shouldn’t be at the Saint-level yet. Even a large group of powerful experts of the ninth rank could at most force the Saint-level to flee. It definitely is highly unlikely that they would seize him.”

Although Linley wasn’t too sure about exactly how powerful that mysterious old man was, without question, that mysterious old man had the ability to deal with multiple experts of the ninth rank.

“This old man must be very important for the Radiant Church to expend so much effort on catching him. I’ll definitely disrupt their scheme.” Linley’s eyes were radiating a cold light. “But killing these six experts of the ninth rank and preventing a single one of them from escaping Cerre is a difficult task.”

Linley himself was living quite close to Cerre. Naturally, he wouldn’t want his movements and his presence to be exposed.

If he was to act, he would have to kill all six of them.

“Myself, Bebe, Haeru. We are totally capable of dealing with three combatants of the ninth rank. Against six…if we use some tactics, it still isn’t out of the realm of possibility. However, it’s best if we release the old man first and have him ally with us. That will give us an even greater chance of success.”

Linley knew how to deal with antimagic manacles.

The power and value of antimagic manacles lay in the complicated magical rune formations etched onto them. But the materials which the manacles were made out of actually weren’t that durable. Although antimagic manacles prevented the prisoner from using any mageforce and was fairly sturdy, Linley was totally confident in his ability to break them.

Linley wasn’t in a rush. At this time, he mentally commanded Haeru to return to the city from within the mountain valley.

Humans and the magical beast companions they had tamed were spiritually bound. The more powerful the spiritual energy of the two was, the greater the distance the two could exchange mental conversations.

For example, Linley and Bebe could exchange thoughts from a distance of several hundred kilometers. But if they were to become separated from an even farther distance, it would no longer be possible.

As for some weak members of noble clans who used soul-binding scrolls to tame magical beasts of the first, second, or third ranks, they might not be able to communicate past a distance of just a few hundred meters.

The main issue was spiritual energy.

Linley and Haeru, as well, could spiritually communicate from a distance of hundreds of kilometers. But once the distance grew too great, they would only be able to vaguely sense the direction each was in, and could no longer send messages.

Darkness descended. It was approximately 9 o’clock at night now.

Dressed in a black warrior’s outfit, Linley was hiding outside the walls of the residence, alongside the similarly black Shadowmouse, Bebe, as well as the Blackcloud Panther, Haeru. They were quietly waiting for their opportunity.

“Bebe, Haeru, the two of you stay here. Only make your move after I mentally command you two to act.” Linley instructed.

Haeru and Bebe both nodded.

Linley immediately removed his black warrior’s outfit, then allowed black scales to manifest on top of his skin. A black spike jutted forth from his forehead, and spikes jutted out along his entire back spine.

That draconic tail silently pierced through Linley’s long pants.

Linley’s eyes became a cold, merciless dark gold color.

“Remember. Await my order.” Linley once again instructed Bebe and Haeru. And then, like a phantom in the darkness, Linley glided towards the courtyard.

After having mastered the ‘impose’ level, Linley could now move without causing any disturbance to the surrounding air.

The main building had two floors. Beside it were three rooms, the central one clearly being the place where the old man had been locked into. Because outside this room, there were two black-robed men.

Linley crept behind a manmade hill, not moving at all as he quietly awaited his opportunity.

“I refuse to believe you won’t lose your focus for even a second.” Linley was extremely patient.

Right now, the two black robed men were engaging in conversation out of boredom.

“Bro, after completing this mission, the two of us have to have a good, long rest. These past two years have exhausted us. I’ve been nervous this entire time, not daring to loosen up at all.” One of the black haired men said.

“Right. On this mission, two of our Ascetics of the ninth rank died, and three Special Executors of the ninth rank as well. Eleven of us had to work together, aided by poison, and yet five of us still died. This old fellow is such a monster.”

Right now, the two black-robed men were fairly relaxed.

In order to pursue and capture this old fellow, their group had been sent out as soon as the Radiant Church had received news of his whereabouts. They had passed through the O’Brien Empire, traversed the 48 Anarchic Duchies, and entered the great plains of the far east. They had battled against this mysterious old man for months, finally capturing him in one of the Duchies of the Anarchic Lands.

But as long as they had managed to seize this old man, all their sacrifices would have been worth it.

They were very careful on their way back as well. They were afraid that the experts of the O’Brien Empire would discover them. But by now, they were halfway back, and the towns they would pass by in the future were all small ones without many experts. They shouldn’t pose much danger.

Naturally, Lampson and the others now felt slightly more relaxed.

“Bro, I’m going to the bathroom. You stand guard here. I’ll be back in a minute.” One of the black-robed men said.

The other black-robed man laughed. “I was fine before you said anything, but now that you mentioned going to the bathroom, I want to go as well. Fine, you go first, and I’ll go later.” Although they were a bit relaxed, they still didn’t dare to have both guards be gone at the same time.

After all, if they let this old man escape, they would have committed a grave sin.

Hiding behind the manmade hill, when Linley saw the black-robed man leave, he felt a hint of surprised excitement. “Only one left. Killing him isn’t a problem at all. Only…I can’t let him make any noise.”

Linley narrowed his eyes, while beginning to quietly mouth the words to a magical spell. ‘Supersonic’.

…..

At this moment, Xartes was currently standing at his bedroom door, keeping a casual eye on his surroundings. In a mere prefectural city, Xartes, an expert of the ninth rank, still felt quite self-confident.

But suddenly, Xartes saw a black light flash in the corner of his eyes.

“What was that?” Xartes turned his head over to look.

An enormous bluish-black sword had suddenly appeared in his field of vision. The most terrifying thing was, this bluish-black sword seemed to be using all of the surrounding area to apply pressure and force on him, locking him into place!

Space itself had been totally locked!

Xartes wanted to cry out in alarm, but he couldn’t make a sound. In truth, even if he had managed to shout, the sound wouldn’t have managed to leak through that frozen space.

Xartes’ eyes were round and bulging. Suddenly, he slammed his palm, now glowing with radiant battle-qi, in the direction of the sword.

“Bam!”

When the enormous sword struck Xartes’ hand, Xartes felt as though he had suddenly slammed against a boundless, roiling flood. He wasn’t able to suppress it at all.

“Boom.” His hand and his arm disintegrated and liquefied, the bones in them shattering.

And then, not slowing down, the adamantine heavy sword struck Xartes on his chest. Xartes only felt his chest tremble, felt something break, and then…he felt nothing else.

In the blink of an eye. The opponent was killed.

He didn’t have a chance. After Dragonforming, Linley was a peak-stage combatant of the ninth rank, and had the adamantine heavy sword for his weapon. At the same time, he had reached the realm of understanding and mastering the power of ‘impose’. The two were on totally different levels.

“Hurry.” Linley gently pushed the door open. As he did, he immediately saw that skinny old man with long white hair and the long white eyebrows, seated cross-legged on the floor. Hearing Linley enter, the old man casually opened his eyes while saying, “Why have you come…”

But upon seeing Linley, the old man’s words immediately came to a halt.

Seeing Linley in full Dragonform, the old man stared at Linley. Lowering his voice, he said, “What plane of existence do you come from, Draconian?”

“Draconian?” Linley was startled.

Could it be that in other planes, there was a race called Draconians that looked similar to him?

“Why have you come here?” The old man said again in that quiet voice.

“To save you.”

Linley was wielding his adamantine heavy sword. “Hold your arms out straight. I will break your antimagic manacles.”

Although the old man was suspicious as to who Linley was, he still very obediently held his arms out. Staring at the pitch-black antimagic manacles, Linley chopped directly down with his adamantine heavy sword.

‘Wielding Something Heavy as Though it Were Light’ – Thunderbolt!

The adamantine heavy sword drifted down, as slowly and gracefully as a leaf, barely brushing against the center of the antimagic manacles. As it did, with a ‘crack’ sound, multiple cracks appeared in the antimagic manacles, and pieces of it even went flying to the edges of the room.

The old man only had to casually shake his hands, and the two halves of the already-destroyed manacles went flying in opposite directions.

“I didn’t ask you to save me, so I owe you nothing.” The emaciated, pale-faced old man stood there, staring at Linley coldly.

Linley glanced at him, but Linley’s dark gold pupils seemed to stir no fear in this old man at all.

“Do you have enmity with the Radiant Church?” Linley said quietly.

Both of them were speaking extremely quietly, and Lampson’s group in the two story building couldn’t hear their conversation at all.

“Enmity? I won’t stop until one of us is destroyed.” The old man said boldly.

“That’s all I need.” Linley said calmly. “Although I don’t know who you are, I must tell you…tonight, none of the Radiant Church’s men can be allowed to leave here alive. I don’t want to reveal myself to them.”

“Reveal yourself?” The old man was curious. “Which plane of existence do you come from, Draconian? Could it be that you are a Draconian from one of the Four Higher Planes? The Infernal Realm?”

Linley glanced at him. “No.”

The old man began to laugh evilly. “Then let me tell you who I am, first. My name is Zassler [Sai’si’le]. I am an Arch Magus, a necromancer of the ninth rank. Yourself?”

Linley was truly shocked.

As a magus, Linley knew very well that there were three types of magic which surpassed earth, fire, wind, water, lightning, light, and darkness style magic. Doehring Cowart had discussed this with him before as well.

These three forms of magic were the Oracular Magic which the Radiant Church was adept at, the Life Magic which was used by the legendary High Priest of the Yulan Empire, and the extremely rare Necromantic Magic.

All three of these types of magic were extremely rare in the Yulan continent.

When Linley realized that Holmer was ambushing him, because Holmer had used poison gas, Linley had asked him if he was a necromancer. If he had been…Linley probably wouldn’t have been able to bear killing him.

After all!

The Four Higher Planes had been created by the Four Overgods. These Overgods were, respectively, the Overgod of Fate, the Overgod of Life, the Overgod of Death, and the Overgod of Destruction.

The Overgod of Fate had passed down Oracular Magic.

The Overgod of Life had passed down Life Magic.

The Overgod of Death had passed down Necromantic Magic.

These three branches of magic were astonishingly powerful, precisely because they originally stemmed from the Four Overgods. As for the Overgod of Destruction, he hadn’t passed down any magic at all. The followers of the Overgod of Destruction held their own power and abilities in prime reverence.

For example, the War God O’Brien was a follower of the Overgod of Destruction.

“An Arch Magus necromancer?” Shock appeared on Linley’s face.

“And you?” The Arch Magus necromancer, Zassler, stared at Linley.

“Why should I tell you about myself? I didn’t ask you to tell me about yourself.” Linley said calmly. The Arch Magus necromancer was instantly stunned, not knowing what to say.

Right at this time, the black-robed man came back from the restroom.

“Bro, where the hell did you go?” Seeing that there was no one outside, the black robed man’s face immediately changed, as he shouted loudly in anger.

Their task of watching over this Arch Magus necromancer was an extremely critical one. How could he not be furious when he saw that his brother had just disappeared without a word?
