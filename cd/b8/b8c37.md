#### Book 8, The Ten Thousand Kilometer Journey – Chapter 37, The Undying Warriors

“Crackle.”

The bonfire was blazing. Cesar, Linley, Zassler, and the five Barker brothers sat around the campfire. Bebe was resting on Linley’s thigh, while Haeru was lying behind Linley.

Camping overnight in the wilderness was fairly dangerous. But who or what could possibly threaten Linley’s group? Especially with that Deity-level expert amongst them.

“Why did you save us?” The eldest of the five brothers said in a loud voice.

Linley and Zassler all turned to look at Cesar. This was a question they were curious about as well.

Cesar glanced at the five of them. He didn’t respond, instead asking them a question of his own. “Your father? Your mother?”

“All our relatives are dead. As for our parents? We were orphans since we were young.” Barker replied. They were now in their thirties. To them, who had spent their entire lives in the war-torn lands of the Eighteen Northern Duchies, growing up without parents wasn’t anything particular special.

After all, in those war-torn lands of the Eighteen Northern Duchies, orphans were a common sight.

“Orphans…”

Cesar let out a long sigh. “I didn’t expect that after all these years, the ‘Armand clan’ [A’man’da] whose fame shook the Yulan continent would fall to such a state.”

The five Barker brothers, Linley, and Zassler all started.

“Lord Cesar, are you saying that the Barker brothers are…” Linley had a guess as to what Cesar was saying.

Cesar nodded. “Right. These five siblings belong to the Armand clan, the clan of the Undying Warriors, one of the Four Supreme Warrior clans of the Yulan continent.”

“Undying Warriors?” Barker and his siblings all stared at each other in shock.

“How is that possible?”

The five brothers rose to their feet, stunned. They were orphans since youth. How could they dare imagine that they belonged to one of the Supreme Warrior clans?

Linley had already guessed the truth as soon as he heard Cesar say the words, ‘Armand clan’. After all, Linley’s own clan records included information on each of the Four Supreme Warrior clans; the Dragonblood Warriors’ Baruch clan, the Violetflame Warriors’ Hyde clan, the Tigerstriped Warriors’ Prey [Bo’lei] clan, and the Undying Warriors’ Armand clan.”

Five thousand years ago, these four clans indeed were extremely famous.

Aside from the War God and the High Priest, without question, the Four Supreme Warriors stood at the absolute pinnacle of human power. Although there were other so-called peak-stage Saint-level combatants, those peak-stage Saint-level combatants couldn’t match the Supreme Warriors.

Power and insight; these were two mutually supporting, mutually complementing things.

For example, right now, Linley’s level of understanding was very high; he had surpassed the ‘impose’ level, and was nearing the peak-stage Saint-level in terms of understanding. But his actual power was extremely weak. Naturally, his attack force was far weaker than that of a Saint-level combatant.

Historically, the third Dragonblood Warrior had used a heavy warhammer.

When he had reached the Saint-level, he was only at the level of ‘wielding something heavy as though it were light’. But despite that, he still possessed astonishing attack force.

This was because his body possessed a terrifyingly high degree of power and battle-qi.

A person’s strength and battle-qi were his most basic foundations. The higher one’s level of understanding, the better one would be able to utilize those basics. For example, if your basics were at 100, but you were at a low level of understanding, your actual attack power might just be 50. But if you had a high level of understanding, you might be able to use all 100 of your attack power, or perhaps even more, reaching 200 attack power.

The Supreme Warriors, by their very nature, possessed several times more physical strength and battle-qi than other Saint-level combatants. Even if they were a bit inferior in terms of insight and understanding, their attacks would still be very terrifying. This was the natural gift of the Supreme Warriors!

There was nothing that could be done for it. They were able to gain an unfair advantage over others via their natural gifts.

“Your bodies must be extremely tough.” Cesar sighed.

The five Barker brothers glanced at each other, then nodded. The second of the five brothers, Anke [An’ke], nodded and said, “It’s impossible for us to train in battle-qi, but just through our muscle power, we are on the level of warriors of the eighth rank.”

“Aside from the Four Supreme Warriors, how could anyone else possibly break past the natural limitations of the body and reach the eighth rank just based on their body and muscles?” Cesar shook his head and said.

Linley was now certain as well.

Only the Four Supreme Warriors were restricted to using their own special battle-qi cultivation methods and be unable to train normal battle-qi.

“Amongst the Four Supreme Warriors, the Undying Warriors have the toughest bodies. Their defense is very powerful, and their attacks are legendary as well. The only weakness is that you are a bit slow.” Cesar sighed. “Barker, you and your brothers are so young, but you were able to reach the eighth rank just based on your muscles and bodies. Aside from the most physically powerful of the Four Supreme Warriors, the Undying Warriors, who could possibly achieve this?”

Linley nodded as well.

Right.

He himself was a Dragonblood Warrior, but if he were to try to reach the eighth rank based purely on physical training, who knows how long it would take? Even his younger brother Wharton, who trained in accordance with the Secret Dragonblood Manual, had only reached the seventh rank this year at age seventeen.

“Aside from the elders of your clan, I’m afraid there is no one who knows more about you Undying Warriors than myself. You are definitely Undying Warriors. There is no question about this at all.” Cesar said with absolute certainty. “And what’s more, the five of you possess an extremely high degree of natural talent. If you were to train using the ‘Secret Undying Manual’, then most likely you would have already entered the ninth rank by now.”

“The Secret Undying Manual?” Barker and his brothers were confused.

Linley explained, “Barker, the truth is, all of the Four Supreme Warrior clans find other types of battle-qi to be unusable. Only by training in accordance with certain special ways can we develop battle-qi. As for your Undying Warrior clan, you can only train using the ‘Secret Undying Manual’.”

“No wonder we couldn’t train battle-qi no matter what we tried.” The fifth brother, Gates [Gai’ci], said with a sigh.

“If the five of you had been in possession of the ‘Secret Undying Manual’, there’s no way you would have been caught originally.” Cesar sighed. “The Four Supreme Warriors all possess extremely powerful attacks. Amongst them, the Undying Warriors possess the highest defense, the Tigerstripe Warriors are the fastest, the Violetflame Warriors possess the strange Nirvana Rebirth ability, while the Dragonblood Warriors are the most balanced, possessing powerful attack, defense, and speed.”

Cesar was a man of Doehring Cowart’s era.

This was also the era when the Supreme Warriors appeared in the world.

“Lord Cesar, why is it that you treat us so…specially.” Barker said with curiosity.

Hearing these words, Cesar couldn’t help but think back to the past. His expressions grew complex. After a long time, he sighed. “Your ancestor, Armand, was the dearest friend and bro that I, Cesar, have ever had.”

Armand, the first clan leader of the Undying Warrior clan, was also the first Undying Warrior.

“Five thousand years ago, the Yulan continent was in the midst of what was most likely the most chaotic, most dangerous era I have ever seen. The Four Supreme Warriors appeared out of nowhere, while the War God O’Brien became famous after his titanic clash with the High Priest. The Yulan Empire fragmented, as did the Pouant Empire. The entire continent sank into a mass of fire and floods.”

Linley and the others all listened carefully, even though they knew this already.

“And this was just what was going on, on the surface.”

Cesar grinned at Linley. “Actually, that era was much more complicated than you can imagine. The Yulan continent had more than just our native experts. Even powerful combatants from other planes had descended to the Yulan continent.”

“Powerful combatants from other planes?”

Linley, Zassler, and the Barker brothers were all stunned.

“Right.” Cesar chuckled. “To you, these are all distant, far away events, but that era really was chaotic. Many Saint-level combatants lost their lives. In that era, Saint-level combatants were nothing special, because there were many powerful experts who had descended…including many Deity-level combatants.”

“Many Deity-level combatants?!” Linley felt his head grow dizzy.

“Right.”

Cesar nodded. “Actually, five thousand years ago, organizations in some higher planes paid a very high price so as to allow their people to enter the Yulan continent. There was a reason they did this. Linley, you simply don’t know how fierce, how ruthless those battles back then were. At that time, Armand and I joined forces as we roamed the Yulan continent. Several times, I nearly died, but Armand rescued me. But of course…I helped out Armand several times as well.”

Cesar fell silent at this point, as though he were reminiscing about past events between himself and Armand.

Linley was growing puzzled.

The Pouant Empire and the Yulan Empire had fragmented five thousand years ago. The War God had entered the Deity-realm and became famous five thousand years ago. The Four Supreme Warriors had also suddenly appeared out of nowhere five thousand years ago…

And now, according to Cesar, five thousand years ago, even experts from other planes had descended to the Yulan continent.

“Five thousand years ago, something incredibly major must have happened.” Linley thought to himself.

“Enough of that. By the time your power reaches a certain level, even if you don’t want to know, there’ll be someone who will tell you.” Cesar chuckled.

Linley suddenly had the sense that the Yulan continent wasn’t as simple a place as he had thought it to be.

“Actually, there’s no need to force many things in life. Look at me. I eat when I should and play with women when I want to. I’m as carefree as I want to be. How wonderful is that? But look at that O’Brien, and that High Priest. Don’t be mistaken by their fame and glory. In reality, they are under enormous pressure.” Cesar quirked his lips.

Linley, Zassler, the five Barker siblings, Bebe, and Haeru all silently listened.

Listening to Cesar, this Deity-level expert, casually discuss the affairs of the most puissant experts on the Yulan continent, Linley had a very strange feeling.

“Only after reaching the Deity-level will one have the power to move mountains at will.” Linley silently thought to himself.

Cesar glanced at Linley. “Linley, let me give you a word of advice.”

“Lord Cesar, please guide me.” Linley said very modestly, as though he were a student again.

Cesar nodded. “I know there is a very deep enmity between yourself and the Radiant Church. But right now, you are far too weak. Even if you are able to wreck some of the plans of the Radiant Church and give them some small problems, you aren’t able to damage their foundations at all. I recommend that you quietly train for a time first. I don’t ask that you train to an excessively high level. But at least, after transforming, you need to be at the Saint-level. That will be enough.”

Cesar had already realized that Linley possessed a very high level of understanding.

As long as Linley’s level of power were to enter the Saint-level, then, aided by his deep understanding of reality, when faced with peak-stage Saint-level combatants, even if he wasn’t able to win, he would still have the hope of escaping.

“Understood.” Linley nodded.

“Barker.” Cesar looked at the five Barker brothers.

“Lord Cesar.” The five of them were extremely respectful. They now believed that they indeed were the descendants of the Undying Warrior clan. Since the man in front of them was a life-and-death friend of their ancestor, naturally they were very respectful.

Cesar nodded. “All of the Four Supreme Warrior clans have decayed. Armand’s clan has now decayed to the point where even your ancestral training methods have been lost. Fortunately…in the past, during the course of the dozens of years I had spent travelling with Armand, I procured a copy of the ‘Secret Undying Manual’. It should still be within the general headquarters of my Sabre organization.”

Hearing these words, the five siblings’ eyes shone.

They had just watched Linley transform. All of the Four Supreme Warriors had their own transformations. Even pre-transformation, the five of them had the power of warriors of the eighth rank. Once they acquired the secret manual, they would be able to transform…and by then, their power would increase enormously.

“However, there’s a bit of distance from here to the general headquarters. Tomorrow morning, I plan to personally make a trip.” Cesar said.

If those high ranking members of Sabre who had been personally trained by him in the past were to hear these words coming from Cesar, they probably would die from shock.

The ‘Old Master’, Cesar, was legendarily lazy.

There was over ten thousand miles distance from here to the general headquarters. This journey would be an extremely tiring one. For someone of Cesar’s lazy nature to make such a long round trip was quite the feat.

“Thank you, Lord Cesar.” Barker and his brothers said gratefully.

“No need. I hope that in the future, the five of you will restore the Undying Warrior clan’s reputation and fame.” Right now, Cesar was feeling quite emotional. Five thousand years ago, when he had roamed the world with Armand, at risk of dying every single day, was the most unforgettable experience in his very long life.

A night passed. The dawn came.

Nothing was left of the campfire but ashes. Linley and his squad all got up to send Cesar off.

“Lord Cesar, we will immediately return to a small town outside the prefectural city of Basil and settle down. When the time comes, you can just come find us there.” Linley said.

Linley knew that Saint-level combatants could use their spiritual energy to search for people. As for Deity-level combatants, as long as you gave them a general location, it was very easy for them to find someone.

“Got it. Haha. Train hard, kiddos. I’ll head off now.” Cesar had returned to his usual lackadaisical, noisy mood. It was as though after that night had passed, he was back to his old self.

Linley, the Barker siblings, Zassler, and the rest of the group all watched as Cesar’s figure flew through the sky at high speed, disappearing past the horizon.
