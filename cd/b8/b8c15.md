#### Book 8, The Ten Thousand Kilometer Journey – Chapter 15, The Apothecary

After experiencing yet another assassination attempt, Jenne and Keane both truly understood how dangerous this trip to Cerre City would be. They were at risk of dying at any moment. Unconsciously, both of them turned towards Linley.

“Big brother Ley, what should we do in the future?” Jenne looked at Linley as she asked this question, her heart filled with worry.

Right now, both Keane and Jenne felt as though they were lost within a boundless haze, unable to see the future. They didn’t know what would happen if they persevered.

Looking at this pair of innocent siblings, Linley consoled them, “Don’t worry. I’m confident in my ability to deal with an acting city governor of a prefectural city.”

Right now, Linley had reached the eighth rank, and was a peak-stage combatant of the ninth rank when Dragonformed. The Blackcloud Panther, Haeru, was also a peak-stage magical beast of the ninth rank, and Bebe’s power was no lower than that of Linley and Haeru’s either.

If this man and these two magical beasts attacked together, if no Saint-level combatants appeared, no matter how many people came, they would not be able to stop these three.

Hearing Linley’s words, Jenne and Keane couldn’t help but begin to worship Linley.

Although up till now, the two of them still had no idea as to how powerful Linley truly was, in their eyes, Linley was an amazing, mysterious individual. As for Lambert, upon seeing all this, he felt gratified as well. As long as Jenne and Keane could live a safe life, he would be happy even if he had to die. For such an expert to be willing to help these two countryside-raised siblings without quibbling about anything else was more than enough for this old servant to be filled with gratitude.

“Knock!” “Knock!” “Knock!”

A knocking sound could be heard from outside.

“I’ll get it.” Lambert chortled. “It is probably the attendants bringing breakfast.”

“Let’s get ready to eat.” Linley chuckled as he led Jenne and Keane to the living room. Lambert opened the gate to their residence, and two attendants pushing two food-laden trolleys entered.

“Deliver these to the living room.” Lambert chortled as he instructed them.

“Yes, sir.” The two attendants were extremely meek as they each pushed their trolleys inside. But as they moved in, they glanced at each other, a hint of determination in their eyes.

In this assassination attempt, regardless of whether or not they would succeed, they definitely would die.

They knew that Linley, that powerful expert, was still present. Either Linley or his black panther could easily kill them.

….

Within the living room, Linley was seated at the head of the table. Jenne and Keane were seated at the sides. The two attendants smiled meekly as they pushed the carts into the room.

“Sir, miss, where should we place this whole roast sheep?” The attendant opened one of the lids.

“Place them over there.” Linley gestured at the stone floor nearby the table. The Blackcloud Panther, Haeru, was resting next to that table. Smelling the roasted meat, he raised his head.

For Haeru, an entire roast sheep was nothing more than a light breakfast.

“Yes, sir.” The attendant very obediently placed that huge lamb-covered tray onto the floor. Bebe immediately ran over as well. With a swipe of his sharp paws, he ripped off one of the roasted lamb’s legs.

Haeru stared at Bebe, and then he too went over and began to bite off large chunks of the roasted sheep.

“Sir, please enjoy.” The attendant placed a tray in front of Linley, and then put another tray in front of Jenne.

At the same moment, the other attendant was placing a tray in front of Keane.

Currently…

The two attendants were to each side of Keane. Keane wasn’t suspicious at all, and happily picked up his knife and his fork as he prepared to enjoy this sumptuous meal.

The two attendants exchanged glances. As though they were psychically connected, they suddenly reached out at the same time towards Keane. Their four hands were formed into claws, piercing at Keane’s chest, head, and throat.

Four hands attacking at once!

Ordinary warriors of the fifth and sixth ranks could shatter stones with a single blow. Even warriors of the fourth rank could shatter thick wooden planks.

The vital points of a weak child like Keane probably couldn’t withstand a single blow, whether it was at his head, his chest, or his throat.

They were simply too close.

The two attendants were simply too close to Keane, and they attacked from too close as well. At such a close range, even a warrior of the eighth rank wouldn’t be able to react before Keane was already dead.

Linley let out a cold snort.

A dazzling violet light suddenly flashed, then disappeared. Ear-piercing screams could be heard as the four limbs of the two attendants fell to the floor.

“Ah!!” Jenne was so scared that she jumped to her feet.

“Young master!” Only now did Lambert realize what had almost happened. He angrily kicked the two attendants into the walls, causing the walls to shake.

Those two attendants were moaning in pain. They only exchanged glances, despair in their eyes.

“You…how…” One of them stared at Linley disbelievingly.

They had been less than half a meter away from Keane. Although they were only warriors of the fourth rank, at such a close distance, they didn’t even need more than a brief instant to kill Keane.

In such a short period of time, even an expert shouldn’t be able to react fast enough.

But not only did Linley manage to react, he had been able to cut all of their arms off.

“Surprised as to why I was able to react in time?” Linley looked calmly at the two of them. “How would ordinary attendants have arms like yours?”

The two of them looked at their severed arms.

The people under the command of that red-haired man were all elite archers. As elite archers, they would often train, causing the veins and muscles in their arms in particular to be protruding.

The two attendants exchanged glances, their eyes filled with despair.

What’s more, their arm sockets were constantly leaking blood. Very soon, the two of them would definitely die of blood loss. But they knew…having failed their mission, even if Linley spared them, their captain and Madame Wade wouldn’t spare them.

“Don’t pay them any mind. We leave now.” Linley stood up.

Jenne and Keane, having experienced two assassination attempts already, didn’t have as huge a reaction to this third one as they had before. Keane said softly, “Big brother Ley, what about breakfast? Should we wrap it up and take it with us?”

“No.”

Linley shook his head. “Be careful about the food you eat in the future. I suspect all this food is poisoned.”

“Poisoned?” Keane looked at the food in his plate, terrified.

“Squeak!” Off to the side, Bebe suddenly began to squeak at Linley. Looking at Bebe, Linley couldn’t help but begin to laugh.

“Yeah, yeah, you aren’t afraid of poison. Alright?” Linley said resignedly.

Magical beasts and humans were very different, biologically. Many magical beasts contained venomous parts and sacs within their bodies to begin with. The poisons which humans feared, they might not fear at all. The more powerful a magical beast was, the stronger their natural immune system was. In addition, since magical beasts generally resided in pristine, untouched forests, they often interacted with various natural toxins from a young age. Thus, one generation after another, magical beasts’ resistance to poisons would increase.

……

Linley’s group left the hotel very early in the morning. The red-haired man watched Linley’s group depart from afar, his face exceedingly ugly to behold.

“Ley?” The red-haired man muttered. “Where did such a powerful expert come from? And why must he travel with these two countryside-raised siblings?”

The red-haired man was extremely unhappy.

This mission to assassinate Keane and Jenne was originally quite simple. That old servant, Lambert, simply wasn’t powerful enough to do anything. But this originally simple mission suddenly became extremely difficult once that mysterious expert got involved.

“Nothing for it. I have to report to the Madame.” Knowing how powerful Linley was, the red-haired man didn’t dare to take any more risks.

……

As the most militarily powerful empire of the Four Great Empires, the O’Brien Empire had an extremely thorough communication system sustained primarily by a special communications corps who used Bluewind Hawks.

Every single prefectural city in the O’Brien Empire had quite a few Bluewind Hawks who were controlled solely by the communications corps. Bluewind Hawks were extremely intelligent. They recognized roads and, under the orders of their owners, could take a letter to any place at all.

But only the governing clans of the O’Brien Empire had the authority to use these Bluewind Hawks. Most commoners, and even most nobles, didn’t have that authority. And of course, the army had its own stand-alone communications system.

Carrying the seal of the city governor of the prefectural city of Cerre, the red-haired man requested Blackrock City to send a Bluewind Hawk towards the city of Cerre.

….

Flying in a straight line in the air was far faster than running on the road. Not long after Linley’s group had left Blackrock City, the Bluewind Hawk arrived at Cerre.

The prefectural city of Cerre. This was a fairly large city.

In the Northwest Administrative Province, it was one of the top ten cities. At this moment, within the castle that was reserved for the city governor, the mood was very dark and very sinister.

The master of this castle was Madame Wade! An infamously cold, grim, arrogant person.

“Sis, sis!”

Two middle-aged men came running into the rear flower garden. At this moment, Madame Wade was enjoying the radiant sun while being tended to by two serving women.

“What’s wrong, my two dear brothers?” Madame Wade lifted her head up as she looked at the two men.

“Sis, this is the mail that just came by courier. This mission was a failure.” The slightly chubbier of the two men said.

“Failed? How could Kerde [Ke’de] be so useless?” Madame Wade took over the letter. Reading it, she began to scowl, confused. “A mysterious expert who has a black panther as a magical beast companion?”

Per what the red-haired man, Kerde, was saying, that black panther was at least a magical beast of the eighth rank, and that mysterious expert was at least a combatant of the eighth rank, and perhaps even the ninth.

Madame Wade suddenly felt that the letter was extremely heavy.

“Sis, what should we do?” Madame Wade’s eldest brother, that chubby man, asked. Madame Wade’s second brother also looked at her hopefully.

Madame Wade frowned as she considered the issue.

“My two brothers, please request the services of Apothecary Holmer [Huo’er’mo].” Madame Wade said calmly.

“Holmer? That old freak?” Her second brother immediately cried out in surprise.

Madame Wade said coldly, “According to Kerde’s investigations, this mysterious ‘Ley’ fellow is at least a combatant of the eighth rank, perhaps even of the ninth. I don’t have the ability to kill a combatant of the ninth rank face to face. It’s best to have Apothecary Holmer take care of this affair. After all, Apothecary Holmer has killed a combatant of the ninth rank before.”

“But Holmer…” Madame Wade’s eldest brother hesitated as well.

“Hmph. If the two of you keep on acting like this, you’ll never accomplish anything. Even if I kill Keane, if you two act like this, do you think you will be fit to be city governors?” Madame Wade snorted coldly.

“Fine, sis. We’ll go speak to Apothecary Holmer right now!” Madame Wade’s two older brothers submitted to her.

…..

‘Apothecary Holmer’ was a title which Holmer had given himself.

Others viewed Holmer as a murderer, but Holmer viewed himself as an Apothecary.

And indeed, Holmer’s abilities in preserving life were quite high. Holmer was almost three hundred years old now. For a warrior of the sixth rank to live for nearly three hundred years was nearly impossible, but Holmer had done so. What’s more, Holmer looked as though he was in quite good shape. This was because Holmer often used various bizarre concoctions, allowing his three-hundred year old body to be as strong and healthy as a young man’s.

“Huh. Madame Wade is quite generous. This business transaction…I accept, I accept.” Holmer stroked his graying beard, laughing delightedly.

In front of Holmer, Madame Wade’s two brothers were still rather nervous.

“Apothecary Homer, it would be best if you act quickly.” Madame Wade’s eldest brother urged. “Our people will deliver you to your target.”

“Haha, first give me a down payment. I’ll head out right away afterwards.” Holmer laughed loudly.

“Down payment?” The two brothers looked at each other.

In the prefectural city of Cerre, the two of them had never been treated like this before. But after learning a bit about Holmer, the two brothers didn’t dare to irritate this elderly, self-proclaimed ‘Apothecary’. Once this old man got angry, no one knew how many people might die as a result.
