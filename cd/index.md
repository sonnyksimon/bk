---
layout: blank
---

## Coiling Dragon

Translator: RWX

盘龙, aka Panlong, aka Coiling Dragon, is a webnovel by popular Chinese Xianxia (fantasy/kung fu) writer I Eat Tomatoes （我吃西红柿）. This novel has been fully translated by RWX. There are a total of 21 books spanning 800+ chapters, so sit back, buckle your seat belts, and get ready for one long ride!

Empires rise and fall on the Yulan Continent. Saints, immortal beings of unimaginable power, battle using spells and swords, leaving swathes of destruction in their wake. Magical beasts rule the mountains, where the brave – or the foolish – go to test their strength. Even the mighty can fall, feasted on by those stronger. The strong live like royalty; the weak strive to survive another day.

This is the world which Linley is born into. Raised in the small town of Wushan, Linley is a scion of the Baruch clan, the clan of the once-legendary Dragonblood Warriors. Their fame once shook the world, but the clan is now so decrepit that even the heirlooms of the clan have been sold off. Tasked with reclaiming the lost glory of his clan, Linley will go through countless trials and tribulations, making powerful friends but also deadly enemies.

Come witness a new legend in the making. The legend of Linley Baruch.

**Book 1**

[Book 1, Chapter 1 – Early Morning at a Township](/cd/b1/b1c1)

[Book 1, Chapter 2 – The Dragonblood Warrior Clan (part one)](/cd/b1/b1c2)

[Book 1, Chapter 3 – The Dragonblood Warrior Clan (part two)](/cd/b1/b1c3)

[Book 1, Chapter 4 – Growth (part 1)](/cd/b1/b1c4)

[Book 1, Chapter 5 – Growth (part 2)](/cd/b2/b1c5)

[Book 1, Chapter 6 – Coiling Dragon Ring (part 1)](/cd/b1/b1c6)

[Book 1, Chapter 7 – Coiling Dragon Ring (part 2)](/cd/b1/b1c7)

[Book 1, Chapter 8 – Magical Beast – Velocidragon!](/cd/b1/b1c8)

[Book 1, Chapter 9 – Magical Beast – Velocidragon! (part 2)](/cd/b1/b1c9)

[Book 1, Chapter 10 – Dance of the Fire Serpents (part 1)](/cd/b1/b1c10)

[Book 1, Chapter 11 – Dance of the Fire Serpents (part 2)](/cd/b1/b1c11)

[Book 1, Chapter 12 – The Will of the Mighty (part 1)](/cd/b1/b1c12)

[Book 1, Chapter 13 – The Will of the Mighty (part 2)](/cd/b1/b1c13)

[Book 1, Chapter 14 – The Battle in the Sky (part 1)](/cd/b1/b1c14)

[Book 1, Chapter 15 – The Battle in the Sky (part 2)](/cd/b1/b1c15)

[Book 1, Chapter 16 – Catastrophe (part 1)](/cd/b1/b1c16)

[Book 1, Chapter 17 – Catastrophe (part 2)](/cd/b1/b1c17)

[Book 1, Chapter 18 – The Coiling Dragon Spirit (part 1)](/cd/b1/b1c18)

[Book 1, Chapter 19 – The Coiling Dragon Spirit (part 2)](/cd/b1/b1c19)

[Book 1, Chapter 20 – Earth-Style Magic (part 1)](/cd/b1/b1c20)

[Book 1, Chapter 21 – Earth-Style Magic (part 2)](/cd/b1/b1c21)

[Book 1, Chapter 22 – Spring Ends, Autumn Comes (part 1)](/cd/b1/b1c22)

[Book 1, Chapter 23 – Spring Ends, Autumn Comes (part 2)](/cd/b1/b1c23)

**Book 2**

**Book 3**

**Book 4**

**Book 5**

**Book 6**

**Book 7**

